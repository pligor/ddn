<?php
return array(
	'1' => array(
		'id' => 1,
		'callee' => 'pvPlant/choose_categDyn',
		'data_name' => 'pvCats',
		'answered' => false,
		'action_call_id' => 1,
		'data' => serialize('data1'),
	),
	'2' => array(
		'id' => 2,
		'callee' => 'pvPlant/some_where',
		'data_name' => 'somedata',
		'answered' => false,
		'action_call_id' => 1,
		'data' => serialize('data2'),
	),
	'3' => array(
		'id' => 3,
		'callee' => 'pvPlant/some_where',
		'data_name' => 'somedata',
		'answered' => true,
		'action_call_id' => 2,
		'data' => serialize('data3'),
	),
);