<?php
class xldateTest extends CDbTestCase {
	
	public $class;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name,$data,$dataName);
		
		$this->class = substr(__CLASS__, 0, -strlen('test'));
		Yii::import('ext.PligorValidators.'.$this->class);	//IMPORT: validators ARE NOT auto-imported
	}
	
	//The array represents a mapping from fixture names that will be
	//used in the tests to model class names or fixture table names
	//(for example, from fixture name projects to model class Project)
	public $fixtures = array(
		//'prosopos' => 'Prosopo',
		//'projUsrAssign' => ':tbl_project_user_assignment',
	);
	//If you need to use a fixture for a table that is not represented
	//by an AR class, you need to prefix table name with a colon
	//(for example, :tbl_project) to differentiate it from the model class name.
	//REMEMBER: fixtures can be accessed as an object or as an array, whichever is suitable
	
	public function testValidate() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$model = new $this->class();
		$function = array($model, $func);
		
		$attribute = 'execution_date';
		$model->attributes = array($attribute);
		
		//15/11/2011
		//40862			(excel timestamp)
		//1321315200	(GMT epoch)
		
		$targetModel = new ArxikiAitisiForm();
		
		$targetModel->$attribute = 40862;
		$targetModel->clearErrors($attribute);
		call_user_func($function,$targetModel);
		$condition = $targetModel->hasErrors($attribute);	//after validation because of integer the value changes
		$this->assertFalse($condition);
		$this->assertEquals(1321315200, $targetModel->$attribute);
		
		$targetModel->$attribute = '15.11.2011';
		$targetModel->clearErrors($attribute);
		call_user_func($function,$targetModel);
		$condition = $targetModel->hasErrors($attribute);	//after validation
		$this->assertFalse($condition);
		
		$targetModel->$attribute = '15.11.011';
		$targetModel->clearErrors($attribute);
		call_user_func($function,$targetModel);
		$condition = $targetModel->hasErrors($attribute);	//after validation
		$this->assertTrue($condition);
	}
}