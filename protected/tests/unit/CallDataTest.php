<?php
class CallDataTest extends CDbTestCase {
	
	public $class;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name,$data,$dataName);
		
		$this->class = substr(__CLASS__, 0, -strlen('test'));
	}
	
	//The array represents a mapping from fixture names that will be used in the tests to model class names or fixture table names
	public $fixtures = array(
		'callDatas' => 'CallData',
		'actionCalls' => 'ActionCall',
		//'projUsrAssign' => ':tbl_project_user_assignment',
	);
	//If you need to use a fixture for a table that is not represented
	//by an AR class, you need to prefix table name with a colon
	//(for example, :tbl_project) to differentiate it from the model class name.
	//REMEMBER: fixtures can be accessed as an object or as an array, whichever is suitable

	/**
	 * NA PAEI NA GAMITHEI, TOSES WRES KAI DEN MPOREI NA KANEI TO TEST
	 * Enter description here ...
	 */
	public function testBlob() {
		//$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$model = new $this->class();
		//$function = array($model, $func);

		$ownerId = $this->actionCalls('1')->id;
		
		$attributes = array(
			'callee' => 'dummy',
			'data_name' => 'dummy',
			'answered' => false,
		);
		
		$ids = array();
		$data = array();
		
		//can store arrays
		$data[0] = array(1,4,5);
		//$this->callDatas('1')->blob = $array;
		$model->blob = $data[0];
		$model->action_call_id = $ownerId;
		$model->attributes = $attributes;
		$model->save(false);
		//$model->update(array('data'));
		
		$ids[0] = $model->id;
		
		//can store objects
		$data[1] = $this->prosopos('prosopo1');
		$model = new $this->class();
		$model->blob = $data[1];
		$model->action_call_id = $ownerId;
		$model->attributes = $attributes;
		$model->save(false);
		$ids[1] = $model->id;
		
		unset($model);
		
		for($i=0;$i<=1;$i++) {
			$model = CHtmlEx::loadModel($this->class, $ids[$i]);
			$this->assertEquals($data[$i], $model->blob);	
		}
	}
}