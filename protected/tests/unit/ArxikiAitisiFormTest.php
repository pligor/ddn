<?php
class ArxikiAitisiFormTest extends CDbTestCase {
	
	public $class;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name,$data,$dataName);
		
		$this->class = substr(__CLASS__, 0, -strlen('test'));
	}
	
	//The array represents a mapping from fixture names that will be
	//used in the tests to model class names or fixture table names
	//(for example, from fixture name projects to model class Project)
	public $fixtures = array(
		//'prosopos' => 'Prosopo',
		//'projUsrAssign' => ':tbl_project_user_assignment',
		//'authAssign' => ':AuthAssignment',
	);
	//If you need to use a fixture for a table that is not represented
	//by an AR class, you need to prefix table name with a colon
	//(for example, :tbl_project) to differentiate it from the model class name.
	//REMEMBER: fixtures can be accessed as an object or as an array, whichever is suitable
	
	public function testGetPowerRange() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$model = new $this->class();
		$function = array($model, $func);
		
		$model->pvCat = 0;
		$actual = call_user_func($function,$attribute,$params);
		$this->assertEquals(PvXt::getPowerRange(), $actual);
		
		$model->pvCat = 1;
		$actual = call_user_func($function,$attribute,$params);
		$this->assertEquals(PvMt::getPowerRange(), $actual);
		
		$model->pvCat = 2;
		$actual = call_user_func($function,$attribute,$params);
		$this->assertEquals(PvSteges::getPowerRange(), $actual);
	}
}