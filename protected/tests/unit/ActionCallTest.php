<?php
class ActionCallTest extends CDbTestCase {
	
	public $class;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name,$data,$dataName);
		
		$this->class = substr(__CLASS__, 0, -strlen('test'));
	}
	
	//The array represents a mapping from fixture names that will be
	//used in the tests to model class names or fixture table names
	//(for example, from fixture name projects to model class Project)
	public $fixtures = array(
		'actionCalls' => 'ActionCall',
		'callDatas' => 'CallData',
		//'prosopos' => 'Prosopo',
		//'projUsrAssign' => ':tbl_project_user_assignment',
	);
	//If you need to use a fixture for a table that is not represented
	//by an AR class, you need to prefix table name with a colon
	//(for example, :tbl_project) to differentiate it from the model class name.
	//REMEMBER: fixtures can be accessed as an object or as an array, whichever is suitable
	
	public function testGetAnswers() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		
		$model = $this->actionCalls('1');
		$function = array($model, $func);
		$actual = call_user_func($function);
		$expected = array('pvCats'=>'data1','somedata'=>'data2');
		$this->assertEquals($expected, $actual);
		
		$model = $this->actionCalls('2');
		$function = array($model, $func);
		$actual = call_user_func($function);
		$expected = array('somedata'=>'data3');
		$this->assertEquals($expected, $actual);
	}
	
	public function testGetAllAnswered() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		
		$model = $this->actionCalls('1');
		$function = array($model, $func);
		$condition = call_user_func($function);
		$this->assertFalse($condition);
		
		$model = $this->actionCalls('2');
		$function = array($model, $func);
		$condition = call_user_func($function);
		$this->assertTrue($condition);
	}
}