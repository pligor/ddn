<?php
class validpowerTest extends CDbTestCase {
	
	public $class;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name,$data,$dataName);
		
		$this->class = substr(__CLASS__, 0, -strlen('test'));
		Yii::import('ext.PligorValidators.'.$this->class);	//IMPORT: validators ARE NOT auto-imported
	}
	
	//The array represents a mapping from fixture names that will be
	//used in the tests to model class names or fixture table names
	//(for example, from fixture name projects to model class Project)
	public $fixtures = array(
		//'prosopos' => 'Prosopo',
		//'projUsrAssign' => ':tbl_project_user_assignment',
		//'authAssign' => ':AuthAssignment',
	);
	//If you need to use a fixture for a table that is not represented
	//by an AR class, you need to prefix table name with a colon
	//(for example, :tbl_project) to differentiate it from the model class name.
	//REMEMBER: fixtures can be accessed as an object or as an array, whichever is suitable
	
	public function testValidate() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$model = new $this->class();
		$function = array($model, $func);
		
		$attribute = 'nominal_power';
		$model->attributes = array($attribute);
		
		$targetModel = new ArxikiAitisiForm();
		
		$targetModel->pvCat = 2;
		$targetModel->$attribute = 1;
		$targetModel->clearErrors($attribute);
		call_user_func($function,$targetModel);
		$condition = $targetModel->hasErrors($attribute);
		$this->assertFalse($condition);
		
		$targetModel->pvCat = 2;
		$targetModel->$attribute = 100;
		$targetModel->clearErrors($attribute);
		call_user_func($function,$targetModel);
		$condition = $targetModel->hasErrors($attribute);
		$this->assertTrue($condition);
		
		$targetModel->pvCat = 0;
		$targetModel->$attribute = 40;
		$targetModel->clearErrors($attribute);
		call_user_func($function,$targetModel);
		$condition = $targetModel->hasErrors($attribute);
		$this->assertFalse($condition);
		
		$targetModel->pvCat = 0;
		$targetModel->$attribute = 110;
		$targetModel->clearErrors($attribute);
		call_user_func($function,$targetModel);
		$condition = $targetModel->hasErrors($attribute);
		$this->assertTrue($condition);
		
		$targetModel->pvCat = 1;
		$targetModel->$attribute = 150;
		$targetModel->clearErrors($attribute);
		call_user_func($function,$targetModel);
		$condition = $targetModel->hasErrors($attribute);
		$this->assertFalse($condition);
		
		$targetModel->pvCat = 1;
		$targetModel->$attribute = 50;
		$targetModel->clearErrors($attribute);
		call_user_func($function,$targetModel);
		$condition = $targetModel->hasErrors($attribute);
		$this->assertTrue($condition);
	}
}