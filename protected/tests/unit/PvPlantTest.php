<?php
/**
 * @author pligor
 */
class PvPlantTest extends CDbTestCase {
	public $class;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name,$data,$dataName);
		
		$this->class = substr(__CLASS__, 0, -strlen('test'));
	}
	
	//The array represents a mapping from fixture names that will be
	//used in the tests to model class names or fixture table names
	//(for example, from fixture name projects to model class Project)
	public $fixtures = array(
		//'prosopos' => 'Prosopo',
		//'users' => 'User',
		//'projUsrAssign' => ':tbl_project_user_assignment',
		//'projUserRole' => ':tbl_project_user_role',
		//'authAssign' => ':AuthAssignment',
	);
	//If you need to use a fixture for a table that is not represented
	//by an AR class, you need to prefix table name with a colon
	//(for example, :tbl_project) to differentiate it from the model class name.
	//REMEMBER: fixtures can be accessed as an object or as an array, whichever is suitable
	
	public function testGetPerioxiCols() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		$var = 'Arxiki Aitisi';
		$actual = call_user_func($function,$var);
		$this->assertEmpty($actual);
		
		$var = 'ArxikiAitisi';
		$actual = call_user_func($function,$var);
		$expected = array(
			'pvCat' => array(
				0 => 'B',
	            1 => 'C',
	            2 => 'D',
			),
		    'protocol' => array(
				0 => 'B',
				1 => 'C',
				2 => 'D',
			),
		
		);
		$this->assertEquals($expected, $actual);
	}
	
	public function testAttributeNames() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$model = new $this->class();
		$function = array($model, $func);
		
		$actual = call_user_func($function);
		
		$expected = array(
			0 => 'id',
    		1 => 'priority',
    		2 => 'island_id',
    		3 => 'prosopo_id',
    		4 => 'address_id',
		);
		
		$this->assertEquals($expected, $actual);
	}
}