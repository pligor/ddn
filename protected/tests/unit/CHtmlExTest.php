<?php
//class CHtmlExTest extends CTestCase {
class CHtmlExTest extends CDbTestCase {
	
	public $class;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name,$data,$dataName);
		
		$this->class = substr(__CLASS__, 0, -strlen('test'));
	}
	
	//The array represents a mapping from fixture names that will be
	//used in the tests to model class names or fixture table names
	//(for example, from fixture name projects to model class Project)
	public $fixtures = array(
		'prosopos' => 'Prosopo',
		//'users' => 'User',
		//'projUsrAssign' => ':tbl_project_user_assignment',
		//'projUserRole' => ':tbl_project_user_role',
		//'authAssign' => ':AuthAssignment',
	);
	//If you need to use a fixture for a table that is not represented
	//by an AR class, you need to prefix table name with a colon
	//(for example, :tbl_project) to differentiate it from the model class name.
	//REMEMBER: fixtures can be accessed as an object or as an array, whichever is suitable
	
	public function testParseGrammiDianomis() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		$invalidStr = '??????';
		
		$var = ' O-263766';
		$actual = call_user_func($function,$var);
		$this->assertEquals('P/263766', $actual);
		
		$var = ' O-126344766';
		$actual = call_user_func($function,$var);
		$this->assertEquals('P/126344766', $actual);
		
		//invalid
		$var = '-126344766';
		$actual = call_user_func($function,$var);
		$this->assertEquals($invalidStr, $actual);
	}
	
	public function testParseRaeNum() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		$invalidStr = '??????';
		
		//current pattern
		$var = '5236/2885';
		$actual = call_user_func($function,$var);
		$this->assertEquals('5236/2885', $actual);
		
		$var = '5236  /    2885';
		$actual = call_user_func($function,$var);
		$this->assertEquals('5236/2885', $actual);
		
		$var = 'jiuh5236/2885  ';
		$actual = call_user_func($function,$var);
		$this->assertEquals('5236/2885', $actual);
		
		$var = '5236noibob/ygughi2885';
		$actual = call_user_func($function,$var);
		$this->assertEquals('5236/2885', $actual);
		
		$var = ' 75437378538/ygughi3000';
		$actual = call_user_func($function,$var);
		$this->assertEquals('75437378538/3000', $actual);
		
		//old pattern
		$var = ' O-11654727247478';
		$actual = call_user_func($function,$var);
		$this->assertEquals('O-11654727247478', $actual);
		
		//invalid
		$var = '-11654727247478';
		$actual = call_user_func($function,$var);
		$this->assertEquals($invalidStr, $actual);
		
		$var = '5236noibob/ygughi285';
		$actual = call_user_func($function,$var);
		$this->assertEquals($invalidStr, $actual);
		
		$var = 'noibob/ygughi8285';
		$actual = call_user_func($function,$var);
		$this->assertEquals($invalidStr, $actual);
	}
	
	public function testParseFloat() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		$var = 1;
		$actual = call_user_func($function,$var);
		$this->assertEquals(1, $actual);
		
		$var = 1.0;
		$actual = call_user_func($function,$var);
		$this->assertEquals(1, $actual);
		
		$var = 1.1;
		$actual = call_user_func($function,$var);
		$this->assertEquals(1.1, $actual);
		
		$var = -1.1;
		$actual = call_user_func($function,$var);
		$this->assertEquals(-1.1, $actual);
		
		$var = '-361.00155';
		$actual = call_user_func($function,$var);
		$this->assertEquals(-361.00155, $actual);
		
		$var = 'sagregre-361.00155';
		$actual = call_user_func($function,$var);
		$this->assertEquals(-361.00155, $actual);
		
		$var = '361.00155ninin';
		$actual = call_user_func($function,$var);
		$this->assertEquals(361.00155, $actual);
		
		$var = '100.884.677.001,55 ';
		$actual = call_user_func($function,$var);
		$this->assertEquals(100884677001.55, $actual);
		
		$var = '100,884,677,001.55 ';
		$actual = call_user_func($function,$var);
		$this->assertEquals(100884677001.55, $actual);
		
		$var = ' ';
		$condition = call_user_func($function,$var);
		$this->assertFalse($condition);
	}
	
	public function testGetColumns() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		CHtmlEx::loadPHPExcel();
		
		$cols = $letters = call_user_func($function,'A','AAA');
		
		/*
		$expected = array(674=>'YY',675=>'YZ',676=>'ZA',677=>'ZB',678=>'ZC',679=>'ZD',680=>'ZE',681=>'ZF',682=>'ZG',
			683=>'ZH',684=>'ZI',685=>'ZJ',686=>'ZK',687=>'ZL',688=>'ZM',689=>'ZN',690=>'ZO',691=>'ZP',692=>'ZQ',
			693=>'ZR',694=>'ZS',695=>'ZT',696=>'ZU',697=>'ZV',698=>'ZW',699=>'ZX',700=>'ZY',701=>'ZZ',702=>'AAA',
		);
		*/
		
		$this->assertEquals(PHPExcel_Cell::stringFromColumnIndex(0), $cols[0]);
		$this->assertEquals(PHPExcel_Cell::stringFromColumnIndex(674), $cols[674]);
		$this->assertEquals(PHPExcel_Cell::stringFromColumnIndex(688), $cols[688]);
		$this->assertEquals(PHPExcel_Cell::stringFromColumnIndex(696), $cols[696]);
		$this->assertEquals(PHPExcel_Cell::stringFromColumnIndex(702), $cols[702]);
		
		$cols = array_flip($cols);
		//columnIndexFromString returns: Column index (base 1 !!!)
		$this->assertEquals(PHPExcel_Cell::columnIndexFromString('ZU')-1, $cols['ZU']);
		$this->assertEquals(PHPExcel_Cell::columnIndexFromString('B')-1, $cols['B']);
		$this->assertEquals(PHPExcel_Cell::columnIndexFromString('AAA')-1, $cols['AAA']);
		
		CHtmlEx::unLoadPHPExcel();
	}
	
	public function testMb_str_split() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		$str = 'γεία ee?';
		$letters = call_user_func($function,$str);
		
		$expected = array('γ','ε','ί','α',' ','e','e','?');
		
		foreach($letters as $key => $letter) {
			$this->assertEquals($expected[$key], $letter);
		}
	}
	
	public function testRemoveLastIntegers() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		$var = 'aaa 1';
		$var = call_user_func($function,$var);
		$this->assertNotContains(1, $var);
		
		$var = 'rh1';
		$var = call_user_func($function,$var);
		$this->assertContains('1', $var);
		
		$var = '1 1 1';
		$var = call_user_func($function,$var);
		$this->assertContains('1', $var);
		$this->assertEquals('1', $var);
		
		$var = 'hello 1 1 1';
		$var = call_user_func($function,$var);
		$this->assertNotContains('1', $var);
	}
	
	public function testGetInteger() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		$var = 19.255;
		$actual = call_user_func($function,$var);
		$this->assertEquals(19, $actual);
		
		$var = '19';
		$actual = call_user_func($function,$var);
		$this->assertEquals(19, $actual);
		
		$var = 'aa 19';
		$actual = call_user_func($function,$var);
		$this->assertEquals(19, $actual);
		
		$var = 'aa19';
		$actual = call_user_func($function,$var);
		$this->assertEquals(19, $actual);
		
		$var = 'aa  ';
		$actual = call_user_func($function,$var);
		$this->assertNull($actual);
		
		$var = array();
		$actual = call_user_func($function,$var);
		$this->assertNull($actual);
	}
	
	public function testGetLastInteger() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		$var = 'aa19';
		$actual = call_user_func($function,$var);
		$this->assertNull($actual);
		
		$var = 'aa 19';
		$actual = call_user_func($function,$var);
		$this->assertEquals(19, $actual);
		
		$var = '19';
		$actual = call_user_func($function,$var);
		$this->assertNull($actual);
		
		$var = "\t19";
		$actual = call_user_func($function,$var);
		$this->assertEquals(19, $actual);
	}
	
	public function testStartsWithInteger() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		$var = '1/ΔΔΝ 308/26.01.10';
		$bool = call_user_func($function,$var);
		$this->assertTrue($bool);
		
		$var = '74758';
		$bool = call_user_func($function,$var);
		$this->assertTrue($bool);
		
		$var = -277;
		$bool = call_user_func($function,$var);
		$this->assertTrue($bool);
		
		$var = '-277  ..';
		$bool = call_user_func($function,$var);
		$this->assertTrue($bool);
		
		$var = '-7.1';
		$bool = call_user_func($function,$var);
		$this->assertTrue($bool);
		
		$var = 477.22;
		$bool = call_user_func($function,$var);
		$this->assertTrue($bool);
		
		$var = 'ahhhet';
		$bool = call_user_func($function,$var);
		$this->assertFalse($bool);
		
		$var = 'g22';
		$bool = call_user_func($function,$var);
		$this->assertFalse($bool);
	}
	
	public function testIsInteger() {
		$var = '1/ΔΔΝ 308/26.01.10';
		$is = CHtmlEx::isInteger($var);
		$this->assertFalse($is);
		
		$var = 'gnian1455';
		$is = CHtmlEx::isInteger($var);
		$this->assertFalse($is);
		
		$var = ' ';
		$is = CHtmlEx::isInteger($var);
		$this->assertFalse($is);
		
		$var = 377.27;
		$is = CHtmlEx::isInteger($var);
		$this->assertFalse($is);
		
		$var = '377.27';
		$is = CHtmlEx::isInteger($var);
		$this->assertFalse($is);
		
		$var = '377,27';
		$is = CHtmlEx::isInteger($var);
		$this->assertFalse($is);
		
		$var = 377;
		$is = CHtmlEx::isInteger($var);
		$this->assertTrue($is);
		
		$var = '27';
		$is = CHtmlEx::isInteger($var);
		$this->assertTrue($is);
	}
	
	public function testIsUnsignedInteger() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		$trues = array(
			0,
			100*100,
			6367,
			'0',
		);
		
		$falses = array(
			-27,
			727.22,
			-696.100,
			' ',
			null,
			false,
			array(),
			'gaooe',
		);
		
		foreach($trues as $var) {
			$bool = call_user_func($function,$var);
			$this->assertTrue($bool);
		}
		
		foreach($falses as $var) {
			$bool = call_user_func($function,$var);
			$this->assertFalse($bool);
		}
		
	}
	
	public function testIsNotEmpty($var) {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		$spaces = " \t\n\r";
		
		$trues = array(
			"$spaces|",
			array(0=>''),
			array(0),
			0,
			'0',
		);
		
		$falses = array(
			false,
			null,
			' ',
			$spaces,
			array(),
			'',
		);
		
		foreach($trues as $var) {
			$bool = call_user_func($function,$var);
			$this->assertTrue($bool);
		}
		
		foreach($falses as $var) {
			$bool = call_user_func($function,$var);
			$this->assertFalse($bool);
		}
    }
	
	public function testIsEmpty() {
		$spaces = " \t\n\r";
				
		$var = false;
		$bool = CHtmlEx::isEmpty($var);
		$this->assertTrue($bool);
		
		$var = null;
		$bool = CHtmlEx::isEmpty($var);
		$this->assertTrue($bool);
		
		$var = ' ';
		$bool = CHtmlEx::isEmpty($var);
		$this->assertTrue($bool);
		
		$var = $spaces;
		$bool = CHtmlEx::isEmpty($var);
		$this->assertTrue($bool);
		
		$var = array();
		$bool = CHtmlEx::isEmpty($var);
		$this->assertTrue($bool);
		
		$var = '';
		$bool = CHtmlEx::isEmpty($var);
		$this->assertTrue($bool);
		
		$var = "$spaces|";
		$bool = CHtmlEx::isEmpty($var);
		$this->assertFalse($bool);
		
		$var = array(0=>'');
		$bool = CHtmlEx::isEmpty($var);
		$this->assertFalse($bool);
		
		$var = array(0);
		$bool = CHtmlEx::isEmpty($var);
		$this->assertFalse($bool);
		
		$var = 0;
		$bool = CHtmlEx::isEmpty($var);
		$this->assertFalse($bool);
		
		$var = '0';
		$bool = CHtmlEx::isEmpty($var);
		$this->assertFalse($bool);
	}
	
	public function testNoWhitespace() {
		$spaces = " \t\n\r";
		
		$str = $spaces;
		$actual = CHtmlEx::noWhitespace($str);
		$this->assertEmpty($actual);
		
		$str = "$spaces snEane \n j3333\t anw";
		$str = CHtmlEx::noWhitespace($str);
		$actual = mb_strlen($str,Yii::app()->charset);
		$this->assertEquals(14, $actual);
	}
	
	public function testNoRedundantSpaces() {
		$space = ord(' ');
		
		$str = 61;
		$str = CHtmlEx::noRedundantSpaces($str);
		$data = count_chars($str);
		$this->assertEquals(0, $data[$space]);
		
		$str = '';
		$str = CHtmlEx::noRedundantSpaces($str);
		$data = count_chars($str);
		$this->assertEquals(0, $data[$space]);
		
		$str = 'neiniege';
		$str = CHtmlEx::noRedundantSpaces($str);
		$data = count_chars($str);
		$this->assertEquals(0, $data[$space]);
		
		$str = 'snbi wtwt';
		$str = CHtmlEx::noRedundantSpaces($str);
		$data = count_chars($str);
		$this->assertEquals(1, $data[$space]);
		
		$str = 'snbi wtwt ';
		$str = CHtmlEx::noRedundantSpaces($str);
		$data = count_chars($str);
		$this->assertEquals(1, $data[$space]);
		
		$str = ' snbi wtwt';
		$str = CHtmlEx::noRedundantSpaces($str);
		$data = count_chars($str);
		$this->assertEquals(1, $data[$space]);
		
		$str = ' snbi wtwt ';
		$str = CHtmlEx::noRedundantSpaces($str);
		$data = count_chars($str);
		$this->assertEquals(1, $data[$space]);
		
		$str = "\tsnbi  \t wtwt\nagehe";
		$str = CHtmlEx::noRedundantSpaces($str);
		$data = count_chars($str);
		$this->assertEquals(2, $data[$space]);
	} 
	
	public function testNoTones() {
		$array = array(
			'ά','Ά','έ','Έ','ή','Ή','ί','Ί','ό','Ό','ύ','Ύ','ώ','Ώ',
		);
		$str = implode('glue',$array);
		$actual = CHtmlEx::noTones($str);
		while( !empty($array) ) {
			$this->assertNotContains(array_pop($array), $actual);
		}
	}
	
	public function testCsv2array() {
		$file = Yii::getPathOfAlias('application.data') .'/test.csv';
		$delimiter = ';';
		$actual = CHtmlEx::csv2array($file, $delimiter);
		
		$expected = array(
			0 => array(
				0 => 'aaw',
				1 => 'ώχ',
				2 => '500',
			),
			1 => array(
				0 => 'Άά',
				1 => 'cx',
				2 => 2,
			),
			2 => array(
				0 => 'RR',
				1 => 'nvt',
				2 => 67,
			),
		);
				
		$this->assertEquals($expected, $actual);
	}
	
	public function testMulti_array_single() {
		$array = array(
			'a' => 1,
			'params' => array(
				'c' => 'cc',
				5 => 'v',
				'params' => array(
					'd' => 25,
					'c' => 'onec',
					'params' => 66,
				),
			),
		);
		$vars = array();
		CHtmlEx::multi_array_single($vars, $array);
		/*
		$expected = array(
			'a' => 1,
			'c' => 'onec',
			5 => 'v',
			'd' => 25,
		); 
		*/
		$this->assertArrayHasKey('a', $vars);
		$this->assertArrayHasKey('c', $vars);
		$this->assertArrayHasKey(5, $vars);
		$this->assertArrayHasKey('d', $vars);
		
		$this->assertArrayNotHasKey('params', $vars);
	}
	
	public function testArray_url() {
		$var = array('a'=>1,'b'=>'c'); 
		$expected = 'a%3A2%3A%7Bs%3A1%3A%22a%22%3Bi%3A1%3Bs%3A1%3A%22b%22%3Bs%3A1%3A%22c%22%3B%7D';	//urlencode(serialize($array))
		$actual = CHtmlEx::array_url($var);
		$this->assertEquals($expected, $actual);
		
		//and also test reverse
		
		$expected = $var;
		$actual = CHtmlEx::array_url($actual,true);
		$this->assertEquals($expected, $actual);
	}
	
	public function testOut() {
		$expression = 'dummy';
		
		ob_start();
		CHtmlEx::out($expression);
		$var = ob_get_clean();
		
		$body = substr($var, 5,-6);
		
		$this->assertEquals($expression, $body);
	}
	
	public function testDump() {
		$expression = new Prosopo();
		
		ob_start();
		CHtmlEx::dump($expression);
		$var = ob_get_clean();
		
		$body = substr($var, 5,-6);
		
		ob_start();
		var_dump($expression);
		$expected = ob_get_clean();
		
		$this->assertEquals($expected, $body);
	}
	
	public function testLoad_Unload_PHPExcel() {		
		CHtmlEx::loadPHPExcel();
		
		$obj = new PHPExcel();
		$this->assertInstanceOf('PHPExcel', $obj);
		
		CHtmlEx::unLoadPHPExcel();
		
		$obj = new Prosopo();
		$this->assertInstanceOf('Prosopo', $obj);
	}
	
	public function testCommaSeparated2array() {
		$str = '';
		$var = CHtmlEx::commaSeparated2array($str);
		$this->assertTrue(is_array($var));
		
		$str = '25';
		$var = CHtmlEx::commaSeparated2array($str);
		$this->assertTrue(in_array('25', $var));
		$this->assertEquals(1, count($var));
		
		$str = '25,67';
		$var = CHtmlEx::commaSeparated2array($str);
		$this->assertTrue(in_array('25', $var));
		$this->assertEquals(2, count($var));
		
		$str = '1, 25';
		$var = CHtmlEx::commaSeparated2array($str);
		$this->assertTrue(in_array('25', $var));
		$this->assertEquals(2, count($var));
		
		$str = ' 25,';
		$var = CHtmlEx::commaSeparated2array($str);
		$this->assertTrue(in_array('25', $var));
		$this->assertEquals(1, count($var));
		
		$str = ', 25,77,29 ';
		$var = CHtmlEx::commaSeparated2array($str);
		$this->assertTrue(in_array('25', $var));
		$this->assertEquals(3, count($var));
		
		$str = '2,5';
		$var = CHtmlEx::commaSeparated2array($str);
		$this->assertFalse(in_array('25', $var));
		$this->assertEquals(2, count($var));
	}

	public function testLoadModel() {
		$model = CHtmlEx::loadModel('Prosopo', 2);
		
		//$this->assertAttributeEquals($this->prosopos('prosopo1')->eponimia, 'eponimia', $model);
		$this->assertEquals($this->prosopos('prosopo2')->id, $model->id);
		$this->assertEquals($this->prosopos('prosopo2')->eponimia, $model->eponimia);
		$this->assertEquals($this->prosopos('prosopo2')->eidos_paragogou_id, $model->eidos_paragogou_id);
		
		$this->expectedException = 'CHttpException';
		$model = CHtmlEx::loadModel('Prosopo', 0);
	}
	
	public function testConvStamp2date() {	
		$stamp=0;
		$actual = CHtmlEx::convStamp2date($stamp);
		$expected = '1/1/1970';
		$this->assertEquals($expected, $actual);
		
		$stamp=100000;
		$actual = CHtmlEx::convStamp2date($stamp);
		$expected = '2/1/1970';
		$this->assertEquals($expected, $actual);
	}
	
	public function testConvDate2stamp() {
		$del = '/';
		
		$date = '';
		$var = CHtmlEx::convDate2stamp($date);
		$this->assertNull($var);
		
		$date = ' ';
		$var = CHtmlEx::convDate2stamp($date);
		$this->assertNull($var);
		
		$date = null;
		$var = CHtmlEx::convDate2stamp($date);
		$this->assertNull($var);
		
		$day = 25;
		$month = 13;
		$year= 1950;
		$date = $day.$del.$month.$del.$year;
		unset($e);try {
			$var = CHtmlEx::convDate2stamp($date);
		}
		catch(Exception $e) {}
		$this->assertInstanceOf('Exception', $e);
		
		$day = 25;
		$month = 12;
		$year= 1950;
		$date = $day.'|'.$month.'|'.$year;
		unset($e);try {
			$var = CHtmlEx::convDate2stamp($date);
		}
		catch(Exception $e) {}
		$this->assertInstanceOf('Exception', $e);
		
		$day = 25;
		$month = 12;
		$year= 1950;
		$date = $day.$del.$month.$del.$year;
		$var = CHtmlEx::convDate2stamp($date);
		$this->assertEquals(gmmktime(0,0,0,$month,$day,$year), $var);
	}
	
	public function testCheckDateFormat() {
		$str = '25';
		$var = CHtmlEx::checkDateFormat($str);
		$this->assertFalse($var);
		
		$str = '25/222/55';
		$var = CHtmlEx::checkDateFormat($str);
		$this->assertFalse($var);
		
		$str = '25/222/5599';
		$var = CHtmlEx::checkDateFormat($str);
		$this->assertFalse($var);
		
		$str = '25/22/5599';
		$var = CHtmlEx::checkDateFormat($str);
		$this->assertTrue($var);
		
		$str = '25v22v5599';
		$var = CHtmlEx::checkDateFormat($str);
		$this->assertTrue($var);
		
		$str = '25/22|5599';
		$var = CHtmlEx::checkDateFormat($str);
		$this->assertTrue($var);
		
		$str = ' 25t22\5599 ggw';
		$var = CHtmlEx::checkDateFormat($str);
		$this->assertTrue($var);
		
		$str = 'hh 25t22\559925t22\5599 ';
		$var = CHtmlEx::checkDateFormat($str);
		$this->assertTrue($var);
	}
	
	public function testExtractDate() {
		$del = '/';
		
		$str = '25';
		$var = CHtmlEx::extractDate($str);
		$this->assertFalse($var);
		
		$str = ' 25 ';
		$var = CHtmlEx::extractDate($str);
		$this->assertFalse($var);
		
		$str = '25h';
		$var = CHtmlEx::extractDate($str);
		$this->assertFalse($var);
		
		$str = '25|';
		$var = CHtmlEx::extractDate($str);
		$this->assertFalse($var);
		
		$str = '25|36|';
		$var = CHtmlEx::extractDate($str);
		$this->assertFalse($var);
		
		$str = '25v36v9999';
		$var = CHtmlEx::extractDate($str);
		$this->assertFalse($var);
		
		$day = 25;
		$month = 36;
		$year= 9999;
		$str = "$day.$month.$year";
		$var = CHtmlEx::extractDate($str);
		$this->assertEquals($day.$del.$month.$del.$year, $var);
		
		$str = " $day.$month.$year dgg";
		$var = CHtmlEx::extractDate($str);
		$this->assertEquals($day.$del.$month.$del.$year, $var);
		
		$str = " $day.$month.$year";
		$var = CHtmlEx::extractDate($str);
		$this->assertEquals($day.$del.$month.$del.$year, $var);
		
		$str = "agg$day.$month.$year";
		$var = CHtmlEx::extractDate($str);
		$this->assertEquals($day.$del.$month.$del.$year, $var);
	}
}