<?php

/**
 * @author pligor
 */
class ActiveRecordTest extends CDbTestCase {
	public $class;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name,$data,$dataName);
		
		$this->class = substr(__CLASS__, 0, -strlen('test'));
	}
	
	//The array represents a mapping from fixture names that will be
	//used in the tests to model class names or fixture table names
	//(for example, from fixture name projects to model class Project)
	public $fixtures = array(
		'prosopos' => 'Prosopo',
		//'users' => 'User',
		//'projUsrAssign' => ':tbl_project_user_assignment',
		//'projUserRole' => ':tbl_project_user_role',
		//'authAssign' => ':AuthAssignment',
	);
	//If you need to use a fixture for a table that is not represented
	//by an AR class, you need to prefix table name with a colon
	//(for example, :tbl_project) to differentiate it from the model class name.
	//REMEMBER: fixtures can be accessed as an object or as an array, whichever is suitable
	
	public function testResetAutoIncrement() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		//$this->prosopos('prosopo1')->tableName();
		
		$model = new Prosopo();
		$model->eponimia = 'test';
		$model->eidos_paragogou_id = $this->prosopos('prosopo1')->eidos_paragogou_id;
		$model->save(false);
		
		//get last id
		$lastProsopo = end($this->prosopos);
		$nextId = $lastProsopo['id']+1;
		
		//print "prosopo id:". $model->id;
		$this->assertEquals($nextId, $model->id);
		
		$model->deleteAll();
		
		$tableName = ActiveRecord::getTableName($model);
		call_user_func($function,$tableName);
		
		$model = new Prosopo();
		$model->eponimia = 'test';
		$model->eidos_paragogou_id = $this->prosopos('prosopo1')->eidos_paragogou_id;
		$model->save(false);
		
		$this->assertEquals(1, $model->id);
		
		$this->expectedException = 'Exception';
		call_user_func($function,'lathos table name');
	}
	
	public function testGetTableName() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);
		
		$model = $this->prosopos('prosopo1');
		$expected = Yii::app()->db->tablePrefix . trim($model->tableName(), '{}');
		$actual = call_user_func($function,$model);
		$this->assertEquals($expected, $actual);
	}
}