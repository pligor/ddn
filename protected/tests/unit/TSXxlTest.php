<?php

/**
 * @author pligor
 */
class TSXxlTest extends CDbTestCase {
	public $class;
	protected $files = array(
		'correct' => 'tsx_correct.xls',
	);
	protected $models = array();
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name,$data,$dataName);
		
		$this->class = substr(__CLASS__, 0, -strlen('test'));
		
		foreach($this->files as $key => $file) {
			$path = Yii::getPathOfAlias('application.tests.files') .'/'. $file;
			$model = new $this->class($path);
			$model->loadExcel();
			$this->models[$key] = & $model;
		}
	}
	
	//The array represents a mapping from fixture names that will be
	//used in the tests to model class names or fixture table names
	//(for example, from fixture name projects to model class Project)
	public $fixtures = array(
		//'prosopos' => 'Prosopo',
		//'users' => 'User',
		//'projUsrAssign' => ':tbl_project_user_assignment',
		//'projUserRole' => ':tbl_project_user_role',
		//'authAssign' => ':AuthAssignment',
	);
	//If you need to use a fixture for a table that is not represented
	//by an AR class, you need to prefix table name with a colon
	//(for example, :tbl_project) to differentiate it from the model class name.
	//REMEMBER: fixtures can be accessed as an object or as an array, whichever is suitable
	
	/*
	public function testForAllSheets() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		
		$model = new $this->class($path);
		$function = array($model, $func);
		
		$parameters = array('test'=>'exception');
		$reportPerioxis = $model->forAllRows($sheet, 'testMethod',$parameters);
		$this->assertNotEmpty($reportPerioxis);
	}
	*/
	
	public function testForAllRows() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$model = $this->models['correct'];
		$function = array($model, $func);
		
		$return = CHtmlEx::loadPHPExcel();
		$sheet = new PHPExcel();	//false sheet
		CHtmlEx::unLoadPHPExcel();
		
		$condition = $model->forAllRows($sheet);// call_user_func($function,$sheet);
		$this->assertFalse($condition);
		
		$model->loadExcel();
		
		$perioxi_dei_name = 'ΔΥΤΙΚΕΣ ΚΥΚΛΑΔΕΣ';
		$sheet = $model->phpExcel->getSheetByName($perioxi_dei_name);
		$attributes = array(
			'perioxi_dei_name' => $perioxi_dei_name,
		);
		$sheet->perioxi_dei_id = PerioxiDei::model()->findByAttributes($attributes);
		
		$parameters = array('test'=>'dummy');
		$reportPerioxis = $model->forAllRows($sheet, 'testMethod',$parameters);
		$this->assertEmpty($reportPerioxis);
		
		$parameters = array('test'=>'exception');
		$reportPerioxis = $model->forAllRows($sheet, 'testMethod',$parameters);
		$this->assertNotEmpty($reportPerioxis);

		$dytkyklades_rows = 26;
		$this->assertEquals($dytkyklades_rows, count($reportPerioxis));
	}
	
	public function testVerifySheetnames() {
		$path = Yii::getPathOfAlias('application.tests.files') .'/tsx_wrong_sheetnames.xls';
		$this->expectedException = 'Exception';
		$model = new TSXxl($path);
	}
}
