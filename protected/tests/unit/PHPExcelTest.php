<?php
class PHPExcelTest extends CDbTestCase {
	/*
	public $class;
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name,$data,$dataName);
		
		$this->class = substr(__CLASS__, 0, -strlen('test'));
		Yii::import('ext.PligorValidators.'.$this->class);	//IMPORT: validators ARE NOT auto-imported
	}
	//*/
	
	//The array represents a mapping from fixture names that will be
	//used in the tests to model class names or fixture table names
	//(for example, from fixture name projects to model class Project)
	public $fixtures = array(
		//'prosopos' => 'Prosopo',
		//'projUsrAssign' => ':tbl_project_user_assignment',
	);
	//If you need to use a fixture for a table that is not represented
	//by an AR class, you need to prefix table name with a colon
	//(for example, :tbl_project) to differentiate it from the model class name.
	//REMEMBER: fixtures can be accessed as an object or as an array, whichever is suitable
	
	public function testValidate() {
		
		$colorCode = 'FFAABB00';
		
		CHtmlEx::loadPHPExcel();
		$color = new PHPExcel_Style_Color($colorCode);
		CHtmlEx::unLoadPHPExcel();
		
		$actual = $color->getRGB();
		$expected = substr($colorCode, 2);
		$this->assertEquals($expected, $actual);
	}
}