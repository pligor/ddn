<?php

Yii::import('application.widgets.Excel.ExcelSheetView');
/**
 * @author pligor
 */
class ExcelSheetViewTest extends CDbTestCase {
	public $class;
	
	public $rawData;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name,$data,$dataName);
		
		$this->class = substr(__CLASS__, 0, -strlen('test'));
		
		$this->rawData = array(
			array(
				'idCol' => 1,
				'columnName' => 'Johnny',
				'columnSurname' => 'Z',
			),
			array(
				'idCol' => 2,
				'columnName' => 'Adonis',
				'columnSurname' => 'Travamiavolta',
			),
		);
	}
	
	//The array represents a mapping from fixture names that will be
	//used in the tests to model class names or fixture table names
	//(for example, from fixture name projects to model class Project)
	public $fixtures = array(
		'prosopos' => 'Prosopo',
		//'users' => 'User',
		//'projUsrAssign' => ':tbl_project_user_assignment',
		//'projUserRole' => ':tbl_project_user_role',
		//'authAssign' => ':AuthAssignment',
	);
	//If you need to use a fixture for a table that is not represented
	//by an AR class, you need to prefix table name with a colon
	//(for example, :tbl_project) to differentiate it from the model class name.
	//REMEMBER: fixtures can be accessed as an object or as an array, whichever is suitable
	
	///*
	public function testRenderItems() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$model = new $this->class();
		
		$model->showTableOnEmpty = false;
		
		$emptyRawData = array();
		$arrayDataProvider = new CArrayDataProvider($emptyRawData, array(
		    'id' => 'dummyId',
		    'keyField' => 'idCol',
		));
		$model->dataProvider = $arrayDataProvider;
		
		ob_start();
		call_user_func(array($model, $func));
		$string = ob_get_clean();
		$this->assertStringStartsWith('<span class="empty">', $string);
		
		///////////////////////////////////////////////////////////////////
		
		$arrayDataProvider = new CArrayDataProvider($this->rawData, array(
		    'id' => 'dummyId',
		    'keyField' => 'idCol',
		));
		$model->dataProvider = $arrayDataProvider;
		
		$model->init();
		
		ob_start();
		call_user_func(array($model, $func));
		$string = ob_get_clean();
		//$this->assertStringStartsWith('<span class="empty">', $string);
		print $string;
	}
	//*/
	
	/*
	public function testInit() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		
		///////////////////////////////////////////////////////////////////
		$model = new $this->class();
		
		$class = 'Prosopo';
		$activeDataProvider = new CActiveDataProvider($class);
		$model->dataProvider = $activeDataProvider;
		call_user_func(array($model, $func));
		
		$actual = count($model->columns);
		$this->assertEquals(3, $actual);
		
		foreach($model->columns as $column) {
			$this->assertTrue($column instanceof ExcelColumn);
		}
		
		//////////////////////////////////////////////////////////////////////
		$model = new $this->class();
		$arrayDataProvider = new CArrayDataProvider($this->rawData, array(
		    'id' => 'dummyId',
		    'keyField' => 'idCol',
		));
		$model->dataProvider = $arrayDataProvider;
		$model->columns = array(
			array(
				'name' => 'columnName',
				'header' => 'Column Name',
			),
			array(
				'name' => 'columnSurname',
				'header' => 'Column Sur Name',
				//'value' => 'number_format($data["power"],2)',
			),
		);
		call_user_func(array($model, $func));
		
		$actual = $model->columns[0]->name;
		$this->assertEquals('columnName', $actual);
		
		$actual = $model->columns[1]->header;
		$this->assertEquals('Column Sur Name', $actual);
		
		////////////////////////////////////////////////////////////////////////////////
		$model = new $this->class();
		$model->dataProvider = $arrayDataProvider;
		$model->columns = array(
			array(
				'name' => 'columnName',
				'header' => 'Column Name',
			),
			array(
				'name' => 'columnSurname',
				'header' => 'Column Sur Name',
				'visible' => false,
				//'value' => 'number_format($data["power"],2)',
			),
		);
		call_user_func(array($model, $func));
		
		$actual = count($model->columns);
		$this->assertEquals(1, $actual);
		
		////////////////////////////////////////////////////////////////////////////////
		$model = new $this->class();
		$model->dataProvider = $arrayDataProvider;
		$model->columns = array(
			array(
				'class' => 'by defining this should throw exception',
				'name' => 'columnName',
				'header' => 'Column Name',
			),
		);
		$this->expectedException = 'Exception';
		//call_user_func(array($model, $func));
	}
	//*/
	
	
}