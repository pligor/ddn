<?php
class ArrayValidatorTest extends CDbTestCase {
	
	public $class;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name,$data,$dataName);
		
		$this->class = substr(__CLASS__, 0, -strlen('test'));
		Yii::import('ext.PligorValidators.'.$this->class);	//IMPORT: validators ARE NOT auto-imported
	}
	
	//The array represents a mapping from fixture names that will be
	//used in the tests to model class names or fixture table names
	//(for example, from fixture name projects to model class Project)
	public $fixtures = array(
		//'prosopos' => 'Prosopo',
		//'projUsrAssign' => ':tbl_project_user_assignment',
		//'authAssign' => ':AuthAssignment',
	);
	//If you need to use a fixture for a table that is not represented
	//by an AR class, you need to prefix table name with a colon
	//(for example, :tbl_project) to differentiate it from the model class name.
	//REMEMBER: fixtures can be accessed as an object or as an array, whichever is suitable
	
	public function testValidate() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );

		//print "ftasame pote edw? {$this->class} \n";
		//$model = new ArrayValidator();
		//print "ftasame pote edw? $string \n";
		
		$model = new $this->class();
		$function = array($model,$func);
		
		
		
		//define validator values
		$model->validatorClass = 'CExistValidator';
		$model->params = array(
			'dimos' => array(
				'allowEmpty' => false,
				'attributeName' => 'dimos_name',
				'className' => 'Dimos',
				'message' => Yii::t('', 'The Municipality is typed incorrectly'),
			),
			'nomos' => array(
				'allowEmpty' => false,
				'attributeName' => 'nomos_name',
				'className' => 'Nomos',
				'message' => Yii::t('', 'The County is typed incorrectly'),
			),
		);
		$model->separateParams = true;
		
		//define attribute
		$attribute = 'dimos_id';
		$model->attributes = array($attribute);
		
		$targetModel = new ArxikiAitisiForm();
		
		$targetModel->$attribute = array('dimos'=>'ΠΑΠΑΡΙΩΝ','nomos'=>'ΑΝΤΕΓΕΙΑ');
		$targetModel->clearErrors($attribute);
		call_user_func($function,$targetModel);
		$condition = $targetModel->hasErrors($attribute);	//after validation because of integer the value changes
		$this->assertTrue($condition);
		
		$targetModel->$attribute = array('dimos'=>'ΑΜΑΡΟΥΣΙΟΥ','nomos'=>'ΑΤΤΙΚΗΣ');
		$targetModel->clearErrors($attribute);
		call_user_func($function,$targetModel);
		$condition = $targetModel->hasErrors($attribute);	//after validation because of integer the value changes
		$this->assertFalse($condition);
	}
}