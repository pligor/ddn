<?php
//SOLUTION to autoloading problem: http://www.mrsoundless.com/post/2011/04/23/PHPExcel-in-Yii.aspx

$path = dirname(__FILE__).'/../../../../../php/downloaded/phpexcel';
$path = realpath($path);
//require 'PHPExcel.php';
require $path .'/PHPExcel.php';

class ExcelReadFilter implements PHPExcel_Reader_IReadFilter {
    private $_startRow = 0;
    private $_endRow   = 0; 
    private $_columns  = array(); 

    /**  Get the list of rows and columns to read  */ 
    public function __construct($startRow, $endRow, $columns) {
        $this->_startRow = $startRow;
        $this->_endRow   = $endRow;
        $this->_columns  = $columns;
    }

    public function readCell($column, $row, $worksheetName = '') { 
        //  Only read the rows and columns that were configured
    	if($this->_endRow === null) {
    		$rowCond = ($row >= $this->_startRow);
    	}
    	else {
    		$rowCond = ($row >= $this->_startRow) && ($row <= $this->_endRow);	
    	}
    	
        if ($rowCond) {
            if ( in_array($column,$this->_columns) ) {
                return true; 
            }
        }
        return false;
    }
}