<?php
/**
 * Public attributes are the parameters, automagically initialized from rules
 * @author pligor
 */
class raenum extends CValidator {
	public $allowEmpty;
	public $emptyMessage;

	/**
	 * Validates the attribute of the object.
	 * If there is any error, the error message is added to the object.
	 * @param CModel $object the object being validated
	 * @param string $attribute the attribute being validated
	 */
	protected function validateAttribute($object, $attribute) {
		
		if( CHtmlEx::isEmpty($object->$attribute) ) {
			if( !$this->allowEmpty ) {
				$this->addError($object,$attribute,$this->emptyMessage);	
			}
			else {
				$object->$attribute = null;
			}
			return;
		}
		
		$patterns = PvXt::getRaePatterns();
		
		foreach($patterns as $pattern) {
			$matches = array();
			$noMatches = preg_match($pattern, $object->$attribute, $matches);
			if($noMatches==1) {
				return;
			}		
		}
		
		$this->addError($object, $attribute, Yii::t('', 'Εξαίρεση ΡΑΕ does not have a valid format'));
	}
}