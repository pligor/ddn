<?php

/**
 * MUST FIX THIS LATER: ALSO ACTS AS A FILTER CHANGING ATTRIBUTE VALUE (NOT ALWAYS, ON CONDITION)
 * Attributes are the parameters, automagically initialized from rules
 * @author pligor
 */
class xldate extends CValidator {
	public $allowEmpty;
	public $emptyMessage;
	public $wrongDate;
	
	/**
	 * Validates the attribute of the object.
	 * If there is any error, the error message is added to the object.
	 * @param CModel $object the object being validated
	 * @param string $attribute the attribute being validated
	 */
	protected function validateAttribute($object, $attribute) {
		
		if( CHtmlEx::isEmpty($object->$attribute) ) {
			if( !$this->allowEmpty ) {
				$this->addError($object,$attribute,$this->emptyMessage);	
			}
			else {
				$object->$attribute = null;
			}
			return;
		}
		
		if(CHtmlEx::isUnsignedInteger($object->$attribute)) { //we got an excel timestamp
			CHtmlEx::loadPHPExcel();
			$object->$attribute = PHPExcel_Shared_Date::ExcelToPHP($object->$attribute);
			CHtmlEx::unLoadPHPExcel();
			return;
		}
		
		if( !CHtmlEx::checkDateFormat($object->$attribute) ) {
			$this->addError($object,$attribute,$this->wrongDate);
		}
	}
}