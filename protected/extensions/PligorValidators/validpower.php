<?php
/**
 * Public attributes are the parameters, automagically initialized from rules
 * @author pligor
 */
class validpower extends CValidator {
	public $allowEmpty;
	public $emptyMessage;

	/**
	 * Validates the attribute of the object.
	 * If there is any error, the error message is added to the object.
	 * @param CModel $object the object being validated
	 * @param string $attribute the attribute being validated
	 */
	protected function validateAttribute($object, $attribute) {
		if( CHtmlEx::isEmpty($object->$attribute) ) {
			if( !$this->allowEmpty ) {
				$this->addError($object,$attribute,$this->emptyMessage);	
			}
			else {
				$object->$attribute = null;
			}
			return;
		}
		
		$powerRange = $object->getPowerRange();
		
		if( ($object->$attribute < $powerRange['min'])
		|| ($object->$attribute > $powerRange['max']) ) {
			$error = Yii::t('', 'Real Power is out of range');
			$this->addError($object,$attribute,$error);
		}
	}
}