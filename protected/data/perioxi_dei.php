<?php
return array(
	'1' => array(
		'id' => 1,
		'perioxi_dei_name' => 'ΑΓΙΟΥ ΝΙΚΟΛΑΟΥ',
	),
	'2' => array(
		'id' => 2,
		'perioxi_dei_name' => 'ΗΡΑΚΛΕΙΟΥ',
	),
	'3' => array(
		'id' => 3,
		'perioxi_dei_name' => 'ΡΕΘΥΜΝΟΥ',
	),
	'4' => array(
		'id' => 4,
		'perioxi_dei_name' => 'ΧΑΝΙΩΝ',
	),
	'5' => array(
		'id' => 5,
		'perioxi_dei_name' => 'ΑΛΙΒΕΡΙΟΥ',
	),
	'6' => array(
		'id' => 6,
		'perioxi_dei_name' => 'ΔΥΤΙΚΩΝ ΚΥΚΛΑΔΩΝ',
	),
	'7' => array(
		'id' => 7,
		'perioxi_dei_name' => 'ΚΩ',
	),
	'8' => array(
		'id' => 8,
		'perioxi_dei_name' => 'ΛΕΣΒΟΥ',
	),
	'9' => array(
		'id' => 9,
		'perioxi_dei_name' => 'ΡΟΔΟΥ',
	),
	'10' => array(
		'id' => 10,
		'perioxi_dei_name' => 'ΣΑΜΟΥ',
	),
	'11' => array(
		'id' => 11,
		'perioxi_dei_name' => 'ΣΥΡΟΥ',
	),
	'12' => array(
		'id' => 12,
		'perioxi_dei_name' => 'ΧΙΟΥ',
	),
	'13' => array(
		'id' => 13,
		'perioxi_dei_name' => 'ΚΕΡΚΥΡΑΣ',
	),
	'14' => array(
		'id' => 14,
		'perioxi_dei_name' => 'ΣΠΑΡΤΗΣ',
	),
);