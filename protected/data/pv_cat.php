<?php
return array(
	'0' => array(
		'id' => 0,
		'table_name' => 'pv_xt',
		'min_power' => 11,
		'max_power' => 100,
		'end_terms_days' => 183,
	),
	'1' => array(
		'id' => 1,
		'table_name' => 'pv_mt',
		'min_power' => 101,
		'max_power' => 500,
		'end_terms_days' => 183,
	),
	'2' => array(
		'id' => 2,
		'table_name' => 'pv_steges',
		'min_power' => 0.1,
		'max_power' => 10,
		'end_terms_days' => 91,
	),
);