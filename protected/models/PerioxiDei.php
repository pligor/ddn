<?php

/**
 * This is the model class for table "perioxi_dei".
 *
 * The followings are the available columns in table 'perioxi_dei':
 * @property integer $id
 * @property string $perioxi_dei_name
 *
 * The followings are the available model relations:
 * @property Island[] $islands
 */
class PerioxiDei extends CActiveRecord {
	/**
	 * @see CModel::behaviors()
	 */
	public function behaviors() {
		return array(
			'fixture-behavior' => array(
				'class' => 'FixtureBehavior',
			),
		);
	}
	
	public static function convLish2greek($name) {		
		return Yii::t('', $name,array(),null,'el');
	}
	
	/*
	public static function convLish2greek($name) {
		$tranArray = array(
			'CHANIA' => 'ΧΑΝΙΑ',
			'SAMOS' => 'ΣΑΜΟΣ',
			'AG.NIKOLAOS' => 'ΑΓ.ΝΙΚΟΛΑΟΣ',
			'CHIOS' => 'ΧΙΟΣ',
			'DYTIKES KYKLADES' => 'ΔΥΤΙΚΕΣ ΚΥΚΛΑΔΕΣ',
			'IRAKLEIO' => 'ΗΡΑΚΛΕΙΟ',
			'KERKYRA' => 'ΚΕΡΚΥΡΑ',
			'KWS' => 'ΚΩΣ',
			'LESBOS' => 'ΛΕΣΒΟΣ',
			'RE8YMNO' => 'ΡΕΘΥΜΝΟ',
			'RODOS' => 'ΡΟΔΟΣ',
			'SKYROS' => 'ΣΚΥΡΟΣ',
			'SPARTI' => 'ΣΠΑΡΤΗ',
			'SYROS' => 'ΣΥΡΟΣ',
		);
		
		return isset($tranArray[$name]) ? $tranArray[$name] : $name;
	}
	//*/
	
	/**
	 * @param string $name
	 * @return PerioxiDei or null if name is not found
	 */
	public static function getModelByName($name) {
		$name = self::convLish2greek($name);
		$attributes = array(
			'perioxi_dei_name' => $name,
		);
		return self::model()->findByAttributes($attributes);
	}
	
	public function getListDataIslands() {
		$valueField = 'id';
		$textField = 'island_name';
		return CHtml::listData($this->islands, $valueField, $textField);
	}
	
	public static function getFormConfig($all=false) {
		$items = array();
		if($all) {
			$items[0] = Yii::t('', 'ALL');
		}
		$items += self::getPerioxes();
		
		return array(
			'type' => 'form',
		    //'title' => '............',
		    //'showErrorSummary' => true,
		    'elements' => array(
				 'id' => array(
		        	'type' => 'dropdownlist',
		        	'items' => $items,
		        ),
			),
		);
	}
	
	/**
	 * Get all islands in format array(island_id => island_name) ordered by island name 
	 * @return Array the static model class
	 */
	public static function getPerioxes()
	{
		$valueField = 'id';
		$textField = 'perioxi_dei_name';
		
		$criteria=new CDbCriteria;
		$criteria->select = array($valueField, $textField);
		$criteria->order = $textField;
		$allRows = self::model()->findAll($criteria);

		return CHtml::listData($allRows, $valueField, $textField);
	}
	
	public static function loadDefaultData() {
		$class = get_class();
		$model = new $class();
		$model->deleteAll();	//delete ALL rows
		
		$csvPath = Yii::getPathOfAlias('application.data') .'/'. ActiveRecord::getTableName($model) .'.csv';
		
		$twoDarray = CHtmlEx::csv2array($csvPath, ';');
		
		$allInserted = true;
		foreach($twoDarray as $rowArray) {
			
			$model = new $class();
			$model->id = $rowArray[0];
			$model->perioxi_dei_name = $rowArray[1];
			$insertion = $model->insert();
			
			$allInserted = $allInserted && $insertion;
		}
		
		return $allInserted;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return PerioxiDei the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{perioxi_dei}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('perioxi_dei_name', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, perioxi_dei_name', 'safe', 'on'=>'search'),
			
			array('id','safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'islands' => array(self::HAS_MANY, 'Island', 'perioxi_dei_id'),
			//'pvMts' => array(self::HAS_MANY, 'PvMt', 'perioxi_dei_id'),
			//'pvSteges' => array(self::HAS_MANY, 'PvSteges', 'perioxi_dei_id'),
			//'pvXts' => array(self::HAS_MANY, 'PvXt', 'perioxi_dei_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('', 'Area Name'),
			'perioxi_dei_name' => Yii::t('', 'Area Name'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('perioxi_dei_name',$this->perioxi_dei_name,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}