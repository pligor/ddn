<?php

Yii::import('system.test.CDbFixtureManager');

/**
 * This is the model class for table "eidos_paragogou".
 *
 * The followings are the available columns in table 'eidos_paragogou':
 * @property integer $id
 * @property string $eidos
 *
 * The followings are the available model relations:
 * @property Prosopo[] $prosopos
 */
class EidosParagogou extends CActiveRecord {
	
	/**
	 * @see CModel::behaviors()
	 */
	public function behaviors() {
		return array(
			'fixture-behavior' => array(
				'class' => 'FixtureBehavior',
			),
		);
	}
	
	/**
	 * Get all in format array(id => name) ordered by name 
	 * @return Array the static model class
	 */
	public static function getEidi()
	{
		$valueField = 'id';
		$textField = 'eidos';
		
		$criteria=new CDbCriteria;
		$criteria->select = array("id", $textField);
		$criteria->order = $textField;
		
		$class = get_class();
		$allRows = $class::model()->findAll($criteria);

		return CHtml::listData($allRows, $valueField, $textField);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return EidosParagogou the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{eidos_paragogou}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('eidos', 'required'),
			array('eidos', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, eidos', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'prosopos' => array(self::HAS_MANY, 'Prosopo', 'eidos_paragogou_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'eidos' => Yii::t('', 'Producer Kind'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('eidos',$this->eidos,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}