<?php

/**
 * This is the model class for table "pv_steges".
 *
 * The followings are the available columns in table 'pv_steges':
 * @property integer $pv_plant_id
 * @property integer $protocol
 * @property integer $perioxi_dei_id
 * @property integer $island_id
 *
 * The followings are the available model relations:
 * @property PvPlant $pvPlant
 */
class PvSteges extends CActiveRecord
{
	const col = 'D';
	
	public static function getPowerRange() {
		return array(
			'min' => 0.1,	//kW
			'max' => 10,	//kW
		);
	}
	
	/**
	 * @return string the description of this class
	 */
	public static function getDescr() {
		return "Φ/Β σε Στέγες";
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return PvSteges the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pv_steges}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pv_plant_id, protocol, perioxi_dei_id', 'required'),
			array('pv_plant_id, protocol, perioxi_dei_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pv_plant_id, protocol, perioxi_dei_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pvPlant' => array(self::BELONGS_TO, 'PvPlant', 'pv_plant_id'),
			//'perioxiDei' => array(self::BELONGS_TO, 'PerioxiDei', 'perioxi_dei_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'protocol' => 'Αριθμός Πρωτοκόλλου Στέγες',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pv_plant_id',$this->pv_plant_id);
		$criteria->compare('protocol',$this->protocol);
		$criteria->compare('perioxi_dei_id',$this->perioxi_dei_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}