<?php

/**
 * This is the model class for table "address".
 *
 * The followings are the available columns in table 'address':
 * @property integer $id
 * @property integer $dimos_id
 * @property string $thesi
 * @property string $noumero
 * @property double $latitude
 * @property double $longitude
 * 
 * @property string fullAddress
 *
 * The followings are the available model relations:
 * @property Dimos $dimos
 * @property PvPlant[] $pvPlants
 */
class Address extends CActiveRecord
{
	public static function getPerioxiCols($step) {
		if($step != 'ArxikiAitisi') return array();
		
		return array(
			'dimos_id' => array(
				'dimos' => 'J',
				'nomos' => 'K',
			),
			'thesi' => 'I',
			'noumero' => 'I',
		);
	}
	
	public function getFullAddress() {
		$dimos_name = '<'. Yii::t('', 'not found') .'>';
		$model = CHtmlEx::loadModel('Dimos', $this->dimos_id);
		if( is_object($model) ) {
			$dimos_name = $model->dimos_name;
		}
		
		return $this->thesi .' '. $this->noumero .' '. Yii::t('', 'in Municipality') .' '. $dimos_name;
	}
	
	/**
	 * Fix thesi to only contain valid capital words
	 * Also trimmed from pre and post whitespace
	 * @param bool $return
	 */
	public function normalizeThesi($return=false) {
		$thesi = $this->thesi;
		
		$thesi = CHtmlEx::noRedundantSpaces($thesi);
		$thesi = CHtmlEx::noTones($thesi);
		$thesi = mb_strtoupper($thesi, Yii::app()->charset);
		
		$thesi = CHtmlEx::removeLastIntegers($thesi);
		
		if($return) return $thesi;
		else $this->thesi = $thesi;
	}
	
	/**
	 * Fix noumero to get the last integer value from a string unless is numeric
	 * @param bool $return
	 */
	public function normalizeNoumero($return=false) {
		if( CHtmlEx::isUnsignedInteger($this->noumero) ) {
			if($return)
				return $this->noumero;
			else
				return;
		}
		
		$noumero = $this->noumero;
		$noumero = CHtmlEx::getLastInteger($noumero);
		
		if($noumero===null) {
			$noumero = 0;	
		}
		
		if($return) return $noumero;
		else $this->noumero = $noumero;
	}
	
	private function searchDimous($dimos, $dimosesListData=null) {
		if($dimosesListData===null) {
			$vector = Dimos::getDimous();	
		}
		else {
			$vector = $dimosesListData;
		}
		$word = $dimos;
		
		$breathModel = new Breathalyzer();
		$breathModel->breathem($vector, $word);
		return $breathModel->finalword;
	}
	
	private function searchDimousByNomo($dimos,$nomos) {
		$vector = Nomos::getNomous();
		$word = $nomos;
		
		$breathModel = new Breathalyzer();
		$breathModel->breathem($vector, $word);
		$nomos_name = $breathModel->finalword;
		
		$attributes = array(
			'nomos_name' => $nomos_name,
		);
		$model = Nomos::model()->findByAttributes($attributes);
		
		$valueField = 'id';
		$textField = 'dimos_name';
		$dimosesListData = CHtml::listData($model->dimoses, $valueField, $textField);
		
		return $this->searchDimous($dimos, $dimosesListData);
	}
	
	public function normalizeDimos_id($return=false) {
		if( CHtmlEx::isUnsignedInteger($this->dimos_id) ) {	//do nothing special
			if(!$return) return;
			return $this->dimos_id;
		}
		
		//supposing dimos_id is an array
		$dimos = $this->dimos_id['dimos'];
		$nomos = $this->dimos_id['nomos'];
		
		$dimos = CHtmlEx::noRedundantSpaces($dimos);
		$dimos = CHtmlEx::noTones($dimos);
		$dimos = mb_strtoupper($dimos, Yii::app()->charset);
		
		$nomos = CHtmlEx::noRedundantSpaces($nomos);
		$nomos = CHtmlEx::noTones($nomos);
		$nomos = mb_strtoupper($nomos, Yii::app()->charset);
		
		$dimos1 = $this->searchDimous($dimos);	//prwta apo ton xyma dromo, ola oi dimoi paizoun		
		$dimos2 = $this->searchDimousByNomo($dimos, $nomos);
		
		if( levenshtein($dimos1,$dimos) <= levenshtein($dimos2,$dimos) ) {
			$dimos_name = $dimos1;
		}
		else {
			$dimos_name = $dimos2;
		}
		
		$attributes = array(
			'dimos_name' => $dimos_name,
		);
		$model = Dimos::model()->findByAttributes($attributes);
		if($model===null) {
			$message = Yii::t('', 'serious error inside {method} method', array(
				'{method}' => __METHOD__,
			));
			throw new Exception($message);
		}
		$dimos_id = $model->id;
		
		if($return)
			return $dimos_id;
		else
			$this->dimos_id = $dimos_id;
	}
	
	public function normalizeAttributes() {	//BE AWARE: you set the correct scenario before using this method
		$attrNames = $this->safeAttributeNames;
		foreach($attrNames as $attrName) {
			$method = 'normalize'. ucfirst($attrName);
			if( method_exists($this, $method) ) {
				$this->$method();
			}
		}
	}
	
	/**
	 * overriding save function
	 */
	public function save($validation=true,$attributes=null) {
		$this->normalizeAttributes();
		return parent::save($validation);
	}
	
	/**
	 * Get relation but checks if the specified relation is exists
	 * @return string the relation OR false otherwise
	 */
	protected function getRelation($class)	//this should be an UPPER function in order to DRY (a class which extends CActiveRecord)
	{
		foreach($this->relations() as $relation)
		{
			if($relation[1]==$class)	return $class;
		}
		return false;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Address the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{address}}';
	}
	
	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to define a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				get_class() => $this->getFormConfig(),
			),
			'buttons'=>array(
		        'submit' => array(
		            'type'=>'submit',
		            'label' => Yii::t('', 'Change'),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		$elements = array(
	        'dimos_id' => array(
				'type' => 'dropdownlist',
				'items' => Dimos::model()->getDimous(),
			),
			'thesi' => array(
				'type' => 'text',
				'attributes' => array(
					'size'=>60,
					'maxlength'=>240,
				),
			),
			'noumero' => array(
				'type' => 'text',
				'attributes' => array(
					'size'=>10,
					'maxlength'=>10,
				),
			),
			'latitude' => array(
				'type' => 'text',
			),
			'longitude' => array(
				'type' => 'text',
			),
	    );
		
		return array(
			'type' => 'form',
		    //'title' => '......',
		    //'showErrorSummary' => true,
		    'elements' => $elements,
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dimos_id', 'required'),
			array('dimos_id', 'numerical', 'integerOnly'=>true),
			array('latitude, longitude', 'numerical'),
			
			array('thesi', 'required'),
			
			array('thesi', 'length', 'max'=>240),
			array('noumero', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, dimos_id, thesi, noumero, latitude, longitude', 'safe', 'on'=>'search'),
			
			array('dimos_id, thesi, noumero', 'safe', 'on'=>'excel'),
			array('id, latitude, longitude', 'unsafe', 'on'=>'excel'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
	//NOTE:you may need to adjust the relation name and the related class name for the relations automatically generated below
		return array(
			'dimos' => array(self::BELONGS_TO, 'Dimos', 'dimos_id'),
			'pvPlants' => array(self::HAS_MANY, 'PvPlant', 'address_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'dimos_id' => Yii::t('', 'Municipality'),
			'thesi' => Yii::t('', 'Position') .' / '. Yii::t('', 'Address'),
			'noumero' => Yii::t('', 'Noumero'),
			'latitude' => Yii::t('', 'Latitude') .' ('. Yii::t('', 'decimal format') .')',
			'longitude' => Yii::t('', 'Longitude') .' ('. Yii::t('', 'decimal format') .')',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		//Warning: Please modify the following code to remove attributes that should not be searched
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dimos_id',$this->dimos_id);
		$criteria->compare('thesi',$this->thesi,true);
		$criteria->compare('noumero',$this->noumero,true);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('longitude',$this->longitude);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}