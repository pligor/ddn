<?php

/**
 * TSXxl class
 */
class TSXxl extends CModel {
	const format = 'Excel5';
	
	public $phpExcel;
	
	protected $path;
	protected $titleRows = array();
	
	protected $filterSubset;
	protected $columns;
	
	protected $startRow = 1;
	protected $dataStartRow = 2;	//row where real data start
	protected $endRow = null;	//null means all the rows
	
	protected $sheetNames = array();
	
	protected $cols = array(
		'island_name' => 'A',
		'protocol' => 'E',
		'nominal_power' => 'H',
		'SimvasiPolisis' => 'N',
	);
	
	public function __construct($path) {
		$this->path = $path;
	    $this->init();
	}
	
	/** 
	 * Enter description here ...
	 */
	public function init() {
		if( !is_file($this->path) ) {
			$message = "Το {$this->path} δεν είναι αρχείο";
		}
		
		$this->titleRows = array(1);
		
		$this->columns = CHtmlEx::getColumns('A', 'P');

		WorksheetHelper::loadPHPExcel();
		$this->filterSubset = new ExcelReadFilter($this->startRow, $this->endRow, $this->columns);
		WorksheetHelper::unLoadPHPExcel();
		
		$this->verifySheetnames();
	}
	
	/**
	 * This is an abstract method and MUST be overloaded
	 * ALL public properties
	 * @see CModel::attributeNames()
	 */
	public function attributeNames() {
		//this trick is to change the scope and therefore retrieve ONLY public properties
		$getVars = function($obj) { return get_object_vars($obj); };
		return array_keys( $getVars($this) );
	}
	
	protected function getIslandId(& $sheet, $i) {
		$attr = 'island_name';
		$island_name = $sheet->getCell($this->cols[$attr].$i)->getValue();
		$attributes = array(
			$attr => $island_name,
		);
		$model = Island::model()->findByAttributes($attributes);
		if($model===null) {
			$message = Yii::t('', 'island name {island_name} is not valid',array(
				'{island_name}' => $island_name,
			));
			throw new Exception($message);
		}
		
		return $model->id;
	}
	
	protected function getPerioxiDeiId(& $sheet) {
		$perioxi_dei_name =  $sheet->getTitle();
		
		$attributes = array(
			'perioxi_dei_name' => $perioxi_dei_name,
		);
		
		$model = PerioxiDei::model()->findByAttributes($attributes);
		if($model===null) {
			$message = Yii::t('', 'the worksheet name {perioxi_dei_name} is not valid',array(
				'{perioxi_dei_name}' => $perioxi_dei_name,
			));
			throw new Exception($message);
		}
		
		return $model->id;
	}
	
	/**
	 * Enter description here ...
	 * @param PHPExcel_Worksheet $sheet
	 * @param int $i
	 */
	protected function getPvCat($sheet, $i) {
		$nominal_power = $sheet->getCell($this->cols['nominal_power'].$i)->getValue();
		foreach(PvPlant::getCats() as $pvCatId => $pvCat) {
			$powerRange = $pvCat::getPowerRange();
			if( ($nominal_power > $powerRange['min']) && ($nominal_power <= $powerRange['max']) ) {
				return $pvCat;
			}
		}
		
		$message = Yii::t('', 'Nominal Power is out of range');
		throw new Exception($message);
	}
	
	/**
	 * Enter description here ...
	 * @param PHPExcel_Worksheet $sheet
	 * @param int $i
	 */
	public function getProtocol(& $sheet, $i) {
		$protocol = $sheet->getCell($this->cols['protocol'].$i)->getValue();
		if( !CHtmlEx::startsWithInteger($protocol) ) {
			$message = Yii::t('', 'Protocol must be INTEGER number');
			throw new Exception($message);
		}
		return intval($protocol);
	}
	
	/**
	 * try to get pv plant id from row information
	 * @param PHPExcel_Worksheet $sheet
	 * @param int $i
	 * @throws Exception
	 */
	protected function getPvPlantId(& $sheet, $i) {	
		$perioxi_dei_id = $sheet->perioxi_dei_id;
		$pvCat = $this->getPvCat($sheet, $i);
		
		if($pvCat == 'PvSteges') {
			$message = Yii::t('', 'The PV plant belongs in ΣΤΕΓΕΣ category'). " (???)";
			throw new Exception($message);
		}
		
		$protocol = $this->getProtocol($sheet, $i);
		
		$attributes = array(
			'protocol' => $protocol,
			'perioxi_dei_id' => $perioxi_dei_id,
		);
		$model = $pvCat::model()->findByAttributes($attributes);
		
		if($model===null) {
			$perioxi_dei_name = CHtmlEx::loadModel('PerioxiDei', $perioxi_dei_id)->perioxi_dei_name;
			$message = Yii::t('', 'Could not find pv plant with protocol {protocol} at Area {perioxi_dei_name}', array(
				'{protocol}' => $protocol,
				'{perioxi_dei_name}' => $perioxi_dei_name,
			));
			throw new Exception($message);
		}
		
		return $model->pvPlant->id;
	}
	
	/**
	 * Enter description here ...
	 * @param int $xlStamp
	 */
	protected function convDate($xlStamp) {
		if( !CHtmlEx::isUnsignedInteger($xlStamp) ) {
			$message = Yii::t('', 'Date has INVALID format');
			throw new Exception($message);
		}
		
		WorksheetHelper::loadPHPExcel();
		$stamp = PHPExcel_Shared_Date::ExcelToPHP($xlStamp);
		WorksheetHelper::unLoadPHPExcel();
		
		return $stamp;
	}
	
	protected function getSimvasiPolisis(& $sheet, $i) {
		$xlStamp = $sheet->getCell($this->cols['SimvasiPolisis'].$i)->getValue();
		if(CHtmlEx::isEmpty($xlStamp)) {
			return null;
		}
		return $this->convDate($xlStamp);
	}
	
	public function checkSimvasiPolisis($parameters) {
		$sheet = $parameters['sheet'];
		$i = $parameters['i'];
		
		$tsxDate = $this->getSimvasiPolisis($sheet, $i);
		
		$perioxiDate = null;
		$pv_plant_id = $this->getPvPlantId($sheet, $i);
		$procedures = CHtmlEx::loadModel('PvPlant', $pv_plant_id)->getProcedures();
		if( isset($procedures['SimvasiPolisis']) ) {
			$perioxiDate = $procedures['SimvasiPolisis']->procedureStep->execution_date;
		}
		
		if( ($tsxDate===null) && ($perioxiDate!==null) ) {
			$message = Yii::t('', 'Date in Τομέα Σύνδεσης Χρηστών is empty while in Area there is a date');
			throw new Exception($message);
		}
		elseif( ($tsxDate!==null) && ($perioxiDate===null) ) {
			$message = Yii::t('', 'Date in Area is empty while in Τομέα Σύνδεσης Χρηστών there is a date');
			throw new Exception($message);
		}
		elseif( $tsxDate != $perioxiDate ) {
			$message = Yii::t('', 'Date in Τομέα Σύνδεσης Χρηστών is different from the date in Area');
			throw new Exception($message);
		}
	}
	
	protected function isRowEmpty(& $row) {
		WorksheetHelper::loadPHPExcel();	//GETTING THIS OUT OF THE LOOP FOR PERFORMANCE
		
		$isRowEmpty = true;
		for($cellIter = $row->getCellIterator();$cellIter->valid();$cellIter->next()) {
			$j = $cellIter->key();
			
			$col = PHPExcel_Cell::stringFromColumnIndex($j);
			
			$cellValue = $cellIter->current()->getValue();
	
			$isRowEmpty = $isRowEmpty && CHtmlEx::isEmpty($cellValue);
		}
		
		WorksheetHelper::unLoadPHPExcel();	//GETTING THIS OUT OF THE LOOP FOR PERFORMANCE
		
		return $isRowEmpty;
	}
	
	/**
	 * @param PHPExcel_Worksheet $sheet
	 * @param callback $function
	 * @param mixed $parameters
	 * @return bool
	 */
	public function forAllRows(& $sheet, $method=null, $parameters=array()) {
		$function = array($this,$method);
		
		if( !is_callable($function) ) {
			$function = function(){};
		}
		
		WorksheetHelper::loadPHPExcel();
		$check = $sheet instanceof PHPExcel_Worksheet;
		WorksheetHelper::unLoadPHPExcel();
		if( !$check ) {
			return false;
		}
		
		$reportSheet=array();
		for($rowIter = $sheet->getRowIterator(); $rowIter->valid(); $rowIter->next()) {
			$i = $rowIter->key();
			if($i < $this->dataStartRow) {
				continue;	//ignore headers	
			}
			
			$row = $rowIter->current();
			if($this->isRowEmpty($row)) {
				break;	//end loop if you find an empty line
			}
			
			$parameters = array_merge($parameters,array('sheet' => & $sheet, 'i' => $i));
			
			try {
				call_user_func($function, $parameters);
			}
			catch(Exception $e) {
				$reportSheet[$i] = $e->getMessage();
			}
			
			//break;////////////////////////////////////////////////////////////////////////////////////////////
		}
		
		return $reportSheet;
	}
	
	/**
	 * EAN THELAME KAI H SYNARTISI FOR ALL ROWS NA KALEITAI APO EDW DYNAMIKA
	 * THA EPREPE NA EIXE TO IDIO SIGNATURE ME TIS YPOLOIPES SYNARTISEIS
	 * ISWS NA TO YLOPOIHSOUME ETSI ARGOTERA AN KAI DEN EINAI APOLYTA KRISIMO
	 * @param callback $function
	 * @param mixed $parameters
	 * @return bool
	 */
	public function forAllSheets($method=null,$parameters=array()) {
		$function = array($this,$method);
		
		if( !is_callable($function) ) {
			$function = function(){};
		}
		
		WorksheetHelper::loadPHPExcel();
		$check = $this->phpExcel instanceof PHPExcel;
		WorksheetHelper::unLoadPHPExcel();
		if( !($check) ) {
			return false;
		}
		
		$reportPerioxes = array();
		
		$sheets = $this->phpExcel->getAllSheets();
		foreach($sheets as & $sheet) {
			$sheet->perioxi_dei_id = $this->getPerioxiDeiId($sheet);
			
			$reportSheet = $this->forAllRows($sheet, $method, $parameters);
			
			if( !empty($reportSheet) ) {
				$reportPerioxes[$sheet->perioxi_dei_id] = $reportSheet;
			}
		}
		
		return $reportPerioxes;
	}
	
	protected function testMethod($parameters) {
		if($parameters['test']=='dummy') {
			print null;
		}
		elseif($parameters['test']=='exception') {
			throw new Exception('hello this is an exception');
		}
	}
	
	/**
	 * verify if the excel contains all the corresponding perioxes
	 * @throws Exception
	 */
	public function verifySheetnames() {
		$objReader = $this->getObjReader();
		
		$listData = CHtml::listData(PerioxiDei::model()->findAll(), 'id', 'perioxi_dei_name');
		
		$messages = array();
		
		$sheetNames = $objReader->listWorksheetNames($this->path);
		
		$array_diff = array_diff($listData, $sheetNames);	//here the order of the parameters matters!
		if( !empty($array_diff) ) {
			$message = "Το αρχείο δεν περιέχει όλες τις Περιοχές που υπάγονται στη ΔΔΝ";
			throw new Exception($message);
		}
		
		$this->sheetNames = $listData;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function loadExcel() {	
		try {
			$objReader = $this->getObjReader($this->sheetNames, $this->filterSubset);
			
			$this->phpExcel = $objReader->load($this->path);
		}
		catch(Exception $e) {
			throw new CHttpException(404, $e->getMessage());	//rethrow
		}
	}
	
	/**
	 * Creates a reader with the corresponding
	 * @param string[] $sheetNames
	 * @param ExcelReadFilter $filterSubset
	 * @param string $format
	 * @return PHPExcel_Reader
	 */
	protected function & getObjReader($sheetNames=array(), $filterSubset=null, $format='Excel5') {
		WorksheetHelper::loadPHPExcel();
		$objReader = PHPExcel_IOFactory::createReader($format);
		WorksheetHelper::unLoadPHPExcel();
		
		if(empty($sheetNames)) {
			$objReader->setLoadAllSheets();	
		}
		else {
			$objReader->setLoadSheetsOnly($sheetNames);
		}
	
		if($filterSubset !== null) {
			$objReader->setReadFilter($filterSubset);	
		}
		
		return $objReader;
	}
}