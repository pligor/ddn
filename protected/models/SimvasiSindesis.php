<?php

/**
 * This is the model class for table "simvasi_sindesis".
 *
 * The followings are the available columns in table 'simvasi_sindesis':
 * @property integer $procedure_step_id
 * @property integer $diatiposi_oron_procedure_step_id
 * @property integer $arithmos_paroxis
 *
 * The followings are the available model relations:
 * @property SimvasiPolisis[] $simvasiPolisises
 * @property ProcedureStep $procedureStep
 * @property DiatiposiOron $diatiposiOronProcedureStep
 */
class SimvasiSindesis extends CActiveRecord {
	
	const arithmos_paroxis_max_len = 10;
	
	public static function getPerioxiCols($step) {
		if($step != 'SimvasiSindesis') return array();
		
		return array(
			'arithmos_paroxis' => 'Y',
		);
	}
	
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => '.........',
		    //'showErrorSummary' => true,
		    'elements' => array(
				'arithmos_paroxis' => array(
					'type' => 'text',
					//'hint' => '',
		        ),
			),
		);
	}
	
	public static function getTitle() {
		return 'Σύμβαση Σύνδεσης';
	}
	
	public function normalizeAttributes() {	//BE AWARE: you set the correct scenario before using this method
		$attrNames = $this->safeAttributeNames;
		foreach($attrNames as $attrName) {
			$method = 'normalize'. ucfirst($attrName);
			if( method_exists($this, $method) ) {
				$this->$method();
			}
		}
	}
	
	public function normalizeArithmos_paroxis($return=false) {
		if( CHtmlEx::isEmpty($this->arithmos_paroxis) ) {
			$this->arithmos_paroxis = null;
			if($return) {
				return $this->arithmos_paroxis;
			}
			return;
		}
		
		$arithmos_paroxis = $this->arithmos_paroxis;
		
		$pattern = '@[0-9]{1,'. self::arithmos_paroxis_max_len .'}@';
		$subject = $arithmos_paroxis;
		$matches = array();
		$noMatches = preg_match($pattern, $subject, $matches);
		if($noMatches==0) {
			$message = 'Δεν βρέθηκε έγκυρος αριθμός παροχής (ακέραιος αριθμός)';
			throw new Exception($message);
		}
		
		$arithmos_paroxis = $matches[0];
		
		if($return) {
			return $arithmos_paroxis;
		}
		
		$this->arithmos_paroxis = $arithmos_paroxis;
	}
	
	public function save($validation=true,$attributes=null) {
		$this->normalizeAttributes();
		return parent::save($validation);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return SimvasiSindesis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{simvasi_sindesis}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('procedure_step_id, arithmos_paroxis, diatiposi_oron_procedure_step_id', 'required'),
			array('procedure_step_id, diatiposi_oron_procedure_step_id', 'numerical', 'integerOnly'=>true),
			array(
				'arithmos_paroxis',
				'length',
				'max' => self::arithmos_paroxis_max_len,
				'allowEmpty' => true,
			),
                        array(
				'arithmos_paroxis',
				'unique',
			),
			array(
				'arithmos_paroxis',
				'numerical',
				'integerOnly' => true,
				'allowEmpty' => true,
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('procedure_step_id, arithmos_paroxis, diatiposi_oron_procedure_step_id', 'safe', 'on'=>'search'),
			
			array('arithmos_paroxis', 'safe', 'on'=>'excel'),
			array('procedure_step_id, diatiposi_oron_procedure_step_id', 'unsafe', 'on'=>'excel'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'simvasiPolisises' => array(self::HAS_MANY, 'SimvasiPolisis', 'simvasi_sindesis_procedure_step_id'),
			'procedureStep' => array(self::BELONGS_TO, 'ProcedureStep', 'procedure_step_id'),
			'diatiposiOronProcedureStep' => array(self::BELONGS_TO, 'DiatiposiOron', 'diatiposi_oron_procedure_step_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'procedure_step_id' => 'Procedure Step',
			'diatiposi_oron_procedure_step_id' => 'Diatiposi Oron Procedure Step',
			'arithmos_paroxis' => 'Αριθμός Παροχής',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('procedure_step_id',$this->procedure_step_id);
		$criteria->compare('diatiposi_oron_procedure_step_id',$this->diatiposi_oron_procedure_step_id);
		$criteria->compare('arithmos_paroxis',$this->arithmos_paroxis,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}