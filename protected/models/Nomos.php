<?php

/**
 * This is the model class for table "nomos".
 *
 * The followings are the available columns in table 'nomos':
 * @property integer $id
 * @property string $nomos_name
 * 
 * @property CForm $form
 *
 * The followings are the available model relations:
 * @property Dimos[] $dimoses
 */
class Nomos extends CActiveRecord {
	/**
	 * Returns true if all data from csv file are inserted. false otherwise
	 * @return bool
	 */
	public static function loadDefaultData() {
		$class = get_class();
		$model = new $class();
		$model->deleteAll();	//delete ALL rows
		
		$csvPath = Yii::getPathOfAlias('application.data') .'/'. ActiveRecord::getTableName($model) .'.csv';
		
		$twoDarray = CHtmlEx::csv2array($csvPath, ';');
		
		$allInserted = true;
		foreach($twoDarray as $rowArray) {
			
			$model = new $class();
			$model->id = $rowArray[0];
			$model->nomos_name = $rowArray[1];
			$insertion = $model->insert();
			
			$allInserted = $allInserted && $insertion;
		}
		return $allInserted;
	}
	
	/**
	 * Get all in format array(id => name) ordered by name 
	 * @return Array
	 */
	public static function getNomous() {
		$valueField = 'id';
		$textField = 'nomos_name';
		
		$criteria=new CDbCriteria;
		$criteria->select = array($valueField, $textField);
		$criteria->order = $textField;
		
		$allRows = self::model()->findAll($criteria);

		return CHtml::listData($allRows, $valueField, $textField);
	}
	
	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to define a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				get_class() => $this->getFormConfig(),
			),
			'buttons'=>array(
		        'submit' => array(
		            'type' => 'submit',
		            'label' => Yii::t('', 'Select'),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		$elements = array(
			'id' => array(
				'type' => 'dropdownlist',
				'items' => self::getNomous(),
				//'hint' => '',
	        ),
		);
		
		return array(
			'type' => 'form',
			//'title' => '......',
			//'showErrorSummary' => true,
		    'elements' => $elements,
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			/*array(
				'nomos_name',
				'length',
				'max' => 50,
			),*/
			array(
				'id',
				'exist',
				'allowEmpty' => false,
			),
			
			array('nomos_name', 'unsafe'),
			
			//Please remove those attributes that should not be searched.
			array('id, nomos_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related class name for the relations automatically generated below.
		return array(
			'dimoses' => array(self::HAS_MANY, 'Dimos', 'nomos_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => Yii::t('', 'County Name'),
			'nomos_name' => Yii::t('', 'County Name'),
		);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Nomos the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{nomos}}';
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		$criteria=new CDbCriteria;

		$criteria->compare('nomos_name',$this->nomos_name,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}