<?php

/**
 * This is the model class for table "dimos_island".
 *
 * The followings are the available columns in table 'dimos_island':
 * @property integer $dimos_id
 * @property integer $island_id
 */
class DimosIsland extends CActiveRecord {

	/**
	 * loads default data from predefined csv file
	 * We have supposed (not absolutely sure) that the returned db attributes have
	 * the same order as the order of the values inside the csv file (value1;value2;value3;...)
	 */
	public static function loadDefaultData() {
		$class = get_class();
		$model = new $class();
		$model->deleteAll();	//delete ALL rows
		
		$dbAttrs = CActiveRecordHelper::getOnlyDbAttributes($model);
		
		$csvPath = Yii::getPathOfAlias('application.data') .'/'. ActiveRecord::getTableName($model) .'.csv';
		$twoDarray = CHtmlEx::csv2array($csvPath, ';');
		
		$allInserted = true;
		foreach($twoDarray as $rowArray) {
			$model = new $class();
			foreach($dbAttrs as $index => $dbAttr) {
				$model->$dbAttr = $rowArray[$index];
			}
			$insertion = $model->insert();
			
			$allInserted = $allInserted && $insertion;
		}
		
		return $allInserted;
	}
	
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return DimosIsland the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{dimos_island}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dimos_id, island_id', 'required'),
			array('dimos_id, island_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('dimos_id, island_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'dimos_id' => 'Dimos',
			'island_id' => 'Island',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('dimos_id',$this->dimos_id);
		$criteria->compare('island_id',$this->island_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}