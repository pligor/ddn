<?php

/**
 * This is the model class for table "pv_enabled".
 *
 * The followings are the available columns in table 'pv_enabled':
 * @property integer $pv_plant_id
 * @property double $real_power
 * @property string $ypostathmos
 * @property string $grammi_dianomis
 * @property string $thesi_syndesis
 * @property string $ys_mt_xt
 * @property string $anaxorisi
 * @property integer $activation_procedure_step_id
 * 
 * @property boolean $tracker
 *
 * The followings are the available model relations:
 * @property PvPlant $pvPlant
 * @property Activation $activationProcedureStep
 * @property Tracker $trackerObj
 */
class PvEnabled extends CActiveRecord {
	const ypostathmosMaxLen = 60;
	const grammi_dianomisMaxLen = 30;
	const thesi_syndesisMaxLen = 30;
	const anaxorisiMaxLen = 60;
	const ys_mt_xtMaxLen = 60;
	
	public static function getPerioxiCols($step) {
		if($step != 'Activation') return array();
		
		return array(
			'real_power' => 'R',
			'tracker' => 'S',
			'ypostathmos' => 'T',
			'grammi_dianomis' => 'U',
			'thesi_syndesis' => 'V',
			'ys_mt_xt' => 'W',
			'anaxorisi' => 'X',
		);
	}
	
	public static function grammiDianomisPattern() {
		return '@[^0-9]-([0-9]+)@';
	}
	
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => '............',
		    //'showErrorSummary' => true,
		    'elements' => array(
				'tracker' => array(
					'type' => 'checkbox',
					//'hint' => '',
					'attributes' => array(
					),
		        ),
		        'real_power' => array(
					'type' => 'text',
					//'hint' => '',
					'attributes' => array(
					),
		        ),
		        'ypostathmos' => array(
					'type' => 'text',
					//'hint' => '',
					'attributes' => array(
		        		'size' => self::ypostathmosMaxLen,
		        		'maxlength' => self::ypostathmosMaxLen,
					),
		        ),
		        'grammi_dianomis' => array(
					'type' => 'text',
					//'hint' => '',
					'attributes' => array(
		        		'size' => self::grammi_dianomisMaxLen,
		        		'maxlength' => self::grammi_dianomisMaxLen,
					),
		        ),
		        'thesi_syndesis' => array(
					'type' => 'text',
					//'hint' => '',
					'attributes' => array(
		        		'size' => self::thesi_syndesisMaxLen,
		        		'maxlength' => self::thesi_syndesisMaxLen,
					),
		        ),
		        'anaxorisi' => array(
					'type' => 'text',
					//'hint' => '',
					'attributes' => array(
		        		'size' => self::anaxorisiMaxLen,
		        		'maxlength' => self::anaxorisiMaxLen,
					),
		        ),
		        'ys_mt_xt' => array(
					'type' => 'text',
					//'hint' => '',
					'attributes' => array(
		        		'size' => self::ys_mt_xtMaxLen,
		        		'maxlength' => self::ys_mt_xtMaxLen,
					),
		        ),
			),
		);
	}
	
	/**
	 * override to also save tracker
	 * @see CActiveRecord::save()
	 */
	public function save($validation=true,$attributes=null) {
		
		$this->normalizeAttributes();
		
		$saved = parent::save($validation);
		if( !$saved ) {
			$message = Yii::t('', 'saving in table {tableName} failed',array(
				'{tableName}' => $this->tableName(),
			));
			throw new Exception($message);
		}

		//PROSOXI DEN PERNAME TO $validation KAI STO saveTracker DIOTI TOTE
		//THA PREPEI NA KANOUME OVERRIDE KAI THN SYNARTISI validate
		if($this->tracker) {
			$this->saveTracker();
		}
		
		return true;
	}
	
	/**
	 * @param bool $return
	 */
	public function normalizeAnaxorisi($return=false) {
		$anaxorisi = $this->anaxorisi;
		
		if(CHtmlEx::isEmpty($anaxorisi)) {
			$this->anaxorisi = CHtmlEx::invalidStr;
			if($return) {
				return $this->anaxorisi;
			}
			return;
		}
		
		//trim to size
		$anaxorisi = mb_substr($anaxorisi, 0, self::anaxorisiMaxLen, Yii::app()->charset);
		
		if($return) {
			return $anaxorisi;
		}
		$this->anaxorisi = $anaxorisi;
	}
	
	/**
	 * @param bool $return
	 */
	public function normalizeYs_mt_xt($return=false) {
		$ys_mt_xt = $this->ys_mt_xt;
		
		if(CHtmlEx::isEmpty($ys_mt_xt)) {
			$this->ys_mt_xt = null;
			if($return) {
				return $this->ys_mt_xt;
			}
			return;
		}
		
		//trim to size
		$ys_mt_xt = mb_substr($ys_mt_xt, 0, self::ys_mt_xtMaxLen, Yii::app()->charset);
		
		if($return) {
			return $ys_mt_xt;
		}
		$this->ys_mt_xt = $ys_mt_xt;
	}
	
	/**
	 * @param bool $return
	 */
	public function normalizeThesi_syndesis($return=false) {
		$thesi_syndesis = $this->thesi_syndesis;
		
		if(CHtmlEx::isEmpty($thesi_syndesis)) {
			$this->thesi_syndesis = CHtmlEx::invalidStr;
			if($return) {
				return $this->thesi_syndesis;
			}
			return;
		}
		
		//trim to size
		$thesi_syndesis = mb_substr($thesi_syndesis, 0, self::thesi_syndesisMaxLen, Yii::app()->charset);
		
		if($return) {
			return $thesi_syndesis;
		}
		$this->thesi_syndesis = $thesi_syndesis;
	}
	
	/**
	 * @param bool $return
	 */
	public function normalizeGrammi_dianomis($return=false) {
		$grammi_dianomis = $this->grammi_dianomis;
		
		if(CHtmlEx::isEmpty($grammi_dianomis)) {
			$this->grammi_dianomis = CHtmlEx::invalidStr;
			if($return) {
				return $this->grammi_dianomis;
			}
			return;
		}
		
		$grammi_dianomis = CHtmlEx::parseGrammiDianomis($grammi_dianomis);
		
		if($return) {
			return $grammi_dianomis;
		}
		$this->grammi_dianomis = $grammi_dianomis;
	}
	
	/**
	 * @param bool $return
	 */
	public function normalizeYpostathmos($return=false) {
		$ypostathmos = $this->ypostathmos;
		
		if(CHtmlEx::isEmpty($ypostathmos)) {
			$this->ypostathmos = CHtmlEx::invalidStr;
			if($return) {
				return $this->ypostathmos;
			}
			return;
		}
		
		//trim to size
		$ypostathmos = mb_substr($ypostathmos, 0, self::ypostathmosMaxLen, Yii::app()->charset);
		
		if($return) {
			return $ypostathmos;
		}
		else {
			$this->ypostathmos = $ypostathmos;
		}
	}
	
	/**
	 * @param bool $return
	 */
	public function normalizeReal_power($return=false) {
		$real_power = CHtmlEx::parseFloat($this->real_power);
		
		if($real_power===false) {
			$real_power = null;
		}
		
		if($return) {
			return $real_power;
		}
		else {
			$this->real_power = $real_power;
		}
	}
	
	public function normalizeAttributes() {	//BE AWARE: you set the correct scenario before using this method
		$attrNames = $this->safeAttributeNames;
		foreach($attrNames as $attrName) {
			$method = 'normalize'. ucfirst($attrName);
			if( method_exists($this, $method) ) {
				$this->$method();
			}
		}
	}
	
	/**
	 * @return bool
	 * returns true EVEN IF trackerObj is a new model ( using setTracker method )
	 */
	public function getTracker()
	{
		if($this->trackerObj) {
			//if( ! $this->trackerObj->isNewRecord )
				return true;
		}
		
		return false;
	}
	
	/**
	 * DO NOT name this method as normalizeTracker because it CANNOT be used this way (tracker is a CUSTOM property)
	 * @param mixed $var
	 */
	private function fixTracker($var) {
		//if boolean leave it as it is
		if( is_bool($var) ) {
			return $var;
		}
		
		//expected values
		if($var==0) {
			return false;
		}
		elseif($var==1) {
			return true;
		}
		
		$var = CHtmlEx::noTones($var);
		$var = CHtmlEx::noWhitespace($var);
		$var = mb_strtoupper($var, Yii::app()->charset);
		
		if($var=='NAI' || $var=='ΝΑΙ') {	//english or greek version
			return true;
		}
		elseif($var=='OXI' || $var=='ΟΧΙ') { //english or greek version
			return false;
		}
		
		//rest values
		return !CHtmlEx::isEmpty($var);
	}
	
	/**
	 * TWRA H THA KANW TO DELETE EDW MESA H THA KANW TO DELETE MESA STO SAVE
	 * @param bool|int $value
	 */
	public function setTracker($value) {
		$value = $this->fixTracker($value);
		
		if($this->tracker == $value) return; //no change is required
		
		if( $this->isNewRecord ) {
			if($value) {
				$this->trackerObj = new Tracker();
			}
			else {
				$this->trackerObj = null;
			}
		}
		else {
			if($value) {
				$this->saveTracker();
			}
			else {
				$this->deleteTracker();
			}
		}
	}
	
	private function saveTracker($validation=true) {
		if( !is_object($this->trackerObj) ) {
			$this->trackerObj = new Tracker();
		}
		elseif( !$this->trackerObj->isNewRecord ) {	//no overwrite
			return false;
		}
		
		if($this->pv_plant_id === null) {
			$message = Yii::t('', 'You tried to set tracker without previously define pv_plant_id');
			throw new Exception($message);
		}

		$this->trackerObj->pv_enabled_pv_plant_id = $this->pv_plant_id;
		$saved = $this->trackerObj->save($validation);
		if( !$saved ) {
			$message = Yii::t('', 'saving in table {tableName} failed',array(
				'{tableName}' => $this->trackerObj->tableName(),
			));
			throw new Exception($message);	
		}
		
		return true;
	}
	
	private function deleteTracker() {
		if( !$this->tracker )	return false;	//no need to delete
		
		try {
			$this->trackerObj->delete();	
		}
		catch(Exception $e) {
			print $e->getMessage();
		}
		
		$this->trackerObj = null;
	
		return true;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return PvEnabled the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pv_enabled}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pv_plant_id, real_power, ypostathmos, grammi_dianomis, thesi_syndesis, anaxorisi, activation_procedure_step_id', 'required'),
			array('pv_plant_id, activation_procedure_step_id', 'numerical', 'integerOnly'=>true),
			array('real_power', 'numerical'),
			array('ys_mt_xt, anaxorisi', 'length', 'max'=>60),
			array(
				'ypostathmos',
				'length',
				'max' => self::ypostathmosMaxLen,
			),
			array('grammi_dianomis, thesi_syndesis', 'length', 'max'=>30),
			array(
				'tracker',
				'in',
				'range' => array(0,1),
			),
			// The following rule is used by search(). Please remove those attributes that should not be searched.
			array('pv_plant_id, real_power, ypostathmos, grammi_dianomis, thesi_syndesis, ys_mt_xt, anaxorisi, activation_procedure_step_id', 'safe', 'on'=>'search'),
			
			array('real_power, tracker, ypostathmos, grammi_dianomis, thesi_syndesis, ys_mt_xt, anaxorisi', 'safe', 'on'=>'excel'),
			array('pv_plant_id, activation_procedure_step_id', 'unsafe', 'on'=>'excel'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pvPlant' => array(self::BELONGS_TO, 'PvPlant', 'pv_plant_id'),
			'activationProcedureStep' => array(self::BELONGS_TO, 'Activation', 'activation_procedure_step_id'),
			'trackerObj' => array(self::HAS_ONE, 'Tracker', 'pv_enabled_pv_plant_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'real_power' => Yii::t('', 'Real Power').' (kW)',
			'ypostathmos' => Yii::t('', 'Local Power Station'),
			'grammi_dianomis' => Yii::t('', 'Power Line'),
			'thesi_syndesis' => Yii::t('', 'Power Connection'),
			'ys_mt_xt' => Yii::t('', 'Transformer HV/LV'),
			'anaxorisi' => Yii::t('', 'Final Connection'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pv_plant_id',$this->pv_plant_id);
		$criteria->compare('real_power',$this->real_power);
		$criteria->compare('ypostathmos',$this->ypostathmos,true);
		$criteria->compare('grammi_dianomis',$this->grammi_dianomis,true);
		$criteria->compare('thesi_syndesis',$this->thesi_syndesis,true);
		$criteria->compare('ys_mt_xt',$this->ys_mt_xt,true);
		$criteria->compare('anaxorisi',$this->anaxorisi,true);
		$criteria->compare('activation_procedure_step_id',$this->activation_procedure_step_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}