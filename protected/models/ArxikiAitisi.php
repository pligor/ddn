<?php

/**
 * This is the model class for table "arxiki_aitisi".
 *
 * The followings are the available columns in table 'arxiki_aitisi':
 * @property integer $procedure_step_id
 * @property double $nominal_power
 *
 * The followings are the available model relations:
 * @property ProcedureStep $procedureStep
 * @property DiatiposiOron[] $diatiposiOrons
 */
class ArxikiAitisi extends CActiveRecord {
	public static function getPerioxiCols($step='ArxikiAitisi') {
		if($step != 'ArxikiAitisi') return array();
		
		return array(
			'nominal_power' => 'G',
		);
	}
	
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => '.........',
		    //'showErrorSummary' => true,
		    'elements' => array(
				'nominal_power' => array(
					'type' => 'text',
					//'hint' => '',
		        ),
			),
		);
	}
	
	public static function getTitle() {
		return 'Αρχική Αίτηση';
	}
	
	public function normalizeAttributes() {	//BE AWARE: you set the correct scenario before using this method
		$attrNames = $this->safeAttributeNames;
		foreach($attrNames as $attrName) {
			$method = 'normalize'. ucfirst($attrName);
			if( method_exists($this, $method) ) {
				$this->$method();
			}
		}
	}
	
	/**
	 * @param bool $return
	 */
	public function normalizeNominal_power($return=false) {
		$nominal_power = CHtmlEx::parseFloat($this->nominal_power);
		
		if($nominal_power===false) {
			$nominal_power = null;
		}
		
		if($return) {
			return $nominal_power;
		}
		else {
			$this->nominal_power = $nominal_power;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::save()
	 */
	public function save($validation=true,$attributes=null) {
		$this->normalizeAttributes();
		
		return parent::save($validation);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return ArxikiAitisi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{arxiki_aitisi}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that will receive user inputs.
		return array(
			array('procedure_step_id, nominal_power', 'required'),
			array('procedure_step_id', 'numerical', 'integerOnly'=>true),
			array('nominal_power', 'numerical'),
			// The following rule is used by search().Please remove those attributes that should not be searched.
			array('procedure_step_id, nominal_power', 'safe', 'on'=>'search'),
			
			array('nominal_power', 'safe', 'on'=>'excel'),
			array('procedure_step_id', 'unsafe', 'on'=>'excel'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'procedureStep' => array(self::BELONGS_TO, 'ProcedureStep', 'procedure_step_id'),
			'diatiposiOrons' => array(self::HAS_MANY, 'DiatiposiOron', 'arxiki_aitisi_procedure_step_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nominal_power' => Yii::t('', 'Nominal Power') .' (kW)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('procedure_step_id',$this->procedure_step_id);
		$criteria->compare('nominal_power',$this->nominal_power);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}