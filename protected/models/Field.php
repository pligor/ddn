<?php

/**
 * Fields for excel methods
 * @author pligor
 */
class Field extends CModel {
	public $header;
	public $colWidth;
	public $value;
	public $ind;
	
	private $itsMethod;
	
	public function __construct($method) {
		$this->itsMethod = $method;
	}
	
	/**
	 * @see CModel::attributeNames()
	 */
	public function attributeNames() {
		//this trick is to change the scope and therefore retrieve ONLY public properties
		$getVars = function($obj) { return get_object_vars($obj); };
		return array_keys( $getVars($this) );
	}
	
	public function execMethod(& $sheet, $i) {
		$method = $this->itsMethod;
		$this->$method($sheet, $i);
	}
	
	/**
	 * A little more complex than you would expect (not using array_diff, etc.)
	 * because there might be a situation where the field repeats itself
	 * REMEMBER: DO NOT CHANGE THE ORDER OF fieldInds array, the foreach loop here is suitable for the job
	 * @param array $fieldInds
	 */
	public static function getConfigFields($fieldInds) {
		$configFields = array();
		$allConfigFields = self::getAllConfigFields();
		foreach($fieldInds as $fieldInd) {
			$configFields[] = $allConfigFields[$fieldInd];
		}
		return $configFields;
	}
	
	public static function getAllConfigFields() {
		return array(
			0 => array(
				'header' => Yii::t('', 'SUBMISSIONS NUMBER'),
				'colWidth' => 15,
				'method' => 'Pli8osAitisewn',
			),
			1 => array(
				'header' => Yii::t('', 'SUBMISSIONS POWER') .' (kW)',
				'colWidth' => 15,
				'method' => 'IschysAitisewn',
			),
			2 => array(
				'header' => Yii::t('', 'ΟΡΟΙ ΣΥΝΔΕΣΗΣ NUMBER'),
				'colWidth' => 15,
				'method' => 'Pli8osOrwnSyndesis',
			),
			3 => array(
				'header' => Yii::t('', 'ΟΡΟΙ ΣΥΝΔΕΣΗΣ POWER') .' (kW)',
				'colWidth' => 15,
				'method' => 'IschysOrwnSyndesis',
			),
			4 => array(
				'header' => Yii::t('', 'END OF TERMS NUMBER'),
				'colWidth' => 15,
				'method' => 'Pli8osLiksisOrwn',
			),
			5 => array(
				'header' => Yii::t('', 'END OF TERMS POWER') .' (kW)',
				'colWidth' => 15,
				'method' => 'IschysLiksisOrwn',
			),
			6 => array(
				'header' => Yii::t('', 'ΣΥΜΒΑΣΗ ΣΥΝΔΕΣΗΣ NUMBER'),
				'colWidth' => 15,
				'method' => 'Pli8osSymbasisSyndesis',
			),
			7 => array(
				'header' => Yii::t('', 'ΣΥΜΒΑΣΗ ΣΥΝΔΕΣΗΣ POWER') .' (kW)',
				'colWidth' => 15,
				'method' => 'IschysSymbasisSyndesis',
			),
			8 => array(
				'header' => Yii::t('', 'ΣΥΜΒΑΣΗ ΠΩΛΗΣΗΣ NUMBER'),
				'colWidth' => 15,
				'method' => 'Pli8osSymbasisPwlisis',
			),
			9 => array(
				'header' => Yii::t('', 'ΣΥΜΒΑΣΗ ΠΩΛΗΣΗΣ POWER') .' (kW)',
				'colWidth' => 15,
				'method' => 'IschysSymbasisPwlisis',
			),
			10 => array(
				'header' => Yii::t('', 'ACTIVATION NUMBER'),
				'colWidth' => 15,
				'method' => 'Pli8osEnergopoiisis',
			),
			11 => array(
				'header' => Yii::t('', 'ACTIVATION POWER') .' (kW)',
				'colWidth' => 15,
				'method' => 'IschysEnergopoiisis',
			),
			12 => array(
				'header' => Yii::t('', 'TOTAL INSTALLED POWER THAT BINDS ELECTRIC SPACE') .' (kW)',
				'colWidth' => 15,
				'method' => 'DesmeymeniIschys',
			),
			13 => array(
				'header' => Yii::t('', 'ΡΑΕ MARGIN POWER') .' (kW)',
				'colWidth' => 15,
				'method' => 'nullFunc',
			),
			14 => array(
				'header' => Yii::t('', 'AVAILABLE POWER') .' (kW)',
				'colWidth' => 15,
				'method' => 'YpoloipoDia8esimo',
			),
			15 => array(
				'header' => Yii::t('', 'REQUESTED NEW POWER') .' (kW)',
				'colWidth' => 15,
				'method' => 'AitoymeniNeaIschys',
			),
			16 => array(
				'header' => Yii::t('', 'INSTALLED POWER THAT BINDS ELECTRIC SPACE MINUS ACTIVATED') .' (kW)',
				'colWidth' => 15,
				'method' => 'DesmeymeniIschysAneuEnergopoiisis',
			),
		);
	}
	
	//OPERATIONS
	
	private function DesmeymeniIschysAneuEnergopoiisis(& $sheet, $i, $return=false) {	//16
	//12 - 11
		 $value = 0;
		 
		 $value = $this->DesmeymeniIschys($sheet, $i, true);
		 $value = $value - $this->IschysEnergopoiisis($sheet, $i, true);
		 
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function IschysEnergopoiisis(& $sheet, $i, $return=false) {	//11
		$value = 0;
		
		$cols1 = ArxikiAitisi::getPerioxiCols();
		$cols2 = ProcedureStep::getPerioxiCols('Activation');
		$cols = CHtmlEx::array_merge_safe($cols1, $cols2);
		
		$curPower = $sheet->getCell($cols['nominal_power'].$i)->getValue();
		$curCell = $sheet->getCell($cols['execution_date'].$i);
		
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$value = $curPower;
		}
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function DesmeymeniIschys(& $sheet, $i, $return=false) {	//12
	//12 = 3 - 5
		$value = 0;
		
		$value = $this->IschysOrwnSyndesis($sheet,$i,true) - $this->IschysLiksisOrwn($sheet,$i,true);
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function IschysOrwnSyndesis(& $sheet, $i, $return=false) {	//3
		$value = 0;
		
		$cols1 = ArxikiAitisi::getPerioxiCols();
		$cols2 = DiatiposiOron::getPerioxiCols();
		$cols = CHtmlEx::array_merge_safe($cols1, $cols2);
		
		$curPower = $sheet->getCell($cols['nominal_power'].$i)->getValue();	//onomastiki isxys
		$curCell = $sheet->getCell($cols['protocol_diatiposis_oron'].$i);	//oroi syndesis
		
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$value = $curPower;
		}
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function IschysLiksisOrwn(& $sheet, $i, $return=false) {	//5
		$value = 0;
		
		$cols = Killed::getPerioxiCols();
		
		$curCell = $sheet->getCell($cols['pv_plant_id'].$i);
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$power2return = $curCell->getValue();
			if($power2return != 0) {
				$value = abs($power2return);
			}
		}
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function AitoymeniNeaIschys(& $sheet, $i, $return=false) {	//15
		$value = 0;
		$value = $this->IschysAitisewn($sheet, $i, true);
		
		$cols1 = ArxikiAitisi::getPerioxiCols();
		$cols2 = DiatiposiOron::getPerioxiCols();
		$cols = CHtmlEx::array_merge_safe($cols1, $cols2);
		
		$curPower = $sheet->getCell($cols['nominal_power'].$i)->getValue();
		$curCell = $sheet->getCell($cols['protocol_diatiposis_oron'].$i);
		
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$value -= $curPower;
		}
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function IschysAitisewn(& $sheet, $i, $return=false) {	//1
		$value = 0;
		
		$cols1 = ArxikiAitisi::getPerioxiCols();
		$cols2 = ProcedureStep::getPerioxiCols('ArxikiAitisi');
		$cols = CHtmlEx::array_merge_safe($cols1, $cols2);
		
		$curPower = $sheet->getCell($cols['nominal_power'].$i)->getValue();
		$curCell = $sheet->getCell($cols['execution_date'].$i);
		
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$value = $curPower;
		}
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function YpoloipoDia8esimo(& $sheet, $i, $return=false) {	//14
	//13 - 12	//must be initialized with perithorio rae
		$value = 0;
		
		$value = - $this->DesmeymeniIschys($sheet, $i, true);
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function Pli8osAitisewn(& $sheet, $i, $return=false) {	//0
		$value = 0;
		
		$cols = ProcedureStep::getPerioxiCols('ArxikiAitisi');
		
		$curCell = $sheet->getCell($cols['execution_date'].$i);	//hmerominia ypovolis ths arxikis aitisis
		
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$value = 1;
		}
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function Pli8osOrwnSyndesis(& $sheet, $i, $return=false) {	//2
		$value = 0;
		
		$cols = DiatiposiOron::getPerioxiCols();
		
		$curCell = $sheet->getCell($cols['protocol_diatiposis_oron'].$i);	//oroi syndesis
		
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$value = 1;
		}
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function Pli8osLiksisOrwn(& $sheet, $i, $return=false) {	//4
		$value = 0;
		
		//OLD automatic calculated way
		//$cells['terms_end'] = $sheet->getCell("N$i");	//liksi orwn
		/*
		$termsEndStamp = xl2GMTstamp( $cells['terms_end']->getValue() );
		
		if( is_numeric( $cells['terms_end']->getValue() )
			&& !is_numeric( $cells['conn_contract']->getValue() )
			&& ($termsEndStamp < $untilStamp)
		)	//yparxei liksi orwn kai DEN yparxei symvasi syndesis
		{
			$counter[$categ]['terms_end'] += 1;
			$power[$categ]['terms_end'] += $curPower;
		}
		*/
		
		$cols = Killed::getPerioxiCols();
	
		$curCell = $sheet->getCell($cols['pv_plant_id'].$i);	//isxus pou epistrefetai sto perithorio, apodesmeush isxuos
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$power2return = $curCell->getValue();
			if($power2return != 0) {
				$value = 1;
			}
		}
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function Pli8osSymbasisSyndesis(& $sheet, $i, $return=false) {	//6
		$value = 0;
		
		$cols = ProcedureStep::getPerioxiCols('SimvasiSindesis');
		
		$curCell = $sheet->getCell($cols['execution_date'].$i);	//symvasi syndesis
		
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$value = 1;
		}
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function IschysSymbasisSyndesis(& $sheet, $i, $return=false) {	//7
		$value = 0;
		
		$cols1 = ArxikiAitisi::getPerioxiCols();
		$cols2 = ProcedureStep::getPerioxiCols('SimvasiSindesis');
		$cols = CHtmlEx::array_merge_safe($cols1, $cols2);
		
		$curPower = $sheet->getCell($cols['nominal_power'].$i)->getValue();
		$curCell = $sheet->getCell($cols['execution_date'].$i);
		
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$value = $curPower;
		}
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function Pli8osSymbasisPwlisis(& $sheet, $i, $return=false) {	//8
		$value = 0;
		
		$cols = ProcedureStep::getPerioxiCols('SimvasiPolisis');
		
		$curCell = $sheet->getCell($cols['execution_date'].$i);
		
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$value = 1;
		}
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function IschysSymbasisPwlisis(& $sheet, $i, $return=false) {	//9
		$value = 0;
		
		$cols1 = ArxikiAitisi::getPerioxiCols();
		$cols2 = ProcedureStep::getPerioxiCols('SimvasiPolisis');
		$cols = CHtmlEx::array_merge_safe($cols1, $cols2);
		
		$curPower = $sheet->getCell($cols['nominal_power'].$i)->getValue();	//onomastiki isxys
		$curCell = $sheet->getCell($cols['execution_date'].$i);	//symvasi pwlisis
		
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$value = $curPower;
		}
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function Pli8osEnergopoiisis(& $sheet, $i, $return=false) {	//10
		$value = 0;
		
		$cols = ProcedureStep::getPerioxiCols('Activation');
		
		$curCell = $sheet->getCell($cols['execution_date'].$i);	//energopoihsh
		
		if( !CHtmlEx::isEmpty($curCell->getValue()) ) {
			$value = 1;
		}
		
		if($return) {
			return $value;
		}
		$this->value += $value;
	}
	
	private function nullFunc(& $sheet, $i, $return=false) {}
	
}