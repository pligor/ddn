<?php
class ActiveRecord extends CActiveRecord {
	
	/**
	 * Note: no problem to alter a table which does not have an auto increment column, nothing happens
	 * @param string $tableName
	 * @throws Exception
	 */
	public static function resetAutoIncrement($tableName) {
		
		if( !in_array($tableName, Yii::app()->db->getSchema()->tableNames) ) {
			$message = Yii::t('', 'You are trying to alter a table which does not belongs inside the database');
			throw new Exception($message);
		}
		
		$based = 1;
		$sql = 'ALTER TABLE '. $tableName .' AUTO_INCREMENT = :based ;';
		
		$command = Yii::app()->db->createCommand($sql);
		$command->bindValue(':based', $based);
		$command->execute();
	}
	
	public static function getTableName($model) {
		return Yii::app()->db->getSchema()->getTable( $model->tableName() )->name;
	}
}