<?php

/**
 * This is the model class for table "diatiposi_oron".
 *
 * The followings are the available columns in table 'diatiposi_oron':
 * @property integer $procedure_step_id
 * @property integer $arxiki_aitisi_procedure_step_id
 * @property string $protocol_diatiposis_oron
 * @property string $liksi_oron
 *
 * The followings are the available model relations:
 * @property ProcedureStep $procedureStep
 * @property ArxikiAitisi $arxikiAitisiProcedureStep
 * @property SimvasiSindesis[] $simvasiSindesises
 */
class DiatiposiOron extends CActiveRecord {
	public static function getPerioxiCols($step='DiatiposiOron') {
		if($step != 'DiatiposiOron') return array();
		
		return array(
			'protocol_diatiposis_oron' => 'L',
			'liksi_oron' => 'AD',
		);
	}
	
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => '............',
		    //'showErrorSummary' => true,
		    'elements' => array(
				'protocol_diatiposis_oron' => array(
					'type' => 'text',
					//'hint' => '',
					'attributes' => array(
						'size' => 10,
						'maxlength' => 10,
					)
		        ),
		        'liksi_oron' => array(
					'type' => 'zii.widgets.jui.CJuiDatePicker',
					//'hint' => '',
					'attributes' => array(
						'options' => array(
						 	'showAnim' => 'fold',
							'dateFormat' => 'd/m/yy',
							//'defaultDate' => date('j/n/Y'),
							//'changeYear' => false,
							//'changeMonth' => true,
							//'yearRange' => '1900',
						),
					),
		        ),
			),
		);
	}
	
	
	public static function getTitle() {
		return 'Διατύπωση Όρων';
	}
	
	/**
	 * overriding init function to give an initial (invalid format) value to the execution date
	 */
	public function init() {
		$this->liksi_oron = date('j/n/Y');	//THIS MUST BE FIXED LATER with the more generic approach
	}
	
	public function normalizeAttributes() {	//BE AWARE: you set the correct scenario before using this method
		$attrNames = $this->safeAttributeNames;
		foreach($attrNames as $attrName) {
			$method = 'normalize'. ucfirst($attrName);
			if( method_exists($this, $method) ) {
				$this->$method();
			}
		}
	}
	
	public function normalizeProtocol_diatiposis_oron($return=false) {
		$protocol_diatiposis_oron = $this->protocol_diatiposis_oron;
		
		$pattern = '@[0-9]+@';
		$subject = $protocol_diatiposis_oron;
		$matches = array();
		$noMatches = preg_match($pattern, $subject, $matches);
		if($noMatches==0) {
			$message = Yii::t('', 'Protocol Number Διατύπωσης Όρων could not be found');
			throw new Exception($message);
		}
		
		$protocol_diatiposis_oron = $matches[0];
		
		if($return) {
			return $protocol_diatiposis_oron;
		}
		
		$this->protocol_diatiposis_oron = $protocol_diatiposis_oron;
	}
	
	/**
	 * @param bool $return
	 */
	public function normalizeLiksi_oron($return=false) {
		if( CHtmlEx::isUnsignedInteger($this->liksi_oron) ) {	//WARNING: the conversion of timestamp is done inside validator
			if($return) {
				return $this->liksi_oron;
			}
			return;
		}
		
		$liksi_oron = $this->liksi_oron;
		
		$liksi_oron = CHtmlEx::extractDate($liksi_oron);
		if($liksi_oron===false) {
			$message = Yii::t('', 'Date End of Terms has invalid format');
			throw new Exception($message);
		}
		
		$liksi_oron = CHtmlEx::convDate2stamp($liksi_oron);
		
		if($return) {
			return $liksi_oron;
		}
		
		$this->liksi_oron = $liksi_oron;
	}
	
	/**
	 * @see CActiveRecord::save()
	 */
	public function save($validation=true,$attributes=null) {
		if( $validation && !$this->validate() ) {
			return false;
		}
		
		$this->normalizeAttributes();
		return parent::save(false);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return DiatiposiOron the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{diatiposi_oron}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('procedure_step_id, arxiki_aitisi_procedure_step_id, protocol_diatiposis_oron, liksi_oron', 'required'),
			array('procedure_step_id, arxiki_aitisi_procedure_step_id', 'numerical', 'integerOnly'=>true),
			array(
				'protocol_diatiposis_oron',
				'numerical',
				'integerOnly'=>true,
			),
			
			//array('liksi_oron', 'length', 'max'=>20),
			array		//edw argotera tha prepei na orisoume mia nea synartisi elegxou h opoia me to mktime tha mporei poly eukola na elegxei thn orthotita ths hmerominias
			(
				'liksi_oron',
				'match',
				'pattern' => "@^[0-9]{1,2}[/][0-9]{1,2}[/][0-9]{4}$@",
				'message' => Yii::t('', 'Format date') .' '. Yii::app()->params['adminBirthday'],
				'allowEmpty' => false,
			),
			// The following rule is used by search(). Please remove those attributes that should not be searched.
			array('procedure_step_id, arxiki_aitisi_procedure_step_id, protocol_diatiposis_oron, liksi_oron', 'safe', 'on'=>'search'),
			
			array('protocol_diatiposis_oron, liksi_oron', 'safe', 'on'=>'excel'),
			array('procedure_step_id, arxiki_aitisi_procedure_step_id', 'unsafe', 'on'=>'excel'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'procedureStep' => array(self::BELONGS_TO, 'ProcedureStep', 'procedure_step_id'),
			'arxikiAitisiProcedureStep' => array(self::BELONGS_TO, 'ArxikiAitisi', 'arxiki_aitisi_procedure_step_id'),
			'simvasiSindesises' => array(self::HAS_MANY, 'SimvasiSindesis', 'diatiposi_oron_procedure_step_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'protocol_diatiposis_oron' => Yii::t('', 'Protocol Number') .' Διατύπωσης Όρων',
			'liksi_oron' => Yii::t('', 'Date End of Terms'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('procedure_step_id',$this->procedure_step_id);
		$criteria->compare('arxiki_aitisi_procedure_step_id',$this->arxiki_aitisi_procedure_step_id);
		$criteria->compare('protocol_diatiposis_oron',$this->protocol_diatiposis_oron,true);
		$criteria->compare('liksi_oron',$this->liksi_oron,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}