<?php

/**
 * This is the model class for table "pv_xt".
 *
 * The followings are the available columns in table 'pv_xt':
 * @property integer $pv_plant_id
 * @property integer $perioxi_dei_id
 * @property integer $protocol
 * @property string $eksairesi_rae
 * @property integer $island_id
 *
 * The followings are the available model relations:
 * @property PvPlant $pvPlant
 */
class PvXt extends CActiveRecord {
	const col = 'B';
	
	public static function getPerioxiCols($step) {
		if($step != 'DiatiposiOron') return array();
		
		return array(
			'eksairesi_rae' => 'Z',
		);
	}
	
	public static function getPowerRange() {
		return array(
			'min' => 11,	//kW
			'max' => 100,	//kW
		);
	}
	
	public static function getRaePatterns() {
		return array(	//BE AWARE: the ORDER of the patterns here MATTERS a lot
			'cur' => '@([0-9]+)[^0-9]*[/][^0-9]*([0-9]{4})@',	// 5316527/6377
			'old' => '@[^0-9]-([0-9]+)@',						// O-11654727247478
		);
	}
	
	/**
	 * @return string the description of this class
	 */
	public static function getDescr() {
		return "Φ/Β ΧΤ (Χαμηλής Τάσης)";
	}
	
	public function normalizeAttributes() {	//BE AWARE: you set the correct scenario before using this method
		$attrNames = $this->safeAttributeNames;
		foreach($attrNames as $attrName) {
			$method = 'normalize'. ucfirst($attrName);
			if( method_exists($this, $method) ) {
				$this->$method();
			}
		}
	}
	
	/**
	 * @param bool $return
	 */
	public function normalizeEksairesi_rae($return=false) {
		$eksairesi_rae = $this->eksairesi_rae;
		
		if(CHtmlEx::isEmpty($eksairesi_rae)) {
			$this->eksairesi_rae = CHtmlEx::invalidStr;
			if($return) {
				return $this->eksairesi_rae;
			}
			return;
		}
		
		$eksairesi_rae = CHtmlEx::parseRaeNum($eksairesi_rae);
		
		if($return) {
			return $eksairesi_rae;
		}
		else {
			$this->eksairesi_rae = $eksairesi_rae;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::save()
	 */
	public function save($validation=true,$attributes=null) {
		
		$this->normalizeAttributes();
		
		return parent::save($validation);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return PvXt the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pv_xt}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pv_plant_id, perioxi_dei_id, protocol', 'required'),
			array('pv_plant_id, perioxi_dei_id, protocol', 'numerical', 'integerOnly'=>true),
			array('eksairesi_rae', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pv_plant_id, perioxi_dei_id, protocol, eksairesi_rae', 'safe', 'on'=>'search'),
			
			array('eksairesi_rae', 'safe', 'on'=>'excel'),
			array('pv_plant_id, perioxi_dei_id, protocol', 'unsafe', 'on'=>'excel'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pvPlant' => array(self::BELONGS_TO, 'PvPlant', 'pv_plant_id'),
			//'perioxiDei' => array(self::BELONGS_TO, 'PerioxiDei', 'perioxi_dei_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'protocol' => 'Αριθμός Πρωτοκόλλου XT',
			'eksairesi_rae' => Yii::t('', 'ΡΑΕ Exclusion'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pv_plant_id',$this->pv_plant_id);
		$criteria->compare('perioxi_dei_id',$this->perioxi_dei_id);
		$criteria->compare('protocol',$this->protocol);
		$criteria->compare('eksairesi_rae',$this->eksairesi_rae,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}