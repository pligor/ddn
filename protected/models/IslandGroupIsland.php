<?php

/**
 * This is the model class for table "island_group_island".
 *
 * The followings are the available columns in table 'island_group_island':
 * @property integer $island_group_id
 * @property integer $island_id
 */
class IslandGroupIsland extends CActiveRecord
{
	public static function loadDefaultData() {
		$class = get_class();
		$model = new $class();
		$model->deleteAll();	//delete ALL rows
		
		$csvPath = Yii::getPathOfAlias('application.data.'.ActiveRecord::getTableName($model)) .'.csv';
		
		$twoDarray = CHtmlEx::csv2array($csvPath, ';');
		
		$allInserted = true;
		foreach($twoDarray as $rowArray) {
			$model = new $class();
			$model->island_group_id = $rowArray[0];
			$model->island_id = $rowArray[1];
			$allInserted = $allInserted && $model->insert();
		}
		
		return $allInserted;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return IslandGroupIsland the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{island_group_island}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('island_group_id, island_id', 'required'),
			array('island_group_id, island_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('island_group_id, island_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'island_group_id' => 'Island Group',
			'island_id' => 'Island',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('island_group_id',$this->island_group_id);
		$criteria->compare('island_id',$this->island_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}