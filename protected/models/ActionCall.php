<?php

/**
 * This is the model class for table "action_call".
 *
 * The followings are the available columns in table 'action_call':
 * @property integer $id
 * @property string $caller
 * @property string $sessionID
 * @property string $tabID
 * 
 * @property boolean $completed
 *
 * The followings are the available model relations:
 * @property CallData[] $callDatas
 * 
 * The followings are from getter functions:
 * @property array $answers
 */
class ActionCall extends CActiveRecord {
	const defaultCaller = 'site/index';
	
	/**
	 * @see CActiveRecord::init()
	 */
	public function init() {
		parent::init();
		$session = new CHttpSession();
		$this->sessionID = $session->sessionID;
		/*
		$session->open();
		$key = 'dynamic_redirection';
		if( !isset($session[$key]) ) {
			$session[$key] = 'dummy';
			self::model()->deleteAll();
		}
		$session->close();
		//*/
	}
	
	/**
	 * 
	 * @return ActionCall[]
	 */
	public function getByCaller() {
		$attributes = array(
			'caller' => $this->caller,
		);
		return self::model()->findAllByAttributes($attributes); 
	}
	
	/**
	 * @return boolean
	 */
	public function getExists() {
		$models = $this->getByCaller();
		return !empty($models);
	}
	
	public function isRelated($calls) {
		$callDatas = $this->callDatas;
		if( count($callDatas) != count($calls) ) {
			return false;
		}
		
		$isRelated = true;
		foreach($callDatas as $callData) {
			$isRelated = $isRelated && $callData->isFoundInCalls($calls);
		}
		return $isRelated;
	}
	
	public function getAllRelated($calls) {
		$related = array();
		
		$models = $this->getByCaller();
		
		foreach($models as $model) {
			
			if( $model->isRelated($calls) ) {
				$related[] = $model;	
			}
		}
		
		return $related;
	}
	
	/**
	 * Find the first call which is NOT completed, null if all calls, in question, are completed
	 * @return ActionCall|null
	 */
	public static function getFirstIncomplete($models=null) {
		if($models===null) {
			$models = self::model()->findAll();
		}

		foreach($models as $model) {
			if($model->completed == false) {
				return $model;
			}
		}
		
		return null;
	}
	
	/**
	 * TESTED
	 * @return boolean
	 */
	public function getAllAnswered() {
		if( CHtmlEx::isEmpty($this->callDatas) ) {
			return false;
		}
		
		$allAnswered = true;
		foreach($this->callDatas as $callData) {
			$allAnswered = $allAnswered && $callData->answered;
		}
		return $allAnswered;
	}
	
	/**
	 * TESTED
	 * @return array
	 */
	public function getAnswers() {
		if( CHtmlEx::isEmpty($this->callDatas) ) {
			return array();
		}
		
		$answers = array();
		foreach($this->callDatas as $callData) {
			$answers[$callData->data_name] = $callData->blob;
		}
		return $answers;
	}

	public static function call($controller, $calls) {
		if($controller->action === null) {
			$message = Yii::t('', 'You tried to create a call from a controller with no active action');
			throw new Exception($message);
		}
		
		if( !is_array( reset($calls) ) ) {	//if it is NOT two-dimensional array, make it one
			$calls = array($calls);
		}
		
		$route = $controller->route;
		
		$model = new ActionCall();
		$model->caller = $route;
		
		$models = $model->getAllRelated($calls);
		unset($model);
		$model = self::getFirstIncomplete($models);			//DO WE REALLY NEED COMPLETED COLUMN ?
		
		if($model===null) { //If it doesn't exist or if all others are completed we get null in return
			$model = new ActionCall();
			$model->caller = $route;
			$model->createNewCall($calls);
		}
		elseif($model->allAnswered) { //RETURN DATA	
			$attr = 'completed';
			$model->$attr = true;
			$attributes = array($attr);
			if( !$model->update($attributes) ) {
				$message = Yii::t('', 'Updating ActionCall table has failed');
				throw new Exception($message);
			}
			$answers = $model->answers;
			
			$model->delete();
			
			return $answers;
		}
		
		if($model->completed == true) {
			$message = Yii::t('', 'by now the action call model must have been an incomplete one');
			throw new Exception($message);
		}
		
		//PERFORM A CALL
		$callData = $model->firstUnanswered;
		
		if($callData === null) {
			$message = Yii::t('', 'if all are answered then we should have returned the data');
			throw new Exception($message);
		}
		
		$controller->redirect(array(
			$callData->callee,
		));
	}
	
	public function getFirstUnanswered() {
		if($this->isNewRecord) {
			$message = Yii::t('', 'please do not call to find firstUnanswered method for a new record');
			throw new Exception($message);
		}
		elseif( CHtmlEx::isEmpty($this->callDatas) ) {
			$message = Yii::t('', 'please do not call to find firstUnanswered method for an action call model with NO call data at all!');
			throw new Exception($message);
		}
		
		foreach($this->callDatas as $callData) {
			if($callData->answered == false) {
				return $callData;
			}
		}
		return null;
	} 
	
	public function createNewCall($calls) {
		$this->completed = false;
		
		$transaction = Yii::app()->db->beginTransaction();
		try {
			$this->save(false);
			
			while( ( $callData=array_shift($calls) ) !== null ) {
				$model = new CallData();
				$model->action_call_id = $this->id;
				$model->answered = false;
				$model->attributes = $callData;
				$model->save(false);
			}
//throw new Exception();/////////////////////////////////////////////////////////////////////////////////////////
			$transaction->commit();
		}
		catch(Exception $e) {
			$transaction->rollBack();
			//$message = Yii::t('', 'Failed saving a new call');
			//throw new Exception($message);
		}
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return ActionCall the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{action_call}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('caller', 'required'),
			array('caller', 'length', 'max'=>255),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		return array(
			'callDatas' => array(self::HAS_MANY, 'CallData', 'action_call_id'),
		);
	}
	
	/**
         * REMEMBER default scope is applied also in relations
	 * Only records with the current sessionID will be considered
	 * @see CActiveRecord::defaultScope()
	 */
	public function defaultScope() {
		$session = new CHttpSession();
		return array(
			'condition' => "sessionID = '". $session->sessionID ."'",
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('caller',$this->caller,true);
		return new CActiveDataProvider($this, compact('criteria'));
	}
}