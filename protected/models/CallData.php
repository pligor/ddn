<?php

/**
 * This is the model class for table "call_data".
 *
 * The followings are the available columns in table 'call_data':
 * @property integer $action_call_id
 * @property string $callee
 * @property string $data_name
 * @property string $data
 * @property integer $answered
 *
 * The followings are the available model relations:
 * @property ActionCall $actionCall
 */
class CallData extends CActiveRecord {
	/**
	 * @see CActiveRecord::init()
	 */
	public function init() {
            parent::init();
	}
	
	/**
         * Get the first call that you find using this route for the current session
	 * @param string $route 
	 * @throws Exception
	 * @return CallData
	 */
	public static function getCall($route) {
            $attributes = array(
                'callee' => $route,
                'answered' => false,
            );
            $models = CallData::model()->findAllByAttributes($attributes);
            foreach($models as $model) {
                if( !is_null($model->actionCall) ) {   //this is because of default scope of action call
                    return $model;
                }
            }
            return null;
	}
	
	/**
	 * @return string
	 */
	public function answerCall() {
                $attributes = array();
                
		$attr = 'answered';
		$this->$attr = true;
		$attributes[] = $attr;
		
		$attr = 'data';
		$attributes[] = $attr;
		
		$transaction = Yii::app()->db->beginTransaction();
		try {
			$this->update($attributes);
			$this->actionCall = CHtmlEx::loadModel('ActionCall', $this->action_call_id);
			$caller = $this->actionCall->caller;
			$transaction->commit();
		}
		catch(Exception $e) {
		    $transaction->rollBack();
		    $caller = ActionCall::defaultCaller;
		}

		return $caller;
	}
	
	//WE SHOULD CHANGE THESE FUNCTION, BY SETTING __get and __set overrided functions	
	public function setBlob($value) {
		$this->data = serialize($value);
	}
	public function getBlob() {
		return unserialize($this->data);
	}
	
	public function isFoundInCalls($calls) {
		foreach($calls as $call) {
			if( ($this->callee == $call['callee']) && ($this->data_name == $call['data_name']) ) {
				return true;
			}
			//$var=$call;CHtmlEx::out($var);
		}
		return false;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return CallData the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{call_data}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('action_call_id, callee, data_name, answered', 'required'),
			
			//array('action_call_id, answered', 'numerical', 'integerOnly'=>true),
			array('answered', 'in', 'range'=>array(true,false)),
			
			array('callee, data_name', 'length', 'max'=>255),
			array('data', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('action_call_id, callee, data_name, data, answered', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
	//NOTE:you may need to adjust the relation name and the related class name for the relations automatically generated below
		return array(
			'actionCall' => array(self::BELONGS_TO, 'ActionCall', 'action_call_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			/*'action_call_id' => 'Action Call',
			'callee' => 'Callee',
			'data_name' => 'Data Name',
			'data' => 'Data',
			'answered' => 'Answered',*/
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('action_call_id',$this->action_call_id);
		$criteria->compare('callee',$this->callee,true);
		$criteria->compare('data_name',$this->data_name,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('answered',$this->answered);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}