<?php

/**
 * This is the model class for table "tracker".
 *
 * The followings are the available columns in table 'tracker':
 * @property integer $pv_enabled_pv_plant_id
 *
 * The followings are the available model relations:
 * @property PvEnabled $pvEnabledPvPlant
 */
class Tracker extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Tracker the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tracker}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pv_enabled_pv_plant_id', 'required'),
			array('pv_enabled_pv_plant_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pv_enabled_pv_plant_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pvEnabledPvPlant' => array(self::BELONGS_TO, 'PvEnabled', 'pv_enabled_pv_plant_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'pv_enabled_pv_plant_id' => 'Pv Enabled Pv Plant',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pv_enabled_pv_plant_id',$this->pv_enabled_pv_plant_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}