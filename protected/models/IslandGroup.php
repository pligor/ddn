<?php

/**
 * This is the model class for table "island_group".
 *
 * The followings are the available columns in table 'island_group':
 * @property integer $id
 * @property string $island_group_name
 *
 * The followings are the available model relations:
 * @property Island[] $islands
 */
class IslandGroup extends CActiveRecord
{
	public static function loadDefaultData() {
		$class = get_class();
		$model = new $class();
		$model->deleteAll();	//delete ALL rows
		
		$csvPath = Yii::getPathOfAlias('application.data.'.ActiveRecord::getTableName($model)) .'.csv';
		
		$twoDarray = CHtmlEx::csv2array($csvPath, ';');
		
		$allInserted = true;
		foreach($twoDarray as $rowArray) {
			$model = new $class();
			$model->id = $rowArray[0];
			$model->island_group_name = $rowArray[1];
			$allInserted = $allInserted && $model->insert();
		}
		
		return $allInserted;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return IslandGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{island_group}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('island_group_name', 'required'),
			array('island_group_name', 'length', 'max'=>100),
			//The following rule is used by search().Please remove those attributes that should not be searched
			array('id, island_group_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'islands' => array(self::MANY_MANY, 'Island', 'island_group_island(island_group_id, island_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'island_group_name' => 'Island Group Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('island_group_name',$this->island_group_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}