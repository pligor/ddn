<?php

/**
 * This is the model class for table "activation".
 *
 * The followings are the available columns in table 'activation':
 * @property integer $procedure_step_id
 * @property integer $simvasi_polisis_procedure_step_id
 *
 * The followings are the available model relations:
 * @property ProcedureStep $procedureStep
 * @property SimvasiPolisis $simvasiPolisisProcedureStep
 * @property PvEnabled[] $pvEnableds
 */
class Activation extends CActiveRecord
{
	public static function getPerioxiCols($step) {
		return array();
	}
	
	public static function getTitle() {
		return 'Ενεργοποίηση';
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Activation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{activation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('procedure_step_id, simvasi_polisis_procedure_step_id', 'required'),
			array('procedure_step_id, simvasi_polisis_procedure_step_id', 'numerical', 'integerOnly'=>true),
			
			array('procedure_step_id, simvasi_polisis_procedure_step_id', 'safe', 'on'=>'search'), //rule is used by search(). Please remove those attributes that should not be searched
			
			//no safe attributes for excel scenario
			array('procedure_step_id, simvasi_polisis_procedure_step_id', 'unsafe', 'on'=>'excel'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related class name for the relations automatically generated below.
		return array(
			'procedureStep' => array(self::BELONGS_TO, 'ProcedureStep', 'procedure_step_id'),
			'simvasiPolisisProcedureStep' => array(self::BELONGS_TO, 'SimvasiPolisis', 'simvasi_polisis_procedure_step_id'),
			'pvEnableds' => array(self::HAS_MANY, 'PvEnabled', 'activation_procedure_step_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'procedure_step_id' => 'Procedure Step',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('procedure_step_id',$this->procedure_step_id);
		$criteria->compare('simvasi_polisis_procedure_step_id',$this->simvasi_polisis_procedure_step_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}