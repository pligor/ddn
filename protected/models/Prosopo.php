<?php

/**
 * This is the model class for table "prosopo".
 *
 * The followings are the available columns in table 'prosopo':
 * @property integer $id
 * @property string $eponimia
 * @property integer $eidos_paragogou_id
 *
 * The followings are the available model relations:
 * @property EidosParagogou $eidosParagogou
 * @property PvPlant[] $pvPlants
 */
class Prosopo extends CActiveRecord
{
	public static function getPerioxiCols($step) {
		if($step != 'ArxikiAitisi') return array();
		
		return array(
			'eponimia' => 'E',
			'eidos_paragogou_id' => 'H',
		);
	}
	
	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to define a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				get_class() => $this->getFormConfig(),
			),
			'buttons'=>array(
		        'submit' => array(
		            'type'=>'submit',
		            'label' => Yii::t('', 'OK'),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		$elements = array(
	        'eponimia' => array(
				'type' => 'text',
				//'hint' => '........',
				'attributes' => array(
					'size' => 60,
					'maxlength' => 250,
				),
	        ),
	        'eidos_paragogou_id' => array(
	        	'type' => 'dropdownlist',
	        	'items' => EidosParagogou::getEidi(),
	        ),
	    );
		
		return array(
			'type' => 'form',
		    //'title' => '......',
		    //'showErrorSummary' => true,
		    'elements' => $elements,
		);
	}
	
	/**
	 * Fix eponimia to only have Capital non toned letters with single spaced words
	 * Also trimmed from pre and post whitespace
	 * @param bool $return
	 */
	public function normalizeEponimia($return=false) {
		$eponimia = $this->eponimia;
		$eponimia = CHtmlEx::noRedundantSpaces($eponimia);
		$eponimia = CHtmlEx::noTones($eponimia);
		$eponimia = mb_strtoupper($eponimia, Yii::app()->charset);
		if($return) return $eponimia;
		else $this->eponimia = $eponimia;
	}
	
	/**
	 * Here we have a convention that if we cannot find eidos paragogou then we select the first one
	 * @param bool $return
	 */
	public function normalizeEidos_paragogou_id($return=false) {
		if( CHtmlEx::isUnsignedInteger($this->eidos_paragogou_id) ) {	//do nothing special
			if(!$return) return;
			return $this->eidos_paragogou_id;
		}
		
		//supposing eidos_paragogou_id is a string
		$vector = EidosParagogou::getEidi();
		$word = $this->eidos_paragogou_id;
		
		$word = CHtmlEx::noRedundantSpaces($word);
		$word = CHtmlEx::noTones($word);
		$word = mb_strtoupper($word, Yii::app()->charset); 
		
		if( CHtmlEx::isEmpty($word) ) {
			$eidos = reset($vector);
		}
		else {
			$breathModel = new Breathalyzer();
			$breathModel->breathem($vector, $word);
			$eidos = $breathModel->finalword;	
		}
		
		$attributes = array(
			'eidos' => $eidos,
		);
		$model = EidosParagogou::model()->findByAttributes($attributes);
		$eidos_paragogou_id = $model->id;
		
		if($return)
			return $eidos_paragogou_id;
		else
			$this->eidos_paragogou_id = $eidos_paragogou_id;
	}
	
	public function normalizeAttributes() {	//BE AWARE: you set the correct scenario before using this method
		$attrNames = $this->safeAttributeNames;
		foreach($attrNames as $attrName) {
			$method = 'normalize'. ucfirst($attrName);
			if( method_exists($this, $method) ) {
				$this->$method();
			}
		}
	}
	
	/**
	 * overriding save function
	 */
	public function save($validation=true,$attributes=null) {
		$this->normalizeAttributes();
		return parent::save($validation);	//true means perform validation
	}
	
	public static function getProsopa($island_id, $pvCatArray=null)
	{
		if($pvCatArray==null) $classes = PvPlant::getCats();
		else $classes = PvPlant::getCats($pvCatArray);
		
		$textField = 'id';
		$criteria = new CDbCriteria;
		$criteria->select = array($textField);
		$criteria->with = array(
			'pvXt', // => array('condition' => "pvXt.pv_plant_id IS NOT NULL"),
			'pvMt', // => array('condition' => "pvMt.pv_plant_id IS NOT NULL"),
			'pvSteges'// => array('condition' => "pvSteges.pv_plant_id IS NOT NULL"),
		);
		
		$conditions = array();
		foreach($classes as $class)
		{
			$relation = lcfirst($class);
			$conditions[] = "$relation.pv_plant_id IS NOT NULL";
		}

		$criteria->addCondition($conditions, 'OR');
		
		$criteria->addCondition("island_id=$island_id");
		
		$models = PvPlant::model()->findAll($criteria);
		
		$pv_plant_ids = array();
		foreach($models as $model)
		{
			$pv_plant_ids[] = $model->$textField;
		}
		
		$textField = 'eponimia';
		$criteria = new CDbCriteria;
		$criteria->select = array($textField);
		$criteria->with = array(
			'pvPlants' => array('condition' => "island_id=$island_id"),
		);
		$criteria->addInCondition('pvPlants.id', $pv_plant_ids);
		
		$models = self::model()->findAll($criteria);
		
		$eponimies = array();
		foreach($models as $model)
		{
			$eponimies[] = $model->$textField;
		}
		
		return $eponimies;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Prosopo the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{prosopo}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('eidos_paragogou_id', 'required'),

			array('eponimia', 'required'),
//AUTO EIXE DEFAULT ALLA EPEIDI EXEI UNIQUE THA PREPEI NA EINAI REQUIRED (EMAIL STOUS Yii DEVELOPERS GIAUTO)
			
			array('eidos_paragogou_id', 'numerical', 'integerOnly'=>true),
			array('eponimia', 'length', 'max'=>250),

			//The following rule is used by search(). Please remove those attributes that should not be searched.
			array('id, eponimia, eidos_paragogou_id', 'safe', 'on'=>'search'),
			
			array('eponimia, eidos_paragogou_id', 'safe', 'on'=>'excel'),
			array('id', 'unsafe', 'on'=>'excel'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'eidosParagogou' => array(self::BELONGS_TO, 'EidosParagogou', 'eidos_paragogou_id'),
			'pvPlants' => array(self::HAS_MANY, 'PvPlant', 'prosopo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'eponimia' => Yii::t('', 'Name of Individual / Legal Entity'),
			'eidos_paragogou_id' => Yii::t('', 'Producer Kind'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('eponimia',$this->eponimia,true);
		$criteria->compare('eidos_paragogou_id',$this->eidos_paragogou_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}