<?php

/**
 * DDNxl class
 * @property array $phpExcels
 */
class DDNxl extends CModel {
	const format = 'Excel5';
	const revisionCell = 'E4';
	
	public $startRow = 1;
	private $dataStartRow = 8;	//row where real data start
	public $endRow = null;	//null means all the rows
	
	public $columns;
	public $dataColumns;
	
	protected $copyright;
	
	private $_filepaths;
	private $_config;
	
	private $filterSubset;
	
	private $titleRows = array();
	private $scale = 34; //Print scaling. Valid values range from 10 to 400
	
	/**
	 * multiple files for stats
	 * @var PHPExcel[]
	 */
	public $phpExcels = array();
	private $sheets = array();
	private $fields = array();
	
	/**
	 * @var PHPExcel
	 */
	public $phpExcel;
	
	/**
	 * @var string[]
	 */
	protected $models;
	
	/**
	 * @var string
	 */
	public $filepath;
	
	/**
	 * @var integer
	 */
	public $perioxi_dei_id;
	
	/**
	 * just calls init method
	 */
	public function __construct() {
		$this->init();
	}
	
	public function init() {
		$this->columns = CHtmlEx::getColumns('A', 'AR');
		$this->dataColumns = CHtmlEx::getColumns('B', 'AC');
		
		$this->titleRows = array(6,7);
		
		$this->models = array(	//order matters!
			'ArxikiAitisi',
			'DiatiposiOron',
			'SimvasiSindesis',
			'SimvasiPolisis',
			'Activation',
			'Comment',
			'Killed',
		);
		
		$this->copyright = Yii::t('', 'Automatic Statistics Generation') .' - © '. date('Y') .' '. Yii::app()->params['adminGreekName'];
		
		$this->onlyData(false);
	}
	
	/**
	 * swap between data or all (rows and columns)
	 * @param boolean $only
	 */
	public function onlyData($only=true) {
		if($only) {
			$startRow = $this->dataStartRow;
			$columns = $this->dataColumns;
		}
		else {
			$startRow = $this->startRow;
			$columns = $this->columns;
		}
		
		CHtmlEx::loadPHPExcel();
		$this->filterSubset = new ExcelReadFilter($startRow, $this->endRow, $columns);
		CHtmlEx::unLoadPHPExcel();
	}
	
	/**
	 * Enter description here ...
	 */
	public function loadExcel() {
		$listDataIslands = CHtmlEx::loadModel('PerioxiDei', $this->perioxi_dei_id)->listDataIslands;
		$objReader = PHPExcelHelper::getObjReader($listDataIslands, $this->filterSubset);
		
		$this->phpExcel = $objReader->load($this->filepath);
		$this->phpExcel->perioxi_dei_id = $this->perioxi_dei_id;
	}
	
	/**
	 * Load the entire area inside db
	 * @return array the report with all area's problems
	 */
	public function loadArea() {
		$reportPerioxi = array();
		
		foreach($this->phpExcel->getAllSheets() as $sheet) {
			$island_name = $sheet->getTitle();
			$attributes = array(
				'island_name' => $island_name,	
			);
			$sheet->island_id = Island::model()->findByAttributes($attributes)->id;
			
			CHtmlEx::loadPHPExcel();
			$sheet->revisionStamp = PHPExcel_Shared_Date::ExcelToPHP( $sheet->getCell(self::revisionCell)->getValue() );
			CHtmlEx::unLoadPHPExcel();
			
			$reportPerioxi[$sheet->island_id] = $this->loadIsland($island_name);
		}
		
		return $reportPerioxi;
	}
	
	public function loadIsland($island_name) {
		$sheet = $this->phpExcel->getSheetByName($island_name);
		
		$reportSheet = array();
		for($rowIter = $sheet->getRowIterator();$rowIter->valid();$rowIter->next()) {
			$i = $rowIter->key();
			if($i < $this->dataStartRow) {
				continue;	//ignore headers	
			}
			
			$row = $rowIter->current();
			if( WorksheetHelper::isRowEmpty($row,$this->dataColumns) ) {
				break;	//stop on empty row
			}
			
			$reportSheet[$i] = $this->loadRow($sheet, $i);
		}
		
		return $reportSheet;
	}
	
	/**
	 * load row data inside database
	 * @param PHPExcel_Worksheet $sheet
	 * @param integer $i
	 */
	protected function loadRow(& $sheet, $i) {
		$reportRow = array();
		foreach($this->models as $step) {
			$class = $step.'Form';
			$formModel = new $class();
			
			$readExcelResult = $formModel->readExcel($sheet, $i);
			if($readExcelResult === false) {
				break;
			}
			$reportRow[$step] = $readExcelResult;
			
			if( !CHtmlEx::isEmpty( $reportRow[$step]['error'] ) ) {	//having a serious error there is no need to proceed
				break;
			}
		}
		return $reportRow;
	}
	
	public function loadExcels() {
		if( !is_array($this->_filepaths) ) return false;
		
		try {
			foreach($this->_filepaths as $perioxi_dei_id => $path) {
				$class = 'PerioxiDei';
				$id = $perioxi_dei_id;
				$listDataIslands = CHtmlEx::loadModel($class, $id)->listDataIslands;
				
				$objReader = PHPExcelHelper::getObjReader($listDataIslands, $this->filterSubset);

				$this->phpExcels[$perioxi_dei_id] = $objReader->load($path);
			}
		}
		catch(Exception $e) {
			throw new CHttpException(404, $e->getMessage());	//rethrow
		}
	}
	
	/**
	 * verify in each file if the excel contains all the corresponding islands
	 * @param array $paths
	 * @throws Exception
	 */
	public static function verifySheetnames($paths) {
		$objReader = PHPExcelHelper::getObjReader();
		
		$class = 'PerioxiDei';
		$messages = array();
		foreach($paths as $perioxi_dei_id => $path) {
			$model = CHtmlEx::loadModel($class, $perioxi_dei_id);
			$listDataIslands = $model->listDataIslands;
			$sheetNames = $objReader->listWorksheetNames($path);
			
			$array_diff = array_diff($listDataIslands, $sheetNames);
			if( !empty($array_diff) ) {
				$messages[] =
				Yii::t('', "The Area {perioxi_dei_name} does not contain all the islands subject to this Area", array(
					'{perioxi_dei_name}' => $model->perioxi_dei_name,
				));
			}
		}
		
		if( !empty($messages) ) {
			$message = implode("<br/>". Yii::t('', "AND") ."<br/>", $messages);
			$message = ucfirst($message);
			throw new Exception($message);
		}
	}
	
	public function getPvPlantId(& $sheet, $i) {
		$island_id = $sheet->island_id;
		$perioxiDei = CHtmlEx::loadModel('Island', $island_id)->perioxiDei;
		$perioxi_dei_id = $perioxiDei->id;
		
		$pvCat = $this->getPvCat($sheet, $i);
		$protocol = $this->getProtocol($sheet, $i);
		
		$class = PvPlant::getCats($pvCat);
		
		$attributes = array(
			'protocol' => $protocol,
			//'perioxi_dei_id' => $perioxi_dei_id,
                        'island_id' => $island_id,
		);
		$model = $class::model()->findByAttributes($attributes);
		if($model===null) {
			$perioxi_dei_name = $perioxiDei->perioxi_dei_name;
			$message = Yii::t('', 'Could not find pv plant with protocol {protocol} in Area {perioxi_dei_name}',array(
				'{protocol}' => $protocol,
				'{perioxi_dei_name}' => $perioxi_dei_name,
			));
			throw new Exception($message);
		}
		
		return $model->pvPlant->id;
	}
	
	public function setFilepaths($paths) {	//write-only
		$this->_filepaths = $paths;
	}
	
	public function unlinkFilepaths() {
		foreach($this->_filepaths as $perioxi_dei_id => $path) {
			if(is_file($path) && !unlink($path)) {
				$message = Yii::t('', 'Failed to delete original files');
				throw new Exception($message);
			}
		}
	}
	
	public function getConfig() {
		$configs_sheets = self::getConfigs_sheets();
		if(   !isset( $configs_sheets[$this->_config] )   ) return null;
		return $configs_sheets[$this->_config];
	}
	
	public function setConfig($config) {
		$this->_config = $config;
	}
	
	/**
	 * Create Sheets and Set Titles
	 * @return array PHPExcel_Worksheet
	 */
	private function & createSheets($phpExcel) {
		foreach($this->config as $key => $configSheet) {
			$title = $configSheet['title'];
			try {
				$sheet = $phpExcel->getSheet($key);		//if fails throws exception
				$sheet->setTitle( $title );
			}
			catch(Exception $e) {
				$phpExcel->createSheet()->setTitle($title);
			}
		}
		
		$sheets = $phpExcel->getAllSheets();
		return $sheets;
	}
	
	private function & createPhpExcel_out() {
		CHtmlEx::loadPHPExcel();
		$phpExcel_out = new PHPExcel(); //Create new PHPExcel object, with ONE worksheet
		CHtmlEx::unLoadPHPExcel();
		
		$phpExcel_out->getProperties()->setCreator(Yii::app()->params['adminName'])
		->setLastModifiedBy(Yii::app()->params['adminGreekName'])
		->setTitle($this->copyright)
		->setSubject("PV Stats")
		->setDescription('Statistics for DDN by '. Yii::app()->params['adminEmail'])
		->setKeywords("george pligor statistics pv ddn tearth")	//space delimited
		->setCategory("statistics");
		
		return $phpExcel_out;
	}
	
	public function pageSetupAll() {
		foreach($this->phpExcels as & $phpExcel) {
			$this->pageSetupPhpExcel($phpExcel);
		}
	}
	
	public function pageSetupPhpExcel(& $phpExcel) {
		$sheets = $phpExcel->getAllSheets();
		foreach($sheets as & $sheet) {
			$this->pageSetupSheet($sheet);
		}
	}
	
	private function pageSetupSheet(& $sheet) {
		$start = min($this->titleRows);
		$end = max($this->titleRows);
		
		WorksheetHelper::clearPageBreaks($sheet);
		
		$pageSetup = $sheet->getPageSetup();
		
		CHtmlEx::loadPHPExcel();
		$pageSetup->setOrientation( PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE );
		$pageSetup->setPaperSize( PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4 );	//make sure the correct scale is set
		CHtmlEx::unLoadPHPExcel();
		
		$pageSetup->setScale($this->scale);
		
		$pageSetup->setRowsToRepeatAtTopByStartAndEnd($start, $end);
	}
	
	private function initFields($fieldInds) {
		$this->fields = array();
		$configFields = Field::getConfigFields($fieldInds);
		
		reset($fieldInds);
		foreach($configFields as $configField) {
			$field = new Field($configField['method']);
			$field->header = $configField['header'];
			$field->colWidth = $configField['colWidth'];
			$field->value = 0;
			
			$field->ind = current($fieldInds);
			next($fieldInds);
			
			$this->fields[] = $field;
		}
	}
	
	private function setFieldsValue($fieldInds, $value) {
		foreach($this->fields as & $field) {
			if( in_array($field->ind, $fieldInds) ) {
				$field->value = $value;	
			}
		}
	}
	
	public function calcStats() {
		$phpExcel_out = $this->createPhpExcel_out();
		$this->sheets = $this->createSheets($phpExcel_out);
		
		foreach($this->config as $key => $configSheet) {
			$this->initFields( $configSheet['fields'] );
			$this->sheets[$key]->firstDataRow = $this->renderHeaders($this->sheets[$key], $configSheet['fields']);
		}
		
		$attributes = array(
			'island_group_name' => 'ΚΡΗΤΗ',
		);
		$model = IslandGroup::model()->findByAttributes($attributes);
		$valueField = 'id';
		$textField = 'island_name';
		$crete = CHtml::listData($model->islands, $valueField, $textField);
		
		$creteRows = array(array());
		$totalRows = array(array());
		$row=0;
		foreach($this->phpExcels as $perioxi => $phpExcel) {
			$allSheets = $phpExcel->getAllSheets();
			foreach($allSheets as $curSheet) {
				$island_name = $curSheet->getTitle();
				$attributes = array(
					'island_name' => $island_name,
				);
				$model = Island::model()->findByAttributes($attributes);
				$rae_margin = $model->rae_margin;
				
				$revisionStamp = $this->getRevisionStamp($curSheet);
				
				foreach($this->config as $key => $configSheet) {
					$this->initFields( $configSheet['fields'] );
					
					$this->setFieldsValue(array(13,14), $rae_margin);
					
					$this->statIsland($curSheet, $revisionStamp, $configSheet['categs']);
					
					$curRow = $this->sheets[$key]->firstDataRow + $row;
					$this->renderStats($this->sheets[$key], $curRow, $island_name, $revisionStamp);
					
					if( in_array($island_name, $crete) ) {
						$creteRows[$key][] = $curRow;
					}
					$totalRows[$key][] = $curRow;
				}
				
				$row++;
			}
		}
		$row++; //skip a row
		
		$this->renderSum($creteRows, $row, 'ΚΡΗΤΗ');
		$row++;
		$row++; //skip a row
		
		$this->renderSum($totalRows, $row, Yii::t('', 'TOTAL'));
		$row++;

		return $phpExcel_out;
	}
	
	/**
	 * Renders the sum formula for the defined rows. Simply uses =SUM(...) to do the job
	 * @param array integer $dataRows
	 * @param integer render $row	where to render the data
	 * @param string $title	to use as the title for this row
	 */
	private function renderSum($dataRows, $row, $title) {		
		$this->fields = array();	//no need to render any stat data
		
		foreach($this->config as $key => $configSheet) {
			$curRow = $this->sheets[$key]->firstDataRow + $row;
			$colstart = $this->renderStats($this->sheets[$key], $curRow, $title, time());
			
			$rows = isset($dataRows[$key]) ? $dataRows[$key] : array();
			
			$countEnabled = count($configSheet['fields']);
			
			for($col=$colstart;$col<($countEnabled+$colstart);$col++) {
				CHtmlEx::loadPHPExcel();
				$colstr = PHPExcel_Cell::stringFromColumnIndex($col);
				CHtmlEx::unLoadPHPExcel();
				
				//$cellStr = CHtmlEx::sumColumn($colstr, $rows);
				$notation = new Notation();
				$notation->getCompact($rows, $colstr);
				$cellStr = $notation->notation;
				
				$value = empty($cellStr) ? 0 : "=SUM($cellStr)";

				$this->sheets[$key]->setCellValueByColumnAndRow($col, $curRow, $value);
			}
		}
		
	}
	
	 /**
	 * @param PHPExcel_Worksheet $sheet
	 * @param integer $row
	 * @param string $island_name
	 * @param integer $revisionStamp
	 * 
	 * @return integer column, the next available column
	 */
	private function renderStats(& $sheet, $row, $island_name, $revisionStamp) {	
		$col=0;
		$sheet->setCellValueByColumnAndRow($col, $row, $island_name);
		$col++;
		
		$sheet->setCellValueByColumnAndRow($col, $row, date('j/n/Y', $revisionStamp));
		$col++;
		
		foreach($this->fields as $field) {
			$value = $field->value;
			if(empty($value)) $value = '0';
			//$sheet->setCellValueByColumnAndRow($col, $row, $field->value);
			$sheet->setCellValueByColumnAndRow($col, $row, $value);
			$col++;
		}
	
		//set bold for island name
		$sheet->getStyle("A$row")->getFont()->setBold(true);
		
		return $col;
	}
	
	private function statIsland(& $sheet, $untilStamp, $categs) {
		for($rowIter = $sheet->getRowIterator();$rowIter->valid();$rowIter->next()) {
			$i = $rowIter->key();
			if($i < $this->dataStartRow) {
				continue;	//ignore headers	
			}
			
			$cols = Prosopo::getPerioxiCols('ArxikiAitisi');
			$c = $cols['eponimia'];
			$eponimiaCell = $sheet->getCell($c.$i);
			if( CHtmlEx::isEmpty($eponimiaCell->getValue()) ) {
				//print "without eponimia<br/>";
				continue;	//skip rows without eponimia	
			}

			if( $this->datePassed($sheet, $i, $untilStamp) ) {
				//print "date passed at row $i";
				break;	//found a newer date
			}
			
			if(   !in_array($this->getPvCat($sheet, $i), $categs)   ) {
				continue; //skip indifferent rows	
			}
			
			$this->calcRow($sheet, $i);
		}
	}
	
	private function calcRow(& $sheet, $i) {
		foreach($this->fields as & $field) {
			$field->execMethod($sheet, $i);
		}
	}
	
	public function getProtocol(& $sheet, $i) {
		$pvCat = $this->getPvCat($sheet, $i);
		
		$cols = PvPlant::getPerioxiCols('ArxikiAitisi');
		$cols = $cols['protocol'];
		
		$col = $cols[$pvCat];
		
		return $sheet->getCell($col.$i)->getValue();
	}
	
	/**
	 * Enter description here ...
	 * @param PHPExcel_Worksheet $sheet
	 * @param integer $i
	 * @throws Exception
	 * @return string
	 */
	public function getPvCat(& $sheet, $i) {
		$categs = PvPlant::getCats();
		foreach($categs as $key => & $categ) {
			$col = $categ::col;
			$categ = is_numeric( $sheet->getCell($col.$i)->getValue() );
		}
		
		if(array_sum($categs) != 1) {	
			$sheetName = $sheet->getTitle();
			$message = Yii::t('', 'Error with protocol in worksheet {sheetName} in line {line}',array(
				'{sheetName}' => $sheetName,
				'{line}' => $i,
			));
			throw new Exception($message);
		}
		
		foreach($categs as $categKey => $categVal) {
			if( !CHtmlEx::isEmpty($categVal) ) {
				return $categKey;
			}
		}
	}
	
	private function datePassed(& $sheet, $i, $untilStamp) {
		$cols = ProcedureStep::getPerioxiCols('ArxikiAitisi');
		$c = $cols['execution_date'];
		$dateSubmit = $sheet->getCell($c.$i)->getValue();	//hmerominia ypovolis ths arxikis aitisis
		
		if( CHtmlEx::isEmpty($dateSubmit) ) {
			return false;	//keep going, never mind
		}
		
		CHtmlEx::loadPHPExcel();
		$submitStamp = PHPExcel_Shared_Date::ExcelToPHP($dateSubmit);
		CHtmlEx::unLoadPHPExcel();
		
		if($submitStamp > $untilStamp) {
			return true;
		}
		return false;
	}
	
	protected function getRevisionStamp(& $sheet) {
		CHtmlEx::loadPHPExcel();
		$revisionStamp = PHPExcel_Shared_Date::ExcelToPHP( $sheet->getCell(self::revisionCell)->getValue() );
		CHtmlEx::unLoadPHPExcel();
		return $revisionStamp;
	}
	
	/**
	 * @param PHPExcel_Worksheet $sheet
	 */
	private function renderHeaders(& $sheet) {
		$col = 0;
		$row = 1;
		//$sheet->setCellValue("A$row", $sheet->getTitle());
		$sheet->setCellValueByColumnAndRow($col,$row,$sheet->getTitle());
		
		$row++;
		
		$sheet->getColumnDimensionByColumn($col)->setWidth(20);
		$sheet->setCellValueByColumnAndRow($col, $row, 'ΜΗ ΔΙΑΣΥΝΔΕΔΕΜΕΝΑ ΝΗΣΙΑ');
		$sheet->getStyleByColumnAndRow($col, $row)->applyFromArray($this->styles['header']);
		$col++;
		
		$sheet->getColumnDimensionByColumn($col)->setWidth(10);
		$sheet->setCellValueByColumnAndRow($col, $row, Yii::t('', 'UPDATE DATE'));
		$sheet->getStyleByColumnAndRow($col, $row)->applyFromArray($this->styles['header']);
		$col++;

		foreach($this->fields as $field) {
			$sheet->getColumnDimensionByColumn($col)->setWidth( $field->colWidth );
			$sheet->setCellValueByColumnAndRow($col, $row, $field->header);
			$sheet->getStyleByColumnAndRow($col, $row)->applyFromArray($this->styles['header']);
			$col++;
		}
		
		return ++$row;
	}
	
	/**
	 * @param PHPExcel $phpExcel
	 */
	public function renderFooters(& $phpExcel) {
		foreach($phpExcel->getAllSheets() as $sheet) {
			$coord = 'A'.($sheet->getHighestRow() + 1);
			$sheet->setCellValue($coord, $this->copyright);
			$sheet->getStyle($coord)->applyFromArray($this->styles['footer']);
		}
	}
	
	public $cssFile = 'application.widgets.assets.ExcelSheetView.styles';
	protected $_styles;
	public function getStyles() {
		if( $this->_styles === null ) {
			$this->_styles = include Yii::getPathOfAlias($this->cssFile) .'.php';
		}
		return $this->_styles;
	}
	
	
	public function writeExcel($phpExcel, $fullpath) {
		if( is_file($fullpath) && !unlink($fullpath)) {
			$message = Yii::t('', 'Failed to unlink previous stats file');
			throw new Exception($message);
		}
		
		$phpExcel->setActiveSheetIndex(0);	//reset active sheet index just before write
		
		try {
			CHtmlEx::loadPHPExcel();
			$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, self::format);
			//$objWriter = new PHPExcel_Writer_Excel5( $model->phpExcels[$new] );
			CHtmlEx::unLoadPHPExcel();
			
			$objWriter->save($fullpath);
		}
		catch (Exception $e) {
			//...
		}
	}
	
	/**
	 * get all public properties
	 * @see CFormModel::attributeNames()
	 */
	public function attributeNames() {
		//this trick is to change the scope and therefore retrieve ONLY public properties
		$getVars = function($obj) { return get_object_vars($obj); };
		return array_keys( $getVars($this) );
	}
	
	public static function getConfigSheetsDescrs() {
		return array(
			'full_old' => Yii::t('', 'Old Full Statistics'),
			'full' => Yii::t('', 'New Full Statistics'),
			'public' => Yii::t('', 'Statistics for Publication'),
			'public2' => Yii::t('', 'Statistics for Publication') .' '. Yii::t('', 'version') . ' 2',
		);
	}
	
	public static function getConfigs_sheets() {
		return array(
			'full_old' => array(
				0 => array(
					'title' => Yii::t('', 'PV XT PER ISLAND'),
					'fields' => array(0,1,2,3,4,5,6,7,8,9,10,11),
					'categs' => array(0),
				),
				1 => array(
					'title' => Yii::t('', 'PV MT PER ISLAND'),
					'fields' => array(0,1,2,3,4,5,6,7,8,9,10,11),
					'categs' => array(1),
				),
				2 => array(
					'title' => Yii::t('', 'PV ROOFS PER ISLAND'),
					'fields' => array(0,1,2,3,4,5,6,7,8,9,10,11),
					'categs' => array(2),
				),
				3 => array(
					'title' => Yii::t('', 'TOTAL STATISTICS'),
					'fields' => array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14),
					'categs' => array(0,1,2),
				),
			),
		////////////////////////////////////////////////////////////////////////////////////
			'full' => array(
				0 => array(
					'title' => Yii::t('', 'PV XT PER ISLAND'),
					'fields' => array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15),
					'categs' => array(0),
				),
				1 => array(
					'title' => Yii::t('', 'PV MT PER ISLAND'),
					'fields' => array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15),
					'categs' => array(1),
				),
				2 => array(
					'title' => Yii::t('', 'PV XT & MT PER ISLAND'),
					'fields' => array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15),
					'categs' => array(0,1),
				),
				3 => array(
					'title' => Yii::t('', 'PV ROOFS PER ISLAND'),
					'fields' => array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15),
					'categs' => array(2),
				),
				4 => array(
					'title' => Yii::t('', 'TOTAL STATISTICS'),
					'fields' => array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15),
					'categs' => array(0,1,2),
				),
			),
		////////////////////////////////////////////////////////////////////////////////////
			'public' => array(
				0 => array(
					'title' => Yii::t('', 'PV XT & MT PER ISLAND'),
					'fields' => array(13,12,15,14),
					'categs' => array(0,1),
				),
				1 => array(
					'title' => Yii::t('', 'PV ROOFS PER ISLAND'),
					'fields' => array(13,12,15,14),
					'categs' => array(2),
				),
			),
			'public2' => array(
				0 => array(
					'title' => Yii::t('', 'PV XT & MT PER ISLAND'),
					'fields' => array(11,16,15),
					'categs' => array(0,1),
				),
				1 => array(
					'title' => Yii::t('', 'PV ROOFS PER ISLAND'),
					'fields' => array(11,16,15),
					'categs' => array(2),
				),
				2 => array(
					'title' => Yii::t('', 'TOTAL STATISTICS'),
					'fields' => array(11,16,15,12,13,14),
					'categs' => array(0,1,2),
				),
			),
		);
	}
}