<?php

/**
 * This is the model class for table "pv_cat".
 *
 * The followings are the available columns in table 'pv_cat':
 * @property integer $id
 * @property string $table_name
 * @property double $min_power
 * @property double $max_power
 * @property string $end_terms_days
 */
class PvCat extends CActiveRecord {
	
	/**
	 * @see CModel::behaviors()
	 */
	public function behaviors() {
		return array(
			'fixture-behavior' => array(
				'class' => 'FixtureBehavior',
			),
		);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return PvCat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pv_cat}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, table_name, min_power, max_power, end_terms_days', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('min_power, max_power', 'numerical'),
			array('table_name', 'length', 'max'=>45),
			array('end_terms_days', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, table_name, min_power, max_power, end_terms_days', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'table_name' => 'Table Name',
			'min_power' => 'Min Power',
			'max_power' => 'Max Power',
			'end_terms_days' => 'End Terms Days',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('table_name',$this->table_name,true);
		$criteria->compare('min_power',$this->min_power);
		$criteria->compare('max_power',$this->max_power);
		$criteria->compare('end_terms_days',$this->end_terms_days,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}