<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $name
 * @property string $pass
 * @property string $email
 * @property string $cookie_key
 *
 * The followings are the available model relations:
 * @property Island[] $islands
 */
class User extends CActiveRecord {
	const hashAlgo = 'sha512';
	public $pass_repeat;
	
	public static function getMaxId() {
		$attr = self::model()->metaData->tableSchema->primaryKey;
		return max( CHtml::listData(User::model()->findAll(),$attr,$attr) );
	}
	
	/**
	 * get all island ids associated with the user
	 */
	public function getAssociatedIslandIds() {
		$models = $this->islands;
		$valueField = 'id';
		$textField = 'id';
		return CHtml::listData($models, $valueField, $textField);
	}
	
	public function save($runValidation=true,$attributes=null) {
		if($runValidation) {
			$this->validate();
		}
		$this->pass = hash(self::hashAlgo, $this->pass);
		return parent::save(false);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, pass, pass_repeat', 'required'),
			array(
				'name',
				'unique',	//clever enough to check inside correct class
			),
			array(
				'pass',
				'compare',	//pretty clever to compare with another attribute with same prefix
			),
			array('name', 'length', 'max'=>30),
			array('pass', 'length', 'max'=>255),
			array('email', 'length', 'max'=>127),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, pass, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'islands' => array(self::MANY_MANY, 'Island', 'user_island(user_id, island_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Όνομα',
			'pass' => 'Κωδικός',
			'pass_repeat' => 'Επανάληψη κωδικού',
			'email' => 'Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('pass',$this->pass,true);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}
