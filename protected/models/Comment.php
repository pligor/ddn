<?php

/**
 * This is the model class for table "comment".
 *
 * The followings are the available columns in table 'comment':
 * @property integer $id
 * @property integer $pv_plant_id
 * @property string $comment_text
 *
 * The followings are the available model relations:
 * @property PvPlant $pvPlant
 */
class Comment extends CActiveRecord
{
	public static function getPerioxiCols($step) {
		if($step != 'Comment') return array();
		
		return array(
			'comment_text' => 'AA',
		);
	}
	
	public static function getTitle() {
		return 'Παρατηρήσεις';
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Comment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{comment}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pv_plant_id, comment_text', 'required'),
			array('pv_plant_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, pv_plant_id, comment_text', 'safe', 'on'=>'search'),
			
			array('comment_text', 'safe', 'on'=>'excel'),
			array('id, pv_plant_id', 'unsafe', 'on'=>'excel'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pvPlant' => array(self::BELONGS_TO, 'PvPlant', 'pv_plant_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'id' => 'ID',
			//'pv_plant_id' => 'Pv Plant',
			'comment_text' => Yii::t('', 'Comment') .'/'. Yii::t('', 'Remark') .'/'. Yii::t('', 'Notice'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pv_plant_id',$this->pv_plant_id);
		$criteria->compare('comment_text',$this->comment_text,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}