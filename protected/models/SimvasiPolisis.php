<?php

/**
 * This is the model class for table "simvasi_polisis".
 *
 * The followings are the available columns in table 'simvasi_polisis':
 * @property integer $procedure_step_id
 * @property integer $simvasi_sindesis_procedure_step_id
 * @property string conn_compl_date
 *
 * The followings are the available model relations:
 * @property Activation[] $activations
 * @property ProcedureStep $procedureStep
 * @property SimvasiSindesis $simvasiSindesisProcedureStep
 */
class SimvasiPolisis extends CActiveRecord
{
	public static function getPerioxiCols($step) {
		if($step != 'SimvasiPolisis') return array();
		
		return array(
			'conn_compl_date' => 'O',
		);
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => '.............',
		    //'showErrorSummary' => true,
		    'elements' => array(
				'conn_compl_date' => array(
					'type' => 'zii.widgets.jui.CJuiDatePicker',
					//'hint' => '',
					'attributes' => array(
						'options' => array(
						 	'showAnim' => 'fold',
							'dateFormat' => 'd/m/yy',
							//'defaultDate' => date('j/n/Y'),
							//'changeYear' => false,
							//'changeMonth' => true,
							//'yearRange' => '1900',
						),
					),
		        ),
			),
		);
	}
	
	public static function getTitle() {
		return 'Σύμβαση Πώλησης/Συμψηφισμού';
	}
	
	/**
	 * overriding init function to give an initial (invalid format) value to the execution date
	 */
	public function init() {
		$this->conn_compl_date = date('j/n/Y');
	}
	
	public function normalizeAttributes() {	//BE AWARE: you set the correct scenario before using this method
		$attrNames = $this->safeAttributeNames;
		foreach($attrNames as $attrName) {
			$method = 'normalize'. ucfirst($attrName);
			if( method_exists($this, $method) ) {
				$this->$method();
			}
		}
	}
	
	/**
	 * @param bool $return
	 */
	public function normalizeConn_compl_date($return=false) {
		if( CHtmlEx::isEmpty($this->conn_compl_date) ) {
			$this->conn_compl_date = null;
			if($return) {
				return $this->conn_compl_date;
			}
			return;
		}
		
		if( CHtmlEx::isUnsignedInteger($this->conn_compl_date) ) {
			if($return) {
				return $this->conn_compl_date;
			}
			return;
		}
		
		$conn_compl_date = $this->conn_compl_date;
		
		$conn_compl_date = CHtmlEx::extractDate($conn_compl_date);
		if($conn_compl_date===false) {
			$message = Yii::t('', 'The Completed Connection Works Date has invalid format');
			throw new Exception($message);
		}
		
		$conn_compl_date = CHtmlEx::convDate2stamp($conn_compl_date);
		 
		if($return) {
			return $conn_compl_date;
		}
		
		$this->conn_compl_date = $conn_compl_date;
	}
	
	/**
	 * overriding save function to convert execution date to GMT timestamp
	 */
	public function save($validation=true,$attributes=null) {
		if( $validation && !$this->validate() ) {
			return false;
		}

		$this->normalizeAttributes();
		return parent::save(false);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return SimvasiPolisis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{simvasi_polisis}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('procedure_step_id, simvasi_sindesis_procedure_step_id', 'required'),
			array('procedure_step_id, simvasi_sindesis_procedure_step_id', 'numerical', 'integerOnly'=>true),
			array		//edw argotera tha prepei na orisoume mia nea synartisi elegxou h opoia me to mktime tha mporei poly eukola na elegxei thn orthotita ths hmerominias
			(
				'conn_compl_date',
				'match',
				'pattern' => "@^[0-9]{1,2}[/][0-9]{1,2}[/][0-9]{4}$@",
				'message' => Yii::t('', 'Date with format') .' '. Yii::app()->params['adminBirthday'],
				'allowEmpty' => true,
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('procedure_step_id, conn_compl_date, simvasi_sindesis_procedure_step_id', 'safe', 'on'=>'search'),
			
			array('conn_compl_date', 'safe', 'on'=>'excel'),
			array('procedure_step_id, simvasi_sindesis_procedure_step_id', 'unsafe', 'on'=>'excel'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'activations' => array(self::HAS_MANY, 'Activation', 'simvasi_polisis_procedure_step_id'),
			'procedureStep' => array(self::BELONGS_TO, 'ProcedureStep', 'procedure_step_id'),
			'simvasiSindesisProcedureStep' => array(self::BELONGS_TO, 'SimvasiSindesis', 'simvasi_sindesis_procedure_step_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'conn_compl_date' => Yii::t('', 'Completed Connection Works Date'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('procedure_step_id',$this->procedure_step_id);
		$criteria->compare('simvasi_sindesis_procedure_step_id',$this->simvasi_sindesis_procedure_step_id);
		$criteria->compare('arithmos_paroxis',$this->arithmos_paroxis);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}