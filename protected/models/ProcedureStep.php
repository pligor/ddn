<?php

/**
 * This is the model class for table "procedure_step".
 *
 * The followings are the available columns in table 'procedure_step':
 * @property integer $id
 * @property string $execution_date
 * @property integer $pv_plant_id
 *
 * The followings are the available model relations:
 * @property Activation $activation
 * @property ArxikiAitisi $arxikiAitisi
 * @property DiatiposiOron $diatiposiOron
 * @property PvPlant $pvPlant
 * @property SimvasiPolisis $simvasiPolisis
 * @property SimvasiSindesis $simvasiSindesis
 */
class ProcedureStep extends CActiveRecord
{
	public static function getPerioxiCols($step) {
		if($step == 'ArxikiAitisi') {
			return array(
				'execution_date' => 'F',
			);	
		}
		elseif($step == 'DiatiposiOron') {
			return array(
				'execution_date' => 'M',
			);
		}
		elseif($step == 'SimvasiSindesis') {
			return array(
				'execution_date' => 'N',
			);
		}
		elseif($step == 'SimvasiPolisis') {
			return array(
				'execution_date' => 'P',
			);
		}
		elseif($step == 'Activation') {
			return array(
				'execution_date' => 'Q',
			);
		}
		
		return array();
	}
	
	/**
	 * @return string[]
	 */
	public static function getWorkflow() {
		return array(
			0 => 'ArxikiAitisi',
			1 => 'DiatiposiOron',
			2 => 'SimvasiSindesis',
			3 => 'SimvasiPolisis',
			4 => 'Activation',
		);
	}
	
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => '..............',
		    //'showErrorSummary' => true,
		    'elements' => array(
				'execution_date' => array(
					'type' => 'zii.widgets.jui.CJuiDatePicker',
					//'hint' => '',
					'attributes' => array(
						'options' => array(
						 	'showAnim' => 'fold',
							'dateFormat' => 'd/m/yy',
							//'defaultDate' => date('j/n/Y'),
							//'changeYear' => false,
							//'changeMonth' => true,
							//'yearRange' => '1900',
						),
					),
		        ),
			),
		);
	}
	
	/**
	 * overriding init function to give an initial (invalid format) value to the execution date
	 */
	public function init() {
		$this->execution_date = date('j/n/Y');
	}
	
	public function normalizeAttributes() {	//BE AWARE: you set the correct scenario before using this method
		$attrNames = $this->safeAttributeNames;
		foreach($attrNames as $attrName) {
			$method = 'normalize'. ucfirst($attrName);
			if( method_exists($this, $method) ) {
				$this->$method();
			}
		}
	}
	
	/**
	 * @param bool $return
	 */
	public function normalizeExecution_date($return=false) {
		if( CHtmlEx::isUnsignedInteger($this->execution_date) ) {
			if($return) {
				return $this->execution_date;
			}
			return;
		}
		
		$execution_date = $this->execution_date;
		
		$execution_date = CHtmlEx::extractDate($execution_date);
		if($execution_date===false) {
			$message = Yii::t('', 'Date has INVALID format');
			throw new Exception($message);
		}
		
		$execution_date = CHtmlEx::convDate2stamp($execution_date);
		 
		if($return) {
			return $execution_date;
		}
		
		$this->execution_date = $execution_date;
	}
	
	/**
	 * overriding save function to convert execution date to GMT timestamp
	 * BE AWARE: Saving with false validation could cause some problems
	 */
	public function save($validation=true,$attributes=null) {
		if( $validation && !$this->validate() ) {
			return false;
		}
		
		$this->normalizeAttributes();
		return parent::save(false);
	}
	
	/**
	 * For any procedure step we get a "brother" step 
	 *  @return ProcedureStep or null if no siblings are available
	 */
	public function getSibling($step='ArxikiAitisi')
	{
		$criteria=new CDbCriteria;
		//$criteria->condition = 'id <> '.$this->id;				//not itsself
		$criteria->compare('pv_plant_id', $this->pv_plant_id);	//all brothers
		
		$procedureSteps = self::model()->findAll($criteria);
		
		$step = lcfirst($step);	//fix it
		
		foreach($procedureSteps as $procedureStep)
		{
			//print $procedureStep->id;
			//print "<br/>";
			if($procedureStep->$step) return $procedureStep;
		}
		
		return null;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return ProcedureStep the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{procedure_step}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pv_plant_id, execution_date', 'required'),
			array(
				'execution_date',
				'match',
				'pattern' => "@^[0-9]{1,2}[/][0-9]{1,2}[/][0-9]{4}$@",
				'message' => Yii::t('', 'Date with format') .' '. Yii::app()->params['adminBirthday'],
				'allowEmpty'=>false,
			),
			array('pv_plant_id', 'numerical', 'integerOnly'=>true),
			array('execution_date', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, execution_date, pv_plant_id', 'safe', 'on'=>'search'),
			
			array('execution_date', 'safe', 'on'=>'excel'),
			array('id, pv_plant_id', 'unsafe', 'on'=>'excel'),
		);
	}
	
	/**
	 * 
	 * normally a single procedure step owns only one child
	 * @return child object
	 */
	public function getChild()
	{
		foreach($this->childrenRelations as $childRelation)
		{
			$child = $this->$childRelation;
			if($child)	return $child;
		}
		return null;	//no children
	}
	
	/**
	 * @return array of class names of all children (HAS::ONE) indexed by corresponding relation, empty array otherwise
	 */
	public function getChildrenRelations()
	{
		$workflow = self::getWorkflow();
		$relations = $this->relations();
		
		$childrenRelations = array();
		
		foreach($relations as $relation => $relationArray)
		{
			if($relationArray[0] !== self::HAS_ONE)	continue;
                        $keys = array_keys($workflow, $relationArray[1]);
			$key = reset($keys);	//array_keys should only return one value
			
			$childrenRelations[$key] = $relation;
		}
		
		return $childrenRelations;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'activation' => array(self::HAS_ONE, 'Activation', 'procedure_step_id'),
			'arxikiAitisi' => array(self::HAS_ONE, 'ArxikiAitisi', 'procedure_step_id'),
			'diatiposiOron' => array(self::HAS_ONE, 'DiatiposiOron', 'procedure_step_id'),
			'pvPlant' => array(self::BELONGS_TO, 'PvPlant', 'pv_plant_id'),
			'simvasiPolisis' => array(self::HAS_ONE, 'SimvasiPolisis', 'procedure_step_id'),
			'simvasiSindesis' => array(self::HAS_ONE, 'SimvasiSindesis', 'procedure_step_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'execution_date' => Yii::t('', 'Execution Date'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('execution_date',$this->execution_date,true);
		$criteria->compare('pv_plant_id',$this->pv_plant_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}