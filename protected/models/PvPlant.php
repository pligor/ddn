<?php
/**
 * This is the model class for table "pv_plant".
 *
 * The followings are the available columns in table 'pv_plant':
 * @property integer $id
 * @property string $priority
 * @property integer $island_id
 * @property integer $prosopo_id
 * @property integer $address_id
 * 
 * @property enumerated $pvCat
 * @property integer $protocol
 * 
 * @property CForm $form
 *
 * The followings are the available model relations:
 * @property ProcedureStep[] $procedureSteps
 * @property PvEnabled $pvEnabled
 * @property PvMt $pvMt
 * @property Island $island
 * @property Prosopo $prosopo
 * @property Address $address
 * @property PvSteges $pvSteges
 * @property PvXt $pvXt
 */
class PvPlant extends CActiveRecord
{
	public static function getPerioxiCols($step) {
		if($step != 'ArxikiAitisi') return array();
		
		$cols = array();
		foreach(self::getCats() as $key => $cat) {
			$cols[$key] = $cat::col;
		}
		
		return array(
			'pvCat' => $cols,
			'protocol' => $cols,
		);
	}
	
	/**
	 * @return array the categories of various PVs
	 */
	public static function getCats($which=null)
	{
		$cats = array(
			0 => "PvXt",
			1 => "PvMt",
			2 => "PvSteges",
		);

		if( is_null($which) ) {
			return $cats;
		}
		elseif( is_array($which) ) {
			return array_intersect_key( $cats, array_fill_keys($which, null) );
		}
		else {
			return $cats[$which];
		}
	}
	
	public function isNotStegi() {
		if($this->pvCat == 2) {
			return false;	
		}
		return true;
	}
	
	/**
	 * Overloading to set some attributes in a specific order
	 * These attributes will be set again
	 * @see CModel::setAttributes()
	 */
	public function setAttributes($values,$safeOnly=true) {
		if(!is_array($values)) return;
		
		$myAttrs = array(
			'pvCat',
			'protocol',
		);
		$myValues = array();
		foreach($myAttrs as $myAttr) {
			if( !isset($values[$myAttr]) ) continue;
			
			$myValues[$myAttr] = $values[$myAttr];
			unset($values[$myAttr]);
		}
		
		$values += $myValues;	//put them at the end in specific order
		
		parent::setAttributes($values,$safeOnly);
	}
	
	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to define a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				get_class() => $this->getFormConfig(),
			),
			'buttons'=>array(
		        'submit' => array(
		            'type'=>'submit',
		            'label' => Yii::t('', 'Change'),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		$elements = array(
			'pvCat' => array(
				'type' => 'dropdownlist',
				'items' => self::getCatDescs(),
				//'hint' => '',
	        ),
	        'protocol' => array(
				'type' => 'text',
	        ),
		);
		
		return array(
			'type' => 'form',
		    //'title' => '......',
		    //'showErrorSummary' => true,
		    'elements' => $elements,
		);
	}
	
	/**
	 * MAYBE LATER WE SHOULD USE THE EAGER LOADING METHOD SO WE GET THE CHILD ALONG WITH THE PARENT
	 * @return array of each procedure step this pv plant has, empty array if no procedures
	 */
	public function getProcedures()
	{
		$procedureSteps = $this->procedureSteps;
		
		$procedures = array();
		
		if($procedureSteps)
		foreach($procedureSteps as $procedureStep)
		{
			$procedureStepChild = $procedureStep->child;
			$procedures[ get_class($procedureStepChild) ] = $procedureStepChild;
		}
		
		return $procedures;
	}
	
	/**
	 * overriding save to store child
	 * @see CActiveRecord::save()
	 */
	public function save($validation=true,$attributes=null) {
		$saved = parent::save($validation);
		if( !$saved ) {
			//throw new Exception('save (parent class) pv plant failed');
			return $saved;
		}
		
		$model = $this->child;
		$model->pv_plant_id = $this->id;
		$model->perioxi_dei_id = $this->island->perioxiDei->id;
                $model->island_id = $this->island->id;
		
//PROSOXI DEN PERNAME TO $validation KAI STO SAVE DIOTI TOTE THA PREPEI NA KANOUME OVERRIDE KAI THN SYNARTISI validation
                $saved = $model->save();

		return $saved;
	}
	
	public function getProtocol()
	{
		if( !$this->child )	return null;
		
		return $this->child->protocol;
	}
	
	public function setProtocol($value) {
		if( !$this->child ) {
			$message = Yii::t('', 'you tried to set protocol in pv plant without setting pvCat first');
			throw new Exception($message);
		}
		
		$value = $this->normalizeProtocol($value);
		
		$this->child->protocol = $value;
	}
	
	/**
	 * Try to extract first integer value from a string
	 * or if you have an array with protocols
	 * @param array|int $value
	 */
	public function normalizeProtocol($value=null) {
		if( !is_array($value) ) {
			return CHtmlEx::getInteger($value);	//keep only integer	
		}
		
		if( !$this->child ) {
			$message = Yii::t('', 'you tried to normalize protocol in pv plant without setting pvCat first');
			throw new Exception($message);
		}
		
		return $value[$this->pvCat];
	}
	
	/**
	 * pvCat custom property
	 * @return integer pv cat id or null if the model is new and no child has been assigned yet
	 */
	public function getPvCat() {
		$cats = self::getCats();
		
		foreach($cats as $catId => $cat) {
			$relation = lcfirst($cat);
			if( $this->$relation )	return $catId;
		}
		
		return null;
	}
	
	/**
	 * pvCat custom property
	 * @param enumerated $value
	 */
	public function setPvCat($value) {
		$value = $this->normalizePvCat($value);
		
		if($value === null) return;
		if($this->pvCat === $value) return;	//no change is required	//WE MUST CHECK THIS MORE THOROUGHLY
		
		$class = self::getCats($value);
		$relation = lcfirst($class);
		
		$newChild = new $class();
		
		if( !$this->isNewRecord ) {
			$curChild = $this->child;
			$newChild->attributes = $curChild->attributes;	//copy attributes. BE AWARE: MAY LOSE ATTRIBUTES
			
			$curChild->delete();
		}
		$this->resetChildren();
		$this->$relation = $newChild;
	}
	
	/**
	 * @param array $value
	 * @throws Exception
	 */
	public function normalizePvCat($value=null) {
		if( !is_array($value) ) return $value;
		
		$categChecks = array();
		foreach($value as $catId => $protocol) {
			$categChecks[$catId] = is_numeric($protocol);
		}
		
		if(array_sum($categChecks) != 1) {
			$message = Yii::t('', 'There is error in protocol');
			throw new Exception($message);
		}
		
		foreach($value as $catId => $protocol) {
			if( is_numeric($protocol) ) {
				return $catId;
			}
		}
	}
	
	private function resetChildren() {
		$this->pvXt = null;
		$this->pvMt = null;
		$this->pvSteges = null;
	}
	
	/**
	 * Creates a new model for the child class
	 * @return mixed if it is an old record and all ok it returns the child, if it is a new record returns bool(false)
	 */
	public function getChild()
	{
		$catId = $this->pvCat;
		
		if( ($catId===null) || ($catId===false) )	return $catId;

		$relation = lcfirst( self::getCats($catId) );
		
		return $this->$relation;
	}
	
	/**
	 * If the island is not found then the expression "false+1" equals 1 which is accepted
	 * @return int the maximum priority for the island plus one
	 */
	public function getNewPriority($island_id) {
		$max_priority = Yii::app()->db->createCommand()
		    ->select('MAX(priority) as max_priority')
		    ->from( $this->tableName() )
		    ->where('island_id=:island_id', array(':island_id'=>$island_id))
		    ->queryScalar();	//http://www.yiiframework.com/doc/api/1.1/CDbCommand#queryScalar-detail
		
		$this->priority = ($max_priority+1);
	}
	
	/**
	 * @return array the descriptions of the categories of various PVs
	 */
	public static function getCatDescs($which=array())
	{
		if(is_array($which)) {
			$desc = array();
			foreach(self::getCats() as $key => $class) {
				$desc[$key] = $class::getDescr();
			}
			return $desc;
		}
		
		$class = self::getCats($which);
		return $class::getDescr();
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return PvPlant the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{pv_plant}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('priority, island_id, prosopo_id, address_id, protocol, pvCat', 'required'),
			array('island_id, prosopo_id, address_id, protocol', 'numerical', 'integerOnly'=>true),
			array('priority', 'length', 'max'=>10),
			array(
				'pvCat',
				'in',
				'range' => array(0,1,2),
			),
			array(
				'protocol',
				'numerical',
				'allowEmpty' => false,
				'integerOnly' => true,
				//'min' => 0,
				//'tooSmall' => 'some error message',
				//'max' => 100,
				//'tooBig' => 'some error message',
			),
			// The following rule is used by search(). Please remove those attributes that should not be searched.
			array('id, priority, island_id, prosopo_id, address_id', 'safe', 'on'=>'search'),
			
			array('pvCat, protocol', 'safe', 'on'=>'excel'),
			array('id, priority, island_id, prosopo_id, address_id', 'unsafe', 'on'=>'excel'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'procedureSteps' => array(self::HAS_MANY, 'ProcedureStep', 'pv_plant_id'),
			'pvEnabled' => array(self::HAS_ONE, 'PvEnabled', 'pv_plant_id'),
			'pvMt' => array(self::HAS_ONE, 'PvMt', 'pv_plant_id'),
			'island' => array(self::BELONGS_TO, 'Island', 'island_id'),
			'prosopo' => array(self::BELONGS_TO, 'Prosopo', 'prosopo_id'),
			'address' => array(self::BELONGS_TO, 'Address', 'address_id'),
			'pvSteges' => array(self::HAS_ONE, 'PvSteges', 'pv_plant_id'),
			'pvXt' => array(self::HAS_ONE, 'PvXt', 'pv_plant_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'priority' => Yii::t('', 'Priority Number'),
			'protocol' => Yii::t('', 'Protocol Number'),
			'pvCat' => Yii::t('', 'PV Category'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('priority',$this->priority,true);
		$criteria->compare('island_id',$this->island_id);
		$criteria->compare('prosopo_id',$this->prosopo_id);
		$criteria->compare('address_id',$this->address_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}