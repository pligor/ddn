<?php

/**
 * This is the model class for table "killed".
 *
 * The followings are the available columns in table 'killed':
 * @property integer $pv_plant_id
 * @property string $date_killed
 *
 * The followings are the available model relations:
 * @property PvPlant $pvPlant
 */
class Killed extends CActiveRecord {
	public static function getPerioxiCols($step='Killed') {
		if($step != 'Killed') {
			return array();	
		}
		
		return array(
			'pv_plant_id' => 'AB',
			'date_killed' => 'AC',
		);
	}
	
	public static function getTitle() {
		return 'Αποδέσμευση Ισχύος';
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Killed the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{killed}}';
	}
	
	public function normalizeAttributes() {	//BE AWARE: you set the correct scenario before using this method
		$attrNames = $this->safeAttributeNames;
		foreach($attrNames as $attrName) {
			$method = 'normalize'. ucfirst($attrName);
			if( method_exists($this, $method) ) {
				$this->$method();
			}
		}
	}
	
	//normalizing pv_plant_id would be invalid
	
	/** 
	 * Enter description here ...
	 * @param bool $return
	 * @throws Exception
	 */
	public function normalizeDate_killed($return=false) {
		$date_killed = $this->date_killed;
		
		try {
			if( CHtmlEx::isEmpty($date_killed) ) {
				$date_killed = null;
				$message = 'date_killed '. Yii::t('', 'is empty');
				throw new Exception($message,1);	//this is NOT an error
			}
			
			if( CHtmlEx::isUnsignedInteger($date_killed) ) {
				$message = 'date_killed '. Yii::t('', 'is unsigned integer');
				throw new Exception($message,1);	//this is NOT an error
			}
			
			$date_killed = CHtmlEx::extractDate($date_killed);
			if($date_killed===false) {
				$message = Yii::t('', 'The Power Release date has invalid format');
				throw new Exception($message);
			}
			
			$date_killed = CHtmlEx::convDate2stamp($date_killed);
		}
		catch(Exception $e) {
			if($e->getCode()!=1) {	//rethrow only if this is really an error
				throw $e;
			}
		}
		 
		if($return) {
			return $date_killed;
		}
		$this->date_killed = $date_killed;
	}
	
	/**
	 * normalize before save
	 * @see CActiveRecord::save()
	 */
	public function save($validation=true,$attributes=null) {
		$this->normalizeAttributes();
		return parent::save($validation);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that will receive user inputs.
		return array(
			//array('pv_plant_id, date_killed', 'required'),
			array('pv_plant_id', 'required'),
			array('pv_plant_id', 'numerical', 'integerOnly'=>true),
			//The following rule is used by search(). Please remove those attributes that should not be searched
			array('pv_plant_id, date_killed', 'safe', 'on'=>'search'),
			
			array('pv_plant_id, date_killed', 'safe', 'on'=>'excel'),
			//there are no unsafe attributes on excel scenario
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related class name for the relations automatically generated below
		return array(
			'pvPlant' => array(self::BELONGS_TO, 'PvPlant', 'pv_plant_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'date_killed' => 'Date Killed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pv_plant_id',$this->pv_plant_id);
		$criteria->compare('date_killed',$this->date_killed,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}