<?php

/**
 * This is the model class for table "island".
 *
 * The followings are the available columns in table 'island':
 * @property integer $id
 * @property string $island_name
 * @property double $rae_margin
 * @property integer $perioxi_dei_id
 *
 * The followings are the available model relations:
 * @property PerioxiDei $perioxiDei
 * @property PvPlant[] $pvPlants
 * @property IslandGroup[] $islandGroups
 * @property Dimos[] $dimoses
 * @property User[] $users
 */
class Island extends CActiveRecord {
	/**
	 * @see CModel::behaviors()
	 */
	public function behaviors() {
		return array(
			'fixture-behavior' => array(
				'class' => 'FixtureBehavior',
			),
		);
	}
	
	public static function getActiveIslandCount() {
		$criteria=new CDbCriteria();
		$criteria->with = 'pvPlants';
		$criteria->together = true;
		
		$criteria->condition = 'pvPlants.id IS NOT NULL';

		/*
		$criteria->condition = 'island_id = t.id';
		return self::model()->with(array(
			'pvPlants' => array('condition' => "island_id = t.id"),
		))->count();
		//*/
		
		return self::model()->count($criteria);
	}
	
	/**
	 * Get all islands in format array(island_id => island_name) ordered by island name 
	 * @return Array the static model class
	 */
	public static function getIslands()
	{
		$valueField = 'id';
		$textField = 'island_name';
		
		$criteria=new CDbCriteria;
		$criteria->select = array($valueField, $textField);
		$criteria->order = $textField;
		$allRows = self::model()->findAll($criteria);

		return CHtml::listData($allRows, $valueField, $textField);
	}
	
	public function filterIslands($islands) {
		$criteria=new CDbCriteria;
		$criteria->compare('island_name', $this->island_name, false);	//partial match: false
		$criteria->compare('rae_margin',$this->rae_margin);
		$criteria->compare('perioxi_dei_id',$this->perioxi_dei_id);
		
		$models = $this->findAll($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Island the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{island}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('island_name, rae_margin, perioxi_dei_id', 'required'),
			array('perioxi_dei_id', 'numerical', 'integerOnly'=>true),
			array('rae_margin', 'numerical'),
			array('island_name', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, island_name, rae_margin, perioxi_dei_id', 'safe', 'on'=>'search'),
			
			array("island_name", 'exist',
				'allowEmpty'=>false,
				//"attributeName"=>Defaults to null, meaning using the name of the attribute being validated,
				//"className"=>Defaults to null, meaning using the ActiveRecord class of the attribute being validated,
				//"criteria"=>additional query criteria. This array will be used to instantiate a CDbCriteria object,
			),
			array("id", 'exist',
				'allowEmpty'=>false,
				//"attributeName"=>Defaults to null, meaning using the name of the attribute being validated,
				//"className"=>Defaults to null, meaning using the ActiveRecord class of the attribute being validated,
				//"criteria"=>additional query criteria. This array will be used to instantiate a CDbCriteria object,
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'perioxiDei' => array(self::BELONGS_TO, 'PerioxiDei', 'perioxi_dei_id'),
			'pvPlants' => array(self::HAS_MANY, 'PvPlant', 'island_id'),
			'islandGroups' => array(self::MANY_MANY, 'IslandGroup', 'island_group_island(island_id, island_group_id)'),			
			'dimoses' => array(self::MANY_MANY, 'Dimos', 'dimos_island(island_id, dimos_id)'),
			'users' => array(self::MANY_MANY, 'User', 'user_island(island_id, user_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('', 'Island'),
			'island_name' => Yii::t('', 'Island Name'),
			'rae_margin' => Yii::t('', 'ΡΑΕ MARGIN POWER'),
			'perioxi_dei_id' => Yii::t('', 'Area in which the Island belongs to'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;

		//$criteria->compare('id',$this->id);
		$criteria->compare('island_name',$this->island_name,true);
		$criteria->compare('rae_margin',$this->rae_margin);
		$criteria->compare('perioxi_dei_id',$this->perioxi_dei_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}