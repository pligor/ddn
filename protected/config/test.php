<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'components'=>array(
			'fixture'=>array(
				'class' => 'system.test.CDbFixtureManager',
			),
			'db' => array(
				//in order for port to work you should use a real ip number
				//localhost and port=5000 will NOT work. 127.0.0.1 and port 5000 will WORK
				'connectionString' => 'mysql:host=127.0.0.1;port=3306;dbname=ddn_test',
				'emulatePrepare' => true,
				'username' => 'root',
				'password' => 'qr21131s35Uy4mT',
				'charset' => 'utf8',
				'enableParamLogging' => true,
				//'schemaCachingDuration' => 0,	//number of seconds that table metadata can remain valid in cache
				'autoConnect' => true,	//Note, this property is only effective when the CDbConnection object is used as an application component.
				'tablePrefix' => '',
			),
		),
	)
);
