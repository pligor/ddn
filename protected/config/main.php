<?php

// uncomment the following to define a path alias
$basePath = realpath( dirname(__FILE__).'/..' );

Yii::setPathOfAlias('beh', $basePath .'/behaviors');
Yii::setPathOfAlias('external', realpath($basePath .'/../../../php/downloaded'));

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

/*
For the configuration of a component, like 'log', we see that we specify the class.
So this configuration relates with that class.
If we have a component that must be loaded whether or not is requested from a user request (typically a controller action) then we include it inside 'preload'
*/

$dbComponent = 'db';

return array(
	'basePath' => $basePath,
	'name' => 'ΔΔΝ Διαχείριση ΑΠΕ',
	'defaultController' => 'site/page',	//default controller/action (or simply controller and the default action will be implemented)
	
	'sourceLanguage' => 'en',
	'language' => 'el',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'beh.*',
		'application.modelsForm.*',
	),
	'behaviors' => array(
		'myBehav' => array(
		    'class' => 'ApplicationConfigBehavior',
		    //'property1'=>'value1',
		)
	),
	
	'modules'=>array(
		'gii'=>array(				//NOTE: REMOVE gii IN PRODUCTION RELEASE
			'class'=>'system.gii.GiiModule',
			'password'=>'gii',
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),	//second argument is a local address (localhost) in Ipv6
		),
		'multilang',
	),

	// application components
	'components'=>array(
		'messages' => array(
			'class' => 'CDbMessageSource',
			'connectionID' => $dbComponent,
		),
		'authManager' => array(
			'class' => 'CDbAuthManager',
			'connectionID' => $dbComponent,
			'showErrors' => true,	//Enable error reporting for bizRules
		),
		'user'=>array(
			'class' => 'WebUser',
			'allowAutoLogin' => true,	//enable cookie-based authentication
			'autoRenewCookie' => false,	//http://www.yiiframework.com/doc/api/1.1/CWebUser#autoRenewCookie-detail
			'loginUrl' => array('/bouncer/login'),
			'authTimeout' => 8*3600,	//timeout in seconds after which user is logged out if inactive, depends from session configuration in php.ini
		),
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		//*/
		/*	uncomment the following to use an SQLite database
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		//*/
		$dbComponent => array(
			//in order for port to work you should use a real ip number
			//localhost and port=5000 will NOT work. 127.0.0.1 and port 5000 will WORK
			'connectionString' => 'mysql:host=127.0.0.1;port=3306;dbname=ddn',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'qr21131s35Uy4mT',
			'charset' => 'utf8',
			'enableParamLogging' => true,
			//'schemaCachingDuration' => 3600,	//number of seconds that table metadata can remain valid in cache
			'autoConnect' => true,	//Note, this property is only effective when the CDbConnection object is used as an application component.
			'tablePrefix' => '',
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
		'log'=>array(
			'class' => 'CLogRouter',
			'routes'=>array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
				/* uncomment the following to show log messages on web pages
				array(
					'class'=>'CWebLogRoute',
				),
				//*/
			),
		),
	),

	// application-level parameters that can be accessed using Yii::app()->params['paramName']
	'params'=>array(
		'adminEmail' => 'pligor@facebook.com',	//this is used in contact page
		'adminBirthday' => '16/1/1984',	//this is used in contact page
		'adminName' => 'George Pligor',
		'adminGreekName' => 'Γεώργιος Πληγορόπουλος',
		'adminUsername' => 'pligor',
	),
);
