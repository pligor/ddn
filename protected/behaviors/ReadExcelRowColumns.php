<?php

/**
 * ReadSheetColumns class
 * @author pligor
 */
class ReadExcelRowColumns extends CBehavior {
	/**
	 * @param string $class
	 * @param PHPExcel_Worksheet $sheet (we pass it by value (not by reference) since we only perform read operation)
	 * @param int $i
	 * 
	 * @return array
	 */
	protected function readCols($class, $sheet, $i) {
		$cols = $class::getPerioxiCols($this->owner->id);
		$colValues = array();
		foreach($cols as $property => $col) {
			if( is_array($col) ) {
				$colValue = array();
				foreach($col as $key => $c) {
					$colValue[$key] = $sheet->getCell($c.$i)->getValue();
				}
			}
			else {
				$colValue = $sheet->getCell($col.$i)->getValue();
			}
			$colValues[$property] = $colValue;
		}
		return $colValues;
	}
}