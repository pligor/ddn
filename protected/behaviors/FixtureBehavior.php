<?php

Yii::import('system.test.CDbFixtureManager');

/**
 * FixtureBehavior behavior for models loaded by a php array file
 * @author pligor
 */
class FixtureBehavior extends CBehavior {
	
	/**
	 * Returns true if all data from csv file are inserted. false otherwise
	 * @return bool
	 */
	public function loadFixtureData() {
		$fixtureManager = new CDbFixtureManager();
		$fixtureManager->basePath = Yii::getPathOfAlias('application.data');
		
		$tableName = ActiveRecord::getTableName( $this->owner );
		$fixtureManager->truncateTable($tableName);
		$fixtureManager->loadFixture($tableName);
		
		return true;
	}
}