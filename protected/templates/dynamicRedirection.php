<?php
//define inside the controller a variable name
//public $variable_name;

//PERFORM CALL INSIDE A FILTER
$controller = $filterChain->controller;
$calls = array(
	array(
		'callee' => 'controller/action',
		'data_name' => 'variable_name',
	),
);
$answers = ActionCall::call($controller, $calls);	
$controller->variable_name = $answers['variable_name'];


//RESPOND TO CALL
$callData = CallData::getCall($this->route);
$data_name = $callData->data_name;
$callData->blob = $model->$data_name;
$route = $callData->answerCall();

$this->redirect(array(
	$route,
));