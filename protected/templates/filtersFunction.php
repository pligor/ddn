<?php
public function filters() {
	// return the filter configuration for this controller, e.g.:
	return array(
		'inlineFilterName',
		array(
			'class'=>'path.to.FilterClass',
			'propertyName'=>'propertyValue',
		),
	);
}