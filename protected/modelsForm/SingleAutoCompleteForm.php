<?php

/**
 * @property CForm $form
 * @author pligor
 */
class SingleAutoCompleteForm extends CFormModel {
	/**
	 * @var string
	 */
	public $text;
	
	/**
	 * @var string
	 */
	public $buttonLabel;
	
	/**
	 * @var string
	 */
	public $attributeLabel;
	
	/**
	 * @var string
	 */
	public $hint;
	
	/**
	 * @var integer typed length before auto complete
	 */
	public $minLength;
	
	/**
	 * @var integer delay in milliseconds
	 */
	public $delay;
	
	/**
	 * @var boolean whether can be left empty or not
	 */
	public $allowEmpty = true;
	
	/**
	 * @var string[]
	 */
	protected $source;
	
	/**
	 * @see CFormModel::init()
	 */
	public function init() {
		parent::init();	
		$this->buttonLabel = Yii::t('', 'Submit');
		$this->attributeLabel = Yii::t('', 'Text Line');
		$this->hint = null;
		$this->minLength = 2;
		$this->delay = 500;
	}
	
	/**
	 * @param string $scenario
	 * @param string[] $source
	 */
	public function __construct($source, $scenario='') {
		parent::__construct($scenario);
		$this->source = $source;
	}
	
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'text' => $this->attributeLabel,
		);
	}
	
	/**
	 * @see CModel::rules()
	 */
	public function rules() {
		return array(
			/*array(
				'text',
				'default',
				'value' => '',
			),*/
			array(
				'text',
				'in',
				'range' => $this->source,
				'strict' => false,
				'allowEmpty' => $this->allowEmpty,
			),
		);
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => '..........',
		    //'showErrorSummary' => true,
		    'elements' => array(
		    	'text' => array(
					'type' => 'zii.widgets.jui.CJuiAutoComplete',
					'hint' => $this->hint,
					'attributes' => array(
						'source' => array_values($this->source),
						'options'=>array(   //options reference: http://jqueryui.com/demos/autocomplete/
			                'minLength' => $this->minLength,
			                'delay' => $this->delay,
			            ),
					),
		        ),
			),
		);
	}
	
	/**
	 * @return CForm
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				get_class() => $this->formConfig,
			),
			'buttons'=>array(
		        'submit_text' => array(
		            'type'=>'submit',
		            'label' => $this->buttonLabel,
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		
		return new CForm($config,$this);	//all subforms get as parent their model
	}
}