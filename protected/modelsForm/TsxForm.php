<?php

//DYSTYXWS THA PREPEI KAI AUTO NA TO KANONIKOPOIHSOUME ME TA PROTYPA TOU MULTI MODEL

class TsxForm extends CFormModel {
	public $buttonLabel = 'Button';
	
	public function getInstance() {
		$model = $this->models['SingleUploadForm'];
		$attribute = 'file';
		$this->$attribute = CUploadedFile::getInstance($model, $attribute);
	}
	
	public function saveFile() {
		$class = $this->classes['SingleUploadForm'];
		return $this->models[$class]->saveFile();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::rules()
	 */
	public function rules() {
		return array(
			array(
				'file',
				'safe',
			),
			array(
				'id',
				'safe',
			),
		);
	}
	
	/**
	 * Initialize model before executing setScenario inside parent's construct
	 */
	public function __construct() {
		$classes = array(
			'PerioxiDei' => 'PerioxiDei',
			'SingleUploadForm' => 'SingleUploadForm',
		);
		
		$name = 'MultiModelBehavior';
		$behavior = new $name($classes);
		$this->attachBehavior($name, $behavior);
		
		parent::__construct();
	}
	
	/**
	 * @see CFormModel::init()
	 */
	public function init() {
		parent::init();
	}
	
	public function getFormConfig() {
		/*
		foreach($this->models as $class => $model) {
			$formConfig[mb_strtolower($class)] = $model->formConfig;
		}
		//*/
		$class = $this->classes['PerioxiDei'];
		$formConfig[mb_strtolower($class)] = $this->models[$class]->getFormConfig(true);
		
		$class = $this->classes['SingleUploadForm'];
		$formConfig[mb_strtolower($class)] = $this->models[$class]->formConfig;
		
		return $formConfig;
	}
	
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> $this->formConfig,
			'buttons'=>array(
		        'submit_file' => array(
		            'type'=>'submit',
		            'label' => $this->buttonLabel,
		        ),
		    ),
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),
		);
		
		$form = new CForm($config);	//all subforms get as parent their model
		
		foreach($this->models as $class => $model) {
			$form[mb_strtolower($class)]->model = $model;
		}
		
		return $form;
	}
	
	/**
	 * Get safe attribute name
	 * @param string $name
	 */
	public function __get($name) {
		if( !$this->checkModelAttribute($name) ) {
			return parent::__get($name);
		}
		return $this->getModelAttribute($name);
	}
	
	/**
	 * @see CComponent::__set()
	 */
	public function __set($name, $value) {
		if( $this->setModelAttribute($name, $value) === false) {
			parent::__set($name, $value);
		}
	}
	
	/**
	 * Set the same scenario also for all the corresponding models
	 * @param string $value
	 */
	public function setScenario($value) {
		$this->setModelsScenario($value);
		parent::setScenario($value);
	}
}