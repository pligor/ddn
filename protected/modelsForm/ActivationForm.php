<?php

/**
 * ActivationForm class
 * @author pligor
 */
class ActivationForm extends MultiModelForm {	
	/**
	 * @var int
	 */
	public $pv_plant_id;
	
	/**
	 * @param string $scenario
	 */
	public function __construct($scenario='insert') {
		$classes = array(
			'ProcedureStep',
			'Activation',
			'PvEnabled',
		);
		
		$name = 'ReadExcelRowColumns';
		$behavior = new $name();
		$this->attachBehavior($name, $behavior);

		parent::__construct($classes,$scenario);
	}
	
	public function getPowerRange() {
		$model = CHtmlEx::loadModel('PvPlant', $this->pv_plant_id);
		$class = PvPlant::getCats($model->pvCat);
		return $class::getPowerRange();
	}

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		$title = self::getTitle();
		
		return array(
			array(
				'execution_date',
				'match',
				'pattern' => "@^[0-9]{1,2}[/][0-9]{1,2}[/][0-9]{4}$@",
				'message' => Yii::t('', 'Date with format') .' '. Yii::app()->params['adminBirthday'],
				'allowEmpty' => false,
				'on' => 'insert, update',
			),
			array(
				'execution_date',
				'ext.PligorValidators.xldate',
				'allowEmpty' => false,
				'emptyMessage' => Yii::t('', 'Date is empty') ." ($title)",
				'wrongDate' => Yii::t('', 'Date {title} does not have the format',array('{title}'=>$title)) .' '. Yii::app()->params['adminBirthday'],
				'on' => 'excel',
			),
			array(
				'real_power',
				'numerical',
				'allowEmpty' => false,
			),
			array(
				'real_power',
				'ext.PligorValidators.validpower',
				'allowEmpty' => false,
				'emptyMessage' => Yii::t('', 'The real power is not allowed to be empty'),
			),
			array(
				'tracker',
				'filter',
				'filter' => array('CHtmlEx','isNotEmpty'), //http://gr2.php.net/manual/en/language.pseudo-types.php#language.types.callback
				'on' => 'excel',
			),
			array(
				'tracker',
				'boolean',
				'allowEmpty' => false,
				'falseValue' => 0,
				'trueValue' => 1,
				'strict' => false,
			),
			array(
				'ypostathmos',
				'length',
				'allowEmpty' => false,
				'max' => PvEnabled::ypostathmosMaxLen,
				//'on' => '',
			),
			array(
				'grammi_dianomis',
				'length',
				'allowEmpty' => false,
				'max' => PvEnabled::grammi_dianomisMaxLen,
				//'on' => '',
			),
			array(
				'grammi_dianomis',
				'ext.PligorValidators.grammidianomis',
				'allowEmpty' => false,
				'emptyMessage' => Yii::t('', 'Γραμμή Διανομής is empty'),
				//'on' => '',
			),
			array(
				'thesi_syndesis',
				'length',
				'allowEmpty' => false,
				'max' => PvEnabled::thesi_syndesisMaxLen,
				//'on' => '',
			),
			array(
				'ys_mt_xt',
				'length',
				'allowEmpty' => true,
				'max' => PvEnabled::ys_mt_xtMaxLen,
				//'on' => '',
			),
			array(
				'anaxorisi',
				'length',
				'allowEmpty' => false,
				'max' => PvEnabled::anaxorisiMaxLen,
				//'on' => '',
			),
		);
	}
	
	public function getTitle() {
		$class = $this->id;
		return $class::getTitle();
	}
	
	public function getId() {
		return substr(get_class(), 0, -strlen('Form'));
	}
	
	/**
	 * Save all models
	 */
	public function save() {
		$transaction = Yii::app()->db->beginTransaction();
		
		try {		//you must save in a specific order because of foreign key constraints
			$pvPlantModel = CHtmlEx::loadModel('PvPlant', $this->pv_plant_id);
			
			$model = $this->models['ProcedureStep'];
			$model->pv_plant_id = $this->pv_plant_id;
			
			$curTitle = self::getTitle();
			$prevClass = 'SimvasiPolisis';
			$prevTitle = $prevClass::getTitle();
			
			$prevProcedureStep = $model->getSibling($prevClass);		
			if($prevProcedureStep===null) {
				$message = Yii::t('', 'You are trying to insert data for the step {step} while there are missing data (at least) for the previous step ({prev_step})',array(
					'{step}' => $curTitle,
					'{prev_step}' => $prevTitle,
				));
				throw new Exception($message);
			}
			elseif($prevProcedureStep->execution_date > $this->execution_date) {
				$message = Yii::t('', 'The date of the step {step} is older than the date of the previous step {prev_step}',array(
					'{step}' => $curTitle,
					'{prev_step}' => $prevTitle,
				));
				throw new Exception($message);
			}
			
			if( !$model->save(false) ) {
				$message = Yii::t('', 'saving in table {tableName} failed',array(
					'{tableName}' => $model->tableName(),
				));
				throw new Exception($message);	
			}
			
			$model = $this->models['Activation'];
			$model->procedure_step_id = $this->models['ProcedureStep']->id;
			$model->simvasi_polisis_procedure_step_id = $prevProcedureStep->id;
			
			if( !$model->save(false) ) {
				$message = Yii::t('', 'saving in table {tableName} failed',array(
					'{tableName}' => $model->tableName(),
				));
				throw new Exception($message);
			}
			
			$model = $this->models['PvEnabled'];
			$model->pv_plant_id = $this->pv_plant_id;
			$model->activation_procedure_step_id = $this->models['ProcedureStep']->id;
			
			if( !$model->save(false) ) {
				$message = Yii::t('', 'saving in table {tableName} failed',array(
					'{tableName}' => $model->tableName(),
				));
				throw new Exception($message);
			}

			$transaction->commit();
		}
		catch(Exception $e) {
		    $transaction->rollBack();
		    throw $e;	//rethrow
		}
	}
	
	public function readExcel(& $sheet, $i) {
		if( !$this->checkModels() ) return false;
		
		$this->setScenario('excel');
		
		$warnings = array();
		$error = null;
		
		try {
			$model = new DDNxl();
			$this->pv_plant_id = $model->getPvPlantId($sheet, $i);

			$models = $this->models;
			foreach($this->classes as $class) {
				//$this->models[$class]->attributes = $this->readCols($class, $sheet, $i);
				$models[$class]->attributes = $this->readCols($class, $sheet, $i);
			}
			$this->models = $models;
			
			if( CHtmlEx::isEmpty($this->execution_date) ) {
				$message = Yii::t('', 'skip step {step} because it is empty',array(
					'{step}' => $this->id,
				));
				throw new Exception($message, 1);
			}
			
			$this->validate();
			
			foreach($this->errors as $attrErrors) {
				foreach($attrErrors as $attrError) {
					$warnings[] = $attrError;
				}
			}
			
			$this->save();
		}
		catch(Exception $e) {
			if( $e->getCode() != 1 ) {	//1 is the occasion where it is expected
				//$error = "EXCEPTION ERROR inside readExcel inside {$this->id}: ". $e->getMessage();
				$error = $e->getMessage();
			}
			$warnings = array(); //no need to have warnings for a line with an exception
		}
		
		return compact('warnings','error');
	}

	public function attributeLabels() {
		return $this->getAttrLabels();
	}

	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to define a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				$this->id => $this->getFormConfig(),
			),
			'buttons'=>array(
		        'submit' => array(
		            'type'=>'submit',
		            'label' => Yii::t('', 'Submit'),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		$elements = array(
			'execution_date' => array(
				'type' => 'zii.widgets.jui.CJuiDatePicker',
				'attributes' => array(
					'options' => array(
						'dateFormat' => 'd/m/yy',
						'showAnim' => 'fold',
						//'defaultDate' => date('j/n/Y'),
						//'changeYear' => false,
						//'changeMonth' => true,
						//'yearRange' => '1900',
					),
				),
			),
			'tracker' => array(
				'type' => 'checkbox',
			),
			'real_power' => array(
				'type' => 'text',
			),
			'ypostathmos' => array(
				'type' => 'text',
				'attributes' => array(
					'size' => 60,
					'maxlength' => 60,
				),
			),
			'grammi_dianomis' => array(
				'type' => 'text',
				'attributes' => array(
					'size' => 30,
					'maxlength' => 30,
				),
			),
			'thesi_syndesis' => array(
				'type' => 'text',
				'attributes' => array(
					'size' => 30,
					'maxlength' => 30,
				),
			),
			'anaxorisi' => array(
				'type' => 'text',
				'attributes' => array(
					'size' => 60,
					'maxlength' => 60,
				),
			),
			'ys_mt_xt' => array(
				'type' => 'text',
				'attributes' => array(
					'size' => 60,
					'maxlength' => 60,
				),
			),
		);
		
		return array(
			'type' => 'form',
		    //'title' => '......',
		    //'showErrorSummary' => true,
		    'elements' => $elements,
		);
	}
}