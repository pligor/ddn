<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel {
	public $name;
	public $email;
	public $subject;
	public $body;
	public $verifyCode;
	
	public $buttonLabel;
	
	/**
	 * @see CFormModel::init()
	 */
	public function init() {
		$this->buttonLabel = Yii::t('', 'Send message');
	}

	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			// name, email, subject and body are required
			array('name, email, subject, body', 'required'),
			// email has to be a valid email address
			array('email', 'email'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'name' => Yii::t('', 'Full Name'),
			'email' => Yii::t('', 'Email'),
			'subject' => Yii::t('', 'Subject'),
			'body' => Yii::t('', 'Body'),
			'verifyCode' => Yii::t('', 'Verification Code'),
		);
	}
	
	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to defile a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				get_class() => $this->getFormConfig(),
			),
			'buttons'=>array(
		        'send_button' => array(
		            'type'=>'submit',
		            'label' => $this->buttonLabel,
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data', //ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		$elements = array(
	    	'name' => array(
				'type' => 'text',
			),
			'email' => array(
				'type' => 'text',
			),
			'subject' => array(
				'type' => 'text',
				'attributes' => array(
					'size'=>60,
					'maxlength'=>128,
				),
			),
			'body' => array(
				'type' => 'textarea',
				'attributes' => array(
					'rows' => 6,
					'cols' => 50,
				),
			),
		);
		
		if(CCaptcha::checkRequirements()) {	//Checks if GD with FreeType support is loaded
			$elements['verifyCode'] = array(
				'type' => 'application.widgets.inputwidgets.CaptchaInputWidget',
				'hint' => Yii::t('', 'Please enter the letters (not case-sensitive) as they are shown in the image'),
			);
		}
		
		return array(
			'type' => 'form',
		    //'title' => '.......',
		    //'showErrorSummary' => true,
		    'elements' => $elements,
		);
	}
}