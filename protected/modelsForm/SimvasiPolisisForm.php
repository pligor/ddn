<?php

/**
 * SimvasiPolisisForm class
 * @property array $classes
 * @property array $models
 */
class SimvasiPolisisForm extends MultiModelForm {
	public $pv_plant_id;
	
	/**
	 * Initialize model before executing setScenario inside parent's construct
	 */
	public function __construct($scenario='insert') {
		$classes = array(
			'ProcedureStep',
			'SimvasiPolisis',
		);
		
		$name = 'ReadExcelRowColumns';
		$behavior = new $name();
		$this->attachBehavior($name, $behavior);
		
		parent::__construct($classes,$scenario);
	}
	
	public function getTitle() {
		$class = $this->id;
		return $class::getTitle();
	}

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		$title = self::getTitle();
		
		return array(
			array(
				'execution_date',
				'match',
				'pattern' => "@^[0-9]{1,2}[/][0-9]{1,2}[/][0-9]{4}$@",
				'message' => Yii::t('', 'Date with format') .' '. Yii::app()->params['adminBirthday'],
				'allowEmpty' => false,
				'on' => 'insert, update',
			),
			array(
				'execution_date',
				'ext.PligorValidators.xldate',
				'allowEmpty' => false,
				'emptyMessage' => Yii::t('', 'Date is empty') ." ($title)",
				'wrongDate' => Yii::t('', 'Date {title} does not have the format',array('{title}'=>$title)) .' '. Yii::app()->params['adminBirthday'],
				'on' => 'excel',
			),
			array(
				'conn_compl_date',
				'match',
				'pattern' => "@^[0-9]{1,2}[/][0-9]{1,2}[/][0-9]{4}$@",
				'message' => Yii::t('', 'Date with format') .' '. Yii::app()->params['adminBirthday'],
				'allowEmpty' => true,
				'on' => 'insert, update',
			),
			array(
				'conn_compl_date',
				'ext.PligorValidators.xldate',
				'allowEmpty' => true,
				'emptyMessage' => Yii::t('', 'Completed Connection Works Date is empty'),
				'wrongDate' => Yii::t('', 'Completed Connection Works Date is not of the format'). ' '. Yii::app()->params['adminBirthday'],
				'on' => 'excel',
			),
		);
	}
	
	public function getId() {
		return substr(get_class(), 0, -strlen('Form'));
	}
	
	/**
	 * Save all models
	 */
	public function save() {
		$transaction = Yii::app()->db->beginTransaction();
		
		try {		//you must save in a specific order because of foreign key constraints
			$pvPlantModel = CHtmlEx::loadModel('PvPlant', $this->pv_plant_id);
			
			$model = $this->models['ProcedureStep'];
			$model->pv_plant_id = $this->pv_plant_id;
			
			$curTitle = self::getTitle();
			$prevClass = 'SimvasiSindesis';
			$prevTitle = $prevClass::getTitle();
			
			$prevProcedureStep = $model->getSibling($prevClass);		
			if($prevProcedureStep===null) {
				$message = Yii::t('', 'You are trying to insert data for the step {step} while there are missing data (at least) for the previous step ({prev_step})',array(
					'{step}' => $curTitle,
					'{prev_step}' => $prevTitle,
				));
				throw new Exception($message);
			}
			elseif($prevProcedureStep->execution_date > $this->execution_date) {
				$message = Yii::t('', 'The date of the step {step} is older than the date of the previous step {prev_step}',array(
					'{step}' => $curTitle,
					'{prev_step}' => $prevTitle,
				));
				throw new Exception($message);
			}
			
			if( !$model->save(false) ) {
				$message = Yii::t('', 'saving in table {tableName} failed',array(
					'{tableName}' => $model->tableName(),
				));
				throw new Exception($message);	
			}
			
			$model = $this->models['SimvasiPolisis'];
			$model->procedure_step_id = $this->models['ProcedureStep']->id;
			$model->simvasi_sindesis_procedure_step_id = $prevProcedureStep->id;
			
			if( !$model->save(false) ) {
				$message = Yii::t('', 'saving in table {tableName} failed',array(
					'{tableName}' => $model->tableName(),
				));
				throw new Exception($message);
			}

			$transaction->commit();
		}
		catch(Exception $e) {
		    $transaction->rollBack();
		    throw new Exception( $e->getMessage(), $e->getCode() );	//rethrow
		}
	}
	
	public function readExcel(& $sheet, $i) {
		if( !$this->checkModels() ) return false;
		
		$this->setScenario('excel');
		
		$warnings = array();
		$error = null;
		
		try {
			$model = new DDNxl();
			$this->pv_plant_id = $model->getPvPlantId($sheet, $i);

			$models = $this->models;
			foreach($this->classes as $class) {
				$models[$class]->attributes = $this->readCols($class, $sheet, $i);
			}
			$this->models = $models;
			
			if( CHtmlEx::isEmpty($this->execution_date) ) {
				$message = Yii::t('', 'skip step {step} because it is empty',array(
					'{step}' => $this->id,
				));
				throw new Exception($message, 1);
			}
			
			$this->validate();

			foreach($this->errors as $attrErrors) {
				foreach($attrErrors as $attrError) {
					$warnings[] = $attrError;
				}
			}
			
			$this->save();
		}
		catch(Exception $e) {
			if( $e->getCode() != 1 ) {	//1 is the occasion where it is expected
				//$error = "EXCEPTION ERROR inside readExcel inside {$this->id}: ". $e->getMessage();
				$error = $e->getMessage();
			}
			$warnings = array(); //no need to have warnings for a line with an exception
		}
		
		return compact('warnings','error');
	}

	public function attributeLabels() {
		return $this->getAttrLabels();
	}

	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to define a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				$this->id => $this->getFormConfig(),
			),
			'buttons'=>array(
		        'submit' => array(
		            'type'=>'submit',
		            'label' => Yii::t('', 'Submit'),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		$elements = array(
			'execution_date' => array(
				'type' => 'zii.widgets.jui.CJuiDatePicker',
				'attributes' => array(
					'options' => array(
						'dateFormat' => 'd/m/yy',
						'showAnim' => 'fold',
						//'defaultDate' => date('j/n/Y'),
						//'changeYear' => false,
						//'changeMonth' => true,
						//'yearRange' => '1900',
					),
				),
			),
			'conn_compl_date' => array(
				'type' => 'zii.widgets.jui.CJuiDatePicker',
				'attributes' => array(
					'options' => array(
						'dateFormat' => 'd/m/yy',
						'showAnim' => 'fold',
						//'defaultDate' => date('j/n/Y'),
						//'changeYear' => false,
						//'changeMonth' => true,
						//'yearRange' => '1900',
					),
				),
			),
		);
		
		return array(
			'type' => 'form',
		    //'title' => '......',
		    //'showErrorSummary' => true,
		    'elements' => $elements,
		);
	}
}