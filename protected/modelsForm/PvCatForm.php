<?php
class PvCatForm extends CFormModel {
	public $pvCats;
	public $buttonLabel;
	
	/**
	 * @see CFormModel::init()
	 */
	public function init() {
		parent::init();
			
		$this->buttonLabel = Yii::t('', 'Choose');
	}
	
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'pvCats' => Yii::t('', 'PV Category'),
		);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::rules()
	 */
	public function rules() {
		return array(
			/*array(
				'pvCats',
				'required',
			),*/
			array(
				'pvCats',
				'ext.PligorValidators.ArrayValidator',
				'validatorClass' => 'CRangeValidator',
				'params' => array(
					'allowEmpty' => false,
					'range' => array_keys( PvPlant::getCats() ),
				),
				'separateParams' => false,
				'allowEmpty' => false,
			),
		);
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => '..........',
		    //'showErrorSummary' => true,
		    'elements' => array(
		    	'pvCats' => array(
					'type' => 'checkboxlist',
					'items' => PvPlant::getCatDescs(),
					'hint' => Yii::t('', 'please choose the combination of categories you wish'),
		        ),
			),
		);
	}
	
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				get_class() => $this->formConfig,
			),
			'buttons'=>array(
		        'submit_cats' => array(
		            'type'=>'submit',
		            'label' => $this->buttonLabel,
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		
		return new CForm($config,$this);	//all subforms get as parent their model
	}
}