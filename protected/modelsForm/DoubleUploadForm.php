<?php
/**
 * DoubleUploadForm class
 * at the swap below if you swap the comments of the two lines then you get a new excel
 * but be careful
 */
class DoubleUploadForm extends CFormModel {
	const format = 'Excel5';
	const colorPrefix = '#';
	
	public $fileAttrs;
	
	public $oldFile;
	public $newFile;
	
	/**
	 * @var string
	 */
	protected $saveDir;
	
	/**
	 * @var string
	 */
	public $type;
	
	/**
	 * @var integer
	 */
	public $startRow;
	/**
	 * @var integer
	 */
	public $endRow;
	/**
	 * @var string of the format #0077FF
	 */
	public $changed;
	/**
	 * @var string of the format #0077FF
	 */
	public $added;
	/**
	 * @var string of the format #0077FF
	 */
	public $deleted;
	
	public function getColumnStr() {
		$cols = PHPExcel_CellHelper::columnIndicesFromStrings($this->columns);
		
		$notation = new Notation();
		$notation->getCompact($cols);
		$notation->convNumbers_xlCols();
		
		return $notation->notation;
	}
	
	/**
	 * 
	 * @param string $value
	 */
	public function setColumnStr($value) {
		$notation = new Notation();
		$notation->notation = $value;
		$notation->convNumbers_xlCols(true);
		
		$this->columns = PHPExcel_CellHelper::stringsFromColumnIndices($notation->expand());
	}
	
	/**
	 * @var PHPExcel_Style_Color[]
	 */
	public $_colors;
	
	/**
	 * @var PHPExcel[]
	 */
	public $phpExcels;
	
	public $columns;
	
	/**
	 * @var PHPExcel the output compared php excel object
	 */
	public $phpExcel;
	
	/**
	 * @var string[] titles of the worksheets being compared
	 */
	public $sheetTitles = array();
	
	/**
	 * @see CFormModel::init()
	 */
	public function init() {
		$this->type = 'xls';
		$this->phpExcels = array();
		$this->fileAttrs = array(
			'oldFile',
			'newFile',
		);
		
		CHtmlEx::loadPHPExcel();
		
		$this->phpExcel = new PHPExcel();
		$this->phpExcel->removeSheetByIndex();
		
		$this->_colors = array(
			'changed' => new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ),
			'added' => new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_GREEN ),
			'deleted' => new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_BLUE ),
		);
		CHtmlEx::unLoadPHPExcel();
		
		foreach($this->_colors as $attr => $color) {
			$this->$attr = self::colorPrefix . $color->getRGB();
		}
		
		$this->saveDir = sys_get_temp_dir();
		
		//$this->columns = $this->getColumns('A', 'AI');
		$this->startRow = 1;
		$this->endRow = null;	//null means all the rows
		$this->columns = array('A');
	}
	
	public function setColors() {
		$opacity = 'FF';
		foreach($this->_colors as $attr => $color) {
			$this->_colors[$attr] = new PHPExcel_Style_Color( $opacity. trim($this->$attr,self::colorPrefix) );
		}
	}
	
	/**
	 * NOTE: WILL COMPARE ONLY SHEETS WITH SAME SHEET NAME!! (common sheetnames)
	 */
	public function compPhpExcels() {
		$old = reset($this->fileAttrs);
		$new = end($this->fileAttrs);
		$sheetNamesOld = $this->phpExcels[$old]->getSheetNames();
		$sheetNamesNew = $this->phpExcels[$new]->getSheetNames();
		
		$commonSheetNames = array_intersect($sheetNamesOld, $sheetNamesNew);
		
		//@SWAP
		$this->phpExcel = $this->phpExcels[$new];
		
		foreach($commonSheetNames as $sheetName) {
			$sheetOld = $this->phpExcels[$old]->getSheetByName($sheetName);
			$sheetNew = $this->phpExcels[$new]->getSheetByName($sheetName);
			
			//PHPExcelHelper::createSheetByName($this->phpExcel, $sheetName);/////
			
			$this->compSheet($sheetOld, $sheetNew);
		}
	}
	
	/**
	 * @param PHPExcel_Worksheet $sheetOld
	 * @param PHPExcel_Worksheet $sheetNew
	 */
	private function compSheet(& $sheetOld, & $sheetNew) {
		$rowItOld = $sheetOld->getRowIterator();
		$rowItNew = $sheetNew->getRowIterator();
		
		$i=0;
		while( $rowItOld->valid() || $rowItNew->valid() )
		{
			//$i = $rowIter->key();
			if( ($i++) < $this->startRow ) {
				continue;	//ignore headers	
			}
			
			$rowOld = $rowItOld->current();
			$rowNew = $rowItNew->current();
			
			$this->compRow($rowOld, $rowNew);
			
			$rowItOld->next();
			$rowItNew->next();
		}
	}
	
	/**
	 * @param PHPExcel_Worksheet_Row $rowOld
	 * @param PHPExcel_Worksheet_Row $rowNew
	 */
	private function compRow(& $rowOld, & $rowNew) {
		$cellItOld = $rowOld->getCellIterator();
		$cellItNew = $rowNew->getCellIterator();
		
		//true here means skip empty cells which is not what we want
		$cellItOld->setIterateOnlyExistingCells(false);
		$cellItNew->setIterateOnlyExistingCells(false);
		
		while( $cellItOld->valid() || $cellItNew->valid() )	//note: OR means all columns
		{
			$cellOld = $cellItOld->current();
			$cellNew = $cellItNew->current();
			
			$this->compCell($cellOld, $cellNew);
			
			$cellItOld->next();
			$cellItNew->next();
		}
	}
	
	/**
	 * @param PHPExcel_Cell $cellOld
	 * @param PHPExcel_Cell $cellNew
	 */
	private function compCell(& $cellOld, & $cellNew) {
		$valOld = $cellOld->getValue();
		$valNew = $cellNew->getValue();
		
		//@SWAP
		$targetCell = & $cellNew;/////
		//$targetCell = $this->phpExcel->getActiveSheet()->getCell( $cellNew->getCoordinate() );/////
	
		if(CHtmlEx::isEmpty($valOld) && !CHtmlEx::isEmpty($valNew)) {
			$this->fillColorCell($targetCell, $this->_colors['added']);
		}
		elseif(!CHtmlEx::isEmpty($valOld) && CHtmlEx::isEmpty($valNew)) {
			$this->fillColorCell($targetCell, $this->_colors['deleted']);
		}
		elseif($valOld != $valNew) {
			$this->fillColorCell($targetCell, $this->_colors['changed']);
		}
		
		//$targetCell->setValue( $cellNew->getCalculatedValue() );
		$targetCell->setDataValidation();	//clear data validation
	}
	
	/**
	 * @param PHPExcel_Cell $cell
	 * @param PHPExcel_Style_Color $color
	 */
	private function fillColorCell(& $cell, $color) {
		CHtmlEx::loadPHPExcel();
		$cell->getParent()->getStyle($cell->getCoordinate())
				->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->setStartColor($color);
		CHtmlEx::unLoadPHPExcel();
	}
	
	/**
	 * @param string $fullpath
	 */
	public function writeCompared($fullpath) {
		//$this->phpExcel = $this->phpExcels[end($this->fileAttrs)];
		
		try {
			$objWriter = PHPExcel_IOFactory::createWriter($this->phpExcel, self::format);
			
			if( is_file($fullpath) && !unlink($fullpath) ) {
				throw new Exception();
			}
			
			$objWriter->save($fullpath);
		}
		catch (Exception $e) {
			$message = Yii::t('', 'Error while trying to write compared excel file') .' ('. $e->getMessage() .')';
			throw new Exception($message);
		}
		
		$this->unlinkBoth();
	}

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array(
				array('changed','added','deleted'),
				'match',			//replace with RGB color validator
				'pattern' => '@^#[0-9A-F]{6}$@',
			),
			array(
				array('columnStr'),
				'safe',
			),
			array(
				'oldFile',
				'file',
				'allowEmpty' => false,
				'maxFiles' => 1,
				//'tooMany' => 'some error message',
				'maxSize' => 10*(1024*1024),	//estw 10MB to megisto megethos arxeiou
				//'tooLarge' => 'some error message',
				'minSize' => 1024,	//estw 1KB (an kai to megethos tou arxeiou mporei na einai mikrotero
				//'tooSmall' => 'some error message',
				'types' => $this->type,	//extension names separated by space or comma
				//'wrongType' => 'some error message',
			),
			array(
				'newFile',
				'file',
				'allowEmpty' => false,
				'maxFiles' => 1,
				//'tooMany' => 'some error message',
				'maxSize' => 10*(1024*1024),	//estw 10MB to megisto megethos arxeiou
				//'tooLarge' => 'some error message',
				'minSize' => 1024,	//estw 1KB (an kai to megethos tou arxeiou mporei na einai mikrotero
				//'tooSmall' => 'some error message',
				'types' => $this->type,	//extension names separated by space or comma
				//'wrongType' => 'some error message',
			),
			array(
				'startRow',
				'numerical',
				'integerOnly' => true,
				'min' => 1,
				'allowEmpty' => false,
			),
			array(
				'endRow',
				'numerical',
				'integerOnly' => true,
				'min' => 1,
				'allowEmpty' => true,
			),
			array(
				'endRow',
				'default',
				'setOnEmpty' => true,
				'value' => null,
			),
		);
	}
	
	/**
	 * @return bool
	 */
	protected function checkFiles() {
		foreach($this->fileAttrs as $fileAttr) {
			if( !is_object($this->$fileAttr) ) {
				return false;
			}
			if( !($this->$fileAttr instanceof CUploadedFile) ) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @return string[]
	 */
	public function getPaths() {
		$paths = array();
		foreach($this->fileAttrs as $fileAttr) {
			$paths[$fileAttr] = $this->getPath($fileAttr);
		}
		return $paths;
	}
	
	/**
	 * @param string $fileAttr
	 * @return string
	 */
	public function getPath($fileAttr) {
		return $this->saveDir .'/'. $fileAttr .'.'. $this->type;
	}
	
	/**
	 * @return bool
	 * @throws Exception
	 */
	public function saveBoth() {
		if( !$this->checkFiles() ) {
			return false;	
		}
		
		//$paths = array();
		foreach($this->fileAttrs as $fileAttr) {
			$paths[$fileAttr] = $this->saveDir .'/'. $fileAttr .'.'. $this->type;
			
			//if( is_file($paths[$fileAttr]) && !unlink($paths[$fileAttr]) ) {
			if( is_file($this->getPath($fileAttr)) && !unlink($this->getPath($fileAttr)) ) {
				return false;
			}
			
			//if( !$this->$fileAttr->saveAs($paths[$fileAttr]) ) {
			if( !$this->$fileAttr->saveAs($this->getPath($fileAttr)) ) {
				$message = Yii::t('', 'Saving file {file_path} has failed',array(
					'{file_path}' => $paths[$attribute],
				));
				throw new Exception($message);
			}
		}
		
		//return $paths;
		return true;
	}
	
	/**
	 * Enter description here ...
	 * @throws CHttpException
	 */
	public function loadBoth() {
		CHtmlEx::loadPHPExcel();
		$filterSubset = new ExcelReadFilter($this->startRow, $this->endRow, $this->columns);
		CHtmlEx::unLoadPHPExcel();
		
		$objReader = PHPExcelHelper::getObjReader($this->sheetTitles, $filterSubset);
		
		try {
			foreach($this->getPaths() as $key => $path) {				
				$this->phpExcels[$key] = $objReader->load($path);
			}
		}
		catch(Exception $e) {
			throw new CHttpException(404, $e->getMessage());	//rethrow
		}
	}
	
	public function unlinkBoth() {
		foreach($this->getPaths() as $path) {
			if(is_file($path) && !unlink($path)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'oldFile' => Yii::t('', 'Older File'),
			'newFile' => Yii::t('', 'Newer File'),
			
			'startRow' => Yii::t('', 'Start Row'),
			'endRow' => Yii::t('', 'End Row'),
		
			'changed' => Yii::t('', 'Change Color'),
			'added' => Yii::t('', 'Added Color'),
			'deleted' => Yii::t('', 'Deletion Color'),
			
			'columnStr' => Yii::t('', 'Columns abbreviation'),
		);
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		$elements = array(
	    	'oldFile' => array(
				'type' => 'file',
				'hint' => Yii::t('', 'please choose the older (in time) file'),
			),
			'newFile' => array(
				'type' => 'file',
				'hint' => Yii::t('', 'please choose the newer (in time) file'),
			),
			'startRow' => array(
				'type' => 'text',
			),
			'endRow' => array(
				'type' => 'text',
				'hint' => Yii::t('', 'if left empty comparison will continue till the last line of the worksheet'),
			),
			'columnStr' => array(
				'type' => 'text',
				'hint' => 'A,B:D,AA,AB:AD',
			),
		);
		
		foreach($this->_colors as $color => $val) {
			$elements[$color] = array(
				'type' => 'external.farbtastic.Farbtastic',
			);
		}
		
		return array(
			'type' => 'form',
		    //'title' => '..........',
		    //'showErrorSummary' => true,
		    'elements' => $elements,
		);
	}
	
	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to defile a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				get_class() => $this->getFormConfig(),
			),
			'buttons'=>array(
		        'submit_files'=>array(
		            'type' => 'submit',
		            'label' => Yii::t('', 'Compare Files'),
		        ),
		    ),
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',
		    ),
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
}