<?php
/**
 * SingleUploadForm class
 * @property file $file
 * @property mixed $types	(This can be either an array or a string consisting of file extension names separated by space or comma)
 * @property int $minSize
 * @property int $maxSize
 * @property string $buttonLabel
 * @property string $hint
 * @property string $saveDir
 */
class SingleUploadForm extends CFormModel {
	public $file;
	public $types;
	public $minSize;
	public $maxSize;
	public $buttonLabel;
	public $hint;
	
	protected $saveDir;
	
	public function getId() {
		return mb_strtolower(get_class());
	}
	
	/**
	 * @see CFormModel::init()
	 */
	public function init() {
		$this->types = 'xls';
		$this->minSize = 1024;	//1KB
		$this->maxSize = 10*(1024*1024);	//10MB
		
		$this->buttonLabel = "Κατάθεση αρχείου";
		$this->hint = null;
		
		$this->saveDir = sys_get_temp_dir();
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @throws Exception
	 */
	public function saveFile() {
		if( !($this->file instanceof CUploadedFile) ) {
			$message = Yii::t('', 'model does not contain a valid file');
			throw new Exception($message);
		}
		
		$path = $this->saveDir .'/'. $this->file->name;
		
		if(is_file($path) && !unlink($path)) {
			$message = "Η επαναποθήκευση του αρχείου $path απέτυχε";
			throw new Exception($message);
		}
		
		if( !$this->file->saveAs($path) ) {
			$message = "Η αποθήκευση του αρχείου $path απέτυχε";
			throw new Exception($message);
		}
		
		return $path;
	}

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array(
				'file',
				'file',
				'allowEmpty' => false,
				'maxFiles' => 1,
				//'tooMany' => 'some error message',
				'maxSize' => $this->maxSize,
				//'tooLarge' => 'some error message',
				'minSize' => $this->minSize,
				//'tooSmall' => 'some error message',
				'types' => $this->types,	//extension names separated by space or comma
				//'wrongType' => 'some error message',
			),
		);
	}
	
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'file' => 'Αρχείο',
		);
	}
	
	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to define a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				$this->id => $this->getFormConfig(),
			),
			'buttons'=>array(
		        'submit_file' => array(
		            'type'=>'submit',
		            'label' => $this->buttonLabel,
		        ),
		    ),
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => 'Υποφόρμα',
		    //'showErrorSummary' => true,
		    'elements' => array(
		    	'file' => array(
					'type' => 'file',
					'hint' => $this->hint,
				),
			),
		);
	}
}