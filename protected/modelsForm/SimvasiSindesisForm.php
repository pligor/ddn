<?php

/**
 * SimvasiSindesisForm class
 * @property array $classes
 * @property array $models
 */
class SimvasiSindesisForm extends MultiModelForm {	
	public $pv_plant_id;
	
	/**
	 * Initialize model before executing setScenario inside parent's construct
	 */
	public function __construct($scenario='insert') {
		$classes = array(
			'ProcedureStep',
			'SimvasiSindesis',
		);
		
		$name = 'ReadExcelRowColumns';
		$behavior = new $name();
		$this->attachBehavior($name, $behavior);
		
		parent::__construct($classes,$scenario);
	}

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		$class = $this->id;
		$title = $class::getTitle();
		
		return array(
			array(
				'execution_date',
				'match',
				'pattern' => "@^[0-9]{1,2}[/][0-9]{1,2}[/][0-9]{4}$@",
				'message' => 'Ημερομηνία της μορφής 16/1/1984',
				'allowEmpty' => false,
				'on' => 'insert, update',
			),
			array(
				'execution_date',
				'ext.PligorValidators.xldate',
				'allowEmpty' => false,
				'emptyMessage' => "Η ημερομηνία είναι κενή ($title)",
				'wrongDate' => "Η ημερομηνία ($title) δεν έχει τη μορφή 16/1/1984",
				'on' => 'excel',
			),
			array(
				'arithmos_paroxis',
				'length',
				'max' => SimvasiSindesis::arithmos_paroxis_max_len,
				'allowEmpty' => true,
			),
			array(
				'arithmos_paroxis',
				'numerical',
				'integerOnly' => true,
				'allowEmpty' => true,
			),
                        array(
				'arithmos_paroxis',
				'unique',
				'className' => 'SimvasiSindesis',
			),
		);
	}
	
	public function getId() {
		return substr(get_class(), 0, -strlen('Form'));
	}
	
	/**
	 * Save all models
	 */
	public function save() {
		$transaction = Yii::app()->db->beginTransaction();
		
		try {		//you must save in a specific order because of foreign key constraints
			$pvPlantModel = CHtmlEx::loadModel('PvPlant', $this->pv_plant_id);
			
			$model = $this->models['ProcedureStep'];
			$model->pv_plant_id = $this->pv_plant_id;
			
			$prevProcedureStep = $model->getSibling('DiatiposiOron');
			if($prevProcedureStep===null) {
				$message = 'Προσπαθείτε να εισάγετε στοιχεία στη φάση Σύμβαση Σύνδεσης ενώ απουσιάζουν στοιχεία (τουλάχιστον) του προηγούμενου βήματος';
				throw new Exception($message);
			}
			elseif($prevProcedureStep->execution_date > $this->execution_date) {
				$message = 'Η ημερομηνία της φάσης Σύμβασης Σύνδεσης είναι προγενέστερη της ημερομηνίας της φάσης Διατύπωσης Όρων';
				throw new Exception($message);
			}
			
			if( !$model->save(false) ) {
				throw new Exception('Υπάρχει πρόβλημα με το ProcedureStep');	
			}
			
			$model = $this->models['SimvasiSindesis'];
			$model->procedure_step_id = $this->models['ProcedureStep']->id;
			$model->diatiposi_oron_procedure_step_id = $prevProcedureStep->id;
			
			if( !$model->save(false) ) {
				$message = 'Υπάρχει πρόβλημα με τη φάση SimvasiSindesis';
				throw new Exception($message);
			}
			
			$transaction->commit();
		}
		catch(Exception $e) {
		    $transaction->rollBack();
		    throw new Exception( $e->getMessage(), $e->getCode() );	//rethrow
		}
	}
	
	public function readExcel(& $sheet, $i) {
		if( !$this->checkModels() ) return false;
		
		$this->setScenario('excel');
		
		$warnings = array();
		$error = null;
		
		try {
			$model = new DDNxl();
			$this->pv_plant_id = $model->getPvPlantId($sheet, $i);
			
			$models = $this->models;
			foreach($this->classes as $class) {
				$models[$class]->attributes = $this->readCols($class, $sheet, $i);
			}
			$this->models = $models;
			
			if( CHtmlEx::isEmpty($this->execution_date) ) {
				$message = "skip step {$this->id} because it is empty";
				throw new Exception($message, 1);
			}
			
			$this->validate();

			foreach($this->errors as $attrErrors) {
				foreach($attrErrors as $attrError) {
					$warnings[] = $attrError;
				}
			}
			
			$this->save();
		}
		catch(Exception $e) {
			if( $e->getCode() != 1 ) {	//1 is the occasion where it is expected
				//$error = "EXCEPTION ERROR inside readExcel inside {$this->id}: ". $e->getMessage();
				$error = $e->getMessage();
			}
			$warnings = array(); //no need to have warnings for a line with an exception
		}
		
		return compact('warnings','error');
	}
	
	public function attributeLabels() {
		return $this->getAttrLabels();
	}

	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to define a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				$this->id => $this->getFormConfig(),
			),
			'buttons'=>array(
		        'submit' => array(
		            'type'=>'submit',
		            'label' => Yii::t('', 'Submit'),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		$elements = array(
			'execution_date' => array(
				'type' => 'zii.widgets.jui.CJuiDatePicker',
				'attributes' => array(
					'options' => array(
						'dateFormat' => 'd/m/yy',
						'showAnim' => 'fold',
						//'defaultDate' => date('j/n/Y'),
						//'changeYear' => false,
						//'changeMonth' => true,
						//'yearRange' => '1900',
					),
				),
			),
			'arithmos_paroxis' => array(
				'type' => 'text',
			),
		);
		return array(
			'type' => 'form',
		    //'title' => '......',
		    //'showErrorSummary' => true,
		    'elements' => $elements,
		);
	}
}