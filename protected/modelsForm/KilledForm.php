<?php

/**
 * KilledForm class
 * @author pligor
 * @property int $pv_plant_id
 */
class KilledForm extends MultiModelForm {
	public $pv_plant_id;
	
	/**
	 * @param string $scenario
	 */
	public function __construct($scenario='') {
		$classes = array(
			'Killed',
		);
		
		$name = 'ReadExcelRowColumns';
		$behavior = new $name();
		$this->attachBehavior($name, $behavior);

		parent::__construct($classes,$scenario);
	}

	/**
	 * Declares the validation rules
	 * @see CModel::rules()
	 */
	public function rules() {
		return array(
			array(
				'date_killed',
				'match',
				'pattern' => "@^[0-9]{1,2}[/][0-9]{1,2}[/][0-9]{4}$@",
				'message' => Yii::t('', 'Date with format') .' '. Yii::app()->params['adminBirthday'],
				'allowEmpty' => true,
				'on' => 'insert, update',
			),
			array(
				'date_killed',
				'xldate',
				'allowEmpty' => true,
				'on' => 'excel',
			),
			array(
				'pv_plant_id',
				'numerical',
				'integerOnly' => true,
				'allowEmpty' => false,
				'on' => 'insert, update',
			),
			array(
				'pv_plant_id',
				'numerical',
				'integerOnly' => false,
				'allowEmpty' => false,
				'on' => 'excel',
			),
		);
	}
	
	public function getTitle() {
		$class = $this->id;
		return $class::getTitle();
	}
	
	public function getId() {
		return substr(get_class(), 0, -strlen('Form'));
	}
	
	/**
	 * Save all models
	 * @param int $pv_plant_id
	 */
	public function save($pv_plant_id) {
		$transaction = Yii::app()->db->beginTransaction();
		
		try {
			$model = $this->models['Killed'];
			
			if( ($model->pv_plant_id == 0) || CHtmlEx::isEmpty($model->pv_plant_id) ) {
				$message = Yii::t('', 'This entry is valid');
				throw new Exception($message, 1);
			}
			
			$model->pv_plant_id = $pv_plant_id;
			
			try {
				if( !$model->save(false) ) {	//save can throw an exception itself even before return true or false
					throw new Exception();
				}
			}
			catch(Exception $e) {
				$message = Yii::t('', 'Problem, maybe you tried to release an entry that is already released');
				throw new Exception($message);
			}

			$transaction->commit();
		}
		catch(Exception $e) {
		    $transaction->rollBack();
			throw $e;	//rethrow
		}
	}
	
	public function readExcel(& $sheet, $i) {
		if( !$this->checkModels() ) {
			return false;	
		}
		
		$this->setScenario('excel');
		
		$warnings = array();
		$error = null;
		
		try {
			$model = new DDNxl();
			$pv_plant_id = $model->getPvPlantId($sheet, $i);
			$this->pv_plant_id = $pv_plant_id;

			$models = $this->models;
			foreach($this->classes as $class) {
				//$this->models[$class]->attributes = $this->readCols($class, $sheet, $i);
				$models[$class]->attributes = $this->readCols($class, $sheet, $i);
			}
			$this->models = $models;
			
			$this->validate();
			
			foreach($this->errors as $attrErrors) {
				foreach($attrErrors as $attrError) {
					$warnings[] = $attrError;
				}
			}
			
			$this->save($pv_plant_id);
		}
		catch(Exception $e) {
			if( $e->getCode() != 1 ) {	//1 is the occasion where it is expected
				//$error = "EXCEPTION ERROR inside readExcel inside {$this->id}: ". $e->getMessage();
				$error = $e->getMessage();
			}
			$warnings = array(); //no need to have warnings for a line with an exception
		}
		
		return compact('warnings','error');
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	public function getForm() {
		$config = array(
		    //'title' => 'O titlos ths ypoformas',
		    'showErrorSummary' => true,
		    'elements' => array(
		        'perioxi_dei_id' => array(
		        	'type' => 'dropdownlist',
		        	'items' => PerioxiDei::getPerioxes(),
		        ),
		        'perioxi_file' => array(
		        	'type' => 'file',
		        ),
			),
		    'buttons'=>array(
		        'submit' => array(
					'type' => 'submit',
					'label' => Yii::t('', 'Submit Area file'),
		        ),
		    ),
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',
		    ),
		);
		
		return new CForm($config,$this);
	}
	
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'perioxi_dei_id' => Yii::t('', 'Area'),
			'perioxi_file' => Yii::t('', 'Excel file with Area data'),
		);
	}
}