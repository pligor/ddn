<?php

/**
 * CommentForm class
 * @author pligor
 */
class CommentForm extends MultiModelForm {
	/**
	 * @var int
	 */
	public $pv_plant_id;
	
	public function __construct($scenario='insert') {
		$classes = array(
			'Comment',
		);
		
		$name = 'ReadExcelRowColumns';
		$behavior = new $name();
		$this->attachBehavior($name, $behavior);
		
		parent::__construct($classes,$scenario);
	}

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			array(
				'comment_text',
				'default',
				'setOnEmpty' => true,
				'value' => '',
			),
		);
	}
	
	public function getTitle() {
		$class = $this->id;
		return $class::getTitle();
	}
	
	public function getId() {
		return substr(get_class(), 0, -strlen('Form'));
	}
	
	/**
	 * Save all models
	 */
	public function save() {
		$transaction = Yii::app()->db->beginTransaction();
		
		try {		//you must save in a specific order because of foreign key constraints
			$newLine = "\n\r";
			$commentText = $this->models['Comment']->comment_text;
			
			$comments = array();
			$tok = strtok($commentText, $newLine);
			while($tok !== false) {
				$comments[] = $tok;
				$tok = strtok($newLine);
			}
			
			foreach($comments as $comment) {
				$model = new Comment();
				$model->pv_plant_id = $this->pv_plant_id;
				$model->comment_text = $comment;
				
				if( !$model->save(false) ) {
					$message = Yii::t('', 'saving in table {tableName} failed',array(
						'{tableName}' => $model->tableName(),
					));
					throw new Exception($message);
				}
			}

			$transaction->commit();
		}
		catch(Exception $e) {
		    $transaction->rollBack();
			throw new Exception( $e->getMessage(), $e->getCode() );	//rethrow
		}
	}
	
	public function readExcel(& $sheet, $i) {
		if( !$this->checkModels() ) return false;
		
		$this->setScenario('excel');
		
		$warnings = array();
		$error = null;
		
		try {
			$model = new DDNxl();
			$this->pv_plant_id = $model->getPvPlantId($sheet, $i);

			$models = $this->models;
			foreach($this->classes as $class) {
				//$this->models[$class]->attributes = $this->readCols($class, $sheet, $i);
				$models[$class]->attributes = $this->readCols($class, $sheet, $i);
			}
			$this->models = $models;
			
			$this->validate();
			
			foreach($this->errors as $attrErrors) {
				foreach($attrErrors as $attrError) {
					$warnings[] = $attrError;
				}
			}
			
			$this->save();
		}
		catch(Exception $e) {
			if( $e->getCode() != 1 ) {	//1 is the occasion where it is expected
				//$error = "EXCEPTION ERROR inside readExcel inside {$this->id}: ". $e->getMessage();
				$error = $e->getMessage();
			}
			$warnings = array(); //no need to have warnings for a line with an exception
		}
		
		return compact('warnings','error');
	}
	
	public function attributeLabels() {
		return $this->getAttrLabels();
	}

	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to define a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				$this->id => $this->getFormConfig(),
			),
			'buttons'=>array(
		        'submit' => array(
		            'type'=>'submit',
		            'label' => Yii::t('', 'Submit'),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		$elements = array(
			'comment_text' => array(
				'type' => 'textarea',
				'attributes' => array(
					'rows' => 6,
					'cols' => 50,
				),
			),
		);
		
		return array(
			'type' => 'form',
		    //'title' => '......',
		    //'showErrorSummary' => true,
		    'elements' => $elements,
		);
	}
}