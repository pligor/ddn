<?php

/**
 * @property CForm $form
 * @author pligor
 */
class ApplicationNumberForm extends CFormModel {
	/**
	 * @var string
	 */
	public $applicationNumber;
	
	/**
	 * @var string
	 */
	public $buttonLabel;
	
	/**
	 * @var string
	 */
	public $attributeLabel;
	
	/**
	 * @var string
	 */
	public $hint;
	
	/**
	 * @see CFormModel::init()
	 */
	public function init() {
		parent::init();	
		$this->buttonLabel = Yii::t('', 'Submit');
		$this->attributeLabel = Yii::t('', 'Application Number');
		$this->hint = Yii::t('', 'an existant application number');
	}
	
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'applicationNumber' => $this->attributeLabel,
		);
	}
	
	/**
	 * @see CModel::rules()
	 */
	public function rules() {
		/**
		 * @todo we should replace the "numerical" validator with the "exist" validator
		 * but in order to do that, we should refactor critical parts of the database !! 
		 */
		return array(
			array(
				'applicationNumber',
				'numerical',
				'allowEmpty' => false,
				'integerOnly' => true,
				//'min' => 0,
				//'tooSmall' => 'some error message',
				//'max' => 100,
				//'tooBig' => 'some error message',
			),
		);
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => '..........',
		    //'showErrorSummary' => true,
		    'elements' => array(
		    	'applicationNumber' => array(
					'type' => 'text',
					'hint' => $this->hint,
		        ),
			),
		);
	}
	
	/**
	 * @return CForm
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				get_class() => $this->formConfig,
			),
			'buttons'=>array(
		        'submit_application_number' => array(
		            'type'=>'submit',
		            'label' => $this->buttonLabel,
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		
		return new CForm($config,$this);	//all subforms get as parent their model
	}
}