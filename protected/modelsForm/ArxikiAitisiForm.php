<?php

/**
 * ArxikiAitisiForm class
 * @property array $classes
 * @property array $models
 * @property int pv_plant_id
 */
class ArxikiAitisiForm extends MultiModelForm //CFormModel {
{
	//http://www.yiiframework.com/forum/index.php?/topic/9852-validating-model-array-attributes/page__p__48502__hl__validator+attribute+array#entry48502
	
	/**
	 * Initialize model before executing setScenario inside parent's construct
	 */
	public function __construct($scenario='insert') {
		$classes = array(
			'Prosopo',
			'Address',
			'PvPlant',
			'ProcedureStep',
			'ArxikiAitisi',
		);
		
		$name = 'ReadExcelRowColumns';
		$behavior = new $name();
		$this->attachBehavior($name, $behavior);
		
		//as an exception we put parent call after the code, since we want behavior attached before calling parent construct
		parent::__construct($classes,$scenario);
	}
	
	public function getPv_plant_id() {
		return $this->models['PvPlant']->id;
	}
	
	/**
	 * Get the power range of the class
	 */
	public function getPowerRange() {
		$class = PvPlant::getCats($this->pvCat);
		return $class::getPowerRange();
	}
	
	/**
	 * @see CModel::rules()
	 */
	public function rules() {
		$title = self::getTitle();
		
		return array(
			array(
				array(
					'latitude',
					'longitude',
				),
				'safe',
				'on' => array('insert','update'),
			),
			array(
				'eponimia',
				'required',
			),
			array(
				'eidos_paragogou_id',
				'exist',
				'allowEmpty' => false,
				'attributeName' => 'id',
				'className' => 'EidosParagogou',
				'message' => Yii::t('', 'This Kind of Producer does NOT exist'),
				'on' => array('insert','update'),
			),
			array(
				'eidos_paragogou_id',
				'exist',
				'allowEmpty' => false,
				'attributeName' => 'eidos',
				'className' => 'EidosParagogou',
				'message' => Yii::t('', 'The Kind of Producer has been typed incorrectly'),
				'on' => 'excel',
			),
			array(
				'dimos_id',
				'exist',
				'allowEmpty' => false,
				'attributeName' => 'id',
				'className' => 'Dimos',
				'on' => array('insert','update'),
			),
			array(
				'dimos_id',
				'ext.PligorValidators.ArrayValidator',
				'validatorClass' => 'CExistValidator',
				'params' => array(
					'dimos' => array(
						'allowEmpty' => false,
						'attributeName' => 'dimos_name',
						'className' => 'Dimos',
						'message' => Yii::t('', 'The Municipality is typed incorrectly'),
					),
					'nomos' => array(
						'allowEmpty' => false,
						'attributeName' => 'nomos_name',
						'className' => 'Nomos',
						'message' => Yii::t('', 'The County is typed incorrectly'),
					),
				),
				'separateParams' => true,
				'on' => 'excel',
			),			
			array(
				'thesi',
				'length',
				'allowEmpty' => false,
				'max' => 240,
			),
			array(
				'noumero',
				'numerical',
				'allowEmpty' => false,
				'integerOnly'=>true,
				'on' => array('insert','update'),
			),
			array(
				'noumero',
				'length',
				'on' => array('insert','update'),
				'max' => 10,
			),
			array(		//this is normalized on set attribute, so no need for special check
				'pvCat',
				'in',
				'range' => array(0,1,2),
			),
			array(		//this is normalized on set attribute, so no need for special check
				'protocol',
				'numerical',
				'allowEmpty' => false,
				'integerOnly' => true,
				//'min' => 0,
				//'tooSmall' => 'some error message',
				//'max' => 100,
				//'tooBig' => 'some error message',
			),
			array(
				'execution_date',
				'match',
				'pattern' => "@^[0-9]{1,2}[/][0-9]{1,2}[/][0-9]{4}$@",
				'message' => Yii::t('', 'Date with format') .' '. Yii::app()->params['adminBirthday'],
				'allowEmpty' => false,
				'on' => array('insert','update'),
			),
			array(
				'execution_date',
				'ext.PligorValidators.xldate',
				'emptyMessage' => Yii::t('', 'Date is empty') ." ($title)",
				'wrongDate' => Yii::t('', 'Date {title} does not have the format',array('{title}'=>$title)) .' '. Yii::app()->params['adminBirthday'],
				'allowEmpty' => false,
				'on' => 'excel',
			),
			array(
				'nominal_power',
				'numerical',
				'allowEmpty' => false,
				'integerOnly' => false,
				//'min' => 0,
				//'tooSmall' => 'some error message',
				//'max' => 100,
				//'tooBig' => 'some error message',
			),
			array(
				'nominal_power',
				//'validPower',
				'ext.PligorValidators.validpower',
				'allowEmpty' => false,
				'emptyMessage' => Yii::t('', 'The nominal power is not allowed to be empty'),
			),
		);
	}
	
	public function getTitle() {
		$class = $this->id;
		return $class::getTitle();
	}
	
	public function getId() {
		return substr(get_class(), 0, -strlen('Form'));
	}
	
	/**
	 * Save all models
	 * @param int $island_id
	 */
	public function save($island_id) {
		$transaction = Yii::app()->db->beginTransaction();
		try {		//you must save in a specific order because of foreign key constraints
			
			$model = $this->models['Prosopo'];
			
			try {
				$model->save(false); //save prosopo
			}
			catch(CDbException $e) {	//REMEMBER: a prosopo which already exists, does not bother us
				$attributes = array(
					'eponimia' => $model->eponimia,
				);
				$models = $this->models;
				$models['Prosopo'] = Prosopo::model()->findByAttributes($attributes);
				$this->models = $models;
				
				if($this->models['Prosopo'] == null) {
					$message = Yii::t('', 'While saving {class} has failed, we also failed to found Επωνυμία {eponimia}',array(
						'{class}' => 'Prosopo',
						'{eponimia}' => $model->eponimia,
					));
					throw new Exception($message);
				}
			}
			
			$model = $this->models['Address'];
			
			try {
				$model->save(false); //save address
			}
			catch(CDbException $e) {	//REMEMBER: an address which already exists, does not bother us
				$attributes = array(
					'dimos_id' => $model->dimos_id,
					'thesi' => $model->thesi,
					'noumero' => $model->noumero,
				);
				$models = $this->models;
				$models['Address'] = Address::model()->findByAttributes($attributes);
				$this->models = $models;
				
				if($this->models['Address'] == null) {
					$message = Yii::t('', 'While saving {class} has failed, we also failed to found an address {full_address}',array(
						'{class}' => 'Address',
						'{full_address}' => $model->fullAddress,
					));
					throw new Exception($message);
				}
			}
			
			$model = $this->models['PvPlant'];
			$model->getNewPriority($island_id);
			$model->island_id = $island_id;
			$model->prosopo_id = $this->models['Prosopo']->id;
			$model->address_id = $this->models['Address']->id;
			
			if( !$model->save(false) ) {
				$message = Yii::t('', 'saving in table {tableName} failed',array(
					'{tableName}' => $model->tableName(),
				));
				throw new Exception($message);	
			}
			
			$model = $this->models['ProcedureStep'];
			$model->pv_plant_id = $this->models['PvPlant']->id;
			
			if( !$model->save(false) ) {
				$message = Yii::t('', 'saving in table {tableName} failed',array(
					'{tableName}' => $model->tableName(),
				));
				throw new Exception($message);	
			}
			
			$model = $this->models['ArxikiAitisi'];
			$model->procedure_step_id = $this->models['ProcedureStep']->id;
			
			if( !$model->save(false) ) {
				$message = Yii::t('', 'saving in table {tableName} failed',array(
					'{tableName}' => $model->tableName(),
				));
				throw new Exception($message);	
			}
			
			$transaction->commit();
		}
		catch(Exception $e) {
		    $transaction->rollBack();
		    throw new Exception( $e->getMessage() );	//rethrow
		}
	}
	
	public function readExcel(& $sheet, $i) {
		if( !$this->checkModels() ) return false;
		
		$this->setScenario('excel');
		
		$warnings = array();
		$error = null;
		
		try {
			$models = $this->models;
			foreach($this->classes as $class) {
				$models[$class]->attributes = $this->readCols($class, $sheet, $i);
			}
			$this->models = $models;

			$this->validate();
		
			foreach($this->errors as $attrErrors) {
				foreach($attrErrors as $attrError) {
					$warnings[] = $attrError;
				}
			}
			
			$this->save( $sheet->island_id );
		}
		catch(Exception $e) {
			//$error = "EXCEPTION ERROR inside readExcel inside ArxikiAitisiForm: ". $e->getMessage();
			$error = $e->getMessage();
			
			$warnings = array(); //no need to have warnings for a line with a serious error
		}
		
		return compact('warnings','error');
	}

	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => '......',
		    //'showErrorSummary' => true,
		    'elements' => array(
		    	'eponimia' => array(
					'type' => 'text',
					//'hint' => $this->hint,
				),
				'eidos_paragogou_id' => array(
					'type' => 'dropdownlist',
					'items' => EidosParagogou::getEidi(),
				),
				'dimos_id' => array(
					'type' => 'dropdownlist',
					'items' => Dimos::model()->getDimous(),
				),
				'thesi' => array(
					'type' => 'text',
					'attributes' => array(
						'size'=>60,
						'maxlength'=>240,
					),
				),
				'noumero' => array(
					'type' => 'text',
					'attributes' => array(
						'size'=>10,
						'maxlength'=>10,
					),
				),
				'latitude' => array(
					'type' => 'text',
				),
				'longitude' => array(
					'type' => 'text',
				),
				'pvCat' => array(
					'type' => 'dropdownlist',
					'items' => PvPlant::getCatDescs(),
				),
				'protocol' => array(
					'type' => 'text',
				),
				'execution_date' => array(
					'type' => 'zii.widgets.jui.CJuiDatePicker',
					'attributes' => array(
						'options' => array(
							'dateFormat' => 'd/m/yy',
							'showAnim' => 'fold',
							//'defaultDate' => date('j/n/Y'),
							//'changeYear' => false,
							//'changeMonth' => true,
							//'yearRange' => '1900',
						),
					),
				),
				'nominal_power' => array(
					'type' => 'text',
				),
			),
		);
	}
	
	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to define a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=> array(
				$this->id => $this->getFormConfig(),
			),
			'buttons'=>array(
		        'submit' => array(
		            'type'=>'submit',
		            'label' => Yii::t('', 'Submit'),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',		//ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	public function attributeLabels() {
		return $this->getAttrLabels();
	}
}