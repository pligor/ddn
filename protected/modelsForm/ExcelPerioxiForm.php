<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'BouncerController'.
 */
class ExcelPerioxiForm extends CFormModel {
	/**
	 * @var integer
	 */
	public $perioxi_dei_id;
	
	/**
	 * @var CUploadedFile
	 */
	public $perioxi_file;
	
	/**
	 * @param string $filepath the path of the excel file
	 */
	public function hasIslands($filepath) {
		$sheetNames = PHPExcelHelper::getObjReader()->listWorksheetNames($filepath);
		$listDataIslands = CHtmlEx::loadModel('PerioxiDei', $this->perioxi_dei_id)->listDataIslands;
		$sheets = array_intersect($sheetNames, $listDataIslands);
		return !empty($sheets);
	}
	
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			array(
				'perioxi_dei_id',
				'exist',
				'allowEmpty' => false,
				'attributeName' => 'id',
				'className' => 'PerioxiDei',
			),
			array(
				'perioxi_file',
				'file',
				'allowEmpty' => false,
				'maxFiles' => 1,
				//'tooMany' => 'some error message',
				'maxSize' => 10*(1024*1024),	//estw 10MB to megisto megethos arxeiou
				//'tooLarge' => 'some error message',
				'minSize' => 1024,	//estw 1KB (an kai to megethos tou arxeiou mporei na einai mikrotero
				//'tooSmall' => 'some error message',
				'types' => 'xls',	//extension names separated by space or comma
				//'wrongType' => 'some error message',
			),
		);
	}
	
	public function getForm() {
		$config = array(
		    //'title' => 'O titlos ths ypoformas',
		    'showErrorSummary' => true,
		    'elements' => array(
		        'perioxi_dei_id' => array(
		        	'type' => 'dropdownlist',
		        	'items' => PerioxiDei::getPerioxes(),
					'hint' => Yii::t('', 'all Area data will be wiped before loading'),
		        ),
		        'perioxi_file' => array(
		        	'type' => 'file',
		        ),
			),
		    'buttons'=>array(
		        'submit' => array(
					'type' => 'submit',
					'label' => Yii::t('', 'Submit Area file'),
		        ),
		    ),
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',
		    ),
		);
		
		return new CForm($config,$this);
	}
	
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'perioxi_dei_id' => Yii::t('', 'Area'),
			'perioxi_file' => Yii::t('', 'Excel file with Area data'),
		);
	}
}