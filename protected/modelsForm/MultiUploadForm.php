<?php

/**
 * MultiUploadForm class
 * @property $files
 * @property string $accepted
 */
class MultiUploadForm extends CFormModel
{
	public $files;
	public $config;
	
	private $saveDir;
	private $accepted;
	
	/**
	 * @see CFormModel::init()
	 */
	public function init() {
		$this->accepted = 'xls';
		$this->saveDir = sys_get_temp_dir();
		$this->config = 'full';
	}
	
	protected function checkFiles() {
		if( !is_array($this->files) ) return false;
		foreach($this->files as $file) {
			if( !is_object($file) ) return false;
			if( !($file instanceof CUploadedFile) ) return false;
		}
		return true;
	}
	
	public function saveAll() {
		if( !$this->checkFiles() ) {
			return;	
		}
		
		$paths = array();
		foreach($this->files as $file) {
			$suffix = '.'.$this->accepted;
			//$name = basename($file->name,$suffix);	//basename does NOT work for greek characters
			$name = mb_substr($file->name, 0, -mb_strlen($suffix, Yii::app()->charset), Yii::app()->charset);//,$length, $encoding);
			
			$index = PerioxiDei::getModelByName($name)->id;
			
			$paths[$index] = $this->saveDir .'/'. $file->name;
			
			if(is_file($paths[$index])) {
				unlink($paths[$index]);
			}
			
			if( !$file->saveAs($paths[$index]) ) {
				$message = Yii::t('', 'Saving file {file_path} has failed',array(
					'{file_path}' => $paths[$index],
				));
				throw new Exception($message);
			}
		}
		
		return $paths;
	} 

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			array(
				'files',
				'file',
				'allowEmpty' => false,
				'maxFiles' => 14,
				//'tooMany' => 'some error message',
				'maxSize' => 10*(1024*1024),	//10MB
				//'tooLarge' => 'some error message',
				'minSize' => 1024,	//1KB
				//'tooSmall' => 'some error message',
				//'types' => $this->types,	//extension names separated by space or comma
				//'wrongType' => 'some error message',
			),
			array(
				'config',
				'in',
				'allowEmpty' => false,
				'range' => array_keys( DDNxl::getConfigs_sheets() ),
			),
		);
	}
	
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'files' => Yii::t('', 'Files'),
			'config' => Yii::t('', 'Configuration'),
		);
	}
	
	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to defile a model for the subform separately
	 */
	public function getForm() {
		$config = array(
			'showErrorSummary' => true,
			'elements'=>array(
				'subform' => $this->getFormConfig(),
			),
			'buttons' => array(
		        'submit_files'=>array(
		            'type' => 'submit',
		            'label' => Yii::t('', 'Submit files'),
		        ),
		    ),
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data',
		    ),
		);
		
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => '..................',
		    //'showErrorSummary' => true,
		    'elements' => array(
				'config' => array(
					'type' => 'dropdownlist',
					'items' => DDNxl::getConfigSheetsDescrs(),
					//'hint' => '',
		        ),
				'files' => array(
					'type' => 'system.web.widgets.CMultiFileUpload',
					'hint' => Yii::t('', 'please choose, one-by-one, all the files'),
					'attributes' => array(
						'accept' => $this->accepted,
						'duplicate' => '!'. Yii::t('', 'This file already exists'),
                		'denied' => Yii::t('', 'Not Accepted File Format'),
						'options'=>array(
							//'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
							//'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
							//'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
							//'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
							//'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
							//'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
						),
					),
				),
			),
		);
	}
}
