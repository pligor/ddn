<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'BouncerController'.
 */
class LoginForm extends CFormModel
{
	public $verifyCode;
	
	public $username = 'admin';
	public $password = '0mmN1';
	//public $username = 'super.user';
	//public $password = 'super.user';
	//public $username;
	//public $password;

	private $_identity = null;
	
	protected function getIdentity() {
		if($this->_identity === null) {
			$this->_identity = new UserIdentity($this->username, $this->password);
			$this->_identity->authenticate();
		}
		return $this->_identity;
	}
	
	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login() {
		if($this->identity->isAuthenticated)
		{
			//$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			$duration = Yii::app()->user->authTimeout;
			Yii::app()->user->login($this->_identity, $duration);
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array(
				'username, password',
				'required',
			),
			array('password', 'authenticate'),	//note this: http://www.yiiframework.com/doc/api/1.1/CModel#c4914
//			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),//checkRequirements checks if GD with FreeType support is loaded
			array('verifyCode', 'captcha', 'allowEmpty'=>true),
		);
	}
	
	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute, $params)
	{
		if(!$this->hasErrors()) { //note this: http://www.yiiframework.com/doc/api/1.1/CModel#c4914
			if( !$this->identity->isAuthenticated ) {
				$error = Yii::t('', 'Incorrect') .' '. $this->getAttributeLabel('username') .' '. Yii::t('', 'or') .' '. $this->getAttributeLabel('password');
				$this->addError('password', $error);
			}
		}
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'username' => Yii::t('', 'Username'),
			'password' => Yii::t('', 'Password'),
			'verifyCode' => Yii::t('', 'Verification Code'),
		);
	}
	
	public function getForm() {
		$config = array(
		    //'title' => '..............',
		    'showErrorSummary' => true,
		    'elements' => array(
				get_class() => $this->getFormConfig(),
			),
		    'buttons' => array(
		        'login_button' => array(
					'type' => 'submit',
					'label' => Yii::t('', 'Login'),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data', //ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		$elements = array(
			'username' => array(
				'type' => 'text',
				'attributes' => array(
					'size' => 15,
					'maxlength'=>30
				),
				//'hint' => '',
	        ),
	        'password' => array(
				'type' => 'password',
				'attributes' => array(
					'size' => 30,
					'maxlength' => 255,
	        	),
	        ),
		);
		
		if(CCaptcha::checkRequirements()) {	//Checks if GD with FreeType support is loaded
			$elements['verifyCode'] = array(
				'type' => 'application.widgets.inputwidgets.CaptchaInputWidget',
				'hint' => Yii::t('', 'Please enter the letters (not case-sensitive) as they are shown in the image'),
			);
		}
		
		return array(
			'type' => 'form',
		    //'title' => '.......',
		    //'showErrorSummary' => true,
		    'elements' => $elements,
		);
	}
}
