<?php

class MultiModelForm extends CFormModel {
	
	/**
	 * Initialize model before executing setScenario inside parent's construct
	 * @param array $classes
	 * @param string $scenario
	 */
	public function __construct($classes,$scenario='') {		
		$name = 'MultiModelBehavior';
		$behavior = new $name($classes);
		$this->attachBehavior($name, $behavior);
		
		//as an exception we put parent call after the code, since we want behavior attached before calling parent construct
		parent::__construct($scenario);
	}
	
	/**
	 * Get safe attribute name
	 * @param string $name
	 * @see CComponent::__get()
	 */
	public function __get($name) {
		if( !$this->checkModelAttribute($name) ) {
			return parent::__get($name);
		}
		return $this->getModelAttribute($name);
	}
	
	/**
	 * @see CComponent::__set()
	 */
	public function __set($name, $value) {
		if( $this->setModelAttribute($name, $value) === false) {
			parent::__set($name, $value);
		}
	}
	
	/**
	 * Set the same scenario also for all the corresponding models
	 * @param string $value
	 * @see CModel::setScenario()
	 */
	public function setScenario($value) {
		$this->setModelsScenario($value);
		parent::setScenario($value);
	}
}