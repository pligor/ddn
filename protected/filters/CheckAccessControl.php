<?php
class CheckAccessControl extends CFilter {
	public $role;
	
	protected function preFilter($filterChain) {
		//$filterChain->controller->redirect(array('index'));
		//print $_GET["kati"];
		$var = 'island_id';
		
		if( Yii::app()->user->hasState($var) ) {
			$island_id = Yii::app()->user->island_id;
		}
		elseif( isset($_GET[$var]) ) {
			$island_id = $_GET[$var];
		}
		else {
			throw new CHttpException(403, Yii::t('', 'You have not defined the island'));
		}
		
		if( Yii::app()->user->checkAccess('super_user', compact($var)) ) {
			return true;
		}
		
		if( Yii::app()->user->checkAccess('island_manager', compact($var)) ) {
			return true;
		}
		throw new CHttpException(403, Yii::t('', 'You do not have access to this island'));
		
		return false;	//we shall not reach this line of code
	}

	protected function postFilter($filterChain) {
		
	}
}