<?php
/**
 * @author pligor
 */
/*
 * Form or redirection must be declared like this:
	$form->action = array(
		$this->route,
		'pv_plant_id' => $pv_plant_id,
	);
 */
class FinderFilter extends CFilter {
	public $use_GET = true;
	
	/**
	 * @param CFilterChain $filterChain
	 * @see CFilter::preFilter()
	 */
	protected function preFilter($filterChain) {
		$data_name = 'pv_plant_id';
		$controller = $filterChain->controller;
		
		if( $this->use_GET && isset($_GET[$data_name]) ) {
			$controller->$data_name = $_GET[$data_name];
		}
		else {
			$calls = array(
				array(
					'callee' => 'finder/index',
					'data_name' => $data_name,
				),
			);
			$answers = ActionCall::call($controller, $calls);	
			$controller->$data_name = $answers[$data_name];
			
			//$_GET[$data_name] = $controller->$data_name;
		}
		
		return true;
	}

	protected function postFilter($filterChain) {
	}
}