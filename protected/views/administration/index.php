<?php
$this->menu=array(
	array(
		'label' => 'Ολοκληρωτική Διαγραφή των Καταχωρύσεων της Βάσης Δεδομένων',
		'url' => '#',
		'linkOptions' => array(
			'submit' => array(
				'clear_db',
			),
			'confirm' => 'Είστε ΒΕΒΑΙΟΙ πως επιθυμείτε ΟΛΟΚΛΗΡΩΤΙΚΗ ΔΙΑΓΡΑΦΗ ΤΩΝ ΔΕΔΟΜΕΝΩΝ ΤΩΝ ΚΑΤΑΧΩΡΥΣΕΩΝ ??',
		),
	),
	array(
		'label' => 'Δημιουργία Νέου Χρήστη',
		'url' => array(
			'user/create',
		),
	),
	array(
		'label' => 'Αρχικοποίηση Νομών & Δήμων',
		'url' => '#',
		'linkOptions' => array(
			'submit' => array(
				'init_dimous_nomous',
			),
			'confirm' => 'Είστε ΒΕΒΑΙΟΙ πως επιθυμείτε ΑΝΤΙΚΑΤΑΣΤΑΣΗ ΤΩΝ ΔΕΔΟΜΕΝΩΝ Νομών & Δήμων ??',
		),
	),
	array(
		'label' => Yii::t('', 'Initialization of Areas & Islands'),
		'url' => '#',
		'linkOptions' => array(
			'submit' => array(
				'init_areas_islands',
			),
			'confirm' => 'Είστε ΒΕΒΑΙΟΙ πως επιθυμείτε ΑΝΤΙΚΑΤΑΣΤΑΣΗ ΤΩΝ ΔΕΔΟΜΕΝΩΝ Περιοχών & Νησιών ??',
		),
	),
	array(
		'label' => Yii::t('', 'Initialization of Islands Groups'),
		'url' => '#',
		'linkOptions' => array(
			'submit' => array(
				'init_island_groups',
			),
			'confirm' => 'Είστε ΒΕΒΑΙΟΙ πως επιθυμείτε να ΑΝΤΙΚΑΤΑΣΤΗΣΕΤΕ τις ομάδες Νησιών ??',
		),
	),
	array(
		'label' => 'Αρχικοποίηση Αντιστοίχησης Δήμων - Νησιά',
		'url' => '#',
		'linkOptions' => array(
			'submit' => array(
				'init_dimos_island',
			),
			'confirm' => 'Είστε ΒΕΒΑΙΟΙ πως επιθυμείτε να ΑΝΤΙΚΑΤΑΣΤΗΣΕΤΕ την αντιστοίχηση των Δήμων με τα Νησιά ??',
		),
	),
	array(
		'label' => 'Αρχικοποίηση Ειδών Παραγωγού',
		'url' => '#',
		'linkOptions' => array(
			'submit' => array(
				'init_eidi_paragogon',
			),
			'confirm' => 'Είστε ΒΕΒΑΙΟΙ πως επιθυμείτε να ΑΝΤΙΚΑΤΑΣΤΗΣΕΤΕ τα διάφορα Είδη Παραγωγών ??',
		),
	),
);
?>
<h1>Διαχείριση</h1>

<p>
Παρακαλώ να χρησιμοποιήσετε πολύ προσεχτικά και με φειδώ τις διπλανές επιλογές διότι έχουν επίπτωση σε κρίσιμα δεδομένα
</p>

<?php if(Yii::app()->user->hasFlash('success')): ?>
<b>
<?php print Yii::app()->user->getFlash('success'); ?>
</b>
<?php endif; ?>