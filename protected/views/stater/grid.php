<?php
$this->menu=array(
	array(
		'label'=>'Επιστροφή στο μενού',
		'url'=>array(
			'index',
		)
	),
);
?>
<h1><?php print $this->menuLabels[$this->action->id]; ?></h1>

<?php
$properties = array(
	'id' => 'excel-grid',
	'dataProvider' => $dataProvider,
	'columns' => isset($columns) ? $columns : array(),
	'showTableOnEmpty' => false,
	'title' => 'table',
);
$this->widget('application.widgets.Excel.ExcelSheetView', $properties);
?>

<?php
$properties = array(
	'id' => 'html-grid',
	'dataProvider' => $dataProvider,
	'columns' => isset($columns) ? $columns : array(),
	'showTableOnEmpty' => false,
);
$this->widget('zii.widgets.grid.CGridView', $properties);
?>