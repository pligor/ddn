<?php
// Important: you need to specify url as 'controller/action',
// not just as 'controller' even if default action is used.
// simple word (no slash) will mean the action of the CURRENT controller
$this->menu=array(
	array(
		'label' => 'Καταχώρηση Νέας Αίτησης',
		'url' => array(
			'submitter/arxikiAitisi',
		),
	),
	array(
		'label' => 'Καταχώρηση Όρων Σύνδεσης',
		'url' => array(
			'submitter/diatiposiOron',
		),
	),
	array(
		'label' => 'Καταχώρηση Σύμβασης Σύνδεσης',
		'url' => array(
			'submitter/simvasiSindesis',
		),
	),
	array(
		'label' => 'Καταχώρηση Σύμβασης Πώλησης / Συμψηφισμού',
		'url' => array(
			'submitter/simvasiPolisis',
		),
	),
	array(
		'label' => 'Καταχώρηση Στοιχείων Ενεργοποίησης',
		'url' => array(
			'submitter/activation',
		),
	),
	array(
		'label' => 'Εισαγωγή Παρατηρήσεων',
		'url' => array(
			'submitter/comment',
		),
	),
);
?>

<h1>Μενού Καταχώρησης στοιχείων</h1>

<h3>
Έχετε επιλέξει το νησί <b><?php print $island_name; ?></b>
</h3>