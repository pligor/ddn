<?php
$this->menu=array(
	array(
		'label' => Yii::t('', 'Cancel'),
		'url'=>array(
			'submitter/index',
	)),
);
?>

<h1>
<?php
	$class = ucfirst( $this->action->id );
	print Yii::t('', 'Submit data for step') .': '. $class::getTitle();
?>
</h1>

<div class="form">
<?php 
print $form->render();
?>
</div><!-- form -->
