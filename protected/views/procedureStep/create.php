<?php
$this->breadcrumbs=array(
	'Procedure Steps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProcedureStep', 'url'=>array('index')),
	array('label'=>'Manage ProcedureStep', 'url'=>array('admin')),
);
?>

<h1>Create ProcedureStep</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>