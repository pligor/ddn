<?php
if(!isset($postfix)) $postfix=null;
else $postfix = ' '.$postfix;

$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		//'execution_date',
		array(
			"label"=>$model->getAttributeLabel('execution_date') .$postfix,
			"value"=> date("j/n/Y", $model->execution_date),
		),
		//'pv_plant_id',
	),
)); ?>
