<?php
$this->breadcrumbs=array(
	'Procedure Steps',
);

$this->menu=array(
	array('label'=>'Create ProcedureStep', 'url'=>array('create')),
	array('label'=>'Manage ProcedureStep', 'url'=>array('admin')),
);
?>

<h1>Procedure Steps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
