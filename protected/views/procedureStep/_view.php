<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('execution_date')); ?>:</b>
	<?php echo CHtml::encode($data->execution_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pv_plant_id')); ?>:</b>
	<?php echo CHtml::encode($data->pv_plant_id); ?>
	<br />


</div>