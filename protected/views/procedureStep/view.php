<?php
$this->breadcrumbs=array(
	'Procedure Steps'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProcedureStep', 'url'=>array('index')),
	array('label'=>'Create ProcedureStep', 'url'=>array('create')),
	array('label'=>'Update ProcedureStep', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProcedureStep', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProcedureStep', 'url'=>array('admin')),
);
?>

<h1>View ProcedureStep #<?php echo $model->id; ?></h1>

<?php
	print $this->renderPartial("_view_body", array(
		'model'=>$model,
	));
?>