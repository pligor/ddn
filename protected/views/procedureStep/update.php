<?php
$this->breadcrumbs=array(
	'Procedure Steps'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProcedureStep', 'url'=>array('index')),
	array('label'=>'Create ProcedureStep', 'url'=>array('create')),
	array('label'=>'View ProcedureStep', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProcedureStep', 'url'=>array('admin')),
);
?>

<h1>Update ProcedureStep <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>