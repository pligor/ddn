<?php
$this->breadcrumbs=array(
	'Pv Plants'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PvPlant', 'url'=>array('index')),
	array('label'=>'Manage PvPlant', 'url'=>array('admin')),
);
?>

<h1>Create PvPlant</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>