<?php
$this->breadcrumbs=array(
	'Pv Plants'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PvPlant', 'url'=>array('index')),
	array('label'=>'Create PvPlant', 'url'=>array('create')),
	array('label'=>'Update PvPlant', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PvPlant', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PvPlant', 'url'=>array('admin')),
);
?>

<h1>View PvPlant #<?php echo $model->id; ?></h1>

<?php
	print $this->renderPartial("_view_body", array(
		'model'=>$model,
	));
?>
