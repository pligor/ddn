<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'priority',
		array(
			"label"=>$model->getAttributeLabel('pvCat'),
			"value"=>$model->child->getDescr(),
		),
		'protocol',
	),
)); ?>
