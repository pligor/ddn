<?php
$this->breadcrumbs=array(
	'Pv Plants',
);

$this->menu=array(
	array('label'=>'Create PvPlant', 'url'=>array('create')),
	array('label'=>'Manage PvPlant', 'url'=>array('admin')),
);
?>

<h1>Pv Plants</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
