<?php
$this->breadcrumbs=array(
	'Pv Plants'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PvPlant', 'url'=>array('index')),
	array('label'=>'Create PvPlant', 'url'=>array('create')),
	array('label'=>'View PvPlant', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PvPlant', 'url'=>array('admin')),
);
?>

<h1>Update PvPlant <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>