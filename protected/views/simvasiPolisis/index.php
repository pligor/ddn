<?php
$this->breadcrumbs=array(
	'Simvasi Polisises',
);

$this->menu=array(
	array('label'=>'Create SimvasiPolisis', 'url'=>array('create')),
	array('label'=>'Manage SimvasiPolisis', 'url'=>array('admin')),
);
?>

<h1>Simvasi Polisises</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
