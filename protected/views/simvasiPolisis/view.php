<?php
$this->breadcrumbs=array(
	'Simvasi Polisises'=>array('index'),
	$model->procedure_step_id,
);

$this->menu=array(
	array('label'=>'List SimvasiPolisis', 'url'=>array('index')),
	array('label'=>'Create SimvasiPolisis', 'url'=>array('create')),
	array('label'=>'Update SimvasiPolisis', 'url'=>array('update', 'id'=>$model->procedure_step_id)),
	array('label'=>'Delete SimvasiPolisis', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->procedure_step_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SimvasiPolisis', 'url'=>array('admin')),
);
?>

<h1>View SimvasiPolisis #<?php echo $model->procedure_step_id; ?></h1>

<?php
	print $this->renderPartial("_view_body", array(
		'model'=>$model,
	));
?>