<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('procedure_step_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->procedure_step_id), array('view', 'id'=>$data->procedure_step_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('simvasi_sindesis_procedure_step_id')); ?>:</b>
	<?php echo CHtml::encode($data->simvasi_sindesis_procedure_step_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('conn_compl_date')); ?>:</b>
	<?php echo CHtml::encode($data->conn_compl_date); ?>
	<br />


</div>