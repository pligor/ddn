<?php

if($model->conn_compl_date)
	$conn_compl_date = date("j/n/Y", $model->conn_compl_date);
else
	$conn_compl_date = null;

$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'procedure_step_id',
		//'simvasi_sindesis_procedure_step_id',
		array(
			"label"=>$model->getAttributeLabel('conn_compl_date'),
			"value"=> $conn_compl_date,
		),
	),
	"nullDisplay" => CHtml::tag('span', array('class'=>'null'), '(δεν έχει οριστεί)'),
)); ?>

<?php
	print $this->renderPartial("/procedureStep/_view_body", array(
		'model'=>$model->procedureStep,
		'postfix' => 'Σύμβασης Πώλησης / Συμψηφισμού'
	));
?>