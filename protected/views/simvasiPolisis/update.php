<?php
$this->breadcrumbs=array(
	'Simvasi Polisises'=>array('index'),
	$model->procedure_step_id=>array('view','id'=>$model->procedure_step_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SimvasiPolisis', 'url'=>array('index')),
	array('label'=>'Create SimvasiPolisis', 'url'=>array('create')),
	array('label'=>'View SimvasiPolisis', 'url'=>array('view', 'id'=>$model->procedure_step_id)),
	array('label'=>'Manage SimvasiPolisis', 'url'=>array('admin')),
);
?>

<h1>Update SimvasiPolisis <?php echo $model->procedure_step_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>