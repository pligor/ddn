<?php
$this->breadcrumbs=array(
	'Simvasi Polisises'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List SimvasiPolisis', 'url'=>array('index')),
	array('label'=>'Create SimvasiPolisis', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('simvasi-polisis-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Simvasi Polisises</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php
	$this->renderPartial('_search',array('model'=>$model,));
?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'simvasi-polisis-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'procedure_step_id',
		'simvasi_sindesis_procedure_step_id',
		'conn_compl_date',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
