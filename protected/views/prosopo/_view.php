<div class="view">
	<b><?php print CHtml::encode($data->getAttributeLabel('eponimia')); ?>:</b>
	<?php
		print CHtml::link( CHtml::encode($data->eponimia), array(
			'return_prosopo_id',
			'prosopo_id' => $data->id,
			'callee' => $callee,
		));
	?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eidos_paragogou_id')); ?>:</b>
	<?php echo CHtml::encode($data->eidosParagogou->eidos); ?>
	<br />
</div>