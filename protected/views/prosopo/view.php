<?php
$this->breadcrumbs=array(
	'Prosopos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Prosopo', 'url'=>array('index')),
	array('label'=>'Create Prosopo', 'url'=>array('create')),
	array('label'=>'Update Prosopo', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Prosopo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Prosopo', 'url'=>array('admin')),
);
?>

<h1>View Prosopo #<?php echo $model->id; ?></h1>

<?php
	print $this->renderPartial("_view_body", array(
		'model'=>$model,
	));
?>