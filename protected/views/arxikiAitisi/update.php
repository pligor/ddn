<?php
$this->breadcrumbs=array(
	'Arxiki Aitisis'=>array('index'),
	$model->procedure_step_id=>array('view','id'=>$model->procedure_step_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ArxikiAitisi', 'url'=>array('index')),
	array('label'=>'Create ArxikiAitisi', 'url'=>array('create')),
	array('label'=>'View ArxikiAitisi', 'url'=>array('view', 'id'=>$model->procedure_step_id)),
	array('label'=>'Manage ArxikiAitisi', 'url'=>array('admin')),
);
?>

<h1>Update ArxikiAitisi <?php echo $model->procedure_step_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>