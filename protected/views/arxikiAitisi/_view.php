<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('procedure_step_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->procedure_step_id), array('view', 'id'=>$data->procedure_step_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nominal_power')); ?>:</b>
	<?php echo CHtml::encode($data->nominal_power); ?>
	<br />


</div>