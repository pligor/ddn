<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'procedure_step_id',
		'nominal_power',
	),
)); ?>

<?php
	print $this->renderPartial("/procedureStep/_view_body", array(
		'model'=>$model->procedureStep,
		'postfix' => 'Αρχικής Αίτησης'
	));
?>