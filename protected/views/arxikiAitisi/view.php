<?php
$this->breadcrumbs=array(
	'Arxiki Aitisis'=>array('index'),
	$model->procedure_step_id,
);
?>

<?php
$this->menu=array(
	array('label'=>'List ArxikiAitisi', 'url'=>array('index')),
	array('label'=>'Create ArxikiAitisi', 'url'=>array('create')),
	array('label'=>'Update ArxikiAitisi', 'url'=>array('update', 'id'=>$model->procedure_step_id)),
	array('label'=>'Delete ArxikiAitisi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->procedure_step_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ArxikiAitisi', 'url'=>array('admin')),
);
?>

<h1>View ArxikiAitisi #<?php echo $model->procedure_step_id; ?></h1>

<?php
	print $this->renderPartial("_view_body", array(
		'model'=>$model,
	));
?>