<?php
$this->breadcrumbs=array(
	'Arxiki Aitisis',
);

$this->menu=array(
	array('label'=>'Create ArxikiAitisi', 'url'=>array('create')),
	array('label'=>'Manage ArxikiAitisi', 'url'=>array('admin')),
);
?>

<h1>Arxiki Aitisis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
