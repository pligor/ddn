<?php
$this->breadcrumbs=array(
	'Pv Mts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PvMt', 'url'=>array('index')),
	array('label'=>'Manage PvMt', 'url'=>array('admin')),
);
?>

<h1>Create PvMt</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>