<?php
$this->breadcrumbs=array(
	'Pv Mts',
);

$this->menu=array(
	array('label'=>'Create PvMt', 'url'=>array('create')),
	array('label'=>'Manage PvMt', 'url'=>array('admin')),
);
?>

<h1>Pv Mts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
