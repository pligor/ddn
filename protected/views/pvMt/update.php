<?php
$this->breadcrumbs=array(
	'Pv Mts'=>array('index'),
	$model->pv_plant_id=>array('view','id'=>$model->pv_plant_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PvMt', 'url'=>array('index')),
	array('label'=>'Create PvMt', 'url'=>array('create')),
	array('label'=>'View PvMt', 'url'=>array('view', 'id'=>$model->pv_plant_id)),
	array('label'=>'Manage PvMt', 'url'=>array('admin')),
);
?>

<h1>Update PvMt <?php echo $model->pv_plant_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>