<?php
$this->breadcrumbs=array(
	'Pv Mts'=>array('index'),
	$model->pv_plant_id,
);

$this->menu=array(
	array('label'=>'List PvMt', 'url'=>array('index')),
	array('label'=>'Create PvMt', 'url'=>array('create')),
	array('label'=>'Update PvMt', 'url'=>array('update', 'id'=>$model->pv_plant_id)),
	array('label'=>'Delete PvMt', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->pv_plant_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PvMt', 'url'=>array('admin')),
);
?>

<h1>View PvMt #<?php echo $model->pv_plant_id; ?></h1>

<?php
	print $this->renderPartial("_view_body", array(
		'model'=>$model,
	));
?>
