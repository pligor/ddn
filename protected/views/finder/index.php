<?php
$this->menu=array(
	array(
		'label' => 'βάσει Επωνυμίας',
		'url' => array(
			'find_prosopo',
			'params' => $params,
		),
	),
	array(
		'label' => 'βάσει Αριθμού Πρωτοκόλλου',
		'url' => array(
			'find_protocol',
			'params' => $params,
		),
	),
	
	//array('label'=>'Delete ArxikiAitisi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->procedure_step_id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>Ευρετήριο</h1>

<p>Έχετε επιλέξει τις παρακάτω κατηγορίες:</p>
<ul>
<?php
foreach($descs as $desc)
{
	print "<li><b>$desc</b></li>" ;
}
?>
</ul>

<p>
	Οι <tt>Ενέργειες</tt> αντιστοιχούν στις διαθέσιμες μεθόδους εύρεσης
</p>
<p>
	Παρακαλώ επιλέξτε μία από τις μεθόδους εύρεσης για να συνεχίσετε
</p>