<div class="view">
	<?php
		print CHtml::link( CHtml::encode("Επιλογή"), array(
			'return_plant_id',
			//$redirect_uri,
			'pv_plant_id' => $data['pv_plant_id'],
		));
	?>
	<br />
	
	<b><?php
		$model = new Prosopo;
		$attribute = 'eponimia';
		print CHtml::encode($model->getAttributeLabel($attribute));
	?>:</b>
	<?php print CHtml::encode($data[$attribute]); ?>
	<br />
	
	<b><?php
		$model = new Address;
		$attribute = 'thesi';
		print CHtml::encode($model->getAttributeLabel($attribute)); ?>:</b>
	<?php
		print CHtml::encode($data[$attribute]);
		$attribute = 'noumero';
		if(!empty($data[$attribute]))
		{
			print CHtml::encode(" ");
			print CHtml::encode($data[$attribute]);
		}
	?>
	<br />
	
	<b><?php
		$model = new ArxikiAitisi;
		$attribute = 'nominal_power';
		print CHtml::encode($model->getAttributeLabel($attribute));
	?>:</b>
	<?php
		print CHtml::encode($data[$attribute]);
	?>
	<br />
	
	<b><?php
		$model = new PvPlant;
		$attribute = 'protocol';
		print CHtml::encode($model->getAttributeLabel($attribute));
	?>:</b>
	<?php
		print CHtml::encode($data[$attribute]);
	?>
	<br />
</div>
