<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'procedure_step_id'); ?>
		<?php echo $form->textField($model,'procedure_step_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'arxiki_aitisi_procedure_step_id'); ?>
		<?php echo $form->textField($model,'arxiki_aitisi_procedure_step_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'protocol_diatiposis_oron'); ?>
		<?php echo $form->textField($model,'protocol_diatiposis_oron',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'liksi_oron'); ?>
		<?php echo $form->textField($model,'liksi_oron',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->