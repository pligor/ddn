<?php
$this->breadcrumbs=array(
	'Diatiposi Orons',
);

$this->menu=array(
	array('label'=>'Create DiatiposiOron', 'url'=>array('create')),
	array('label'=>'Manage DiatiposiOron', 'url'=>array('admin')),
);
?>

<h1>Diatiposi Orons</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
