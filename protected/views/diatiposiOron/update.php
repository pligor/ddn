<?php
$this->breadcrumbs=array(
	'Diatiposi Orons'=>array('index'),
	$model->procedure_step_id=>array('view','id'=>$model->procedure_step_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DiatiposiOron', 'url'=>array('index')),
	array('label'=>'Create DiatiposiOron', 'url'=>array('create')),
	array('label'=>'View DiatiposiOron', 'url'=>array('view', 'id'=>$model->procedure_step_id)),
	array('label'=>'Manage DiatiposiOron', 'url'=>array('admin')),
);
?>

<h1>Update DiatiposiOron <?php echo $model->procedure_step_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>