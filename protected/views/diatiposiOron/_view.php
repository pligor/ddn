<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('procedure_step_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->procedure_step_id), array('view', 'id'=>$data->procedure_step_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('arxiki_aitisi_procedure_step_id')); ?>:</b>
	<?php echo CHtml::encode($data->arxiki_aitisi_procedure_step_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('protocol_diatiposis_oron')); ?>:</b>
	<?php echo CHtml::encode($data->protocol_diatiposis_oron); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('liksi_oron')); ?>:</b>
	<?php echo CHtml::encode($data->liksi_oron); ?>
	<br />


</div>