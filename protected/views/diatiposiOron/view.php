<?php
$this->breadcrumbs=array(
	'Diatiposi Orons'=>array('index'),
	$model->procedure_step_id,
);

$this->menu=array(
	array('label'=>'List DiatiposiOron', 'url'=>array('index')),
	array('label'=>'Create DiatiposiOron', 'url'=>array('create')),
	array('label'=>'Update DiatiposiOron', 'url'=>array('update', 'id'=>$model->procedure_step_id)),
	array('label'=>'Delete DiatiposiOron', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->procedure_step_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DiatiposiOron', 'url'=>array('admin')),
);
?>

<h1>View DiatiposiOron #<?php echo $model->procedure_step_id; ?></h1>

<?php
	print $this->renderPartial("_view_body", array(
		'model'=>$model,
	));
?>
