<?php
$this->breadcrumbs=array(
	'Diatiposi Orons'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List DiatiposiOron', 'url'=>array('index')),
	array('label'=>'Create DiatiposiOron', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('diatiposi-oron-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Diatiposi Orons</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'diatiposi-oron-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'procedure_step_id',
		'arxiki_aitisi_procedure_step_id',
		'protocol_diatiposis_oron',
		'liksi_oron',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
