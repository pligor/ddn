<?php
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'procedure_step_id',
		//'arxiki_aitisi_procedure_step_id',
		'protocol_diatiposis_oron',
		array(
			"label"=>$model->getAttributeLabel('liksi_oron'),
			"value"=> date("j/n/Y", $model->liksi_oron),
		),
	),
)); ?>

<?php
	print $this->renderPartial("/procedureStep/_view_body", array(
		'model'=>$model->procedureStep,
		'postfix' => 'Διατύπωσης Όρων'
	));
?>