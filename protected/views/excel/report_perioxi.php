<?php
$this->menu=array(
	array(
		'label'=>'Επιστροφή στο μενού',
		'url' => array(
			'excel/index',
		),
	),
	array(
		'label'=>'Φόρτωση επιπλέον περιοχής',
		'url' => array(
			'excel/load_perioxi',
		),
	),
);
?>
<h1>Προβλήματα και Παρατήρησεις για την Περιοχή: <?php print $perioxi_dei_name; ?></h1>

<?php
/*
$reportPerioxi = Array(
    5 => Array(
            8 => Array
                (
                    'ArxikiAitisi' => Array
                        (
                            'warnings' => Array
                                (
                                    0 => 'Το είδος παραγωγού έχει πληκτρολογηθεί λανθασμένα',
                                ),

                            'error' => null,
                        )

                ),

            9 => Array
                (
                    'ArxikiAitisi' => Array
                        (
                            'warnings' => Array
                                (
                                    0 => 'Το είδος παραγωγού έχει πληκτρολογηθεί λανθασμένα',
                                    1 => 'Το είδος παραγωγού έχει πληκτρολογηθεί λανθασμένα',
                                ),

                            'error' => null,
                        )

                ),
           11 => Array
                (
                    'ArxikiAitisi' => Array
                        (
                            'warnings' => Array
                                (
                                    0 => 'Το είδος παραγωγού έχει πληκτρολογηθεί λανθασμένα',
                                    1 => 'Το είδος παραγωγού έχει πληκτρολογηθεί λανθασμένα',
                                ),

                            'error' => null,
                        )

                ),
	)
);
//*/
?>

<?php
if(is_array($reportPerioxi))
foreach($reportPerioxi as $island_id => $reportIsland) {
	$island_name = CHtmlEx::loadModel('Island', $island_id)->island_name;
	$islandErrors = array();
	foreach($reportIsland as $line => $reportLine) {
		$lineErrors = array();
		foreach($reportLine as $step => $reports) {
			$prefix = 'Στη φάση '. $step::getTitle();
			if( !CHtmlEx::isEmpty($reports['error']) ) {
				$lineErrors[] = $prefix .': '.$reports['error'];
			}
		}
		if( !CHtmlEx::isEmpty($lineErrors) ) {
			$prefix = "Στη γραμμή $line:";
			$li = "";
			foreach($lineErrors as $lineError) {
				$li .= CHtml::tag('li',array(),$lineError);
			}
			$ul = CHtml::tag('ul',array(),$li);
			$islandErrors[] = $prefix.$ul;
		}
	}
	if( !CHtmlEx::isEmpty($islandErrors) ) {
		$prefix = "Στο νησί $island_name παρουσιάστηκαν τα εξής προβλήματα:";
		$islandError = implode('', $islandErrors);
		print CHtml::tag('h3',array(),$prefix).CHtml::tag('h6',array(),$islandError);
	}
}
?>
<br/>
<?php
if(is_array($reportPerioxi))
foreach($reportPerioxi as $island_id => $reportIsland) {
	$island_name = CHtmlEx::loadModel('Island', $island_id)->island_name;
	$islandErrors = array();
	foreach($reportIsland as $line => $reportLine) {
		$lineErrors = array();
		foreach($reportLine as $step => $reports) {
			$prefix = 'Στη φάση '. $step::getTitle();
			if( !CHtmlEx::isEmpty($reports['warnings']) ) {
				$li = "";
				foreach($reports['warnings'] as $warning) {
					$li .= CHtml::tag('li',array(),$warning);
				}
				$ul = CHtml::tag('ul',array(),$li);
				$lineErrors[] = $prefix .':'.$ul;
			}
		}
		if( !CHtmlEx::isEmpty($lineErrors) ) {
			$prefix = "Στη γραμμή $line:";
			$li = "";
			foreach($lineErrors as $lineError) {
				$li .= CHtml::tag('li',array(),$lineError);
			}
			$ul = CHtml::tag('ul',array(),$li);
			$islandErrors[] = $prefix.$ul;
		}
	}
	if( !CHtmlEx::isEmpty($islandErrors) ) {
		$prefix = "Στο νησί $island_name έχουμε τις εξής παρατηρήσεις:";
		$islandError = implode('', $islandErrors);
		print CHtml::tag('b',array(),$prefix).CHtml::tag('p',array(),$islandError);
	}
}
?>