<?php
if(isset($menu)) {
	$this->menu = $menu;
}
?>
<?php
$properties = array(
	'id' => $this->action->id .'-grid',
	'dataProvider' => $dataProvider,
	'columns' => isset($columns) ? $columns : array(),
	'showTableOnEmpty' => false,
	'title' => 'sample',
);
$this->widget('application.widgets.Excel.ExcelSheetView', $properties);
//$this->widget('zii.widgets.grid.CGridView', $properties);
?>