<?php
$this->menu = array(
	array(
		'label'=>'Επιστροφή στο μενού',
		'url' => array(
			'excel/index',
		),
	),
	array(
		'label'=>'Έλεγχος νέου αρχείου Τομέα Σύνδεσης Χρηστών',
		'url' => array(
			'excel/tsx',
		),
	),
);
?>
<h1><?php print Yii::t('', 'Report of the file of Τομέα Σύνδεσης Χρηστών'); ?></h1>

<?php
/*
Array
(
	[1] => Array
	(
		[2] => Η ημερομηνία στην Περιοχή είναι κενή ενώ στον Τομέα Σύνδεσης Χρηστών υπάρχει ημερομηνία
		[3] => Το πρωτόκολλο πρέπει να είναι ΑΚΕΡΑΙΟΣ αριθμός
		[4] => Η ημερομηνία στην Περιοχή είναι κενή ενώ στον Τομέα Σύνδεσης Χρηστών υπάρχει ημερομηνία
    )
)
*/

$errors = array();

foreach($report as $perioxi_dei_id => $reportPerioxi) {
	$title = Yii::t('', 'In Area {perioxi_dei_name} we have the following problems',array(
		'{perioxi_dei_name}' => CHtmlEx::loadModel('PerioxiDei', $perioxi_dei_id)->perioxi_dei_name,
	)).":";
	
	$header = CHtml::tag('h3',array(),$title);
	
	$li = "";
	foreach($reportPerioxi as $line => $lineError) {
		$prefix = Yii::t('', 'In line') ." $line: ";
		
		$li .= CHtml::tag('li',array(),$prefix.$lineError);
	}
	$ul = CHtml::tag('ul',array(),$li);
	
	$errorBody = CHtml::tag('h6',array(),$ul);
	
	$errors[] = $header.$errorBody;
}

print implode('<br/>', $errors);