<?php
$this->menu=array(
	array('label'=>'Επιστροφή στη Διαχείριση', 'url'=>array('administration/index')),
);
?>

<h1>Δημιουργία νέου χρήστη</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>