<h1><?php print Yii::t('', 'Contact Us'); ?></h1>

<?php if(Yii::app()->user->hasFlash('contact')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>
<?php else: ?>
<p>
<?php print Yii::t('', 'If you have inquiries or other questions') .', '.
Yii::t('', 'please fill out the following form to contact us') . '. '. Yii::t('', 'Thank you'). '.'; ?>
</p>

<div class="form">
<?php
print $form->render();
?>
</div>
<?php endif; ?>