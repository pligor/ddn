<h1><?php print Yii::t('', 'Welcome to the R.S.E. Data Management Application')?></h1>

<p>
<?php print Yii::t('', 'You may navigate to the above menu'); ?> 
<?php print Yii::t('', 'to register new applications'); ?>,
<?php print Yii::t('', 'to edit existing applications'); ?>,
<?php print Yii::t('', 'to display the full data report of each registration in a printable version'); ?>,
<?php print Yii::t('', 'and to extract useful information through automatic statistic calculations'); ?>.
</p>