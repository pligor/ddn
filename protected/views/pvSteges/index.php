<?php
$this->breadcrumbs=array(
	'Pv Steges',
);

$this->menu=array(
	array('label'=>'Create PvSteges', 'url'=>array('create')),
	array('label'=>'Manage PvSteges', 'url'=>array('admin')),
);
?>

<h1>Pv Steges</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
