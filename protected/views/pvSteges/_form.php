<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pv-steges-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'pv_plant_id'); ?>
		<?php echo $form->textField($model,'pv_plant_id'); ?>
		<?php echo $form->error($model,'pv_plant_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'protocol'); ?>
		<?php echo $form->textField($model,'protocol'); ?>
		<?php echo $form->error($model,'protocol'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'perioxi_dei_id'); ?>
		<?php echo $form->textField($model,'perioxi_dei_id'); ?>
		<?php echo $form->error($model,'perioxi_dei_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->