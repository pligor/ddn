<?php
$this->breadcrumbs=array(
	'Pv Steges'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PvSteges', 'url'=>array('index')),
	array('label'=>'Manage PvSteges', 'url'=>array('admin')),
);
?>

<h1>Create PvSteges</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>