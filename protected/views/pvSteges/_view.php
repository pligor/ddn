<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('pv_plant_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->pv_plant_id), array('view', 'id'=>$data->pv_plant_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('protocol')); ?>:</b>
	<?php echo CHtml::encode($data->protocol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('perioxi_dei_id')); ?>:</b>
	<?php echo CHtml::encode($data->perioxi_dei_id); ?>
	<br />


</div>