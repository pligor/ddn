<?php
$this->breadcrumbs=array(
	'Pv Steges'=>array('index'),
	$model->pv_plant_id,
);

$this->menu=array(
	array('label'=>'List PvSteges', 'url'=>array('index')),
	array('label'=>'Create PvSteges', 'url'=>array('create')),
	array('label'=>'Update PvSteges', 'url'=>array('update', 'id'=>$model->pv_plant_id)),
	array('label'=>'Delete PvSteges', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->pv_plant_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PvSteges', 'url'=>array('admin')),
);
?>

<h1>View PvSteges #<?php echo $model->pv_plant_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'pv_plant_id',
		'protocol',
		'perioxi_dei_id',
	),
)); ?>
