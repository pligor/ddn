<?php
$this->breadcrumbs=array(
	'Pv Steges'=>array('index'),
	$model->pv_plant_id=>array('view','id'=>$model->pv_plant_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PvSteges', 'url'=>array('index')),
	array('label'=>'Create PvSteges', 'url'=>array('create')),
	array('label'=>'View PvSteges', 'url'=>array('view', 'id'=>$model->pv_plant_id)),
	array('label'=>'Manage PvSteges', 'url'=>array('admin')),
);
?>

<h1>Update PvSteges <?php echo $model->pv_plant_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>