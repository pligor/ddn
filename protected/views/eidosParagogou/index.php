<h1>Αρχικοποίηση Ειδών Παραγωγού</h1>

<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'eidosParagogou-form',
		'enableAjaxValidation'=>false,
	)); ?>
	
	<?php /* 
	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<?php echo $form->errorSummary($model); ?>
	*/ ?>

	<div class="row">
		<?php echo $form->labelEx($model,'eidos'); ?>
		<?php echo $form->listBox($model,'eidos',$listData); ?>
	</div>

	<div class="row buttons">
		<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
		<?php
			print CHtml::button('Reinitialize', array
			(
			    'submit'=>array
			    (
			        'defaultInit',
			        //'id'=>$model->id,
			        //'ajax'=>$target,   //$target means the iframe's id
			    ),
			    'confirm'=>'Are you sure you want to load data from the stored csv file?',
			));
		?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
