<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php print CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo">
			<?php				
				print CHtml::link(CHtml::encode(Yii::app()->name), Yii::app()->homeUrl);
				print ' [';
				print CHtml::link(Yii::app()->language, array('/site/toggleLang'), array(
					'title' => Yii::t('', 'changing language will redirect you back to the home page'),
				));
				print ']';
			?>
		</div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php
		
		if( Yii::app()->user->hasState('island_id') ) {
			$model = CHtmlEx::loadModel('Island', Yii::app()->user->island_id);
			$islandName = $model->island_name;
		}
		
		$this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array(
					'label' => Yii::t('', 'Submission'),
					'url'=> array('/submitter/index'),
					'visible' => !Yii::app()->user->isGuest,
				),
				array(
					'label' => Yii::t('', 'Report'),
					'url' => array('/reporter/index'),
					'visible' => !Yii::app()->user->isGuest,
				),
				array(
					'label' => Yii::t('', 'Edit'),
					'url' => array('/editer/index'),
					'visible' => !Yii::app()->user->isGuest,
				),
				array(
					'label' => Yii::t('', 'Statistics'),
					'url' => array('/stater/index'),
					'visible' => !Yii::app()->user->isGuest,
				),
				array(
					'label' => Yii::t('', 'Island') . ( isset($islandName) ? " ($islandName)" : '' ),
					'url' => array(
						'/island/select',
						'params' => CHtmlEx::array_url(array( array(
							'redirect_uri' => 'island/persistent_island_id',
						) )),
					),
					'visible' => !Yii::app()->user->isGuest,
				),
				array(
					'label' => Yii::t('', 'Contact'),
					'url' => array('/site/contact'),
				),
				array(
					'label' =>  Yii::t('', 'Administration'),
					'url' => array('/administration/index'),
					'visible' => !Yii::app()->user->isGuest,
				),
				array(
					'label' => '!excel',
					'url' => array('/excel/index'),
					'visible' => !Yii::app()->user->isGuest,
				),
				array(
					'label' => Yii::t('', 'Login'),
					'url' => array('/bouncer/login'),
					'visible' => Yii::app()->user->isGuest,
				),
				array(
					'label' => Yii::t('', 'Logout') .' ('.Yii::app()->user->name.')',
					'url' => array('/bouncer/logout'),
					'visible' => !Yii::app()->user->isGuest,
				),
			),
			//'lastItemCssClass' => 'right', //not compatible with ms internet explorer 8
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php $this->pageTitle = Yii::app()->name;	//text on browser bar ?>
	<?php print $content; ?>

	<div id="footer">
            Πνευματική ιδιοκτησία &copy; <?php
			print date('Y') .' '.
					CHtml::link(
							Yii::app()->params['adminGreekName'],
							'https://facebook.com/'.Yii::app()->params['adminUsername']);
			?>.<br/>
            Με επιφύλαξη κάθε νόμιμου δικαιώματος.<br/>
            <?php print Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>