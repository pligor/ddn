<?php
$this->menu = $menu;
?>

<h1><?php print $title; ?></h1>

<?php	
	foreach($models as $class => $model) {
		print $this->renderPartial("/".lcfirst($class)."/_view_body", compact('model'));
	}
?>