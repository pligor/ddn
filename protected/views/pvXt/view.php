<?php
$this->breadcrumbs=array(
	'Pv Xts'=>array('index'),
	$model->pv_plant_id,
);

$this->menu=array(
	array('label'=>'List PvXt', 'url'=>array('index')),
	array('label'=>'Create PvXt', 'url'=>array('create')),
	array('label'=>'Update PvXt', 'url'=>array('update', 'id'=>$model->pv_plant_id)),
	array('label'=>'Delete PvXt', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->pv_plant_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PvXt', 'url'=>array('admin')),
);
?>

<h1>View PvXt #<?php echo $model->pv_plant_id; ?></h1>

<?php
	print $this->renderPartial("_view_body", array(
		'model'=>$model,
	));
?>