<?php
$this->breadcrumbs=array(
	'Pv Xts'=>array('index'),
	$model->pv_plant_id=>array('view','id'=>$model->pv_plant_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PvXt', 'url'=>array('index')),
	array('label'=>'Create PvXt', 'url'=>array('create')),
	array('label'=>'View PvXt', 'url'=>array('view', 'id'=>$model->pv_plant_id)),
	array('label'=>'Manage PvXt', 'url'=>array('admin')),
);
?>

<h1>Update PvXt <?php echo $model->pv_plant_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>