<?php
$this->breadcrumbs=array(
	'Pv Xts',
);

$this->menu=array(
	array('label'=>'Create PvXt', 'url'=>array('create')),
	array('label'=>'Manage PvXt', 'url'=>array('admin')),
);
?>

<h1>Pv Xts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
