<?php
$this->breadcrumbs=array(
	'Pv Xts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PvXt', 'url'=>array('index')),
	array('label'=>'Manage PvXt', 'url'=>array('admin')),
);
?>

<h1>Create PvXt</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>