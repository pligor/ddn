<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'pv_plant_id'); ?>
		<?php echo $form->textField($model,'pv_plant_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'perioxi_dei_id'); ?>
		<?php echo $form->textField($model,'perioxi_dei_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'protocol'); ?>
		<?php echo $form->textField($model,'protocol'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'eksairesi_rae'); ?>
		<?php echo $form->textField($model,'eksairesi_rae',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->