<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'procedure_step_id'); ?>
		<?php echo $form->textField($model,'procedure_step_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'diatiposi_oron_procedure_step_id'); ?>
		<?php echo $form->textField($model,'diatiposi_oron_procedure_step_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->