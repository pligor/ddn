<?php
$this->breadcrumbs=array(
	'Simvasi Sindesises'=>array('index'),
	$model->procedure_step_id=>array('view','id'=>$model->procedure_step_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SimvasiSindesis', 'url'=>array('index')),
	array('label'=>'Create SimvasiSindesis', 'url'=>array('create')),
	array('label'=>'View SimvasiSindesis', 'url'=>array('view', 'id'=>$model->procedure_step_id)),
	array('label'=>'Manage SimvasiSindesis', 'url'=>array('admin')),
);
?>

<h1>Update SimvasiSindesis <?php echo $model->procedure_step_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>