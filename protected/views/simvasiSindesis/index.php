<?php
$this->breadcrumbs=array(
	'Simvasi Sindesises',
);

$this->menu=array(
	array('label'=>'Create SimvasiSindesis', 'url'=>array('create')),
	array('label'=>'Manage SimvasiSindesis', 'url'=>array('admin')),
);
?>

<h1>Simvasi Sindesises</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
