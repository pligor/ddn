<?php 
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'procedure_step_id',
		//'diatiposi_oron_procedure_step_id',
		'arithmos_paroxis',
	),
	'itemCssClass' => array('even','odd'),	//MPOREI NA XRISIMOPOIHTHEI ENALLAKS
));
 ?>

<?php
	print $this->renderPartial("/procedureStep/_view_body", array(
		'model'=>$model->procedureStep,
		'postfix' => 'Σύμβασης Σύνδεσης'
	));
?>