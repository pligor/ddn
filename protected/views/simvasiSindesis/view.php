<?php
$this->breadcrumbs=array(
	'Simvasi Sindesises'=>array('index'),
	$model->procedure_step_id,
);

$this->menu=array(
	array('label'=>'List SimvasiSindesis', 'url'=>array('index')),
	array('label'=>'Create SimvasiSindesis', 'url'=>array('create')),
	array('label'=>'Update SimvasiSindesis', 'url'=>array('update', 'id'=>$model->procedure_step_id)),
	array('label'=>'Delete SimvasiSindesis', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->procedure_step_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SimvasiSindesis', 'url'=>array('admin')),
);
?>

<h1>View SimvasiSindesis #<?php echo $model->procedure_step_id; ?></h1>

<?php
	print $this->renderPartial("_view_body", array(
		'model'=>$model,
	));
?>
