<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=> $this->id .'-select-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php print "Παρακαλώ επιλέξτε το". $form->labelEx($model,'id'); ?>
		<?php
		//print $form->textField($model,'island_name'); 
		print $form->dropDownList( $model, 'id', $listData );
		//public string dropDownList(CModel $model, string $attribute, array $data, array $htmlOptions=array ( ));
		
		?>
		<?php echo $form->error($model,'id'); ?>
		<div class="hint">
		<?php print Yii::t('', 'Please note') .': '. Yii::t('', 'If you stay inactive too long you will be asked again for the island'); ?>
		</div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Επιλογή'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->