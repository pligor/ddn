<?php
$this->menu = array(
	array(
		'label'=>'Επιστροφή στο μενού',
		'url' => array(
			'submenu',
			'pv_plant_id' => $pv_plant_id,
		),
	),
);
?>

<h1>Επεξεργασία Στοιχείων: <?php print $class::getTitle(); ?></h1>

<div class='form'>
<?php
print $form->render();
?>
</div>