<?php
$this->menu=array(
	array(
		'label'=>'Επεξεργασία Επωνυμίας',
		'url' => array(
			'editer/prosopo',
			//'params' => $params,
		),
	),
	array(
		'label'=>'Επεξεργασία Διεύθυνσης',
		'url' => array(
			'editer/address',
			//'params' => $params,
		),
	),
	array(
		'label'=>'Επεξεργασία Καταχώρησης',
		'url' => array(
			'editer/submenu',
			//'params' => $params,
		),
	),
);
?>

<h1>Κεντρικό Μενού Επεξεργασίας</h1>

<p>
Παρακαλώ επιλέξτε από το διπλανό μενού τις διάφορες επιλογές για να αλλάξετε/επεξεργαστείτε τα υπάρχοντα δεδομένα
</p>

<?php if(Yii::app()->user->hasFlash('edit_result')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('edit_result'); ?>
</div>
<?php endif; ?>