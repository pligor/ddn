<?php
$this->menu = $menu;
?>
<h1>Μενού επεξεργασίας καταχώρησης</h1>

<?php
	print $this->renderPartial('/pvPlant/_basic_identification', compact('perioxi_dei_name','protocol','categ') );
?>

<?php if(Yii::app()->user->hasFlash('edit_result')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('edit_result'); ?>
</div>
<?php endif; ?>