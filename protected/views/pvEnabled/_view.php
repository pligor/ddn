<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('pv_plant_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->pv_plant_id), array('view', 'id'=>$data->pv_plant_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('real_power')); ?>:</b>
	<?php echo CHtml::encode($data->real_power); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ypostathmos')); ?>:</b>
	<?php echo CHtml::encode($data->ypostathmos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grammi_dianomis')); ?>:</b>
	<?php echo CHtml::encode($data->grammi_dianomis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('thesi_syndesis')); ?>:</b>
	<?php echo CHtml::encode($data->thesi_syndesis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ys_mt_xt')); ?>:</b>
	<?php echo CHtml::encode($data->ys_mt_xt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anaxorisi')); ?>:</b>
	<?php echo CHtml::encode($data->anaxorisi); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('activation_procedure_step_id')); ?>:</b>
	<?php echo CHtml::encode($data->activation_procedure_step_id); ?>
	<br />

	*/ ?>

</div>