<?php
$this->breadcrumbs=array(
	'Pv Enableds'=>array('index'),
	$model->pv_plant_id=>array('view','id'=>$model->pv_plant_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PvEnabled', 'url'=>array('index')),
	array('label'=>'Create PvEnabled', 'url'=>array('create')),
	array('label'=>'View PvEnabled', 'url'=>array('view', 'id'=>$model->pv_plant_id)),
	array('label'=>'Manage PvEnabled', 'url'=>array('admin')),
);
?>

<h1>Update PvEnabled <?php echo $model->pv_plant_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>