<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'pv_plant_id'); ?>
		<?php echo $form->textField($model,'pv_plant_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'real_power'); ?>
		<?php echo $form->textField($model,'real_power'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ypostathmos'); ?>
		<?php echo $form->textField($model,'ypostathmos',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'grammi_dianomis'); ?>
		<?php echo $form->textField($model,'grammi_dianomis',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'thesi_syndesis'); ?>
		<?php echo $form->textField($model,'thesi_syndesis',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ys_mt_xt'); ?>
		<?php echo $form->textField($model,'ys_mt_xt',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'anaxorisi'); ?>
		<?php echo $form->textField($model,'anaxorisi',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'activation_procedure_step_id'); ?>
		<?php echo $form->textField($model,'activation_procedure_step_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->