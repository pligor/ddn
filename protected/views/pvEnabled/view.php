<?php
$this->breadcrumbs=array(
	'Pv Enableds'=>array('index'),
	$model->pv_plant_id,
);

$this->menu=array(
	array('label'=>'List PvEnabled', 'url'=>array('index')),
	array('label'=>'Create PvEnabled', 'url'=>array('create')),
	array('label'=>'Update PvEnabled', 'url'=>array('update', 'id'=>$model->pv_plant_id)),
	array('label'=>'Delete PvEnabled', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->pv_plant_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PvEnabled', 'url'=>array('admin')),
);
?>

<h1>View PvEnabled #<?php echo $model->pv_plant_id; ?></h1>

<?php
	print $this->renderPartial("_view_body", array(
		'model'=>$model,
	));
?>