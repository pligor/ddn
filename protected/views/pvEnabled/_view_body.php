<?php
$attributes = array(
	//'pv_plant_id',
	//'activation_procedure_step_id',
	array(
		"label" => $model->getAttributeLabel('tracker'),
		"value" => $model->tracker ? 'ΝΑΙ' : 'ΟΧΙ',
	),
	'real_power',
	'ypostathmos',
	'grammi_dianomis',
	'thesi_syndesis',
	'anaxorisi',
);
if($model->ys_mt_xt)
{
	$attributes[] = 'ys_mt_xt';
}

$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes' =>	$attributes,
));
?>