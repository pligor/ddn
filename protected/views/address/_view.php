<div class="view">
	<b><?php echo CHtml::link(Yii::t('', 'Select'), array('return_address_id', 'address_id'=>$data->id)); ?></b>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dimos_id')); ?>:</b>
	<?php echo CHtml::encode($data->dimos->dimos_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('thesi')); ?>:</b>
	<?php echo CHtml::encode($data->thesi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('noumero')); ?>:</b>
	<?php echo CHtml::encode($data->noumero); ?>
	<br />
</div>