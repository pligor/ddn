<?php
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		//'dimos_id',
		array(
			"label"=>$model->dimos->getAttributeLabel('nomos_id'),
			"value"=>$model->dimos->nomos->nomos_name,
		),
		array(
			"label"=>$model->getAttributeLabel('dimos_id'),
			"value"=>$model->dimos->dimos_name,
		),
		'thesi',
		'noumero',
		'latitude',
		'longitude',
	),
	"nullDisplay" => CHtml::tag('span', array('class'=>'null'), '(δεν έχει οριστεί)'),
));
?>