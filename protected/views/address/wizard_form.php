<?php print $event->sender->menu->run(); ?>

<div>
<?php print Yii::t('', 'Step') .' '. $event->sender->currentStep .'/'. $event->sender->stepCount; ?>
</div>

<h3>
<?php print $event->sender->getStepLabel($event->step); ?>
</h3>

<?php print CHtml::tag('div', array('class'=>'form'), $form->render()); ?>