<?php
$this->breadcrumbs=array(
	'Activations'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Activation', 'url'=>array('index')),
	array('label'=>'Create Activation', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('activation-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Activations</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'activation-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'procedure_step_id',
		'simvasi_polisis_procedure_step_id',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
