<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'procedure_step_id'); ?>
		<?php echo $form->textField($model,'procedure_step_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'simvasi_polisis_procedure_step_id'); ?>
		<?php echo $form->textField($model,'simvasi_polisis_procedure_step_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->