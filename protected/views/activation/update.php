<?php
$this->breadcrumbs=array(
	'Activations'=>array('index'),
	$model->procedure_step_id=>array('view','id'=>$model->procedure_step_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Activation', 'url'=>array('index')),
	array('label'=>'Create Activation', 'url'=>array('create')),
	array('label'=>'View Activation', 'url'=>array('view', 'id'=>$model->procedure_step_id)),
	array('label'=>'Manage Activation', 'url'=>array('admin')),
);
?>

<h1>Update Activation <?php echo $model->procedure_step_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>