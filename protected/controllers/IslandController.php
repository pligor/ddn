<?php

class IslandController extends Controller {
	public $layout='//layouts/column1';
	public $defaultAction = 'index';
	
	public function actionPersistent_island_id($island_id) {
		Yii::app()->user->setState('island_id', $island_id);
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionSelect($params) {
	//redirect_uri
		$params = CHtmlEx::array_url($params, true);
		extract(end($params));
		
	    $class = ucfirst($this->id);
		$model = new $class();
	
	    if(isset($_POST['Island']))
	    {
	        $model->attributes=$_POST['Island'];

	        if($model->validate(array("id")))
	        {	        	        	
	        	//$this->redirect(array('submitter/index', 'island_id'=>$model->id));
	        	//controller/action is mandatory, otherwise current controller is implied and the parameter is the action (simply we write the action)
				
	        	$this->redirect(array(
	        		$redirect_uri,
	        		'island_id' => $model->id,
	        	));
	        }
	    }
	    
	    $listData = $model->getIslands();
	    
	    //$this->filterIslands($listData);
	    
	    $this->render('select', compact('model', 'listData') ); 
	}
	
	private function filterIslands(& $listData) {
		$validIslandIds =  Yii::app()->user->model->validIslandIds;
		
		foreach($listData as $key => $data)
		{
			if( !in_array($key, $validIslandIds) )
				unset($listData[$key]);
		}
	}
}