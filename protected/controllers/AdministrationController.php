<?php

class AdministrationController extends Controller {
	public $layout = '//layouts/column2';
	public $defaultAction = 'index';
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(
			array(
				'allow',
				'roles' => array('administrator'),
			),
			array(
				'deny',  // deny all users
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * @return array action filters
	 * @see CController::filters()
	 */
	public function filters() {	//http://www.yiiframework.com/doc/guide/1.1/en/basics.controller
		return array(
			'accessControl', // perform access control for CRUD operations
			/*array(
                'application.filters.CheckAccessControl - index',	//all actions except index
                'role' => 'administrator',
            ),*/
		);
	}
	
	public function actionClear_db() {
		
		$component = new Wipent();
		$affectedRows = $component->wipeAll();
		
		$this->render('clear_db',compact('affectedRows'));
	}

	public function actionIndex() {
		$this->render('index');
	}
	
	public function actionInit_dimous_nomous() {
		//repeat complete deletion of tables because of order of initialization (foreign key constraint)
		Dimos::model()->deleteAll();
		Nomos::model()->deleteAll();
		
		$isLoaded = Nomos::loadDefaultData() && Dimos::loadDefaultData();
		
		if($isLoaded) {
			$reportResult = Yii::t('', 'loaded successfully') ." (:";
		}
		else {
			$reportResult = Yii::t('', 'loading failed') ." :(";
		}
		
		$modelNames = array('Dimos','Nomos');
		
		$this->render('default_init', compact('modelNames','reportResult'));
	}
	
	public function actionInit_areas_islands() {
		//repeat complete deletion of tables because of order of initialization (foreign key constraint)
		Island::model()->deleteAll();
		PerioxiDei::model()->deleteAll();
		
		$perioxiDei = new PerioxiDei();
		$island = new Island();

		$isLoaded = $perioxiDei->loadFixtureData() && $island->loadFixtureData();
		
		if($isLoaded) {
			$reportResult = Yii::t('', 'loaded successfully') ." (:";
		}
		else {
			$reportResult = Yii::t('', 'loading failed') ." :(";
		}
		
		$modelNames = array('PerioxiDei','Island');
		
		$this->render('default_init', compact('modelNames','reportResult'));
	}
	
	public function actionInit_island_groups() {
		//repeat complete deletion of tables because of order of initialization (foreign key constraint)
		IslandGroupIsland::model()->deleteAll();
		IslandGroup::model()->deleteAll();
		
		//$isLoaded = PerioxiDei::loadDefaultData() && Island::loadDefaultData();
		
		$isLoaded = IslandGroup::loadDefaultData() && IslandGroupIsland::loadDefaultData();
		
		if($isLoaded) {
			$reportResult = Yii::t('', 'loaded successfully') ." (:";
		}
		else {
			$reportResult = Yii::t('', 'loading failed') ." :(";
		}
		
		$modelNames = array('IslandGroup','IslandGroupIsland');
		
		$this->render('default_init', compact('modelNames','reportResult'));
	}
	
	public function actionInit_dimos_island() {	
		$isLoaded = DimosIsland::loadDefaultData();
		
		if($isLoaded) {
			$reportResult = Yii::t('', 'loaded successfully') ." (:";
		}
		else {
			$reportResult = Yii::t('', 'loading failed') ." :(";
		}
		
		$modelNames = array('DimosIsland');
		
		$this->render('default_init', compact('modelNames','reportResult'));
	}
	
	public function actionInit_eidi_paragogon() {	
		$isLoaded = EidosParagogou::loadFixtureData();
		
		if($isLoaded) {
			$reportResult = Yii::t('', 'loaded successfully') ." (:";
		}
		else {
			$reportResult = Yii::t('', 'loading failed') ." :(";
		}
		
		$modelNames = array('EidosParagogou');
		
		$this->render('default_init', compact('modelNames','reportResult'));
	}
}