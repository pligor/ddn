<?php

class ReporterController extends Controller {
	/**
	 * @var string
	 */
	public $layout='//layouts/column2';
	
	/**
	 * @var string
	 */
	public $defaultAction = 'index';
	
	/**
	 * @var integer
	 */
	public $pv_plant_id = null;
	
	/**
	 * @see CController::filters()
	 * @return array action filters
	 */
	public function filters() {	//http://www.yiiframework.com/doc/guide/1.1/en/basics.controller
		$filters = array(
			//'accessControl',
			/*array(
                'application.filters.CheckAccessControl - index',	//all actions except index
                'role' => 'island_manager',
            ),*/
            array( //index only
                'application.filters.FinderFilter + index',
			),
		);
		
		return $filters;
	}
	
	/**
	 * @todo PROBABLY WE SHOULD REPLACE THIS FUNCTION LATER WITH A REAL FILTER CLASS
	 * @throws CHttpException
	 * @return int
	 */
	protected function getIslandId() {
		if( Yii::app()->user->hasState('island_id') ) {
			return Yii::app()->user->island_id;
		}
		else {
			$message = Yii::t('', 'Please choose an Island before you proceed');
			throw new CHttpException(404, $message);
		}
	}
	
	/**
	 * @param integer $pv_plant_id
	 */
	protected function getSimpleMenu($pv_plant_id) {
		$simpleMenu = array();
		foreach(ProcedureStep::getWorkflow() as $class) {
			$simpleMenu[] = array(
				'label' => Yii::t('', 'Report data for step') .': '. $class::getTitle(),
				'url' => array(
					'simple_report',
					'params' => CHtmlEx::array_url(compact('pv_plant_id','class')),
				),
			);
		}
		return $simpleMenu;
	}
	
	/**
	 * @param integer $pv_plant_id
	 */
	protected function getMenu($pv_plant_id) {
		$menu = array(
			array(
				'label'=>'Πλήρης Αναφορά',
				'url'=>array(
					'full_report',
					'params' => CHtmlEx::array_url(compact('pv_plant_id')),
				),
			),
			array(
				'label'=>'Αναφορά Παρατηρήσεων',
				'url' => array(
					'list_comments',
					'params' => CHtmlEx::array_url(compact('pv_plant_id')),
				),
			),
		);
		return array_merge($menu, $this->getSimpleMenu($pv_plant_id));
	}

	public function actionIndex() {
		$island_id = $this->getIslandId();
		
		$pv_plant_id = $this->pv_plant_id;
		
		$perioxi_dei_name = CHtmlEx::loadModel('Island', $island_id)->perioxiDei->perioxi_dei_name;
		$model = CHtmlEx::loadModel('PvPlant', $pv_plant_id);
		$protocol = $model->protocol;
		$categ = $model->child->descr;
		
		$menu = $this->getMenu($pv_plant_id);
		
		$this->render('index', compact('pv_plant_id','perioxi_dei_name','protocol', 'categ','menu'));
	}
	
	private function ArxikiAitisi(& $models) {
		$models["Prosopo"] = $models["PvPlant"]->prosopo;
		$models["Address"] = $models["PvPlant"]->address;
	}
	
	private function DiatiposiOron(& $models) {
		$child = $models["PvPlant"]->child;
		$models[get_class($child)] = $child;
	}
	
	private function SimvasiSindesis(& $models) {
	}
	
	private function SimvasiPolisis(& $models) {
	}
	
	private function Activation(& $models) {
		$models['PvEnabled'] = $models["PvPlant"]->pvEnabled;
	}
	
	public function actionSimple_report($params) {
	//pv_plant_id, class
		$params = CHtmlEx::array_url($params, true);
		extract($params);
		
		$models = array();
		$models['PvPlant'] = CHtmlEx::loadModel('PvPlant', $pv_plant_id);
		
		$procedures = $models["PvPlant"]->procedures;
		
		try {
			if( !in_array($class, array_keys($procedures)) ) {
				$message = Yii::t('', 'There are no information') .': '. $class::getTitle();
				throw new Exception($message);	
			}
			
			$this->$class($models);
			$models[$class] = $procedures[$class];
		}
		catch(Exception $e) {
			$models = array('PvPlant' => $models['PvPlant']);
			$title = $e->getMessage();
		}
		
		if(!isset($menu)) {
			$menu =array(
				array(
					'label' => Yii::t('', 'Return back to menu'),
					'url' => array(
						'index',
						'pv_plant_id'=>$pv_plant_id,
					),
				),
			);
		}
		
		if(!isset($title)) {
			$title = Yii::t('', 'Report data for step') .': '. $class::getTitle();
		}
		
		$this->render('report', compact('models','title','menu'));
	}
	
	public function actionFull_report($params) {
	//pv_plant_id, menu
		$params = CHtmlEx::array_url($params, true);
		extract($params);
		
		$models = array();
		$models['PvPlant'] = CHtmlEx::loadModel('PvPlant', $pv_plant_id);
		
		$procedures = $models['PvPlant']->procedures;
		$prodsClasses = array_keys($procedures);
		
		foreach(ProcedureStep::getWorkflow() as $class) {
			if( !in_array($class, $prodsClasses) ) {
				continue;	
			}
			
			$this->$class($models);
			$models[$class] = $procedures[$class];
		}
		
		if(!isset($menu)) {
			$menu =array(
				array(
					'label' => Yii::t('', 'Return back to menu'),
					'url' => array(
						'index',
						'pv_plant_id'=>$pv_plant_id,
					),
				),
			);
		}
		
		if(!isset($title)) {
			$title = Yii::t('', 'Full Report');
		}
		
		$this->render('report', compact('models','title','menu'));
	}
	
	public function actionList_comments($params) {
	//pv_plant_id, menu
		$params = CHtmlEx::array_url($params, true);
		extract($params);
		
		$models = array();
		$models['PvPlant'] = CHtmlEx::loadModel('PvPlant', $pv_plant_id);
		
		$criteria=new CDbCriteria;
		$criteria->compare('pv_plant_id', $pv_plant_id);
		
		$dataProvider=new CActiveDataProvider('Comment', compact('criteria'));
		
		if(!isset($menu)) {
			$menu =array(
				array(
					'label' => Yii::t('', 'Return back to menu'),
					'url'=>array(
						'index',
						'pv_plant_id'=>$pv_plant_id,
					),
				),
			);
		}
		
		if(!isset($title)) {
			$title = Yii::t('', 'Comments') .' / '.  Yii::t('', 'Remarks') .' / '. Yii::t('', 'Notices');
		}
		
		$this->render($this->action->id, compact('dataProvider','menu','title') );		
	}
}