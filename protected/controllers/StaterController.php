<?php

class StaterController extends Controller {
	public $layout = '//layouts/column2';
	
	public $menuLabels;
	
	/**
	 * @see CController::init()
	 */
	public function init() {
            $this->menuLabels = array(
                'index' => Yii::t('', 'Return back to menu'),
                'arxiki_aitisi' => Yii::t('', 'Number & Power of Αρχικές Αιτήσεις per island'),
                'diatiposi_oron' => Yii::t('', 'Number & Power of Διατυπώσεις Όρων per island'),
                'simvasi_sindesis' => Yii::t('', 'Number & Power of Συμβάσεις Σύνδεσης per island'),
                'simvasi_polisis' => Yii::t('', 'Number & Power of Συμβάσεις Πώλησης per island'),
                'activation' => Yii::t('', 'Number & Power of Activations per island (based on installed power)'),
                'activation_nom' => Yii::t('', 'Number & Power of Activations per island (based on initially declared power)'),
                'liksi' => Yii::t('', 'Number & Power of those who had their Terms Ended'),
                'pv_plants_per_area' => Yii::t('', 'Pv Plants') .' '. Yii::t('', 'per') .' '. Yii::t('', 'Area'),
                'count_killed_last_month' => Yii::t('', 'Pv Plants') .' '. Yii::t('', 'got terminated') .' '. Yii::t('', 'last month'),
                'killed_with_simvasi_sindesis' => Yii::t('', 'Killed with Simvasi Sindesis'),
                'simvasi_polisis_but_activation' => Yii::t('', 'Αριθμός και Ισχύς Συμβάσεων Πώλησης πλην Ενεργοποιημένα'),
                'should_kill_xt' => Yii::t('', 'Should have been killed') . ' XT',
                'should_kill_mt' => Yii::t('', 'Should have been killed') . ' MT',
                'should_kill_steges' => Yii::t('', 'Should have been killed') . ' ΣΤΕΓΕΣ',
                'should_kill_steges' => Yii::t('', 'Should have been killed') . ' ΣΤΕΓΕΣ',
                'monthly_arxiki_aitisi' => Yii::t('', 'Monthly Number & Power') .' Αρχικών Αιτήσεων Στεγών',
                'monthly_activation' => Yii::t('', 'Monthly Number & Power') .' Ενεργοποιήσεων Στεγών',
            );
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(
			/*array(	//allow all registered users to view initial menu
				'allow',
				'actions' => array('index'),
				'users' => array('@'),
			),*/
			array(
				'allow',
				'roles' => array('super_user'),
			),
			array(
				'deny',  // deny all users
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * @return array action filters
	 * @see CController::filters()
	 */
	public function filters() {	//http://www.yiiframework.com/doc/guide/1.1/en/basics.controller
		return array(
			'accessControl', // perform access control for CRUD operations
			/*array(
                'application.filters.CheckAccessControl - index',	//all actions except index
                'role' => 'administrator',
            ),*/
		);
	}
	
	/**
	 * 
	 */
	public function actionIndex() {
		$menu = $this->getMenu(false);
		
		$perioxes = PerioxiDei::model()->count();
		$nisia = Island::model()->count();
		
		$this->render('index',compact('menu','perioxes','nisia'));
	}
        
	public function actionMonthly_arxiki_aitisi() {
		$sql = "
		SELECT
		DATE_FORMAT(
			FROM_UNIXTIME(procedure_step.execution_date),
			'%Y-%m') as yyyy_mm,
			COUNT(*) as monthly_count,
			SUM(arxiki_aitisi.nominal_power) as monthly_power
		FROM pv_plant,procedure_step,arxiki_aitisi
		WHERE pv_plant_id = pv_plant.id
		AND procedure_step_id = procedure_step.id
		AND pv_plant.id IN (
			SELECT pv_plant_id
			FROM pv_steges
		)
		GROUP BY yyyy_mm
		;";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    'pagination'=>array(
		        'pageSize'=>$totalItemCount,	//so disable pagination
			),
			'keyField'=>'yyyy_mm',
		));
		
		$columns = array(
			array(
				'name' => 'yyyy_mm',
				'header' => Yii::t('', 'Date'),
			),
			array(
				'name' => 'monthly_count',
				'header' => Yii::t('', 'Number'),
			),
			array(
				'name' => 'monthly_power',
				'header' => Yii::t('', 'Power (kW)'),
				'value' => 'number_format($data["monthly_power"],2)',
			),
		);
		
		$this->render('grid', compact('dataProvider','columns'));
	}
        
	public function actionMonthly_activation() {
		$sql = "
		SELECT
		DATE_FORMAT(
			FROM_UNIXTIME(procedure_step.execution_date),
			'%Y-%m'
		) as yyyy_mm,
			COUNT(*) as monthly_count,
			SUM(pv_enabled.real_power) as monthly_power
		FROM
			pv_plant,
			procedure_step,
			pv_enabled
		WHERE procedure_step.pv_plant_id = pv_plant.id
		AND pv_plant.id = pv_enabled.pv_plant_id
		AND pv_plant.id IN (
			SELECT pv_plant_id
			FROM pv_steges
		)
		AND procedure_step.id IN
		(
			SELECT procedure_step_id
			FROM activation
		)
		GROUP BY yyyy_mm
		;";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    'pagination'=>array(
		        'pageSize'=>$totalItemCount,	//so disable pagination
			),
			'keyField'=>'yyyy_mm',
		));
		
		$columns = array(
			array(
				'name' => 'yyyy_mm',
				'header' => Yii::t('', 'Date'),
			),
			array(
				'name' => 'monthly_count',
				'header' => Yii::t('', 'Number'),
			),
			array(
				'name' => 'monthly_power',
				'header' => Yii::t('', 'Power (kW)'),
				'value' => 'number_format($data["monthly_power"],2)',
			),
		);
		
		$this->render('grid', compact('dataProvider','columns'));
	}
	
	public function actionShould_kill_xt() {
		$this->render('grid', $this->should_kill('xt'));
	}
	public function actionShould_kill_mt() {
		$this->render('grid', $this->should_kill('mt'));
	}
	public function actionShould_kill_steges() {
		$this->render('grid', $this->should_kill('steges'));
	}
	protected function should_kill($table_postfix) {
		$model = PvCat::model()->find(array(
			'condition' => 'table_name = :table_name',
			'params' => array(
				':table_name' => "pv_$table_postfix",
			),
		));
		
		$sql = "
		SELECT island_name, priority, pv_plant.id AS keyField,
		DATEDIFF(
			DATE_ADD(
				FROM_UNIXTIME(execution_date),
				INTERVAL :end_terms_days DAY
			),
			NOW()
		)AS days_difference
		FROM pv_plant, diatiposi_oron, island, procedure_step
		WHERE
		procedure_step.pv_plant_id = pv_plant.id
		AND
		diatiposi_oron.procedure_step_id = procedure_step.id
		AND
		diatiposi_oron.procedure_step_id NOT IN
		(
			SELECT diatiposi_oron_procedure_step_id
			FROM simvasi_sindesis
		)
		AND
		pv_plant.id NOT IN
		(
			SELECT pv_plant_id
			FROM killed
		)
		AND
		pv_plant.island_id = island.id
		AND
		pv_plant.id IN
		(
			SELECT pv_plant_id
			FROM pv_$table_postfix
		)
		HAVING days_difference < :days_diff;
		";
		
		$bindParams = array(
			':days_diff' => 0,
			':end_terms_days' => $model->end_terms_days,
		);
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll(true,$bindParams) );
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    /*'pagination'=>array(
		        'pageSize' => 10,
			),*/
			'params' => $bindParams,
			'keyField' => 'keyField',
		));
		
		$columns = array(
			array(
				'name' => 'island_name',
				'header' => Yii::t('', 'Island'),
			),
			array(
				'name' => 'priority',
				'header' => Yii::t('', 'Priority Number'),
			),
			array(
				'name' => 'days_difference',
				'header' => Yii::t('', 'Days Difference'),
				'value' => 'abs($data["days_difference"])',
			),
		);
		
		return compact('dataProvider','columns');
	}
	
	public function actionKilled_with_simvasi_sindesis() {
		$sql = "
		SELECT island_name, priority, pv_plant.id AS keyField
		FROM pv_plant, island, arxiki_aitisi
		WHERE pv_plant.island_id = island.id
		AND procedure_step_id IN
		(
			SELECT procedure_step.id
			FROM procedure_step
			WHERE procedure_step.pv_plant_id = pv_plant.id
		)
		AND procedure_step_id IN
		(
			SELECT diatiposi_oron.arxiki_aitisi_procedure_step_id
			FROM diatiposi_oron
			WHERE diatiposi_oron.procedure_step_id IN
			(
				SELECT diatiposi_oron_procedure_step_id
				FROM simvasi_sindesis
			)
		)
		AND pv_plant.id IN
		(
			SELECT pv_plant_id
			FROM killed
		)
		ORDER BY island_name, priority
		";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    'pagination'=>array(
		        'pageSize'=>$totalItemCount,	//so disable pagination
			),
			/*'sort'=>array(
		        'attributes'=>array(
		             'id', 'username', 'email',
				),
			),
			'params'=>array(
				'prosopo_id'=>$prosopo_id,
			),//*/
			'keyField'=>'keyField',
		));
		
		$columns = array(
			array(
				'name' => 'island_name',
				'header' => Yii::t('', 'Island'),
			),
			array(
				'name' => 'priority',
				'header' => Yii::t('', 'Priority Number'),
				//'value' => 'number_format($data["power"],2)',
			),
		);
		
		$this->render('grid', compact('dataProvider','columns'));
	}
	
	public function actionSimvasi_polisis_but_activation() {
		$sql = "
		SELECT COUNT(*) AS plithos, SUM(nominal_power) AS power
		FROM pv_plant, arxiki_aitisi
		WHERE procedure_step_id IN
		(
			SELECT procedure_step.id
			FROM procedure_step
			WHERE procedure_step.pv_plant_id = pv_plant.id
		)
		AND procedure_step_id IN
		(
			SELECT diatiposi_oron.arxiki_aitisi_procedure_step_id
			FROM diatiposi_oron
			WHERE diatiposi_oron.procedure_step_id IN
			(
				SELECT diatiposi_oron_procedure_step_id
				FROM simvasi_sindesis
				WHERE simvasi_sindesis.procedure_step_id IN
				(
					SELECT simvasi_sindesis_procedure_step_id
					FROM simvasi_polisis
					WHERE simvasi_polisis.procedure_step_id NOT IN
					(
						SELECT simvasi_polisis_procedure_step_id
						FROM activation
					)
				)
			)
		)
		";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    'pagination'=>array(
		        'pageSize'=>10,
			),
			'keyField'=>'plithos',
		));
		
		$columns = array(
			array(
				'name' => 'plithos',
				'header' => Yii::t('', 'Number'),
			),
			array(
				'name' => 'power',
				'header' => Yii::t('', 'Declared Power (kW)'),
				'value' => 'number_format($data["power"],2)',
			),
		);
		
		$this->render('grid', compact('dataProvider','columns'));
	}
	
	public function actionCount_killed_last_month() {
		$sql = "
		SELECT COUNT( DISTINCT DATE( FROM_UNIXTIME(date_killed) ) ) AS plithos
		FROM `ddn`.`killed`
		WHERE YEAR(FROM_UNIXTIME(date_killed))=YEAR(NOW())
		AND MONTH(FROM_UNIXTIME(date_killed))=MONTH(NOW())-1
		ORDER BY plithos DESC;
		";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    'pagination'=>array(
		        'pageSize'=>10,
			),
			/*'sort'=>array(
		        'attributes'=>array(
		             'id', 'username',
				),
			),
			'params'=>array(
				'prosopo_id'=>$prosopo_id,
			),*/
			'keyField' => 'plithos',
		));
		
		$columns = array(
			array(
				'name' => 'plithos',
				'header' => Yii::t('', 'Number'),
			),
		);
		
		$this->render('grid', compact('dataProvider','columns'));
	}
	
	public function actionPv_plants_per_area() {
		$sql = "
		SELECT perioxi_dei_name, count(*) as plithos
		FROM pv_plant, island, perioxi_dei
		WHERE pv_plant.island_id = island.id
		AND perioxi_dei_id = perioxi_dei.id
		GROUP BY perioxi_dei.id
		ORDER BY plithos DESC
		";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    'pagination'=>array(
		        'pageSize'=>10,
			),
			/*'sort'=>array(
		        'attributes'=>array(
		             'id', 'username',
				),
			),
			'params'=>array(
				'prosopo_id'=>$prosopo_id,
			),*/
			'keyField'=>'perioxi_dei_name',
		));
		
		$columns = array(
			array(
				'name' => 'perioxi_dei_name',
				'header' => Yii::t('', 'Area'),
			),
			array(
				'name' => 'plithos',
				'header' => Yii::t('', 'Number'),
			),
		);
		
		$this->render('grid', compact('dataProvider','columns'));
	}
	
	public function actionActivation_nom()
	{	
		//SELECT SQL_CALC_FOUND_ROWS island_name, COUNT(*) AS plithos, SUM(nominal_power) AS power
		$sql = "
		SELECT island_name, COUNT(*) AS plithos, SUM(nominal_power) AS power
		FROM pv_plant, island, arxiki_aitisi
		WHERE pv_plant.id IN
		(
			SELECT pv_enabled.pv_plant_id
			FROM pv_enabled
		)
		AND
		pv_plant.island_id=island.id
		AND procedure_step_id IN
		(
			SELECT procedure_step.id
			FROM procedure_step
			WHERE procedure_step.pv_plant_id = pv_plant.id
		)
		GROUP BY island_id
		";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		//$totalItemCount = Yii::app()->db->createCommand('SELECT FOUND_ROWS()')->queryScalar();	//->bindValue(':prosopo_id', $prosopo_id)
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    /*'sort'=>array(
		        'attributes'=>array(
		             'id', 'username', 'email',
				),
			),*/
		    'pagination'=>array(
		        'pageSize'=>10,
			),
			/*'params'=>array(
				'prosopo_id'=>$prosopo_id,
				'island_id'=>$island_id,
			),*/
			'keyField'=>'island_name',
		));
		
		$columns = array(
			array(
				'name' => 'island_name',
				'header' => Yii::t('', 'Island'),
			),
			array(
				'name' => 'plithos',
				'header' => Yii::t('', 'Number'),
			),
			array(
				'name' => 'power',
				'header' => Yii::t('', 'Declared Power (kW)'),
				'value' => 'number_format($data["power"],2)',
			),
		);
		
		$this->render('grid', compact('dataProvider','columns'));
	}
	
	public function actionActivation()
	{
		$sql = "
		SELECT island_name, COUNT(*) AS plithos, SUM(real_power) AS power
		FROM pv_enabled, pv_plant, island
		WHERE pv_plant_id=pv_plant.id
		AND
		pv_plant.island_id=island.id
		GROUP BY island_id
		";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    /*'sort'=>array(
		        'attributes'=>array(
		             'id', 'username', 'email',
				),
			),*/
		    'pagination'=>array(
		        'pageSize'=>10,
			),
			/*'params'=>array(
				'prosopo_id'=>$prosopo_id,
				'island_id'=>$island_id,
			),*/
			'keyField'=>'island_name',
		));
		
		$columns = array(
			array(
				'name' => 'island_name',
				'header' => Yii::t('', 'Island'),
			),
			array(
				'name' => 'plithos',
				'header' => Yii::t('', 'Number'),
			),
			array(
				'name' => 'power',
				'header' => Yii::t('', 'Real Power (kW)'),
				'value' => 'number_format($data["power"],2)',
			),
		);
		
		$this->render('grid',compact('dataProvider','columns'));
	}

	public function actionArxiki_aitisi()
	{
		$sql = "
		SELECT island_name, COUNT(*) AS plithos, SUM(nominal_power) AS power
		FROM pv_plant, island, arxiki_aitisi
		WHERE pv_plant.island_id=island.id
		AND procedure_step_id IN
		(
			SELECT procedure_step.id
			FROM procedure_step
			WHERE procedure_step.pv_plant_id = pv_plant.id
		)
		GROUP BY island_id
		";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    /*'sort'=>array(
		        'attributes'=>array(
		             'id', 'username', 'email',
				),
			),*/
		    'pagination'=>array(
		        'pageSize'=>10,
			),
			/*'params'=>array(
				'prosopo_id'=>$prosopo_id,
				'island_id'=>$island_id,
			),*/
			'keyField'=>'island_name',
		));
		
		$columns = array(
			array(
				'name' => 'island_name',
				'header' => Yii::t('', 'Island'),
			),
			array(
				'name' => 'plithos',
				'header' => Yii::t('', 'Number'),
			),
			array(
				'name' => 'power',
				'header' => Yii::t('', 'Power (kW)'),
				'value' => 'number_format($data["power"],2)',
			),
		);
		
		$this->render('grid', compact('dataProvider','columns'));
	}

	public function actionDiatiposi_oron()
	{
		
		$sql = "
		SELECT island_name, COUNT(*) AS plithos, SUM(nominal_power) AS power
		FROM pv_plant, island, arxiki_aitisi
		WHERE pv_plant.island_id = island.id
		AND procedure_step_id IN
		(
			SELECT procedure_step.id
			FROM procedure_step
			WHERE procedure_step.pv_plant_id = pv_plant.id
		)
		AND procedure_step_id IN
		(
			SELECT arxiki_aitisi_procedure_step_id
			FROM diatiposi_oron
		)
		GROUP BY island_id
		";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    /*'sort'=>array(
		        'attributes'=>array(
		             'id', 'username', 'email',
				),
			),*/
		    'pagination'=>array(
		        'pageSize'=>10,
			),
			/*'params'=>array(
				'prosopo_id'=>$prosopo_id,
			),*/
			'keyField'=>'island_name',
		));
		
		$columns = array(
			array(
				'name' => 'island_name',
				'header' => Yii::t('', 'Island'),
			),
			array(
				'name' => 'plithos',
				'header' => Yii::t('', 'Number'),
			),
			array(
				'name' => 'power',
				'header' => Yii::t('', 'Power (kW)'),
				'value' => 'number_format($data["power"],2)',
			),
		);
		
		$this->render('grid', compact('dataProvider','columns'));
	}
	
	public function actionSimvasi_sindesis()
	{
		$sql = "
		SELECT island_name, COUNT(*) AS plithos, SUM(nominal_power) AS power
		FROM pv_plant, island, arxiki_aitisi
		WHERE pv_plant.island_id = island.id
		AND procedure_step_id IN
		(
			SELECT procedure_step.id
			FROM procedure_step
			WHERE procedure_step.pv_plant_id = pv_plant.id
		)
		AND procedure_step_id IN
		(
			SELECT diatiposi_oron.arxiki_aitisi_procedure_step_id
			FROM diatiposi_oron
			WHERE diatiposi_oron.procedure_step_id IN
			(
				SELECT diatiposi_oron_procedure_step_id
				FROM simvasi_sindesis
			)
		)
		GROUP BY island_id
		";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    /*'sort'=>array(
		        'attributes'=>array(
		             'id', 'username', 'email',
				),
			),*/
		    'pagination'=>array(
		        'pageSize'=>10,
			),
			/*'params'=>array(
				'prosopo_id'=>$prosopo_id,
			),*/
			'keyField'=>'island_name',
		));
		
		$columns = array(
			array(
				'name' => 'island_name',
				'header' => Yii::t('', 'Island'),
			),
			array(
				'name' => 'plithos',
				'header' => Yii::t('', 'Number'),
			),
			array(
				'name' => 'power',
				'header' => Yii::t('', 'Power (kW)'),
				'value' => 'number_format($data["power"],2)',
			),
		);
		
		$this->render('grid', compact('dataProvider','columns'));
	}
	
	public function actionSimvasi_polisis()
	{
		$sql = "
		SELECT island_name, COUNT(*) AS plithos, SUM(nominal_power) AS power
		FROM pv_plant, island, arxiki_aitisi
		WHERE pv_plant.island_id = island.id
		AND procedure_step_id IN
		(
			SELECT procedure_step.id
			FROM procedure_step
			WHERE procedure_step.pv_plant_id = pv_plant.id
		)
		AND procedure_step_id IN
		(
			SELECT diatiposi_oron.arxiki_aitisi_procedure_step_id
			FROM diatiposi_oron
			WHERE diatiposi_oron.procedure_step_id IN
			(
				SELECT diatiposi_oron_procedure_step_id
				FROM simvasi_sindesis
				WHERE simvasi_sindesis.procedure_step_id IN
				(
					SELECT simvasi_sindesis_procedure_step_id
					FROM simvasi_polisis
				)
			)
		)
		GROUP BY island_id
		";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    /*'sort'=>array(
		        'attributes'=>array(
		             'id', 'username', 'email',
				),
			),*/
		    'pagination'=>array(
		        'pageSize'=>10,
			),
			/*'params'=>array(
				'prosopo_id'=>$prosopo_id,
			),*/
			'keyField'=>'island_name',
		));
		
		$columns = array(
			array(
				'name' => 'island_name',
				'header' => Yii::t('', 'Island'),
			),
			array(
				'name' => 'plithos',
				'header' => Yii::t('', 'Number'),
			),
			array(
				'name' => 'power',
				'header' => Yii::t('', 'Power (kW)'),
				'value' => 'number_format($data["power"],2)',
			),
		);
		
		$this->render('grid', compact('dataProvider','columns'));
	}

	public function actionLiksi() {
		
		$sql = "
		SELECT island_name, COUNT(*) AS plithos, SUM(nominal_power) AS power
		FROM pv_plant, island, arxiki_aitisi
		WHERE pv_plant.island_id = island.id
		AND procedure_step_id IN
		(
			SELECT procedure_step.id
			FROM procedure_step
			WHERE procedure_step.pv_plant_id = pv_plant.id
		)
		AND pv_plant.id IN
		(
			SELECT pv_plant_id FROM killed
		)
		GROUP BY island_id
		";
		
		$totalItemCount = count( Yii::app()->db->createCommand($sql)->queryAll() );
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount' => $totalItemCount,
		    /*'sort'=>array(
		        'attributes'=>array(
		             'id', 'username', 'email',
				),
			),*/
		    'pagination'=>array(
		        'pageSize'=>10,
			),
			/*'params'=>array(
				'prosopo_id'=>$prosopo_id,
			),*/
			'keyField'=>'island_name',
		));
		
		$columns = array(
			array(
				'name' => 'island_name',
				'header' => Yii::t('', 'Island'),
			),
			array(
				'name' => 'plithos',
				'header' => Yii::t('', 'Number'),
			),
			array(
				'name' => 'power',
				'header' => Yii::t('', 'Power (kW)'),
				'value' => 'number_format($data["power"],2)',
			),
		);
		
		$this->render('grid', compact('dataProvider','columns'));
	}
	
	protected function getInternalActions($includeIndex) {
		$methods = get_class_methods($this);
		$inActions = array();
		foreach($methods as $method) {
			if(!$includeIndex && $method=='actionIndex') {
				continue;	
			}
			if( substr($method, 0, strlen('action')) == 'action' && ctype_upper($method[strlen('action')]) )
				$inActions[] = lcfirst( substr($method, strlen('action')) );
		}
		return $inActions;
	}
	
	protected function getMenu($includeIndex=true) {
		$c=1;
		$menu=array();
		foreach($this->getInternalActions($includeIndex) as $inAction) {
			if(isset($this->menuLabels[$inAction])) {
				$label = $this->menuLabels[$inAction];
			}
			else {
				$label = 'menu item '.$c++;
			}
			
			$menu[] = array( 'label'=> $label, 'url'=>array("{$this->id}/$inAction",) );
		}
		return $menu;
	}

	/*
	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}