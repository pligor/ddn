<?php

class PvPlantController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','choose_categ','choose_categDyn'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionChoose_categDyn() {
		$model = new PvCatForm();
		$form = $model->form;
		
		if( $form->submitted('submit_cats', true) && $form->validate() ) {
			$callData = CallData::getCall($this->route);
			$data_name = $callData->data_name;
			$callData->blob = $model->$data_name;
			$route = $callData->answerCall();
			
			$this->redirect(array(
				$route,
			));
		}
		
		$this->render('choose_categ', compact('form'));
	}
	
	/**
	 * @deprecated
	 * @param mixes $params
	 * @throws Exception
	 */
	public function actionChoose_categ($params=null) {
	//redirect_uri
		$paramsArray = CHtmlEx::array_url($params, true);
		extract( end($paramsArray) );
		
		$formBuilder = array(
		    //'title' => '............',
		    //'showErrorSummary' => true,
		    'elements' => array(
				'pvCat' => array(
					'type' => 'checkboxlist',
					'items' => PvPlant::getCatDescs(),
					'hint' => Yii::t('', 'please choose the combination of categories you wish'),
		        ),
		        /*'test' => array(
					'type' => 'string',
					'content' => 'some content here',
		        ),*/
			),
		    'buttons'=>array(
		        'submit' => array(
		            'type' => 'submit',
		            'label' => 'Επιλογή',
		        ),
		    ),
		);
		
		$class = 'PvPlant';
		$model = new $class;
		$form = new CForm($formBuilder, $model);
		
		if( $form->submitted('submit', false) ) { //true means load data to all models associated with this form
			try {
				$pvCatArray = $_POST[$class]['pvCat'];
				if($pvCatArray==null) {
					$message = Yii::t('', 'Please choose at least one category');
					throw new Exception($message);	
				}
				
				array_pop($paramsArray);	//drop unwanted element

				//append new data to current params
				end($paramsArray);
				$params = & $paramsArray[key($paramsArray)];
				
				$params['pvCatArray'] = $pvCatArray;
				
				$this->redirect(array(
					$redirect_uri,
					'params' => CHtmlEx::array_url($paramsArray),
				));
			}
			catch(Exception $e) {
				//print $e->getMessage();
			}
		}
		
		//
		//die;
		
		$this->render('choose_categ', compact('form'));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new PvPlant;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PvPlant']))
		{
			$model->attributes=$_POST['PvPlant'];
			
			if($model->save())
			{
				$child = $model->setCat(0);
				if($child)
				{
					$child->protocol = $model->protocol;
					$child->perioxi_dei_id = 1;
					
					if( $child->save() ) {
						print Yii::t('', 'child was saved ok');
					}
				}
				else {
					print Yii::t('', 'no child was created');
				}
				Yii::app()->end();
				
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PvPlant']))
		{
			$model->attributes=$_POST['PvPlant'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else {
			$message = Yii::t('', "Invalid request. Please do not repeat this request again.");
			throw new CHttpException(400,$message);
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PvPlant');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PvPlant('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PvPlant']))
			$model->attributes=$_GET['PvPlant'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PvPlant::model()->findByPk((int)$id);
		if($model===null) {
			$message = Yii::t('', "The requested page does not exist.");
			throw new CHttpException(404,$message);
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pv-plant-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
