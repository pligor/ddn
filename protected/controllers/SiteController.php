<?php

class SiteController extends Controller {
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
/*////*/$logger = new CLogger();
		
/*////*/$var = $logger->getMemoryUsage();
		CHtmlEx::out($var);
		$this->render('index', array(
			//'dataProvider' => $dataProvider,
		));
	}
	
	/**
	 * @return array action filters
	 * @see CController::filters()
	 */
	public function filters() {
		return array(
			'accessControl',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('error','contact','page','captcha','toggleLang'),
				'users'=>array('*'),
			),
			array('allow',
				//'roles' => array('island_manager'),
				'actions'=>array('index'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class' => 'CCaptchaAction',
				//'backColor' => 0xFFFFFF,
				'transparent' => true,
				'testLimit' => 1,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}
	
	/**
	 * Displays the contact page
	 */
	public function actionContact() {
		$model=new ContactForm;
		$form = $model->form;
		
		if( $form->submitted('send_button') && $form->validate() ) {
			//$headers="From: {$model->email}\r\nReply-To: {$model->email}";
			//mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
			$mailer = new Mailer();
			$mailer->Subject = $model->email .' - '. $model->subject;
			$mailer->Message = $model->body;
			$mailer->maile();
			
			$value = Yii::t('', 'Thank you for contacting us. We will respond to you as soon as possible.');
			Yii::app()->user->setFlash('contact',$value);
			$this->refresh();
		}
		
		$this->render('contact',array(
			'model' => $model,
			'form' => $form,
		));
	}
	
	public function actionToggleLang() {
		if( Yii::app()->session['lang'] == Yii::app()->sourceLanguage ) {
			Yii::app()->session['lang'] = 'el';
		}
		else {
			Yii::app()->session['lang'] = Yii::app()->sourceLanguage;
		}
		
		//$url = Yii::app()->request->urlReferrer;	//drives you back to the original page, can be null
		$url = Yii::app()->homeUrl;
		$this->redirect($url);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest) {
	    		echo $error['message'];
	    	}
	    	else {
	        	$this->render('error', $error);
	    	}
	    }
	}
}