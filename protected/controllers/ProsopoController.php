<?php

class ProsopoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			//'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','islanders'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * @param integer $island_id
	 */
	public function actionIslanders($island_id) {
		$prosopa = Prosopo::getProsopa($island_id);
		//$arr = array('aaa', 'bbb', 'ccc', 'ddd', 'eee');
		print json_encode($prosopa);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
		$this->render('view', array(
			'model' => CHtmlEx::loadModel('Prosopo', $id),
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		if(Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			CHtmlEx::loadModel('Prosopo', $id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		}
		else {
			$message = Yii::t('', 'Invalid request. Please do not repeat this request again.');
			throw new CHttpException(400,$message);
		}
	}
	
	/**
	 * @todo PROBABLY WE SHOULD REPLACE THIS FUNCTION LATER WITH A REAL FILTER CLASS
	 * @throws CHttpException
	 * @return int
	 */
	protected function getIslandId() {
		if( Yii::app()->user->hasState('island_id') ) {
			return Yii::app()->user->island_id;
		}
		else {
			$message = Yii::t('', 'Please choose an Island before you proceed');
			throw new CHttpException(404, $message);
		}
	}

	/**
	 * Lists all models
	 */
	public function actionIndex($island_id=null,$callee='prosopo/index') {
		$criteria = new CDbCriteria;
		
		if($island_id !== null) {
			$source = Prosopo::getProsopa($island_id);	
			$criteria->addInCondition('eponimia', $source);
		}
		
		$dataProvider = new CActiveDataProvider('Prosopo', compact('criteria'));
		
		$this->render('index', compact('dataProvider','callee') );
	}
	
	/**
	 * @param integer $prosopo_id
	 */
	public function actionReturn_prosopo_id($prosopo_id,$callee) {
		$datas = array('prosopo_id' => $prosopo_id);
		
		$callData = CallData::getCall($callee);
		$data_name = $callData->data_name;
		$callData->blob = $datas[$data_name];	//just an assertion here
		$route = $callData->answerCall();
		
		$this->redirect(array(
			$route,
		));
	}
	
	public function actionFind_eponimia() {
		$class = ucfirst($this->id);

		$island_id = $this->getIslandId();
		
		$source = $class::getProsopa($island_id);
		
		$model = new SingleAutoCompleteForm($source);
		$model->attributeLabel = $class::model()->getAttributeLabel('eponimia');
		$model->hint = 'επωνυμία προς τροποποίηση. αν μείνει κενό θα επιστραφούν όλες οι επωνυμίες';
		$model->allowEmpty = true;
		
		$form = $model->form;
		
		if( $form->submitted('submit_text',true) && $form->validate() ) {
			//CHtmlEx::dump($model->text);
			if(CHtmlEx::isEmpty($model->text)) {
				$this->redirect( array(
					'prosopo/index',
					'island_id' => $island_id,
					'callee' => $this->route,
				));
			}
			
			$attributes = array(
				'eponimia' => $model->text,
			);
			$model = $class::model()->findByAttributes($attributes);
			
			$this->redirect(array(
				'return_prosopo_id',
				'prosopo_id' => $model->id,
				'callee' => $this->route,
			));
		}
		
		$title = 'Εύρεση επωνυμίας';
		
		$this->render('find_form', compact('form','title'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin() {
		$model=new Prosopo('search');
		$model->unsetAttributes();  //clear any default values
		if(isset($_GET['Prosopo']))
			$model->attributes=$_GET['Prosopo'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='prosopo-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
