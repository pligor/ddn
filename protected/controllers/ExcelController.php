<?php
//http://www.yiiframework.com/wiki/101/how-to-use-phpexcel-external-library-with-yii/

class ExcelController extends Controller {
	public $layout = '//layouts/column2';
	public $defaultAction = 'index';
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 * @see CController::accessRules()
	 */
	public function accessRules() {
		return array(
			///*
			array(	//allow all registered users to view initial menu
				'allow',
				'actions' => array('index'),
				'users' => array('@'),
			),
			array(	//allow all registered users to view initial menu
				'allow',
				'actions' => array('load_perioxi','report_perioxi','tsx','report_tsx'),
				'users' => array('tsx'),
			),
			array(
				'allow',
				'roles' => array('administrator'),
			),
			array(
				'deny',  // deny all users
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * @return array action filters
	 * @see CController::filters()
	 */
	public function filters() {	//http://www.yiiframework.com/doc/guide/1.1/en/basics.controller
		return array(
			'accessControl', // perform access control for CRUD operations
			/*array(
                'application.filters.CheckAccessControl - index',	//all actions except index
                'role' => 'administrator',
            ),*/
		);
	}
	
	public function actionIndex() {
		$this->render('index');
	}
	
	public function actionExport() {
		$rawData = array(
			array(
				'idCol' => 1,
				'columnName' => 'George',
				'columnSurname' => 'Pligor',
			),
			array(
				'idCol' => 2,
				'columnName' => 'No',
				'columnSurname' => 'Name',
			),
		);
		
		//Elements in the raw data array may be either objects (e.g. model objects) or
		//associative arrays (e.g. query results of DAO) 
		$dataProvider = new CArrayDataProvider($rawData, array(
		    'id' => $this->action->id,
		    /*
		    'sort'=>array(
		        'attributes'=>array(
		        	//'columnSurname',
					'sortcol' => array(
					    'desc' => 'columnName DESC',
					    'label' => 'Item Price',
					    'default' => 'desc',
					),
		        ),
		    ),
		    //*/
		    'keyField' => 'idCol',
		));
		
		///*
		$columns = array(
			array(
				'name' => 'columnName',
				'header' => 'Column Name',
				'footer' => 'george',
			),
			array(
				'name' => 'columnSurname',
				'header' => 'Column Sur Name',
				//'value' => 'number_format($data["power"],2)',
			),
		);
		//*/
		$this->render('export', compact('dataProvider','columns'));
	}
	
	public function actionTsx() {
		$title = Yii::t('', "Check the file of") ." Τομέα Σύνδεσης Χρηστών";
		
		$menu = array(
			array(
				'label' => Yii::t('', "Cancel"),
				'url' => array(
					'index',
					/*'params' => $params*/
				),
				//'linkOptions' => array('title' => '........'),
			),
		);
		
		$model = new TsxForm();
		$model->buttonLabel = Yii::t('', "Submit file");
		$form = $model->form;
		
		
		try {
			if( $form->submitted('submit_file', true) && $form->validate() ) {
				$model->getInstance();
				
				$perioxi_dei_id = $model->id;
				
				$path = $model->saveFile();
				
				$xlModel = new TSXxl($path);
				
				$params = compact('path','perioxi_dei_id');
				$params = CHtmlEx::array_url($params);

				///*
				$this->redirect(array(
					'report_tsx',
					'params' => $params,
				));
				//*/
				
			}
		}
		catch(Exception $e) {
			$model->addError('', $e->getMessage());
		}
		
		$this->render('form', compact('form','menu','title'));
	}
	
	public function actionReport_tsx($params) {
	//path, perioxi_dei_id
		$params = CHtmlEx::array_url($params,true);
		extract($params);
		
		$model = new TSXxl($path);
		$model->loadExcel();
		
		
		if($perioxi_dei_id==0) {
			$report = $model->forAllSheets('checkSimvasiPolisis');
		}
		else {
			$perioxi_dei_name = CHtmlEx::loadModel('PerioxiDei', $perioxi_dei_id)->perioxi_dei_name;
			$sheet = $model->phpExcel->getSheetByName($perioxi_dei_name);	//supposingly sheet names have been verified for existance
			$sheet->perioxi_dei_id = $perioxi_dei_id;
			$report[$perioxi_dei_id] = $model->forAllRows($sheet, 'checkSimvasiPolisis');
		}
		
		$this->render('report_tsx', compact('report'));
	}
	
	/**
	 * Compare two excel files that are formatted correctly
	 */
	public function actionCompxl() {
		$model = new DoubleUploadForm();
		
		$ddnModel = new DDNxl();
		$model->startRow = $ddnModel->startRow;
		$model->endRow = $ddnModel->endRow;
		$model->columns = $ddnModel->columns;
		
		$form = $model->form;
		
		try {
			if( $form->submitted('submit_files', true) && $form->validate() ) {
				
				$attribute = 'oldFile';
				$model->$attribute = CUploadedFile::getInstance($model, $attribute);
				$attribute = 'newFile';
				$model->$attribute = CUploadedFile::getInstance($model, $attribute);
				
				$filename = $model->$attribute->name; //this is based on compare excel component, since the output file is the newer file
				
				$model->saveBoth();
				
				$colors = array();
				foreach($model->_colors as $attr => $color) {
					$colors[$attr] = $model->$attr;
				}
				
				$startRow = $model->startRow;
				$endRow = $model->endRow;
				$columnStr = $model->columnStr;
				
				$params = compact('filename','colors','startRow','endRow','columnStr');
				$params = CHtmlEx::array_url($params);

				$this->redirect(array(
					'excel/gen_comp',
					'params' => $params,
				));
			}
		}
		catch(Exception $e) {
			$model->addError('', $e->getMessage());
		}
		
		$this->render('compxl', compact('form'));
	}
	
	/**
	 * @todo include sheetTitles as parameter
	 * @param string $params
	 * @throws CHttpException
	 */
	public function actionGen_comp($params) {
	//filename, colors, startRow, endRow, columns
		$params = CHtmlEx::array_url($params,true);
		extract($params);
		
		$model = new DoubleUploadForm();
		
		$model->startRow = $startRow;
		$model->endRow = $endRow;
		$model->columnStr = $columnStr;
		
		foreach($colors as $attr => $color) {
			$model->$attr = $color;
		}
		$model->setColors();

		//$model->sheetTitles = CHtml::listData(Island::model()->findAll(), 'id', 'island_name') + array(0=>'C');
		
		$model->loadBoth();
		
		$model->compPhpExcels();
		
		$ddnModel = new DDNxl();
		$ddnModel->pageSetupPhpExcel($model->phpExcel); //fix page setup
		
		//$text = 'comp_'. $filename;
		$text = 'comp.xls';
		$subdir = 'files';
		extract( CHtmlEx::getUrlNfullpath($subdir, $text) );
		
		$model->writeCompared($fullpath);
		
		$menu = array(
			array(
				'label' => Yii::t('', 'New Comparison'),
				'url' => array(
					'excel/compxl',
				),
			),
			array(
				'label' => Yii::t('', 'Return back to menu'),
				'url' => array(
					'excel/index',
				),
			),
		);
		
		$this->render('download_file', compact('text','url','menu'));
	}
	
	/**
	 * Compare two excel files that are formatted correctly
	 */
	public function actionStats() {
		$model = new MultiUploadForm();
		
		$form = $model->form;
		
		try {
			if( $form->submitted('submit_files', true) && $form->validate() ) {
				$attribute = 'files';
				$model->$attribute = CUploadedFile::getInstances($model, $attribute);
				
				$paths = $model->saveAll();
				
				DDNxl::verifySheetnames($paths);
				
				$config = $model->config;
				
				$params = compact('paths','config');
				$params = CHtmlEx::array_url($params);

				$this->redirect(array(
					'excel/gen_stats',
					'params' => $params,
				));
			}
		}
		catch(Exception $e) {
			$model->addError('', $e->getMessage());
		}
		
		$this->render('stats', compact('form'));
	}
	
	/**
	 * Calculate and generate stats
	 * @param string $params
	 */
	public function actionGen_stats($params) {
	//paths, config
		$params = CHtmlEx::array_url($params,true);
		extract($params);
		
		$model = new DDNxl();
		$model->config = $config;
		$model->filepaths = $paths;
		
		//$model->onlyData();
		
		$model->loadExcels();
		
		$phpExcel = $model->calcStats();
		
		WorksheetHelper::fillBorders($phpExcel);
		
		$model->renderFooters($phpExcel);
		
		$text = "$config-stats.xls";
		$subdir = 'files';
		extract( CHtmlEx::getUrlNfullpath($subdir, $text) );
		
		$model->writeExcel($phpExcel,$fullpath);
		
		$model->unlinkFilepaths();
		
		$menu = array(
			array(
				'label' => Yii::t('', 'New Generation of Statistics'),
				'url' => array(
					'excel/stats',
				),
			),
			array(
				'label' => Yii::t('', 'Return back to menu'),
				'url' => array(
					'excel/index',
				),
			),
		);
		
		$this->render('download_file', compact('text','url','menu'));
	}
	
	public function actionLoad_perioxi() {
		$model = new ExcelPerioxiForm();
		
		$form = $model->form;
		
		if( $form->submitted('submit',true) && $form->validate() ) {
			$attribute = 'perioxi_file';
			$model->$attribute = CUploadedFile::getInstance($model, $attribute); //http://www.yiiframework.com/doc/api/1.1/CUploadedFile
			
			//$filepath = $model->$attribute->tempName;
			$filepath = tempnam('files', $attribute);
			$model->$attribute->saveAs($filepath);
			
			try {
				if( !$model->hasIslands($filepath) ) {
					$message = Yii::t('', 'There are no islands inside the file which are subject to the Area of your choice');
					throw new Exception($message);
				}
				
				$perioxi_dei_id = $model->perioxi_dei_id;
				
				$params = compact('perioxi_dei_id','filepath');
				
				$this->redirect( array(
					'excel/report_perioxi',
					'params' => CHtmlEx::array_url($params),
				));
			}
			catch(Exception $e) {
				$model->addError('', $e->getMessage() );
			}
		}
		
		$this->render('load_perioxi', compact('form'));
	}

	public function actionReport_perioxi($params) {
	//perioxi_dei_id, filepath
		$params = CHtmlEx::array_url($params, true);
		extract($params);
		
		//DELETE ALL PERIOXI DATA BEFORE LOADING
		$component = new Wipent();
		$component->wipePerioxi($perioxi_dei_id);
		
		$model = new DDNxl();
		$model->filepath = $filepath;
		$model->perioxi_dei_id = $perioxi_dei_id;
		
		$model->onlyData();
		
		$model->loadExcel();
		
		$reportPerioxi = $model->loadArea();
		
		$perioxi_dei_name = CHtmlEx::loadModel('PerioxiDei', $perioxi_dei_id)->perioxi_dei_name;
		
		$this->render('report_perioxi', compact('perioxi_dei_name', 'reportPerioxi'));
	}
}