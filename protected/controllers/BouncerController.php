<?php
class BouncerController extends Controller {
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout = '//layouts/column2';
	public $defaultAction = 'login';

	/**
	 * External class-based actions
	 * @see CController::actions()
	 */
	public function actions() {
		// return external action classes, e.g.:
		return array(
			/*'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),*/
			'captcha'=>array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFF00,
				'transparent' => false,
				'testLimit' => 1,
				'offset' => -5,	//closer to zero => more readable
				'minLength' => 6,
				'maxLength' => 7,
			),
		);
	}
	
	/**
	 * @see CController::filters()
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  //allow anonymous users only to login
				'users' => array('?'),
				'actions' => array('login','captcha'),
			),
			array('allow',  //allow authenticated users to logout
				'users' => array('@'),
				'actions' => array('logout'),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			),
		);
	}
	
	/**
	 * Displays the login page
	 */
	public function actionLogin() {
		$model = new LoginForm();
		
		/* // if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		//*/
		
		$form = $model->form;
		
		if( $form->submitted('login_button') && $form->validate() && $model->login() ) {
			$this->redirect( Yii::app()->user->returnUrl );
		}
		
		$this->render('login',compact('form'));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}	
}