<?php

class AddressController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','create_ajax','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/** 
	 * @see CController::behaviors()
	 */
	public function behaviors() {
		return array(
			'address-wizard' => array(
				'class' => 'external.WizardBehavior.WizardBehavior',
				'steps' => array(
					Yii::t('', 'County Choice') => 'nomos',
					Yii::t('', 'Municipality Choice') => 'dimos',
				),
			),
		);
	}
	
	public function wizardStart($event) {
		$event->handled = true;
	}
	
	public function wizardInvalidStep($event) {
		Yii::app()->user->setFlash('notice', $event->step .' '. Yii::t('', 'is not a valid step in this wizard'));
	}
	
	/**
	 * convetion: for each step there is a corresponding model which has a getForm method
	 * for example: for step 'hello' there is the model 'Hello'
	 * @param WizardEvent $event
	 */
	public function wizardProcessStep($event) {
		$step = $event->step;
		$class = ucfirst($step);
		
		/*
		if($event->step == reset($event->sender->steps)) {
			$event->sender->reset();
		}*/
		
		$model = new $class();
		
		$param = null;
		if( $prevStep = $this->getPrevStep($event->sender->steps,$step) ) {
			$data = $event->sender->read($prevStep);
			
			if($step == 'dimos') {
				$param = $data['id'];
			}
		}

		$form = $model->getForm($param);
		
		if( $form->submitted('submit',true) && $form->validate() ) {
			$event->sender->save($model->getAttributes($model->safeAttributeNames));
			$event->handled = true;
		}
		
		if( !$event->handled ) {
			
			//$var = $event->data;
			//CHtmlEx::dump($var);
			//CHtmlEx::out($var);
			
			//$var = $event->sender->read();
			//CHtmlEx::out($var);
			
			$this->render('wizard_form', compact('form', 'event'));
		}	
	}
	
	/**
	 * The wizard has finished; use $event->step to find out why.
	 * Normally on successful completion ($event->step===true) data would be saved
	 * to permanent storage;
	 * @param WizardEvent The event
	 */
	public function wizardFinished($event) {
		if($event->step === true) {
			//print "completed<br/>";
			//CHtmlEx::out($event->data);
			
			$event->sender->reset();
			
			$dimos_id = $event->data['dimos']['id'];
			
			$this->redirect(array(
				'index',
				'dimos_id' => $dimos_id,
			));

			/*
			foreach($event->data as $step => $subarray) {
				$model->$step = $subarray[$step];	
			}*/
		}
		else {
			$message = Yii::t('', 'Fatal error');
			throw new Exception();
		}
	}
	
	/**
	 * @param string[] $steps
	 */
	protected function getPrevStep($steps,$step) {
		reset($steps);
		while(current($steps)!=$step){
			next($steps);	
		}
		return prev($steps);
	}
	
	/**
	 * @param string $step
	 */
	public function actionWizard($step=null) {
		$this->process($step);
	}
	
	/**
	 * @param integer $prosopo_id
	 */
	public function actionReturn_address_id($address_id) {
		$datas = array('address_id' => $address_id);
		
		$callee = 'address/wizard';	//fixed
		$callData = CallData::getCall($callee);
		
		if($callData===null) {
			$this->redirect(array(
				'view',
				'id'=>$address_id,
			));
		}
		else {
			$data_name = $callData->data_name;
			$callData->blob = $datas[$data_name];	//just an assertion here
			$route = $callData->answerCall();
			
			$this->redirect(array(
				$route,
			));
		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
		$class = ucfirst($this->id);
		$this->render('view',array(
			'model' => CHtmlEx::loadModel($class,$id),
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($dimos_id=null) {
		$criteria = new CDbCriteria();
		
		if($dimos_id!==null) {
			$criteria->compare('dimos_id', $dimos_id);
		}
		
		$dataProvider=new CActiveDataProvider('Address',array(
			'criteria' => $criteria,
		));
		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
}
