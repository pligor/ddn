<?php

class FinderController extends Controller {
	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';
	
	/**
	 * @var string
	 */
	public $defaultAction = 'index';
	
	/**
	 * @var integer[]
	 */
	public $pvCats = array();
	
	/**
	 * @return array action filters
	 * @see CController::filters()
	 */
	public function filters() {	//http://www.yiiframework.com/doc/guide/1.1/en/basics.controller
		return array(
			array(
                'application.filters.PvCatFilter + index',	//only index
                //'param1' => 'param value',
            ),
		);
	}
	
	/**
	 * @todo PROBABLY WE SHOULD REPLACE THIS FUNCTION LATER WITH A REAL FILTER CLASS
	 * @throws CHttpException
	 * @return int
	 */
	protected function getIslandId() {
		if( Yii::app()->user->hasState('island_id') ) {
			return Yii::app()->user->island_id;
		}
		else {
			$message = Yii::t('', 'Please choose an Island before you proceed');
			throw new CHttpException(404, $message);
		}
	}
	
	public function actionIndex() {
		$pvCats = $this->pvCats;
		
		$descs = array();
		foreach($pvCats as $pvCat) {
			$descs[] = PvPlant::getCatDescs($pvCat);
		}
		
		$params['pvCatArray'] = $pvCats;
		$params = CHtmlEx::array_url($params);
		$this->render('index', compact('params','descs'));
	}
	
	public function actionReturn_plant_id($pv_plant_id) {
		$array = compact('pv_plant_id');	//performing this trick for assertion
		
		$route = $this->id.'/index';
		
		$callData = CallData::getCall($route);
//CHtmlEx::dump($callData);die;
		$data_name = $callData->data_name;
		$callData->blob = $array[$data_name];	//so this must be correct now
		$route = $callData->answerCall();
		
		$this->redirect(array(
			$route,
		));
	}
	
	public function actionFind_prosopo($params) {
	//pvCatArray
		$params = CHtmlEx::array_url($params, true);
		extract($params);
		
		if(!isset($pvCatArray)) {
			$pvCatArray = null;	//if pvCatArray is not available execute for ALL pv categories	
		}
		
		$island_id = $this->getIslandId();
		$class = 'Prosopo';
		$source = $class::getProsopa($island_id, $pvCatArray);
		
		$model = new SingleAutoCompleteForm($source);
		$model->attributeLabel = $class::model()->getAttributeLabel('eponimia');
		$model->hint = 'αφήνοντας το πεδίο κενό θα επιστρέψει ΟΛΕΣ τις καταχωρύσεις <--- ΠΟΛΥ ΑΡΓΟ';
		$form = $model->form;
		
		if( $form->submitted('submit_text', true) && $form->validate() ) {
			$attributes = array(
				'eponimia' => $model->text,
			);
			$model = $class::model()->findByAttributes($attributes);	//SingleAutoCompleteForm -> Prosopo | null
			
			if( is_object($model) ) { //only valid eponimia
				$params['prosopo_id'] = $model->id;
			}
			
			$this->redirect( array(
				'by_prosopo',
				'params' => CHtmlEx::array_url($params),
			));
		}
		
		$title = Yii::t('', 'Find plant based on name');
		
		$this->render('find_form', compact('title','form'));
	}

	public function actionBy_prosopo($params) {
	//pvCatArray, prosopo_id
		$params = CHtmlEx::array_url($params, true);
		//extract( end($params) );
		extract($params);
		
		$island_id = $this->getIslandId();
		
		$classes = PvPlant::getCats($pvCatArray);
		
		$sql = 
		"
		CREATE TEMPORARY TABLE all_protocol
		";
		
		$sqlArray = array();
		foreach($classes as $class) {
			$model = new $class;
			$sqlArray[] = "
			SELECT pv_plant_id, protocol
			FROM ". $model->tableName() ."
			";
		}
		$sql .= implode(" UNION ", $sqlArray);
		
		Yii::app()->db->createCommand($sql)->execute();
		
		$sql_pre = "
		SELECT pv_plant.id AS pv_plant_id, eponimia, thesi, noumero, nominal_power, protocol
		";
		
		$sql_post =
		"
		FROM prosopo, pv_plant, address, arxiki_aitisi, all_protocol
		WHERE prosopo.id=pv_plant.prosopo_id
		AND pv_plant.address_id = address.id
		AND arxiki_aitisi.procedure_step_id IN
		(
			SELECT procedure_step.id AS procedure_step_id FROM procedure_step
			WHERE procedure_step.pv_plant_id=pv_plant.id
		)
		AND pv_plant.id = all_protocol.pv_plant_id
		AND pv_plant.island_id = :island_id
		";
		
		if( isset($prosopo_id) ) {
			$sql_post .=  
			"
			AND prosopo.id = :prosopo_id
			";
		}
		
		//note: after "arxiki_aitisi.procedure_step_id" we used "IN" because we have multiple procedure steps although
		//only one of them we'll be parent of the arxiki_aitisi so we get back only one arxiki aitisi
		
		$sql_count = "
		SELECT COUNT(*)
		";
		$command= Yii::app()->db->createCommand($sql_count.$sql_post)
					->bindValue(':island_id', $island_id);
		if( isset($prosopo_id) ) $command->bindValue(':prosopo_id', $prosopo_id);
		$totalItemCount = $command->queryScalar();
		
		$sql = $sql_pre.$sql_post;
		
		$bindParams = array('island_id' => $island_id);
		if( isset($prosopo_id) ) {
			$bindParams['prosopo_id'] = $prosopo_id;	
		}
		
		$dataProvider = new CSqlDataProvider($sql, array(
		    'totalItemCount'=>$totalItemCount,
		    /*'sort'=>array(
		        'attributes'=>array(
		             'id', 'username', 'email',
				),
			),*/
		    'pagination'=>array(
		        'pageSize'=>10,
			),
			'params' => $bindParams,
			'keyField'=>'pv_plant_id',
		));

		$this->render('list_short', compact('dataProvider'/*,'params'*/));
	}
	
	public function actionFind_protocol($params) {
	//pvCatArray
		$params = CHtmlEx::array_url($params, true);
		extract($params);

		$model = new ApplicationNumberForm();
		$model->buttonLabel = Yii::t('', 'Search');
		$form = $model->form;
		
		if( $form->submitted('submit_application_number', true) && $form->validate() ) {
			$params['protocol'] = $model->applicationNumber;
			
			$this->redirect( array(
				'by_protocol',
				'params' => CHtmlEx::array_url($params),
			));
		}
		
		$title = Yii::t('', 'Find plant based on application number');
		
		$this->render('find_form', compact('title','form'));
	}
	
	public function actionBy_protocol($params) {
	//pvCatArray, protocol
		$params = CHtmlEx::array_url($params, true);
		//extract(end($params));
		extract($params);
		
		$island_id = $this->getIslandId();
		//$perioxi_dei_id = CHtmlEx::loadModel('Island', $island_id)->perioxiDei->id;
		
		$models = array();
		$classes = PvPlant::getCats($pvCatArray);

		foreach($classes as $class)
		{
			$model = $class::model()->findByAttributes(array(
				'protocol' => $protocol,
				//'perioxi_dei_id' => $perioxi_dei_id,
                                'island_id' => $island_id,
			));
			if(is_object($model))
			{
				$models[] = $model;
			}
		}
		unset($model);
		
		$rawData = array();
		foreach($models as $model)
		{
			$data = array();
			$data['pv_plant_id'] = $model->pv_plant_id;
			$data['eponimia'] = $model->pvPlant->prosopo->eponimia;
			$data['thesi'] = $model->pvPlant->address->thesi;
			$data['noumero'] = $model->pvPlant->address->noumero;
			$data['nominal_power'] = $model->pvPlant->procedures['ArxikiAitisi']->nominal_power;
			$data['protocol'] = $model->protocol;
			$rawData[] = $data;
		}
		
		//Elements in the raw data array may be either objects (e.g. model objects) or
		//associative arrays (e.g. query results of DAO) 
		$dataProvider = new CArrayDataProvider($rawData, array(
		    'id' => $this->action->id,
		    /*'sort'=>array(
		        'attributes'=>array(
		             'id', 'username', 'email',
		        ),
		    ),*/
		    'pagination'=>array(
		        'pageSize'=>10,
		    ),
		    'keyField' => 'pv_plant_id',
		));
		
		//$params = end($params);
		$this->render('list_short', compact('dataProvider','params'));
	}
}