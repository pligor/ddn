<?php

/**
 * @author pligor
 */
class EditerController extends Controller {
	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';
	
	/**
	 * @var array main form configuration
	 */
	protected $config;
	
	/**
	 * @var integer
	 */
	public $prosopo_id;
	
	/**
	 * @var integer
	 */
	public $address_id;
	
	/**
	 * @var integer
	 */
	public $pv_plant_id;
	
	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			//'accessControl', // perform access control for CRUD operations
			array( //only for prosopo action
                'application.filters.ProsopoFilter + prosopo',
			),
			array( //only for submenu action
                'application.filters.AddressFilter + address',
			),
			array( //only for submenu action
                'application.filters.FinderFilter + submenu',
			),
		);
	}
	
	/**
	 * @see CController::init()
	 */
	public function init() {
		$this->config = array(
			'showErrorSummary' => true,
			'elements' => array(
			),
			'buttons' => array(
		        'change' => array(
		            'type' => 'submit',
		            'label' => Yii::t('', "Edit"),
		        ),
		    ),
		);
	}

	public function actionIndex() {
		$this->render('index');
	}
	
	public function actionProsopo() {
		$prosopo_id = $this->prosopo_id;
		
		$model = CHtmlEx::loadModel('Prosopo', $prosopo_id);
		$prev_eponimia = $model->eponimia;
		
		$form = $model->form;
		
		$form->action = array(
			$this->route,
			'prosopo_id' => $prosopo_id,
		);
		
		if($form->submitted('submit',true) && $form->validate()) {	
			if($model->save(false)) {
				$edit_result = Yii::t('', 'The official name {prev_eponimia} was replaced by the official name {cur_eponimia}', array(
					'{prev_eponimia}' => $prev_eponimia,
					'{cur_eponimia}' => $model->eponimia,
				));
				
				Yii::app()->user->setFlash('edit_result', $edit_result);
				
				$this->redirect(array(
					'index',
				));
			}
		}
		
		$menu = array(
			array(
				'label'=>'Επιστροφή στο μενού',
				'url' => array(
					'index',
				),
			),
		);
		
		$title = Yii::t('', 'Επεξεργασία Επωνυμίας');
		
		$this->render('menu_title_form', compact('menu','title','form'));
	}
	
	public function actionAddress() {
		$address_id = $this->address_id;
		
		$model = CHtmlEx::loadModel('Address', $address_id);
		$prevFullAddress = $model->fullAddress;
		
		$form = $model->form;
		
		$form->action = array(
			$this->route,
			'address_id' => $address_id,
		);
		
		if($form->submitted('submit',true) && $form->validate()) {	
			if($model->save(false)) {
				$edit_result = Yii::t('', 'The address {prevFullAddress} was replaced by address {curFullAddress}', array(
					'{prevFullAddress}' => $prevFullAddress,
					'{curFullAddress}' => $model->fullAddress,
				));
				Yii::app()->user->setFlash('edit_result', $edit_result);
				
				$this->redirect(array(
					'index',
				));
			}
		}
		
		$menu = array(
			array(
				'label' => 'Επιστροφή στο μενού',
				'url' => array(
					'index',
				),
			),
		);
		
		$title = Yii::t('', 'Επεξεργασία Διεύθυνσης');
		
		$this->render('menu_title_form', compact('menu','title','form'));
	}
	
	/**
	 * @param ProcedureStep[] $procedures
	 */
	protected function getSubmenu($procedures) {
		$pv_plant_id = $this->pv_plant_id;
		$params = CHtmlEx::array_url(compact('pv_plant_id'));
		$menu = array();
		foreach($procedures as $class => $procedure) {
			$action = lcfirst($class);
			
			$menu[] = array(
				'label' => Yii::t('', "Change data") .": ". $class::getTitle(),
				'url' => array(
					$action,
					'params' => $params,
				)
			);
		}
		return $menu;
	}
	
	public function actionSubmenu() {
		$pv_plant_id = $this->pv_plant_id;

		$model = CHtmlEx::loadModel('PvPlant', $pv_plant_id);
		$perioxi_dei_name = $model->island->perioxiDei->perioxi_dei_name;
		$protocol = $model->protocol;
		$categ = $model->child->descr;
		
		$params = CHtmlEx::array_url(compact('pv_plant_id'));
		
		$menu = array(
			array(
				'label' => Yii::t('', 'End of Terms'),
				'url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'kill',
						'params' => $params,
					),
					'confirm' => Yii::t('', 'Are you certain you would like to end the terms of this registration?'),
				),
			),
			array(
				'label' => Yii::t('', 'Change Category and/or Protocol number') ,
				'url' => array(
					'categ_protocol',
					'params' => $params,
				),
			),
		);
		
		$menu = array_merge($menu,$this->getSubmenu($model->procedures));
		
		$this->render('submenu', compact('perioxi_dei_name', 'protocol', 'categ', 'menu'));
	}
	
	public function actionArxikiAitisi($params) {
	//pv_plant_id
		$params = CHtmlEx::array_url($params, true);
		extract($params);
		
		$models = $this->getModels($pv_plant_id);
		
		$form = $this->getForm($models);
		
		$this->redirectStep($form, $models, $pv_plant_id);

		$class = ucfirst($this->action->id);
		
		//GIA NA ALLAKSOUME THN HMEROMINIA PRIN TO RENDERING PIO SWSTA PREPEI NA EIXAME ENA CFormModel
		//H MALLON PIO APLA NA XEIRISTOUME PIO EKSIPNA OLES TIS HMEROMINIES ME MIA NEA KLASSI
		$models['ProcedureStep']->execution_date = CHtmlEx::convStamp2date($models['ProcedureStep']->execution_date);
		
		$this->render('step', compact('form','class','pv_plant_id'));
	}
	
	public function actionDiatiposiOron($params) {
	//pv_plant_id
		$params = CHtmlEx::array_url($params, true);
		extract($params);
		
		$models = $this->getModels($pv_plant_id);
		
		$form = $this->getForm($models);
		
		$this->redirectStep($form, $models, $pv_plant_id);

		$class = ucfirst($this->action->id);
		
		//GIA NA ALLAKSOUME THN HMEROMINIA PRIN TO RENDERING PIO SWSTA PREPEI NA EIXAME ENA CFormModel
		//H MALLON PIO APLA NA XEIRISTOUME PIO EKSIPNA OLES TIS HMEROMINIES ME MIA NEA KLASSI
		$models['ProcedureStep']->execution_date = CHtmlEx::convStamp2date($models['ProcedureStep']->execution_date);
		$models['DiatiposiOron']->liksi_oron = CHtmlEx::convStamp2date($models['DiatiposiOron']->liksi_oron);
		
		$this->render('step', compact('form','class','pv_plant_id'));
	}
	
	public function actionSimvasiSindesis($params) {
	//pv_plant_id
		$params = CHtmlEx::array_url($params, true);
		extract($params);
		
		$models = $this->getModels($pv_plant_id);
		
		$form = $this->getForm($models);
		
		$this->redirectStep($form, $models, $pv_plant_id);

		$class = ucfirst($this->action->id);
		
		//GIA NA ALLAKSOUME THN HMEROMINIA PRIN TO RENDERING PIO SWSTA PREPEI NA EIXAME ENA CFormModel
		//H MALLON PIO APLA NA XEIRISTOUME PIO EKSIPNA OLES TIS HMEROMINIES ME MIA NEA KLASSI
		$models['ProcedureStep']->execution_date = CHtmlEx::convStamp2date($models['ProcedureStep']->execution_date);
		
		$this->render('step', compact('form','class','pv_plant_id'));
	}
	
	public function actionSimvasiPolisis($params) {
	//pv_plant_id
		$params = CHtmlEx::array_url($params, true);
		extract($params);
		
		$models = $this->getModels($pv_plant_id);
		
		$form = $this->getForm($models);
		
		$this->redirectStep($form, $models, $pv_plant_id);

		$class = ucfirst($this->action->id);
		
		//GIA NA ALLAKSOUME THN HMEROMINIA PRIN TO RENDERING PIO SWSTA PREPEI NA EIXAME ENA CFormModel
		//H MALLON PIO APLA NA XEIRISTOUME PIO EKSIPNA OLES TIS HMEROMINIES ME MIA NEA KLASSI
		$models['ProcedureStep']->execution_date = CHtmlEx::convStamp2date($models['ProcedureStep']->execution_date);
		$models['SimvasiPolisis']->conn_compl_date = CHtmlEx::convStamp2date($models['SimvasiPolisis']->conn_compl_date);
		
		$this->render('step', compact('form','class','pv_plant_id'));
	}
	
	public function actionActivation($params) {
	//pv_plant_id
		$params = CHtmlEx::array_url($params, true);
		extract($params);
		
		$models = $this->getModels($pv_plant_id);
		
		$class = ucfirst($this->action->id);
		
		//HERE WE ADJUST MODELS BECAUSE IN ACTIVATION WE HAVE A SPECIAL OCCASION WHERE ALL THE DATA ARE SAVED INSIDE pv_enabled table
		unset($models[$class]);
		$models['PvEnabled'] = $models['ProcedureStep']->pvPlant->pvEnabled;
		
		$form = $this->getForm($models);
		
		$this->redirectStep($form, $models, $pv_plant_id);
		
		//GIA NA ALLAKSOUME THN HMEROMINIA PRIN TO RENDERING PIO SWSTA PREPEI NA EIXAME ENA CFormModel
		//H MALLON PIO APLA NA XEIRISTOUME PIO EKSIPNA OLES TIS HMEROMINIES ME MIA NEA KLASSI
		$models['ProcedureStep']->execution_date = CHtmlEx::convStamp2date($models['ProcedureStep']->execution_date);
		
		$this->render('step', compact('form','class','pv_plant_id'));
	}
	
	/**
	 * @param CForm $form
	 * @param array $models
	 * @param integer $pv_plant_id
	 */
	protected function redirectStep(& $form, & $models, $pv_plant_id) {
		$submitButton = key($this->config['buttons']);
		
		if( $form->submitted($submitButton, true) && $form->validate() ) {
			$transaction = Yii::app()->db->beginTransaction();
			try {
				foreach($models as $class => & $model) {
					if( !$model->save(false) ) {
						$message = Yii::t('', 'saving model {class} has failed', array(
							'{class}' => $class,
						));
						throw new Exception($message);	
					}
				}

				$transaction->commit();
				
				$edit_result = Yii::t('', 'Data changed successfully!');
				Yii::app()->user->setFlash('edit_result', $edit_result);

				$this->redirect(array(
					'submenu',
					'pv_plant_id' => $pv_plant_id,
				));
			}
			catch(Exception $e) {
			    $transaction->rollBack();
			}
		}
	}
	
	protected function & getModels($pv_plant_id) {
		$model = CHtmlEx::loadModel('PvPlant', $pv_plant_id);
		$procedures = $model->procedures;
		
		$class = ucfirst($this->action->id);
		
		$models = array('ProcedureStep' => null, $class => null);	//this initialization is to help the order only
		
		$models[$class] = $procedures[$class];
		$models['ProcedureStep'] = $models[$class]->procedureStep;
		
		return $models;
	}
	
	protected function & getForm(& $models) {
		$config = $this->config;	//initialize config
		
		$elements = & $config['elements'];
		
		foreach($models as $class => $model) {
			$elements[$class] = $model->formConfig;
		}
		
		$form = new CForm($config);
		
		foreach($models as $class => & $model) {
			$form[$class]->model = $model;
		}
		
		return $form;
	}
	
	public function actionCateg_protocol($params) {
	//pv_plant_id
		$params = CHtmlEx::array_url($params, true);
		extract($params);

		$model = CHtmlEx::loadModel('PvPlant', $pv_plant_id);
		$form = $model->form;
		
		if( $form->submitted('submit', false) ) {	//false means we do not want to load data along with the submission
			
			$transaction = Yii::app()->db->beginTransaction();
			try {
			//transaction is necessary because a deletion of the child may occur inside the database while loading data
				$form->loadData();
				
				if( !$form->validate() ) {
					$message = Yii::t('', 'validation failed');
					throw new Exception($message);	
				}
				
				if( !$model->save(false) ) {
					$message = Yii::t('', 'saving failed');
					throw new Exception($message);
				}
				
				$transaction->commit();
				
				$edit_result = Yii::t('', 'Data changed successfully!');
				Yii::app()->user->setFlash('edit_result', $edit_result);
				
				$this->redirect(array(
					'submenu',
					'pv_plant_id' => $pv_plant_id,
				));
			}
			catch(Exception $e) {
			    $transaction->rollBack();
			    $message = Yii::t('', 'Application number already exists');
			    $model->addError('', $message);
			}
		}
		
		$this->render('categ_protocol', compact('form'));
	}
	
	public function actionKill($params) {
	//pv_plant_id
		$params = CHtmlEx::array_url($params, true);
		extract($params);
		
		$model = new Killed();
		$model->pv_plant_id = $pv_plant_id;
		$model->date_killed = gmmktime();
		
		try {
			if( $model->save(true) ) {
				$edit_result = Yii::t('', 'Ending of terms has been executed successfully');
			}
		}
		catch(Exception $e) { //to catch the DB exception if the user tries to re-kill the same pv plant
			$edit_result = Yii::t('', 'Ending of terms has failed');
			$edit_result .= '<br/>';
			$edit_result .= Yii::t('', 'Are you trying to kill the same plant twice?');
		}
		
		$edit_result .= '<br/>';
		$edit_result .= 'Μπορείτε απλά να κατευθυνθείτε στην προβολή της καταχώρησης για να το επιβεβαιώσετε';
		
		Yii::app()->user->setFlash('edit_result', $edit_result);
		
		$this->redirect(array(
			'submenu',
			'pv_plant_id' => $pv_plant_id,
		));
	}
}