<?php

class SubmitterController extends Controller {
	/**
	 * @var string
	 */
	public $layout = '//layouts/column2';
	
	/**
	 * @var string
	 */
	public $defaultAction = 'index';
	
	/**
	 * @var integer
	 */
	public $pv_plant_id = null;
	
	/**
	 * @see CController::filters()
	 * @return array action filters
	 */
	public function filters() {	//http://www.yiiframework.com/doc/guide/1.1/en/basics.controller
		$filters = array(
			//'accessControl',
			array(
                'application.filters.CheckAccessControl - index',	//all actions except index
                'role' => 'island_manager',
            ),
            array( //only for certain actions
                'application.filters.FinderFilter + diatiposiOron,simvasiSindesis,simvasiPolisis,activation,comment',
			),
		);
		
		return $filters;
	}
	
	/**
	 * @todo PROBABLY WE SHOULD REPLACE THIS FUNCTION LATER WITH A REAL FILTER CLASS
	 * @throws CHttpException
	 * @return int
	 */
	protected function getIslandId() {
		if( Yii::app()->user->hasState('island_id') ) {
			return Yii::app()->user->island_id;
		}
		else {
			$message = Yii::t('', 'Please choose an Island before you proceed');
			throw new CHttpException(404, $message);
		}
	}
	
	public function actionIndex() {
		$island_id = $this->getIslandId();
		
		$this->render('index',array(
			'island_name' => CHtmlEx::loadModel('Island', $island_id)->island_name,
		));
	}
	
	public function actionArxikiAitisi() {
		$island_id = $this->getIslandId();
		
		$model = new ArxikiAitisiForm();
		$form = $model->form;
		
		try {
			if( $form->submitted('submit', true) && $form->validate() ) {
				$model->save($island_id);
				
				$menu = array(
					array(
						'label' => Yii::t('', 'Next Submission') .' - '. ArxikiAitisi::getTitle(),
						'url' => array(
							$this->route,
						),
					),
					array(
						'label' => Yii::t('', 'End'),
						'url' => array(
							'submitter/index',
						),
					),
				);
				
				$title = Yii::t('', 'Successful submission of data for step') .': '. ArxikiAitisi::getTitle(); //DEN XRISIMOPOIEITAI AKOMA
				
				$pv_plant_id = $model->pv_plant_id;
				
				$params = compact('pv_plant_id', 'menu');
				
				$this->redirect( array(
					'reporter/full_report',
					'params' => CHtmlEx::array_url($params),
				));
			}
		}
		catch(Exception $e) {
			$model->addError('', $e->getMessage());
		}
		
		$this->render('submit_form',compact('form'));
	}
	
	/*
	protected function finderRedirect() {
		$redirect_uri = $this->route;
		
		$params = compact('redirect_uri');
		
		$this->redirect( array(
			'finder/index',
			'params' => CHtmlEx::array_url($params),
		));
	}
	//*/
	
	public function actionDiatiposiOron() {		
		$pv_plant_id = $this->pv_plant_id;
		
		$model = new DiatiposiOronForm();
		$model->pv_plant_id = $pv_plant_id;
		$form = $model->form;
				
		$form->action = array(
			$this->route,
			'pv_plant_id' => $pv_plant_id,
		);
		
		try {
			if( $form->submitted('submit', true) && $form->validate() ) {
				$model->save();
				
				$menu = array(
					array(
						'label' => Yii::t('', 'Next Submission') .' - '. DiatiposiOron::getTitle(),
						'url' => array(
							$this->route,
						),
					),
					array(
						'label' => Yii::t('', 'End'),
						'url' => array(
							'submitter/index',
						),
					),
				);
				
				$title = Yii::t('', 'Successful submission of data for step') .': '. DiatiposiOron::getTitle(); //DEN XRISIMOPOIEITAI AKOMA
				
				$params = compact('pv_plant_id', 'menu');
				
				$this->redirect( array(
					'reporter/full_report',
					'params' => CHtmlEx::array_url($params),
				));
			}
		}
		catch(Exception $e) {
			$model->addError('', $e->getMessage());
		}
		
		$this->render('submit_form',compact('form'));
	}

	/**
	 * 
	 */
	public function actionSimvasiSindesis() {
		$pv_plant_id = $this->pv_plant_id;
		
		$model = new SimvasiSindesisForm();
		$model->pv_plant_id = $pv_plant_id;
		$form = $model->form;
		
		$form->action = array(
			$this->route,
			'pv_plant_id' => $pv_plant_id,
		);
		
		try {
			if( $form->submitted('submit', true) && $form->validate() ) {
				$model->save();
				
				$menu = array(
					array(
						'label' => Yii::t('', 'Next Submission') .' - '. SimvasiSindesis::getTitle(),
						'url' => array(
							$this->route,
						),
					),
					array(
						'label' => Yii::t('', 'End'),
						'url' => array(
							'submitter/index',
						),
					),
				);
				
				$title = Yii::t('', 'Successful submission of data for step') .': '. SimvasiSindesis::getTitle(); //DEN XRISIMOPOIEITAI AKOMA
				
				$params = compact('pv_plant_id', 'menu');

				$this->redirect( array(
					'reporter/full_report',
					'params' => CHtmlEx::array_url($params),
				));
			}
		}
		catch(Exception $e) {
			$model->addError('', $e->getMessage());
		}
		
		$this->render('submit_form',compact('form'));
	}
	
	/**
	 * 
	 */
	public function actionSimvasiPolisis() {
		$pv_plant_id = $this->pv_plant_id;
		
		$model = new SimvasiPolisisForm();
		$model->pv_plant_id = $pv_plant_id;
		$form = $model->form;
		
		$form->action = array(
			$this->route,
			'pv_plant_id' => $pv_plant_id,
		);
		
		try {
			if( $form->submitted('submit', true) && $form->validate() ) {
				$model->save();
				
				$menu = array(
					array(
						'label' => Yii::t('', 'Next Submission') .' - '. SimvasiPolisis::getTitle(),
						'url' => array(
							$this->route,
						),
					),
					array(
						'label' => Yii::t('', 'End'),
						'url' => array(
							'submitter/index',
						),
					),
				);
				
				$title = Yii::t('', 'Successful submission of data for step') .': '. SimvasiPolisis::getTitle(); //DEN XRISIMOPOIEITAI AKOMA
				
				$params = compact('pv_plant_id', 'menu');

				$this->redirect( array(
					'reporter/full_report',
					'params' => CHtmlEx::array_url($params),
				));
			}
		}
		catch(Exception $e) {
			$model->addError('', $e->getMessage());
		}
		
		$this->render('submit_form',compact('form'));
	}
	
	/**
	 * 
	 */
	public function actionActivation() {
		$pv_plant_id = $this->pv_plant_id;
		
		$model = new ActivationForm();
		$model->pv_plant_id = $pv_plant_id;
		$form = $model->form;
		
		$form->action = array(
			$this->route,
			'pv_plant_id' => $pv_plant_id,
		);
		
		try {
			if( $form->submitted('submit', true) && $form->validate() ) {
				$model->save();
				
				$menu = array(
					array(
						'label' => Yii::t('', 'Next Submission') .' - '. Activation::getTitle(),
						'url' => array(
							$this->route,
						),
					),
					array(
						'label' => Yii::t('', 'End'),
						'url' => array(
							'submitter/index',
						),
					),
				);
				
				$title = Yii::t('', 'Successful submission of data for step') .': '. Activation::getTitle(); //DEN XRISIMOPOIEITAI AKOMA
				
				$params = compact('pv_plant_id', 'menu');

				$this->redirect( array(
					'reporter/full_report',
					'params' => CHtmlEx::array_url($params),
				));
			}
		}
		catch(Exception $e) {
			$model->addError('', $e->getMessage());
		}
		
		$this->render('submit_form',compact('form'));
	}
	
	/**
	 * 
	 */
	public function actionComment() {
		$pv_plant_id = $this->pv_plant_id;
		
		$model = new CommentForm();
		$model->pv_plant_id = $pv_plant_id;
		$form = $model->form;
		
		$form->action = array(
			$this->route,
			'pv_plant_id' => $pv_plant_id,
		);
		
		try {
			if( $form->submitted('submit', true) && $form->validate() ) {
				$model->save();
				
				$menu = array(
					array(
						'label' => Yii::t('', 'Insert new comment'),
						'url' => array(
							$this->route,
							'pv_plant_id' => $pv_plant_id,
						),
					),
					array(
						'label' => Yii::t('', 'End'),
						'url' => array(
							'submitter/index',
						),
					),
				);
				
				$title = Yii::t('', 'All comments');	//DEN XRISIMOPOIEITAI AKOMA
				
				$params = compact('pv_plant_id', 'menu');

				$this->redirect( array(
					'reporter/list_comments',
					'params' => CHtmlEx::array_url($params),
				));
			}
		}
		catch(Exception $e) {
			$model->addError('', $e->getMessage());
		}
		
		$this->render('submit_form',compact('form'));
	}
}