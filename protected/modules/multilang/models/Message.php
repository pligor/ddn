<?php

/**
 * This is the model class for table "Message".
 *
 * The followings are the available columns in table 'Message':
 * @property integer $id
 * @property string $language
 * @property string $translation
 *
 * The followings are the available model relations:
 * @property SourceMessage $source
 */
class Message extends CActiveRecord
{
	/**
	 * @see CActiveRecord::init()
	 */
	public function init() {
	}
	
	public static function getLanguages() {
		$path = YiiBase::getPathOfAlias('system.i18n.data');
		$suffix = ".php";
		$files = scandir($path);
		
		$languages = array();
		foreach($files as $file) {
			if(mb_substr($file, -mb_strlen($suffix)) != $suffix) {
				continue;
			}
			$language = basename($file,$suffix);
			$languages[$language] = $language;
		}

		return $languages;
	}
	
	private $languageMaxLen = 16;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Message the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that will receive user inputs.
		return array(
			array('translation, language', 'required'),
			array('language', 'length', 'max'=>$this->languageMaxLen),
			array('translation, language', 'safe'),
			array('id', 'unsafe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, language, translation', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => 'Υποφόρμα Αρχικής Αίτησης',
		    //'showErrorSummary' => true,
		    'elements' => array(
				'language' => array(
					'type' => 'dropdownlist',
					'items' => self::getLanguages(),
					'hint' => 'language in which we want to translate to',
				),
				'translation' => array(
		        	'type' => 'text',
					'hint' => 'the translated message',
					'attributes' =>  array(
						'size' => 60,
					),
		        ),
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'source' => array(self::BELONGS_TO, 'SourceMessage', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'language' => 'Language',
			'translation' => 'Translation',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('translation',$this->translation,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}