<?php
/**
 * TranslationForm class
 * @property string $message
 * @property string $translation
 * @property string $language
 * @property string $category
 */
class TranslationForm extends CFormModel {
	
	/**
	 * Initialize model before executing setScenario inside parent's construct
	 */
	public function __construct($scenario='') {
		$classes = array(
			'SourceMessage',
			'Message',
		);

		$name = 'MultiModelBehavior';
		$behavior = new $name($classes);
		$this->attachBehavior($name, $behavior);
		
		$name = 'InheritRulesBehavior';
		$behavior = new $name();
		$this->attachBehavior($name, $behavior);
		
		parent::__construct($scenario);
	}
	
	/**
	 * Validation for each specific model is by default false because current model implements its own validation
	 * @param bool $runValidation
	 */
	public function save($runValidation=false) {
		$transaction = Yii::app()->db->beginTransaction();
		
		try {		//you must save in a specific order because of foreign key constraints
		
			$model = $this->models['SourceMessage'];
			
			$attributes = array(
				'message' => $model->message,
			);
			$existingModel = SourceMessage::model()->findByAttributes($attributes);
			if($existingModel == null) {
				if( !$model->save($runValidation) ) {
					$message = "saving SourceMessage failed";
					throw new Exception($message);
				}
			}
			else {
				$models = $this->models;
				$models['SourceMessage'] = $existingModel;
				$this->models = $models;
			}
			
			$model = $this->models['Message'];
			$model->id = $this->models['SourceMessage']->id;
			if( !$model->save($runValidation) ) {
				$message = "saving Message failed";
				throw new Exception($message);
			}
			
			$transaction->commit();
		}
		catch(Exception $e) {
		    $transaction->rollBack();
		    return false;
		}
		
		return true;
	}
	
	protected $behaviors = array(
		'MultiModelBehavior',
		'InheritRulesBehavior',
	);
	
	public function behaviors() {
		return array(
			/*'behaviorName'=>array(
			    'class'=>'path.to.BehaviorClass',
			    'property1'=>'value1',
			    'property2'=>'value2',
			),*/
		);
	}
	
	/**
	 * get rules for currently safe attributes for all models
	 */
	protected function appendSafeRules() {
		$behavior = 'InheritRulesBehavior';
		$behaviors = $this->behaviors;
		if( !in_array($behavior, $behaviors ) && !in_array($behavior, array_keys($behaviors)) ) {
			return;
		}

		foreach($this->models as $model) {
			$this->rules = array_merge($this->rules, $this->getSafeRules($model));
		}
	}
	
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		$behavior = 'InheritRulesBehavior';
		$behaviors = $this->behaviors;
		if( in_array($behavior, $behaviors ) || in_array($behavior, array_keys($behaviors)) ) {
			return $this->rules;
		}
		
		return array(
			/*array(
				'translation',
				'safe',
			),*/
		);
	}
	
	public function getId() {
		return mb_strtolower(get_class());
	}
	
	/**
	 * @see CFormModel::init()
	 */
	public function init() {
		$this->appendSafeRules();
		$this->language = Yii::app()->language;
	}
	
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		$attrLabels = array(
		);
		
		$attrLabels += $this->attrLabels;
			
		return $attrLabels;
	}
	
	/**
	 * Note: in the form below there is only one subform referring to the same model as its parent
	 * therefore there is no need to defile a model for the subform separately
	 */
	public function getForm() {
		//$formConfig = $this->getFormConfig();
		$config = array(
			//'title' => 'Submit a new translation',
			'showErrorSummary' => true,
			'elements'=> $this->getFormConfigs(),	//http://www.yiiframework.com/doc/guide/1.1/en/form.builder#creating-a-nested-form
			'buttons'=>array(
		        'submit_translation' => array(
		            'type' => 'submit',
		            'label' => 'Insert Translation',
		        ),
		    ),
		    /*'attributes' => array(
		    	'enctype' => 'multipart/form-data',
		    ),*/
		);
		return new CForm($config, $this);	//all subforms get as parent their model
	}
	
	public function getFormConfigs() {
		$formConfigs = array();
		foreach($this->models as $model) {
			$formConfigs[mb_strtolower(get_class($model))] = $model->getFormConfig();
		}
		return $formConfigs;
	}
	
	/**
	 * Get safe attribute name
	 * @param string $name
	 */
	public function __get($name) {
		if( !$this->checkModelAttribute($name) ) {
			return parent::__get($name);
		}
		return $this->getModelAttribute($name);
	}
	
	/**
	 * @see CComponent::__set()
	 */
	public function __set($name, $value) {
		if( $this->setModelAttribute($name, $value) === false) {
			parent::__set($name, $value);
		}
	}
	
	/**
	 * Set the same scenario also for all the corresponding models
	 * @param string $value
	 */
	public function setScenario($value) {
		$this->setModelsScenario($value);
		parent::setScenario($value);
	}
}