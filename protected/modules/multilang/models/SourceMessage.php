<?php

/**
 * This is the model class for table "SourceMessage".
 *
 * The followings are the available columns in table 'SourceMessage':
 * @property integer $id
 * @property string $category
 * @property string $message
 *
 * The followings are the available model relations:
 * @property Message[] $messages
 */
class SourceMessage extends CActiveRecord
{
	protected $categoryDefault = '';
	protected $categoryMaxLen = 32;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return SourceMessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'SourceMessage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that will receive user inputs.
		return array(
			array('message', 'required'),
			array('category','default','value'=>$this->categoryDefault),
			array('category', 'length', 'max'=>$this->categoryMaxLen),
			array('message, category', 'safe'),
			array('id', 'unsafe'),
			// The following rule is used by search(). Please remove those attributes that should not be searched.
			array('id, category, message', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormConfig() {
		return array(
			'type' => 'form',
		    //'title' => 'Υποφόρμα Αρχικής Αίτησης',
		    //'showErrorSummary' => true,
		    'elements' => array(
				'category' => array(
					'type' => 'text',
					'hint' => "If left empty the default category value will be '{$this->categoryDefault}'",
					'attributes' =>  array(
						'size' => 10,
						'maxlength' => $this->categoryMaxLen,
					),
				),
		    	'message' => array(
		        	'type' => 'text',
					'hint' => "REMINDER: source language is set as: <b>". Yii::app()->sourceLanguage ."</b>",
					'attributes' =>  array(
						'size' => 60,
					),
		        ),
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'messages' => array(self::HAS_MANY, 'Message', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => 'Category',
			'message' => 'Message',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('message',$this->message,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}