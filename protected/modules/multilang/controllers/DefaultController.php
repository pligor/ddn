<?php

class DefaultController extends Controller
{
	public function actionInsert() {	
		$model = new TranslationForm();
		$form = $model->form;
		
		if($form->submitted('submit_translation') && $form->validate()) {
			if($model->save()) {
				$message = "translation saved successfully";
			}
			else {
				$message = "failed to save translation";
			}
		}
		
		$this->render('form',compact('form','message'));
	}
	
	public function actionDelete($id) {
		//remember by deleting source message all the translations are deleted as well
		SourceMessage::model()->findByPk($id)->delete();
		$this->redirect(array('index'));
	}
	
	public function actionIndex() {
		$criteria=new CDbCriteria;
		$criteria->with = 'source';
		$criteria->together = true;
		
		$dataProvider = new CActiveDataProvider('Message', array(
			'criteria' => $criteria,
			'pagination'=>array(
		        'pageSize'=>15,
		    ),
		));
		
		$this->render('index',compact('dataProvider'));
	}
}