<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
	'columns' => array(
		//'language',
		//'translation',
		'source.category',
		'source.message',
		'language',
		'translation',
		array(            // display a column with "view", "update" and "delete" buttons
            'class'=>'CButtonColumn',
			//disable view button
            'viewButtonImageUrl' => false,
			'viewButtonLabel' => '',
            'viewButtonOptions' => array(
				'style' => "display:none",
			),
			//disable update button
			'updateButtonImageUrl' => false,
			'updateButtonLabel' => '',
            'updateButtonOptions' => array(
				'style' => "display:none",
			),
			'deleteButtonUrl' => 'Yii::app()->controller->createUrl("delete", array("id"=>$data->source->id))',
        ),
	),
));