<h1><?php echo $this->uniqueId . '/' . $this->action->id; ?></h1>

<p>
The action "<?php echo $this->action->id; ?>" belongs to the controller "<?php echo get_class($this); ?>"
in the "<?php echo $this->module->id; ?>" module.
</p>

<div class="form">
<?php
print $form->render();
?>
</div>

<?php
//http://www.yiiframework.com/wiki/21/how-to-work-with-flash-messages
Yii::app()->clientScript->registerScript(
   'myHideEffect',
   '$("#message").hide(10000);',
   CClientScript::POS_READY
);
?>

<div id="message">
<?php if(isset($message)): ?>
<p>
<?php print $message; ?>
</p>
<?php endif; ?>
</div>