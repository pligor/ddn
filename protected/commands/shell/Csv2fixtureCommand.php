<?php
class Csv2fixtureCommand extends CConsoleCommand {
	const csvDelimiter = ';';
	
	public function getHelp() {
		$text = "\n". Yii::t('', 'USAGE') .": csv2fixture\n";
		$text .= Yii::t('', 'DESCRIPTION: This command converts a csv file to php fixture')."\n";
		$text .= 'csv2fixture <csv filename inside data folder> <column1> <column2> <column3> ...' ."\n";
		return $text;
	}
	
	/**
	* Execute the action.
	* @param array command line parameters specific for this command
	*/
	public function run($args) {
		if( count($args)<2 ) {
			print 'please provide at least two arguments';
			return;
		}
		
		$filename = array_shift($args);
		$columns = $args;	//rest are columns
				
		/**
		 * @todo move the following procedure inside a class method
		 */
		
		$filename = basename($filename, '.csv');
		$path = Yii::getPathOfAlias('application.data');
		$csvPath = $path .'/'. $filename .'.csv';		
		
		$output = "<?php"."\n";
		$output .= "return array("."\n";
		
		$twoDarray = CHtmlEx::csv2array($csvPath, self::csvDelimiter);
		
		foreach($twoDarray as $key => $rowArray) {
			$id = $key+1;
			
			$output .= "\t$id => array("."\n";
			
			foreach($columns as $keyCol => $col) {
				$value = $rowArray[$keyCol];
				if(!is_numeric($value)) {
					$value = "'$value'";
				}
				$output .= "\t\t'$col' => $value,"."\n";
			}
			
			$output .= "\t),"."\n";
		}
		
		$output .= ");"."\n";
		
		print $output;
		
		$phpPath = $path .'/'. $filename .'.php';
		file_put_contents($phpPath, $output);
	}
}