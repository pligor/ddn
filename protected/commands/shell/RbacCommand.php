<?php
class RbacCommand extends CConsoleCommand {
	
	private $authManager;
	
	public function getHelp() {
		$text = "\n". Yii::t('', 'USAGE') .": rbac\n";
		$text .= Yii::t('', "DESCRIPTION: This command generates an initial RBAC authorization hierarchy")."\n";
		return $text;
	}
	
	/**
	* Execute the action.
	* @param array command line parameters specific for this command
	*/
	public function run($args) {
		//ensure that an authManager is defined as this is mandatory for creating an auth heirarchy
		if( ($this->authManager=Yii::app()->authManager) === null )
		{
			print Yii::t('', "Error: an authorization manager, a component named 'authManager' must");
			print Yii::t('', "be defined inside configuration (config.php) to use this command").".\n";
			return;
		}
		
		//provide the opportunity for the user to abort the request
		print Yii::t('', "This command will create all the necessary roles")."\n";
		print Yii::t('', "Would you like to continue?") ." [Yes|No]";
		
		//check the input from the user and continue if they indicated yes to the above question
		if( strncasecmp(trim(fgets(STDIN)),'y',1) != 0 ) {
			print "\n". Yii::t('', "authorization hierarchy generation cancelled") ."\n";
			return;	
		}

		//first we need to remove all operations, roles, child relationship and assignments
		$this->authManager->clearAll();

		//OPERATIONS
		$this->authManager->createOperation("insert_island", "insert data into one or more islands");
		$this->authManager->createOperation("read_island", "read and display datas from one or more islands");
		$this->authManager->createOperation("update_island", "update data of one or more islands");
		$this->authManager->createOperation("delete_island", "delete data from one or more islands");
		
		$this->authManager->createOperation("calc_stats", "calculate statistics for all islands");
		
		$this->authManager->createOperation("excel", "perform operations for excel");
		$this->authManager->createOperation("administration", "perform db related and other administrative actions");
		
		
		//ROLES
		$role = $this->authManager->createRole("manager", "general manager");
		$role->addChild("insert_island");
		$role->addChild("read_island");
		$role->addChild("update_island");
		$role->addChild("delete_island");
		
		//because of this: http://www.yiiframework.com/forum/index.php?/topic/25222-rbac-not-expected-behavior/
		//we added: isset($params["island_id"]) &&
		$bizrule = 'return isset($params["island_id"]) && in_array($params["island_id"], User::model()->findByPk(Yii::app()->user->id)->associatedIslandIds);';
		$role = $this->authManager->createRole("island_manager","manager for one or more islands",$bizrule);
		$role->addChild("manager");
		
		$role = $this->authManager->createRole("super_user", "manages all islands and calcs stats");
		$role->addChild("manager");
		$role->addChild("calc_stats");
		
		$role=$this->authManager->createRole("administrator","full permission to even critical db actions");
		$role->addChild("super_user");
		$role->addChild("excel");
		$role->addChild("administration");
		
		print Yii::t('', "Authorization hierarchy successfully generated");	//provide a message indicating success
	}
}