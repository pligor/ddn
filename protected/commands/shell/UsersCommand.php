<?php
class UsersCommand extends CConsoleCommand {
	
	private $authManager;
	
	public function getHelp() {
		$text = "\n". Yii::t('', "USAGE") .": users\n";
		$text .= Yii::t('', "DESCRIPTION: Automatic user generation for each island (island managers)") ."\n";
		return $text;
	}
	
	private function clearAll() {
		$this->authManager->clearAuthAssignments();
		UserIsland::model()->deleteAll();
		User::model()->deleteAll();
	}
	
	private function saveUser($id,$name,$pass) {
		//delete user if exists
		if($user=User::model()->findByAttributes(array('name'=>$name))) {
			$user->delete();
		}
		
		$model = new User();
		$model->id = $id;
		$model->attributes = compact('name','pass');
		return $model->save(false);
	}
	
	private function saveUserIsland($user_id,$island_id) {
		$association = new UserIsland();
		$association->user_id = $user_id;
		$association->island_id = $island_id;
		return $association->save(false);
	}
	
	/**
	* Execute the action.
	* @param array command line parameters specific for this command
	*/
	public function run($args) {
		//ensure that an authManager is defined as this is mandatory for creating an auth heirarchy
		if( ($this->authManager=Yii::app()->authManager) === null )
		{
			print Yii::t('', "Error: an authorization manager, a component named 'authManager' must");
			print Yii::t('', "be defined inside configuration (config.php) to use this command").".\n";
			return;
		}
		
		//provide the opportunity for the user to abort the request
		print Yii::t('', "This command will create all users for each island as well as admin and super_user") ."\n";
		print Yii::t('', "Would you like to continue?") ." [Yes|No]";
		//check the input from the user and continue if they indicated yes to the above question
		if( strncasecmp(trim(fgets(STDIN)),'y',1) != 0 ) {
			print "\nusers generation cancelled\n";
			return;	
		}
		
		$this->clearAll();
		
		//create admin
		$uid = 1;
		$name = 'admin';
		$pass = '0mmN1';
		$this->saveUser($uid, $name, $pass);
		$role = "administrator";
		$this->authManager->assign($role, $uid);
		$uid++;
		
		//create super_user
		$name = 'super.user';
		$pass = 'super.user';
		$this->saveUser($uid, $name, $pass);
		$role = "super_user";
		$this->authManager->assign($role, $uid);
		$uid++;
		
		//create one user for each island
		$role = "island_manager";
		$islands = Island::model()->findAll();
		foreach($islands as $island) {
			$name = $island->island_name .'.manager';
			$pass = "manager";
			
			$this->saveUser($uid, $name, $pass);
			$this->saveUserIsland($uid, $island->id);
			$this->authManager->assign($role, $uid);
			
			$uid++;
		}
		
		//create tsx user (tomea syndesis xristwn)
		$name = 'tsx';
		$pass = 'afroditi';
		$this->saveUser($uid, $name, $pass);
		//$role = "??????";
		//$this->authManager->assign($role, $uid);
		$uid++;
		
		print Yii::t('', "All users, user associations and authorization assignments were successfully generated!");
	}
}