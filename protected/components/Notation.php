<?php
class Notation extends CComponent {
	const groups_del = ',';
	const serial_del = ':';
	
	/**
	 * @var string
	 */
	public $notation;
	
	public function convNumbers_xlCols($reverse=false) {
		$notation = CHtmlEx::noWhitespace($this->notation);	//clear notation

		if($reverse) {
			$func = 'columnIndexFromString';
		}
		else {
			$func = 'stringFromColumnIndex';
		}
		$function = array('PHPExcel_CellHelper', $func);
		
		$cols = array();
		foreach( explode(self::groups_del, $notation) as $group ) {
			if( strpos($group, self::serial_del)===false ) {
				$cols[] = call_user_func($function,$group);
			}
			else {
				$exploded = explode(self::serial_del, $group);
				$colStart = call_user_func($function,reset($exploded));
				$colEnd = call_user_func($function,end($exploded));
				$cols[] = implode(self::serial_del, array($colStart,$colEnd));
			}
		}
		$this->notation = implode(self::groups_del, $cols);
	}
	
	public function expand() {
		$notation = CHtmlEx::noWhitespace($this->notation);	//clear notation
		
		$numbers = array();
		foreach( explode(self::groups_del, $notation) as $group ) {
			if( strpos($group, self::serial_del)===false ) {
				$numbers[] = $group;
			}
			else {
				$exploded = explode(self::serial_del, $group);
				$numbers = array_merge($numbers, range(reset($exploded), end($exploded)));
			}
		}
		sort($numbers);
		return array_unique($numbers);
	}
	
	/**
	 * Returns something similar to this: B3:B4,B7,B9:B11 to be inserted into sum
	 * So we have a complex SUM type compatible with Excel2003
	 * @param string[] $numbers
	 * @param string $prefix
	 */
	public function getCompact($numbers, $prefix=null) {
		$numbers = array_unique($numbers);
		sort($numbers);
		
		$cellGroups = array();
		$countNumber = reset($numbers);
		$cells = array();
		$cells['start'] = $prefix.$countNumber;
		
		foreach($numbers as $key => $number) {
			$nextNumber = isset($numbers[$key+1]) ? $numbers[$key+1] : false;
			
			$cells['end'] = $prefix.$number;
			
			if( ((++$countNumber) != $nextNumber) || $nextNumber===false ) {
				if($cells['start'] != $cells['end']) {
					$cellGroups[] = implode(self::serial_del, $cells);
				}
				else {
					$cellGroups[] = reset($cells);
				}
				
				$countNumber = $nextNumber;
				$cells['start'] = $prefix.$countNumber;
			}
		}
		
		$this->notation = implode(self::groups_del, $cellGroups);
	}
}