<?php

class WorksheetHelper extends CComponent {
	public static function loadPHPExcel() {
		spl_autoload_unregister(array('YiiBase','autoload'));	//Turn off our amazing library autoload			
		Yii::import('ext.phpexcel.PligorPHPExcel',true);	//include("$phpExcelPath/PHPExcel.php");	
	}
	
	public static function unLoadPHPExcel() {
		spl_autoload_register(array('YiiBase','autoload'));	//give back the power to Yii
	}
	
	/**
	 * @param PHPExcel_Worksheet_Row $row
	 * @param string[]|null $columns if columns is null then all the valid columns will be searched
	 * @return boolean
	 */
	public static function isRowEmpty($row,$columns=null) {
		$isRowEmpty = true;
		for($cellIter = $row->getCellIterator();$cellIter->valid();$cellIter->next()) {
			$j = $cellIter->key();
			$col = PHPExcel_CellHelper::stringFromColumnIndex($j);

			if( is_array($columns) && !in_array($col, $columns) ) {
				continue; //dont care for this column
			}
			
			$cellValue = $cellIter->current()->getValue();
	
			$isRowEmpty = $isRowEmpty && CHtmlEx::isEmpty($cellValue);
		}
		return $isRowEmpty;
	}
	
	public static function clearPageBreaks(& $sheet) {
		self::loadPHPExcel();
		$pBreak = PHPExcel_Worksheet::BREAK_NONE;
		self::unLoadPHPExcel();
		
		for($rowIter = $sheet->getRowIterator();$rowIter->valid();$rowIter->next()) {
			$i = $rowIter->key();
			$row = $rowIter->current();
			
			$cellIter = $row->getCellIterator();
			for($cellIter->setIterateOnlyExistingCells(false);$cellIter->valid();$cellIter->next()) {
				$j = $cellIter->key();
				//$cell = $cellIter->current();
				$sheet->setBreakByColumnAndRow($j,$i,$pBreak);
			}
		}
	}
	
	/**
	 * @param PHPExcel $phpExcel
	 */
	public static function fillBorders(& $phpExcel) {
		$styles = include Yii::getPathOfAlias('application.widgets.assets.ExcelSheetView.styles') .'.php';
		$borderStyle = $styles['thin_border'];
		
		$sheets = $phpExcel->getAllSheets();
		
		foreach($sheets as & $sheet) {
			for($rowIter = $sheet->getRowIterator();$rowIter->valid();$rowIter->next()) {
				$row = $rowIter->current();
		
				$cellIter = $row->getCellIterator();
				$cellIter->setIterateOnlyExistingCells(false);//true here means skip empty cells which is not wanted
		
				while( $cellIter->valid() )	//note: OR means all columns
				{
					$cell = $cellIter->current();
						
					$sheet->getStyle( $cell->getCoordinate() )->applyFromArray( $borderStyle );
						
					$cellIter->next();
				}
			}
		}
	}
}