<?php

/**
 * @author pligor
 */
class PHPExcelHelper extends CComponent {
	
	/**
	 * @param PHPExcel $phpExcel
	 * @param string $sheetName
	 */
	public static function createSheetByName(& $phpExcel, $sheetName) {
		$phpExcel->createSheet()->setTitle($sheetName);
		return $phpExcel->setActiveSheetIndexByName($sheetName);
	}
	
	/**
	 * Creates an excel reader
	 * @param array $sheetNames
	 * @param ExcelReadFilter $filterSubset
	 * @param string $format
	 * @return PHPExcel_Reader_Excel5
	 */
	public static function & getObjReader($sheetNames=array(), $filterSubset=null, $format='Excel5') {
		CHtmlEx::loadPHPExcel();
		$objReader = PHPExcel_IOFactory::createReader($format);
		CHtmlEx::unLoadPHPExcel();
		
		if(empty($sheetNames)) {
			$objReader->setLoadAllSheets();	
		}
		else {
			$objReader->setLoadSheetsOnly($sheetNames);
		}
	
		if($filterSubset !== null) {
			$objReader->setReadFilter($filterSubset);	
		}
		
		return $objReader;
	}
}