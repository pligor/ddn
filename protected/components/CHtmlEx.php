<?php

/**
 * Enter description here ...
 * @author pligor
 */
class CHtmlEx extends CComponent {
	const spaces = " \t\n\r";   // space characters
	const dateDelimiter = '/';
	const invalidStr = '??????';
	
	/**
	 * If two arrays contain same key then it throws an exception
	 * @throws Exception
	 * @param array $array1
	 * @param array $array2
	 */
	public static function array_merge_safe($array1, $array2) {
		$c1 = count($array1);
		$c2 = count($array2);
		
		$array = array_merge($array1,$array2);
		if(count($array) !== ($c1+$c2)) {
			$message = Yii::t('', 'You tried to combine two arrays with same keys!');
			throw new Exception($message);
		}
		
		return $array;
	}
	
	public static function complyString2host(& $str) {
		if( PHP_OS === 'Linux' ) {
			;	//noop
		}
		elseif( strpos(PHP_OS, 'Windows') !== false ) {
			$str = iconv(Yii::app()->charset, 'Windows-1253', $str);
		}
		else {
			$message = Yii::t('', 'Please fix complyString2host function in CHtmlEx');
			throw new Exception($message);
		}
	}
	
	public static function stripScripts($str) {
		//sto telos exoume 3 pattern modifiers
		//s: dot character matches all characters including newlines
		//i: case in-sensitive
		//u: ungreedy
		$pattern = '@<script[^>]*?.*?</script>@siu';
		return preg_replace($pattern, '', $str);
	}
	
	public static function html2utf8($input) {
		return preg_replace_callback("/(&#[0-9]+;)/", function($m) {
			return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES");
		}, $input);
	}
	
	//ALL BELOW ARE TESTED////////////////////////////////////////////////
	
	public static function parseGrammiDianomis($grammiDianomis) {
		$pattern = PvEnabled::grammiDianomisPattern();
		$matches = array();
		$noMatches = preg_match($pattern, $grammiDianomis, $matches);
		if($noMatches==1) {
			return 'P/'.$matches[1];
		}
		return self::invalidStr;	//not found
	}
	
	public static function parseRaeNum($raeNum) {
		$patterns = PvXt::getRaePatterns();
		
		$matches = array();
		$pattern = $patterns['cur'];
		$noMatches = preg_match($pattern, $raeNum, $matches);
		if($noMatches==1) {
			return $matches[1].'/'.$matches[2];
		}
		
		$matches = array();
		$pattern = $patterns['old'];
		$noMatches = preg_match($pattern, $raeNum, $matches);
		if($noMatches==1) {
			return 'O-'.$matches[1];
		}
		
		return self::invalidStr;	//not found
	}
	
	public static function parseFloat($value) {
		if(is_numeric($value)) {
			return (float)$value;
		}
		
		$value = self::noWhitespace($value);
		
		if(strlen($value) == 0) {
			return false;
		}
		
		$matches = array(
			'point' => array(),
			'comma' => array(),
		);
		
		$pattern = array(
			'point' => '@[-]?([0-9]+(,[0-9]{3})*)(\.[0-9]+)?@',
			'comma' => '@[-]?([0-9]+(\.[0-9]{3})*)(,[0-9]+)?@',
		);
		
		$count = array();
		$result = array();
		
		$count['point'] = preg_match($pattern['point'], $value, $matches['point']);
		if( $count['point'] == 1 ) {
			$result['point'] = reset($matches['point']);
			$result['point'] = str_replace(',', '', $result['point']);
		}
		
		$count['comma'] = preg_match($pattern['comma'], $value, $matches['comma']);
		if( $count['comma'] == 1 ) {
			$result['comma'] = reset($matches['comma']);
			$result['comma'] = str_replace('.', '', $result['comma']);
			$result['comma'] = str_replace(',', '.', $result['comma']);
		}
		
		$final=null;
		
		if(array_sum($count) == 0) {	//nothing found
			return false;
		}
		elseif(array_sum($count) == 1) {
			if( $count['point'] == 1 ) {
				$final = $result['point'];
			}
			elseif( $count['comma'] == 1 ) {
				$final = $result['comma'];
			}	
		}
		elseif(array_sum($count) == 2) {
			if( strlen($result['point']) > strlen($result['comma']) ) {
				$final = $result['point'];
			}
			else {
				$final = $result['comma'];
			}
		}
		
		if( is_numeric($final) ) {
			return floatval($final);
		}
		
		return false;
	}
	
	/**
	 * not very efficient
	 * @param string $firstCol
	 * @param string $lastCol
	 */
	public static function getColumns($firstCol, $lastCol) {
		$alphabet = range('A', 'Z');
	
		$lenFirst = strlen($firstCol);
		$lenLast = strlen($lastCol);
	
		if( $lenFirst>$lenLast )	return false;
	
		$columns = array();
		
		for($i=1;$i<=$lenLast;$i++)
		{
			if($lenFirst>$i)	continue;	//no need to calculate this length
			$allcolumns = array();
			$tempColumns = array();
			self::recurCols($i, $allcolumns, $tempColumns);
	
			foreach($tempColumns as $tempColumn)
			{
				if($i==$lenFirst && $tempColumn<$firstCol)	continue;
				elseif($i==$lenLast && $tempColumn>$lastCol)	break;
					
				$columns[] =  $tempColumn;
			}
		}

		return $columns;
	}
	
	private static function recurCols($depth, & $allcolumns, & $columns, $prefix="") {
		$alphabet = range('A', 'Z');
		foreach($alphabet as $l)
		{
			$col =  $prefix.$l;
			$allcolumns[] = $col;
			if($depth>1) self::recurCols($depth-1, $allcolumns, $columns, $col);
			elseif($depth=1)
			{
				$columns[] = $col;
			}
		}
	}
	
	public static function mb_str_split($string) {
		# Split at all position after the start: ^
		# and not before the end: $
		return preg_split('/(?<!^)(?!$)/u',$string);
	}
	
	/**
	 * this function can only be tested in a functional test
	 * unfortunately a command line unit test is not suitable
	 * @param string $subdir
	 * @param string $filename
	 */
	public static function getUrlNfullpath($subdir,$filename) {
		$subdir = trim($subdir,DIRECTORY_SEPARATOR);
		$fullpath = Yii::getPathOfAlias("webroot.$subdir") ."/$filename";
		
		//self::complyString2host($subdir);
		//self::complyString2host($filename);
		//$subdir = urlencode($subdir);
		//$filename = urlencode($filename);
		$url = Yii::app()->getBaseUrl() ."/$subdir/$filename";
		
		return compact('url', 'fullpath');
	}
	
	/**
	 * NOTE: it replaces all integers with whitespace prefix
	 * @param string $subject
	 * @return string
	 */
	public static function removeLastIntegers($subject) {
		$pattern = "/\s[0-9]+/";
		$replacement_empty_string = '';
		return preg_replace($pattern, $replacement_empty_string, $subject);
	}
	
	public static function getInteger($subject) {
		$pattern = "/[0-9]+/";	//simple integer
		$matches = array();
		if( preg_match($pattern,$subject,$matches) == 0 ) {
			return null;
		}
		else {
			return reset($matches); 
		}
	}
	
	/**
	 * 
	 * NOTE: it gets only integer with whitespace prefix
	 * For example this is not grabbed "aa19", but this is: "aa 19"
	 * Also '19' does not have a whitespace prefix so it is not grabbed
	 * @param string $str
	 * @return integer
	 */
	public static function getLastInteger($subject) {
		$pattern = "/\s([0-9]+)/";
		$matches = array();
		if( preg_match_all($pattern,$subject,$matches) == 0 ) {
			return null;
		}
		else {
			return end($matches[1]); 
		}
	}
	
	public static function startsWithInteger($var) {
		if(is_int($var)) {
			return true;
		}
		
		if(intval($var) != 0) {
			return true;
		}

		if( $var===0 ) {
			return true;
		}
		
		return false;
	}
	
	public static function isInteger($var) {
		if(is_int($var)) {
			return true;
		}
		
		if( $var===0 ) {
			return true;
		}
		
		if( is_numeric($var)) {
			return ((intval($var)-$var) === 0);
		}
		
		return false;
	}
	
	public static function isUnsignedInteger($val) {
		if(is_array($val) || is_object($val)) return false;
        $subject=str_replace(" ","",trim($val));
        $pattern = '/^([0-9]+)$/';
        $noMatches = preg_match($pattern, $subject);
        return ($noMatches===1);
    }
    
    public static function isNotEmpty($var) {
    	return !self::isEmpty($var);
    }
	
	/**
	 * A more suited isEmtpy function than php empty(): http://kr2.php.net/manual/en/function.empty.php
	 * The following things are considered to be empty:
	 * whitespace
	 * NULL
	 * FALSE
	 * array()
	 * 
	 * @param mixed $var
	 */
	public static function isEmpty($var) {
		if($var===null) return true;
		if($var===false) return true;
		if(is_array($var)) return empty($var);
		$var = trim($var);
		return ($var=='');
	}
	
	/**
	 * eliminate whitespace
	 * @param string $str
	 */
	public static function noWhitespace($str) {
		$str = self::noRedundantSpaces($str);
		return str_replace(' ', '', $str);
	}
	
	/**
	 * remove all redundant spaces
	 * @param string $str
	 */
	public static function noRedundantSpaces($str) {
		$strArray= array();
		$tok = strtok($str, self::spaces);
		while($tok !== false) {
			$strArray[] = $tok;
			$tok = strtok(self::spaces);
		}
		return implode(' ', $strArray);
	}
	
	/**
	 * Remove all tones from greek characters
	 * @param string $str
	 */
	public static function noTones($str) {
		$csvPath = Yii::getPathOfAlias('application.data') .'/notone.csv';
		$csv2Darray= self::csv2array($csvPath, ';');
		
		$trs = array();
		foreach($csv2Darray as $csv) {
			$trs += array($csv[0] => $csv[1]);
		}
		
		return strtr($str, $trs);
	}
	
	/**
	 * Convert a csv file in a Two-Dimension array
	 * @param string $file
	 * @param string $delimiter
	 */
	public static function csv2array($file, $delimiter) {
        if(($handle = fopen($file, "r")) === FALSE) {
        	return false;	
        }
        $token = "$delimiter\n\r";
		$i = 0;
		while( ($string = fgets($handle)) !== false ) {
			$tok = strtok($string, $token);
			$j=0;
			while($tok !== false) {
				$data2DArray[$i][$j] = $tok;
				$tok = strtok($token);
				$j++;
			}
			$i++;
		}
		fclose($handle);
        return $data2DArray;
    }
	
	/**
	 * Let's assume ONE SINGLE KEY is repeated in deeper levels of the array
	 * We want to bring all variables on the surface
	 * If this key DOES NOT point to an array, then nothing to go deeper to
	 * @param array $vars
	 * @param array $array
	 * @param string $keyString
	 */
	public static function multi_array_single(& $vars, $array, $keyString='params') {
		if( !is_array($array) ) {
			return;
		}
		
		foreach($array as $key => $arr) {
			if($key == $keyString) {
				continue;	
			}
			$vars[$key] = $arr;
		}
		
		if(isset($array[$keyString])) {							//if the same key is found, it is overwritten
			self::multi_array_single($vars, $array[$keyString]);	//in other words, deeper levels get higher priority
		}
	}
	
	public static function array_url($var, $reverse=false)
	{
		if($reverse) return unserialize(urldecode($var));
		if(is_array($var))	return urlencode(serialize($var));
		return false;
	}
	
	public static function out($var)
	{
		print "<pre>";
		print_r($var);
		print "</pre>";
	}
	
	public static function dump($var,$return=false) {
		print "<pre>";
		var_dump($var);
		print "</pre>";
	}
	
	/**
	 * Load and unload external library
	 */
	public static function loadPHPExcel() { //Turn off autoload library to let PHPExcel use its own
		spl_autoload_unregister(array('YiiBase','autoload'));
		return Yii::import('ext.phpexcel.PligorPHPExcel',true);
	}
	public static function unLoadPHPExcel() { //Once we have finished using the PHPExcel library, give the power back to Yii
		spl_autoload_register(array('YiiBase','autoload'));
	}
	
	/**
	 * Enter description here ...
	 * @param string $str
	 */
	public static function commaSeparated2array($str) {
		return preg_split('/[\s,]+/',$str,-1,PREG_SPLIT_NO_EMPTY);
	}
	
	/**
	 * Returns the data model based on the primary key
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public static function loadModel($class,$id)
	{
		$model=$class::model()->findByPk((int)$id);
		if($model===null) {
			$message = Yii::t('', "Cannot find model of the class") ." $class";
			throw new CHttpException(404,$message);
		}
		return $model;
	}
	
	//MAYBE CREATE A CLASS FOR DATE VARIABLES LATER////////////////////////////////////////////////////////////////
	
	/**
	 * if stamp is NOT numeric then it returns stamp
	 * @param integer $stamp
	 * @return string date format (16/1/1984)
	 */
	public static function convStamp2date($stamp) {
		if( !is_numeric($stamp) ) return $stamp;
		return date('j'. self::dateDelimiter .'n'. self::dateDelimiter .'Y', $stamp);
	}
	
	/**
	 * convert date format (16/1/1984) to GMT timestamp
	 */
	public static function convDate2stamp($date) {
		if( self::isEmpty($date) ) {
			return null;
		}
		
		$date = explode(self::dateDelimiter, $date);
		if( count($date)!=3 ) {
			$message = Yii::t('', 'Date has wrong format');
			throw new Exception($message);	
		}
		
		$day = $date[0];
		$month = $date[1];
		$year = $date[2];
		
		if( !checkdate($month, $day, $year) ) {
			$message = Yii::t('', 'Date is wrong');
			throw new Exception($message);	
		}
		
		//hour, minute, sec, month, day, year
		return gmmktime(0,0,0,$month,$day,$year);
	}
	
	/**
	 * Enter description here ...
	 * @param string $str
	 */
	public static function checkDateFormat($str) {
		$pattern = "@([0-9]{1,2})[^0-9]([0-9]{1,2})[^0-9]([0-9]{4})@";
		$matches = array();
		$subject = $str;
		if( preg_match($pattern,$subject,$matches) == 0 ) {
			return false;
		}
		return true;
	}
	
	/**
	 * Enter description here ...
	 * @param string $str
	 * @return string|bool
	 */
	public static function extractDate($str) {
		$str = self::noWhitespace($str);
		
		$pattern = '@(^|[^0-9])([0-9]{1,2})([^0-9])@';
		$matches = array();
		$subject = $str;
		if( preg_match($pattern,$subject,$matches) == 0 ) {
			return false;	
		}
		$day = $matches[2];
		$seper = $matches[3];
		
		if( ctype_alpha($seper) ) {
			return false;
		}
		
		$strArray = explode($seper, $str);
		if( count($strArray) !=3 ) {
			return false;
		}
		
		$pattern = '@(^|[^0-9])([0-9]{1,2})([^0-9]|$)@';
		$matches = array();
		$subject = $strArray[1];
		if( preg_match($pattern,$subject,$matches) == 0 ) {
			return false;	
		}
		
		$month = $matches[2];
		
		$pattern = '@(^|[^0-9])([0-9]{4})([^0-9]|$)@';
		$matches = array();
		$subject = $strArray[2];
		if( preg_match($pattern,$subject,$matches) == 0 ) {
			return false;
		}
		
		$year = $matches[2];
		
		return $day.self::dateDelimiter.$month.self::dateDelimiter.$year;
	}
	
	/**
	 * @deprecated
	 * Greeklish <-> Greek conversion is not so easy, just by converting single characters
	 * @param string $str
	 * @param bool $g2l
	 */
	public static function greek2lish($str, $g2l=true) {
		$filepath = Yii::getPathOfAlias('application.data') .'/greek2lish.csv';
		$fp = fopen($filepath,'r');
		$lator = array();
		while( $row = trim(fgets($fp)) ) {
			$l = explode(";",$row);
			
			if($g2l) {
				$index = $l[0];
				$lett = $l[1];
			}
			else {
				$index = $l[1];
				$lett = $l[0];
			}
			
			if( isset($lator[$index]) ) {
				continue;	//avoid tones	
			}

			$lator[$index] = $lett;
			//if($g2l) $lator[$l[0]] = $l[1];
			//else $lator[$l[1]] = $l[0];
		}
		fclose($fp);
	
		$greeklish = "";
		$letters = self::mb_str_split($str);
		foreach($letters as $letter) {
			if( isset($lator[$letter]) )	$lated = $lator[$letter];
			else	$lated=$letter;
			$greeklish .= $lated;
		}
		
		return $greeklish;
	}
}