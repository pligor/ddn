<?php
//TUTORIAL:http://www.yiiframework.com/wiki/60/ (Add information to Yii::app()->user by extending CWebUser)
class WebUser extends CWebUser
{
	protected $_model;
	
	/**
	 * @throws Exception
	 */
	public function getModel() { //http://www.yiiframework.com/doc/api/1.1/CWebUser#id-detail
		if( !isset($this->id) ) {
			$message = 'Error inside WebUser->getModel(). please always provide user id';
			throw new Exception($message);	
		}
		
		if($this->_model === null) {
			$this->_model = CHtmlEx::loadModel('User', $this->id);
		}
		
		return $this->_model;
	}
	
	protected function beforeLogin($id, $states, $fromCookie) {	
		if($fromCookie) {
			if($states['cookie_key'] != CHtmlEx::loadModel('User', $id)->cookie_key) {
				return false;
			}
		}
		return true;
	}
}