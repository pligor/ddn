<?php

/**
 * Behavior for classes handling multiple models
 * @author pligor
 * @property array $classes
 * @property array $models
 */
class MultiModelBehavior extends CBehavior {
	public $classes = array();
	public $models = array();
	
	public function __construct( $classes=array() ) {
		$this->classes = $classes;
		foreach($this->classes as $class) {
			$this->models[$class] = new $class();
		}
	}
	
	/**
	 * check if all models are set and are objects of the correct class each
	 * @return bool
	 */
	public function checkModels() {
		foreach($this->classes as $class) {
			if( !isset($this->models[$class]) ) return false;
			if( !is_object($this->models[$class]) ) return false;
			if( get_class($this->models[$class]) != $class ) return false;
		}
		return true;
	}
	
	public function setModelsScenario($value) {
		if( $this->checkModels() ) {
			foreach($this->classes as $class) {
				$this->models[$class]->scenario = $value;
			}
		}
	}
	
	public function checkModelAttribute($name) {
		if( $this->checkModels() ) {
			foreach($this->classes as $class) {
				if( in_array($name, $this->models[$class]->safeAttributeNames) ) {
					return true;
				}	
			}	
		}
		return false;
	}
	
	public function getModelAttribute($name) {
		if( $this->checkModels() ) {
			foreach($this->classes as $class) {
				if( in_array($name, $this->models[$class]->safeAttributeNames) ) {
					return $this->models[$class]->$name;
				}	
			}	
		}
		return false;
	}
	
	public function setModelAttribute($name, $value) {
		if( $this->checkModels() ) {
			foreach($this->classes as $class) {
				if( in_array($name, $this->models[$class]->safeAttributeNames) ) {
					$this->models[$class]->$name = $value;
					return true;
				}	
			}	
		}
		return false;
	}
	
	/**
	 * Get all attributes with their values
	 */
	public function getAttrs() {
		if( !$this->checkModels() ) return false;
		$attrs = array();
		foreach($this->attrNames as $attrName) {
			$attrs[$attrName] = $this->getModelAttribute($attrName); //$this->$attrName;
		}
		return $attrs;
	}

	/**
	 * Helper function to get all attribute names
	 */
	public function getAttrNames() {
		if( !$this->checkModels() ) return false;
		$attrNames = array();
		foreach($this->classes as $class) {
			foreach($this->models[$class]->safeAttributeNames as $safeAttributeName) {
				$attrNames[] = $safeAttributeName;
			}
		}
		return $attrNames;
	}
	
	public function getAttrLabels() {
		$attrLabels = array();
		foreach($this->models as $model) {
			$attrLabels += $model->attributeLabels();
		}
		return $attrLabels;
	}
}