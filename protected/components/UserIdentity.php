<?php
//An identity class may also declare additional identity information that needs to be persistent during the user session
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {
	
	public function __construct($username, $password) {
		$password = hash(User::hashAlgo, $password);
		parent::__construct($username, $password);
	}

	private $_id = null;
	
	/**
	 * override to support that user id from inside database is stored
	 * @see CUserIdentity::getId()
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Authenticates a user
	 * SUPPOSINGLY the $this->username and $this->password are ALREADY defined by the call of this method
	 * @return boolean whether authentication succeeds
	 */
	public function authenticate() {
		$name = $this->username;
		$model = User::model()->findByAttributes( compact('name') );
		
		if( $model === null ) {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}
		elseif($model->pass !== $this->password) {
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		}
		else {
			$this->errorCode=self::ERROR_NONE;
			$this->_id = $model->id;
			$this->setState('email', $model->email);
			$this->genRandomCookieKey($model);
		}
		
		return !$this->errorCode;
	}
	
	//http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#cookie-based-login
	protected function genRandomCookieKey(& $model) {
		$string = str_shuffle(date("F j, Y, g:i a")).mt_rand();
		$cookie_key = hash(User::hashAlgo, $string);	//randomly generated

		$attribute = 'cookie_key';
		$model->$attribute = $cookie_key; //save to db
		if( !$model->update( array($attribute) ) ) {
			$message = Yii::t('', 'failed to store cookie key at cookie_key column of user table');
			throw new CDbException($message);
		}
		
		$this->setState($attribute, $cookie_key); //save to state
	}
}