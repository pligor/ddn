<?php

/**
 * 
 * Enter description here ...
 * @author pligor
 *
 */
class InheritRulesBehavior extends CBehavior {
	public $rules = array();
	
	/**
	 * get rules for currently safe attributes for specific model
	 */
	public function getSafeRules($model) {
		$safeRules = array();
		$rules = $model->rules();
		foreach($rules as $rule) {
			if(isset($rule['on']) && $model->scenario != $rule['on']) {	//skip invalid scenario rules
				continue;
			}
			
			if($rule[1]=='safe' || $rule[1]=='unsafe') {	//skip safe and unsafe rules
				continue;
			}
			
			$attributes = CHtmlEx::commaSeparated2array( $rule[0] );
			$safeRuleAttrs = array_intersect($model->safeAttributeNames, $attributes);
			$rule[0] = implode(', ', $safeRuleAttrs);
			
			$safeRules[] = $rule;
		}
		return $safeRules;
	}
}