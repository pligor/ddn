<?php

//Delete doesn't use scopes!
//http://www.yiiframework.com/doc/guide/1.1/en/database.ar#c3042
//MAYBE WE COULD TRY TO DELETE USING SCOPES WITH THE WORKAROUND PROVIDED ABOVE


//* kseroume pws px. to pv_enabled_pv_plant_id einai yposynolo tou pv_plant_id ara xrisimopoioume pleonektika to IN condition

/**
 * class Wipent
 * Clear database from its crucial data. Static tables (dimos, nomos, etc.) remain unharmed
 * @author pligor
 */
class Wipent extends CComponent {
	
	protected $classes = array(	//remember: the order of the classes matters
		'Killed',
		'Tracker',
		'PvEnabled',
		'Activation',
		'SimvasiPolisis',
		'SimvasiSindesis',
		'DiatiposiOron',
		'ArxikiAitisi',
		'ProcedureStep',
		'Comment',
		'PvXt',
		'PvMt',
		'PvSteges',
		'PvPlant',
		'Address',
		'Prosopo',
	);
	
	public function wipeAll() {
		$affectedRows = array();
		
		foreach($this->classes as $class) {
			$affectedRows[$class] = $class::model()->deleteAll();
			
			$tableName = ActiveRecord::getTableName( $class::model() );
			ActiveRecord::resetAutoIncrement($tableName);
		}
		
		return $affectedRows;
	}
	
	public function wipePerioxi($perioxi_dei_id) {
		$model = CHtmlEx::loadModel('PerioxiDei', $perioxi_dei_id);
		$affectedRows = array();
		foreach($model->listDataIslands as $island_id => $island_name) {
			$affectedRows[$island_name] = $this->wipeIsland($island_id); 
		}
		return $affectedRows;
	}
	
	public function wipeIsland($island_id) {
		$affectedRows = array();
		
		$classes = $this->classes;
		
		$transaction = Yii::app()->db->beginTransaction();	//////////////////////////////////////////////////////////
		try {
			$criteria = new CDbCriteria();
			$criteria->condition = 'island_id = :island_id';
			$criteria->params = array(':island_id'=>$island_id);
			$models = PvPlant::model()->findAll($criteria);
			$pv_plant_ids = CHtml::listData($models, 'id', 'id');
			$address_ids_specific = CHtml::listData($models, 'address_id', 'address_id');	//same index to avoid array_unique
			$prosopo_ids_specific = CHtml::listData($models, 'prosopo_id', 'prosopo_id');	//same index to avoid array_unique
			
			$criteria = new CDbCriteria();
			$criteria->condition = 'island_id <> :island_id';
			$criteria->params = array(':island_id'=>$island_id);
			$models = PvPlant::model()->findAll($criteria);
			$address_ids_rest = CHtml::listData($models, 'address_id', 'address_id');	//same index to avoid array_unique
			$prosopo_ids_rest = CHtml::listData($models, 'prosopo_id', 'prosopo_id');	//same index to avoid array_unique
			
			$address_ids = array_diff($address_ids_specific, $address_ids_rest);
			$prosopo_ids = array_diff($prosopo_ids_specific, $prosopo_ids_rest);
			
			$criteria = new CDbCriteria();
			$criteria->addInCondition('pv_plant_id', $pv_plant_ids);
			$class = array_shift($classes);	//Killed
			$affectedRows[$class] = $class::model()->deleteAll($criteria);
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$criteria = new CDbCriteria();
			$criteria->addInCondition('pv_enabled_pv_plant_id', $pv_plant_ids);
			$class = array_shift($classes);	//Tracker
			$affectedRows[$class] = $class::model()->deleteAll($criteria);	//pleonektika*
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$criteria = new CDbCriteria();
			$criteria->addInCondition('pv_plant_id', $pv_plant_ids);
			
			$class = array_shift($classes);	//PvEnabled
			$affectedRows[$class] = $class::model()->deleteAll($criteria);
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$models = ProcedureStep::model()->findAll($criteria);
			$procedure_step_ids = CHtml::listData($models, 'id', 'id');
			
			$criteria = new CDbCriteria();
			$criteria->addInCondition('procedure_step_id', $procedure_step_ids);
			
			$class = array_shift($classes);	//Activation
			$affectedRows[$class] = $class::model()->deleteAll($criteria);	//pleonektika*
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$class = array_shift($classes);	//SimvasiPolisis
			$affectedRows[$class] = $class::model()->deleteAll($criteria);	//pleonektika*
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$class = array_shift($classes);	//SimvasiSindesis
			$affectedRows[$class] = $class::model()->deleteAll($criteria);	//pleonektika*
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$class = array_shift($classes);	//DiatiposiOron
			$affectedRows[$class] = $class::model()->deleteAll($criteria);	//pleonektika*
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$class = array_shift($classes);	//ArxikiAitisi
			$affectedRows[$class] = $class::model()->deleteAll($criteria);	//pleonektika*
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $procedure_step_ids);
			
			$class = array_shift($classes);	//ProcedureStep
			$affectedRows[$class] = $class::model()->deleteAll($criteria);	//pleonektika*
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$criteria = new CDbCriteria();
			$criteria->addInCondition('pv_plant_id', $pv_plant_ids);
			
			$class = array_shift($classes);	//Comment
			$affectedRows[$class] = $class::model()->deleteAll($criteria);
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$class = array_shift($classes);	//PvXt
			$affectedRows[$class] = $class::model()->deleteAll($criteria);
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$class = array_shift($classes);	//PvMt
			$affectedRows[$class] = $class::model()->deleteAll($criteria);
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$class = array_shift($classes);	//PvSteges
			$affectedRows[$class] = $class::model()->deleteAll($criteria);
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $pv_plant_ids);
			
			$class = array_shift($classes);	//PvPlant
			$affectedRows[$class] = $class::model()->deleteAll($criteria);
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $address_ids);
			
			$class = array_shift($classes);	//Address
			$affectedRows[$class] = $class::model()->deleteAll($criteria);
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $prosopo_ids);
			
			$class = array_shift($classes);	//Prosopo
			$affectedRows[$class] = $class::model()->deleteAll($criteria);
			
			$transaction->commit();
		}
		catch(CDbException $e) {
		    $transaction->rollBack();
		    //print $e->getMessage();
		}
		
		return $affectedRows;
	}
}