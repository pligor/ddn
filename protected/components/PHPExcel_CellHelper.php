<?php
class PHPExcel_CellHelper extends CComponent {
	/**
	 * @param integer $index
	 * @return string
	 */
	public static function stringFromColumnIndex($index=0) {
		CHtmlEx::loadPHPExcel();
		$str = PHPExcel_Cell::stringFromColumnIndex($index);
		CHtmlEx::unLoadPHPExcel();
		return $str;
	}
	
	/**
	 * Same as original class but ZERO-BASED (original is one-based)
	 * @param string $str
	 * @return integer
	 */
	public static function columnIndexFromString($str='A') {
		CHtmlEx::loadPHPExcel();
		$index = PHPExcel_Cell::columnIndexFromString($str);
		CHtmlEx::unLoadPHPExcel();
		return $index-1;
	}
	
	/**
	 * 
	 * @param integer[] $indices
	 */
	public static function stringsFromColumnIndices($indices) {
		$strs = array();
		foreach($indices as $key => $index) {
			$strs[$key] = self::stringFromColumnIndex($index);
		}
		return $strs;
	}
	
	/**
	 * 
	 * @param string[] $strs
	 */
	public static function columnIndicesFromStrings($strs) {
		$indices = array();
		foreach($strs as $key => $str) {
			$indices[$key] = self::columnIndexFromString($str);
		}
		return $indices;
	}
}