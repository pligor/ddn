<?php
class Breathalyzer extends CComponent {
//KATI EXOUME KANEI LATHOS STHN METAVLITI MIN ALLA TO APOTELESMA TOU WORD EINAI SWSTO
	public $min;
	public $finalword;
	
	/**
	 * Enter description here ...
	 * @param array $vector
	 * @param string $word
	 */
	public function breathem($vector, $word) {
		try {
			if(empty($vector)) {
				$this->min = mb_strlen($word, Yii::app()->charset);
				$this->finalword = $word;
				
				$message = Yii::t('', "vector is empty");
				throw new CException($message,1);
			}
			
			$list=array();
			foreach($vector as $line) {
			    $list[$line] = mb_strlen($line, Yii::app()->charset);
			}
			asort($list,SORT_NUMERIC);
			
			if(CHtmlEx::isEmpty($word)) {
				$shortWords = array_keys($list, min($list));
				//don't know so.. random!
				$guessedWord = $shortWords[ array_rand($shortWords) ];
				$this->min = mb_strlen($guessedWord, Yii::app()->charset);
				$this->finalword = $guessedWord;
				
				$message = Yii::t('', "word is empty");
				throw new CException($message,1);
			}
			
			$allwords = array_keys($list);
		    if( !in_array($word,$allwords) ) {
		        $result = $this->breath($word, $list);
		    }
		    else $result = array('min' => 0, 'finalword' => $word);
		    $this->min = $result['min'];
		    $this->finalword = $result['finalword'];
		}
		catch(CException $e) {
			if($e->getCode() != 1) {
				$message = Yii::t('', "inside") ." ". get_class() .": ". $e->getMessage();
				throw new Exception($e->getMessage(), $e->getCode());
			}
		}
	    
	    return array(
			'min' => $this->min,
			'finalword' => $this->finalword,
		);
	}
	
	private function breath($str, & $list) {   //VLEPE THEORY 2 (LENGTH DEPENDENCY)
	    if(empty($str)) return; //if empty do nothing
	    
	    $minLen = reset($list);
		$maxLen = end($list);
	    
	    $len = mb_strlen($str, Yii::app()->charset);
	    extract( $this->minDist($str, $len, $list) );    //check words of same length
	    $finalword = $correct;
	    
	    if($min==0) {
	    	$message = Yii::t('', "!!ERROR:found min==0 while we should check for exact match words by typical searching"); 
	    	throw new CException($message);
	    }
	    
	    $top = min( $len+($min-1), $maxLen );   //saturate at max length
	    $countUp=$len;
	    
	    $countDown=$len;
	    $bottom = max( $len-($min-1), $minLen );   //saturate at min length
	    
	    $leave = false;
	    while(!$leave)
	    {
	        $leave=true;
	        
	        $countDown--;
	        if($countDown>=$bottom)
	        {
	        	$array = $this->minDist($str, $countDown, $list);
	            $minDown = $array["min"];
	            $correct = $array["correct"];
	            //print "for len $countDown we have min: $minDown\n";
	            
	            if($minDown==0) {
	            	$message = Yii::t('',"!!ERROR:found minDown==0 while we should check for exact match words by typical searching"); 
	    			throw new CException($message);
	            }
	            
	            if($minDown<$min)
	            {
	                $min=$minDown;          //new min
	                $finalword = $correct;
	                
	                //bottom and top change dynamically according to new findings
	                $bottom = max( $len-($min-1), $minLen );
	                $top = min( $len+($min-1), $maxLen );
	            }
	            
	            $leave=false;
	        }
	        
	        $countUp++;
	        if($countUp<=$top)
	        {
	            $array = $this->minDist($str, $countUp, $list);
	            $minUp = $array["min"];
	            $correct = $array["correct"];
	            //print "for len $countUp we have min: $minUp\n";
	            
	            if($minUp==0) {
	            	$message = Yii::t('', "!!ERROR:found minUp==0 while we should check for exact match words by typical searching"); 
	    			throw new CException($message);
	            }
	            
	            if($minUp<$min)
	            {
	                $min=$minUp;          //new min
	                $finalword = $correct;
	                
	                //bottom and top change dynamically according to new findings
	                $bottom = max( $len-($min-1), $minLen );
	                $top = min( $len+($min-1), $maxLen );
	            }
	            
	            $leave=false;
	        }
	    }
	    
	    return array( "min" => $min, "finalword" => $finalword);
	}
	
	private function minDist($str, $len, & $list) {    
	    $min = PHP_INT_MAX; //arxizoume apo kati arketa psila
	    $correct = $str;	//is the same word
	    
	    $listKeys = array_keys($list,$len); //if this is empty then we return PHP_INT_MAX which is fine, since we have to look in other implementations
	    
	    foreach($listKeys as $listKey)
	    {
	        //$dist = $this->scrabble($str,$listKey);   //by convention we use as first argument the unknown word and as a second argument the correct word
	        $dist = levenshtein($str,$listKey);
	        if($dist<$min)
	        {
	            $min=$dist;
	            $correct = $listKey;
	        }
	    }
	    
	    return array("min" => $min,"correct"=>$correct);
	}
	
	private function scrabble($str,$scrabbleWord)   //we store memory that ONLY HELPS if we come across the exactly same bad word or the same bad word with a suffix
	{
	    global $memory;
	    
	    $strPre=$str;
	    $strSuf=$str;
	    
	    $len=mb_strlen($str, Yii::app()->charset);  
	    while($strPre)
	    {
	        if( isset($memory[$strPre][$scrabbleWord]) ) { //search inside memory for specific match of words
	        	//http://gr2.php.net/manual/en/function.mb-substr.php#77515
	            $strSuf=mb_substr($str, $len, mb_strlen($str, Yii::app()->charset), Yii::app()->charset);
	            break;
	        }
	        $strPre=mb_substr($strPre,0,--$len, Yii::app()->charset);
	    }
	    //to $strPre periexei twra to calculated kommati(left part) enw to $strSuf to not calculated(right part)
	    //print "prefix: $strPre\n" . "suffix: $strSuf\n";
	    
	    if(empty($strSuf)) { //the calculation has already taken place. the unknown word's' string is inside strPre
	        return $memory[$strPre][$scrabbleWord];
	    }
	    
	    if(empty($strPre))  //NO memory for this word. Calculating for the first time
	    {
	        return $this->levenshteinMem($strSuf,$scrabbleWord);
	    }
	    
	    $strArray = array
	    (
	        "prefix" => $strPre,
	        "suffix" => $strSuf,
	    );
	    return $this->levenshteinMem($strArray,$scrabbleWord);
	}
	
	//$prefix to prefix ths agnwsths leksis
	//$str1 to suffix ths agnwsths lesksis
	//$str2 h leksi scrabble pou gnorizoume
	private function levenshteinMem($str1, $str2)
	{   //vlepe sto bottom tou kodika: THEORY 1 (MEMOIZATION)
	    global $memory;
	    
	    //$chars1="";
	    $chars2="";
	    
	    if(is_array($str1))
	    {
	        $prefix=$str1["prefix"];
	        $str1=$str1["suffix"];
	        $useMem=true;
	    }
	    else
	    {
	        $prefix="";
	        $useMem=false;
	    }
	    
	    $prefixLen = mb_strlen($prefix, Yii::app()->charset);
	    
	    $len1 = mb_strlen($str1, Yii::app()->charset);    //emeis tha asxolithoume me to suffix
	    $len2 = mb_strlen($str2, Yii::app()->charset);
	    
	    $col=range($prefixLen,$prefixLen+$len1);    //ara simplironoume mono to kommati pou xreiazomaste oson afora to suffix
	    
	    for($j=1;$j<=$len2;$j++)
	    {
	        $chars2 .= $str2[$j-1]; //to -1 einai logo zero-base index sta strings
	        
	        if($useMem)
	        {
	            if(!isset($memory[$prefix][$chars2])) {
	            	$message = "!!!DIE FATAL ERROR!!!"; 
	    			throw new CException($message);
	            }
	            
	            $newCol=array( $memory[$prefix][$chars2] ); //enw edw apo to memory exoume thn PROypologismeni (apo panw) grammi
	        }
	        else
	        {
	            $newCol=array($j);
	        }
	        
	        for($i=1;$i<=$len1;$i++)
	        {           
	            if($str1[$i-1] == $str2[$j-1])  //prosekse to -1 pou ofeiletai sto oti ta strings einai zero-base indexed
	            {
	                $newCol[$i] =  $col[$i-1];  //iso me tis proigoumenis stilis to apo panw stoixeio, diagwnia panw aristera diladi
	            }
	            else
	            {
	                $newCol[$i] = min
	                (
	                    $newCol[$i-1],  //deletion
	                    $col[$i],   //insertion
	                    $col[$i-1]  //substitution (deletion AND insertion)
	                )+1;
	            }
	            $col[$i-1]=$newCol[$i-1];
	            
	            if($i==$len1)   //prosthese pragmata sti mnimi mono an prokeitai na mhn th xrisimopoihseis
	            {
	                $memory[$prefix.$str1][$chars2]=$newCol[$i];
	            }
	        }
	        $col[$i-1]=$newCol[$i-1];   //also for the last element
	    }
	    
	    return end($col);
	}
}