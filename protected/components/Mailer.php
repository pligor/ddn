<?php
Yii::import('external.PHPMailer.PHPMailer');
/**
 * 
 * @author pligor
 */
class Mailer extends PHPMailer {
	public $Recipient;
	//Subject;
	public $Message;
	
	/**
	 * three parameters must be defined 'From', 'Host', 'Password'
	 * @param array $params
	 * @param type $exceptions 
	 */
	public function __construct($params = null,$exceptions = true) {
		parent::__construct($exceptions);	//we revert default functionality where exceptions are disabled. we enabled them!
		
		$this->Recipient = Yii::app()->params['adminEmail'];
		
		if(is_array($params)) {
			$param = 'From';
			$this->$param = $params[$param];
			
			$param = 'Host';
			$this->$param = $params[$param];
			
			$param = 'Password';
			$this->$param = $params[$param];
		}
		else {
			$this->From = 'ryco@otenet.gr';
			$this->Host = 'smtp.otenet.gr'; // SMTP server
			$this->Password = 'qm1FTb6l';   // SMTP server password
			
			//$message = 'Parameters for '. __CLASS__ .'are not set';
			//throw new CException($message);
		}
		
		$this->FromName = ucfirst( reset( explode('@', $this->From) ) );
	}
	
	protected function AddAddresses() {
		if( is_array($this->Recipient) ) {
			foreach($this->Recipient as $address) {
				$this->AddAddress($address);
			}
		}
		else {
			$this->AddAddress($this->Recipient);
		}
	}
	
	public function maile() {
		try {
			$this->IsSMTP();                           // tell the class to use SMTP
			$this->SMTPAuth   = true;                  // enable SMTP authentication
			$this->Port       = 25;                    // set the SMTP server port
			$this->Username   = $this->From; //happens to be the same // SMTP server username
			
			//$this->IsSendmail();  //tell the class to use Sendmail
		
			$this->AddReplyTo($this->From, $this->FromName);
			
			$this->AddAddresses();
		
			$this->AltBody = Yii::t('', 'Please use an email client which allows you to receive html formatted email');
			//$this->WordWrap   = 80; // set word wrap
			
			$this->Message = preg_replace('/\\\\/','', $this->Message); //Strip backslashes
			$this->MsgHTML($this->Message);
			
			//$this->AddAttachment($folder."libredpn_html_m6633087.jpg");      // attachment
			//$this->AddAttachment($folder."stylesheet.css"); // attachment
		
			$this->IsHTML(true); //send as HTML
		
			$this->CharSet = 'utf-8';
			
			$this->Send();
		}
		catch (phpmailerException $e) {
			$message = $e->errorMessage();
			throw new Exception($message);	//rethrow
		}
	}
}