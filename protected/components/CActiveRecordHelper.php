<?php
/**
 * Some helper functions for ActiveRecord objects
 * @author pligor
 */
class CActiveRecordHelper {
	public static function getOnlyDbAttributes($model) {
		$allAttrs = array_keys( $model->getAttributes(true) );
		$noDbAttrs = array_keys( $model->getAttributes(null) );
		return array_diff($allAttrs, $noDbAttrs);
	}
}
