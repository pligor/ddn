<?php
$yii = dirname(__FILE__) .'/../../php/framework/yii.php';
$config = dirname(__FILE__) .'/protected/config/main.php';

require dirname(__FILE__) .'/protected/config/ini.php';

defined('YII_DEBUG') or define('YII_DEBUG',true); // remove the following lines when in production mode
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3); // specify how many levels of call stack should be shown in each log message

require_once($yii);
Yii::createWebApplication($config)->run();	//here we pass the configuration file to create an new Yii::application instance
